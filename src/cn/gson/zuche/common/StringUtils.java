package cn.gson.zuche.common;

import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * 字符串处理工具
 * @author gson
 *
 */
public final class StringUtils {

	private StringUtils() {}
	
	/**
	 * 判断字符串是否为空
	 * @param str
	 * @return
	 */
	public static boolean isEmpty(String str){
		return str == null || str.isEmpty();
	}
	
	 public static boolean isNumeric(String str){ 
	        Pattern pattern = Pattern.compile("[0-9]*"); 
	        return pattern.matcher(str).matches();    
	     }
	 /**
	  * 生成订单号
	  * @return
	  */
	 public static  String getOrdernum() {  
	    	Integer dd=(int)((Math.random()*9+1)*1000);
	        String str = new SimpleDateFormat("yyMMddHHmmss").format(new Date());  
	        long orderNo = Long.parseLong((str)) * 10000;  
	        orderNo += dd;  
	        return "ZC"+orderNo;  
	    }  
	 
	 /**
	  * 过滤手机号
	  */
	 
	 public static String phone(String phone){
			return phone.replaceAll("(\\d{3})\\d{4}(\\d{4})", "$1****$2");
			 
		 }
	 
	 /**
	  * 过滤姓名
	  * @param name
	  * @return
	  */
	 public static String name(String name) {  
		    
		    if (name.length() <= 1) {  
		        System.out.println("*");  
		    } else {  
		        return name.replaceAll("([\\u4e00-\\u9fa5]{1})(.*)", "$1" + createAsterisk(name.length() - 1));  
		    }
			return name;  
		  
		}  
		
		//生成很多个*号  
		public static String createAsterisk(int length) {  
		    StringBuffer stringBuffer = new StringBuffer();  
		    for (int i = 0; i < length; i++) {  
		        stringBuffer.append("*");  
		    }  
		    return stringBuffer.toString();  
		}  
		
		public static String md5Encode(String inStr) throws Exception {  
	        MessageDigest md5 = null;  
	        try {  
	            md5 = MessageDigest.getInstance("MD5");  
	        } catch (Exception e) {  
	            System.out.println(e.toString());  
	            e.printStackTrace();  
	            return "";  
	        }  
	  
	        byte[] byteArray = inStr.getBytes("UTF-8");  
	        byte[] md5Bytes = md5.digest(byteArray);  
	        StringBuffer hexValue = new StringBuffer();  
	        for (int i = 0; i < md5Bytes.length; i++) {  
	            int val = ((int) md5Bytes[i]) & 0xff;  
	            if (val < 16) {  
	                hexValue.append("0");  
	            }  
	            hexValue.append(Integer.toHexString(val));  
	        }  
	        return hexValue.toString();  
	    }  
		/**
		 * 判断电话
		 * @param mobiles
		 * @return
		 */
		public static boolean isMobileNO(String mobiles){  
			  
			Pattern p = Pattern.compile("^((13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$");  
			Matcher m = p.matcher(mobiles);  
			return m.matches();
		}
	  
}
