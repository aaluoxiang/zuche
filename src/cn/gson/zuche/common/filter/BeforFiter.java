package cn.gson.zuche.common.filter;

import java.io.IOException;
import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet Filter implementation class BeforFiter
 */
@WebFilter(dispatcherTypes = { DispatcherType.REQUEST, DispatcherType.FORWARD }, urlPatterns = { "/*" })
public class BeforFiter implements Filter {

	/**
	 * Default constructor.
	 */
	public BeforFiter() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) resp;
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		String path = request.getServletPath();
		// System.out.println(path);
		if ("/owncenter".equals(path) || "/updatepassword".equals(path)|| "/mylevel".equals(path)
				|| "/mybalancemoney".equals(path) || "/myyhq".equals(path) || "/myjifen".equals(path)
				|| "/myorder".equals(path) || "/pay".equals(path) ||  "/dingdan".equals(path) || "/chenggong".equals(path) || "/ordercomment".equals(path) ||"/myatten".equals(path)) {
			if (session.getAttribute("userId") == null) {
				response.sendRedirect(request.getContextPath() + "/login");
			} else {
				chain.doFilter(request, response);
			}
		} else if ("/iframe".equals(path) || "/jbxxnr".equals(path) || "/waipingyuangon".equals(path)
				|| "/systemhdp".equals(path) || "/xinximoban".equals(path) || "/guanliyuanguanli".equals(path)
				|| "/dindan".equals(path) || "/huiyuanliebiao".equals(path) || "/shangpingunali".equals(path)
				|| "/index".equals(path) || "/tongjimokuai".equals(path)) {
			if (session.getAttribute("zhanghao") == null) {
				response.sendRedirect(request.getContextPath() + "/adminlogin");
			} else {
				chain.doFilter(request, response);
			}
		} else {
			chain.doFilter(request, response);
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
