package cn.gson.zuche.model.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import javafx.scene.control.SplitPane.Divider;

public class MySqlDb extends Db {
	private static MySqlDb instance;
	// 创建Context对象
	private DataSource ds;

	private MySqlDb() {
	}

	public synchronized static MySqlDb getInstance() {
		try {
			if (instance == null) {
				instance = new MySqlDb();
				Context context = new InitialContext();
				// 获得数据源对象
				instance.ds = (DataSource) context.lookup("java:comp/env/jdbc/ds");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return instance;
	}

	@Override
	public Connection geConn() throws SQLException {
		return ds.getConnection();
	}

}
