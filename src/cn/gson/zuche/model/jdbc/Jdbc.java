package cn.gson.zuche.model.jdbc;

import java.sql.SQLException;
import java.util.List;

public interface Jdbc {

	/**
	 * 查询
	 * @param <T>
	 */
	<T> List<T>  executeQuery(String sql, ParamHandler paramHandler,ResultHandler<T> resultHandler) throws SQLException;

	/**
	 * 更新
	 * 
	 * @return
	 */
	int executeUpdate(String sql, ParamHandler paramHandler) throws SQLException;
}
