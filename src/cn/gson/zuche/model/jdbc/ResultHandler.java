package cn.gson.zuche.model.jdbc;

import java.util.Map;

public interface ResultHandler<T> {

	T doHander(Map<String, Object> row);
}
