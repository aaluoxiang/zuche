package cn.gson.zuche.model.jdbc;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * 参数处理
 * @author gson
 *
 */
public interface ParamHandler {

	void doHander(PreparedStatement pStatement)  throws SQLException;
}
