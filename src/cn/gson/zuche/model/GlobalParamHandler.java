package cn.gson.zuche.model;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import cn.gson.zuche.model.jdbc.ParamHandler;

/**
 * 通用参数处理器
 * @author gson
 *
 */
public class GlobalParamHandler implements ParamHandler {

	private Object[] params = new Object[0];

	public GlobalParamHandler(Object... params) {
		this.params = params;
	}

	@Override
	public void doHander(PreparedStatement pStatement) throws SQLException {
		for (int i = 0; i < params.length; i++) {
			Object item = params[i];
			pStatement.setObject(i + 1, item);
		}
	}
}
