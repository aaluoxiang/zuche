package cn.gson.zuche.model.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import cn.gson.zuche.model.bean.Jbxxnr;
import cn.gson.zuche.model.jdbc.MySqlDb;
import cn.gson.zuche.model.jdbc.ParamHandler;
import cn.gson.zuche.model.jdbc.ResultHandler;

public class Jbxxnrdao implements ResultHandler<Jbxxnr> {
	private MySqlDb db = MySqlDb.getInstance();

	public List<Jbxxnr> findall() throws SQLException {
		String sql = "SELECT * from web_information";
		return db.executeQuery(sql, this);
	}

	public boolean sava(List list) throws SQLException {
		String sql = "UPDATE web_information set web_point='"+list.get(0)+"', web_des='"+list.get(1)+"',web_company_name='"+list.get(2)+"',web_bq='"+list.get(3)+"',web_intro='"+list.get(4)+"' ,web_tel='"+list.get(5)+"' ,web_bt='"+list.get(6)+"' WHERE web_id=1";
		return db.executeUpdate(sql) > 0;
	}

	private class saveprm implements ParamHandler {
		Jbxxnr sys;
		saveprm(Jbxxnr sys) {
			this.sys = sys;
		}
		public void doHander(PreparedStatement pStatement) throws SQLException {
			pStatement.setString(1, sys.getWebpoint());
			pStatement.setString(2, sys.getWebdes());
			pStatement.setString(3, sys.getWebcompanyname());
			pStatement.setString(4, sys.getWebbq());
			pStatement.setString(5, sys.getWebintro());
			pStatement.setString(6, sys.getWebtel());
			pStatement.setString(7, sys.getWebbt());
		}

	}

	@Override
	public Jbxxnr doHander(Map<String, Object> row) {
		Jbxxnr s = new Jbxxnr();
		s.setWebbq((String) row.get("web_bq"));
		s.setWebbt((String) row.get("web_bt"));
		s.setWebcompanyname((String) row.get("web_company_name"));
		s.setWebdes((String) row.get("web_des"));
		s.setWebintro((String) row.get("web_intro"));
		s.setWebpoint((String) row.get("web_point"));
		s.setWebtel((String) row.get("web_tel"));
		return s;
	}
}
