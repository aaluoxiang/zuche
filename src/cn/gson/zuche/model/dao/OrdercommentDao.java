package cn.gson.zuche.model.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import cn.gson.zuche.model.bean.Cartype;
import cn.gson.zuche.model.bean.Ordercomment;
import cn.gson.zuche.model.jdbc.MySqlDb;
import cn.gson.zuche.model.jdbc.ParamHandler;
import cn.gson.zuche.model.jdbc.ResultHandler;

public class OrdercommentDao implements ResultHandler<Ordercomment> {

	private MySqlDb db = MySqlDb.getInstance();

	public boolean insertcomment(Ordercomment odcom) throws SQLException {
		String sql = "INSERT into order_comment (order_comment_content,order_comment_date,order_comment_driver,order_comment_car,user_order_id,user_id,order_comment_type)VALUES (?,?,?,?,?,?,?)";
		return db.executeUpdate(sql, new Addcom(odcom)) > 0;
	}
	/**
	 * 根据车类型查找车评论
	 * @param cartpname
	 * @return
	 * @throws SQLException
	 */
	public List<Ordercomment> findbycartpname(String cartpname) throws SQLException{
		String sql = "select order_comment_content,order_comment_date,user_name as user_id,order_comment_type from order_comment,`user` where  order_comment.user_id = `user`.user_id and order_comment_car = '"+cartpname+"' ORDER BY `order_comment_date` DESC";
		return db.executeQuery(sql, this);
	}

	class Addcom implements ParamHandler {
		Ordercomment odcom;

		public Addcom(Ordercomment odcom) {
			this.odcom = odcom;
		}

		@Override
		public void doHander(PreparedStatement pStatement) throws SQLException {
			pStatement.setString(1, odcom.getCommentcon());
			pStatement.setObject(2, odcom.getCommenttime());
			pStatement.setString(3, odcom.getCommentdriver());
			pStatement.setString(4, odcom.getCommentcar());
			pStatement.setString(5, odcom.getUserodid());
			pStatement.setString(6, odcom.getUserid());
			pStatement.setString(7, odcom.getCommenttype());
		}

	}

	@Override
	public Ordercomment doHander(Map<String, Object> row) {
		Ordercomment odcom = new Ordercomment();
		odcom.setCommentid(row.get("order_comment_id") + "");
		odcom.setCommentcon(row.get("order_comment_content") + "");
		odcom.setCommenttime(row.get("order_comment_date"));
		odcom.setCommentdriver(row.get("order_comment_driver") + "");
		odcom.setUserodid(row.get("user_order_id") + "");
		odcom.setUserid(row.get("user_id") + "");
		odcom.setCommenttype(row.get("order_comment_type") + "");
		odcom.setCommentcar(row.get("order_comment_car") + "");
		return odcom;
	}

}
