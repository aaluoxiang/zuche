package cn.gson.zuche.model.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import cn.gson.zuche.model.bean.Lingshibiao;
import cn.gson.zuche.model.jdbc.MySqlDb;
import cn.gson.zuche.model.jdbc.ResultHandler;

public class Lingshibiaodao implements ResultHandler<Lingshibiao>{
private MySqlDb db=MySqlDb.getInstance();
	public List<Lingshibiao> select() throws SQLException{
		String sql="select * from linshibiao";
		return db.executeQuery(sql, this);		
	}
	public boolean delete() throws SQLException{
		String sql="delete from linshibiao";
		return db.executeUpdate(sql)>0;	
	}
	public boolean insert(String orderid) throws SQLException {
		String sql = "INSERT INTO linshibiao (id) VALUE ('"+orderid+"')";
		return db.executeUpdate(sql)>0;
	}
	@Override
	public Lingshibiao doHander(Map<String, Object> row) {
		Lingshibiao b=new Lingshibiao();
		b.setId((String) row.get("id"));		
		return b;
	}

}
