package cn.gson.zuche.model.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import cn.gson.zuche.model.bean.Chozhi;
import cn.gson.zuche.model.jdbc.MySqlDb;
import cn.gson.zuche.model.jdbc.ParamHandler;
import cn.gson.zuche.model.jdbc.ResultHandler;

public class Chozhidao implements ResultHandler<Chozhi>{
	private MySqlDb db=MySqlDb.getInstance();
	public List<Chozhi> select(String tel) throws SQLException{
		String sql="select * from chozhi where chozhi_tel='"+tel+"'";
		return db.executeQuery(sql, this);
	}
	/*
	 * 查询某个时间段中的充值记录
	 */
	public List<Chozhi> selectbydata(String begain,String down) throws SQLException{
		String sql="select * from chozhi where chozhi_data>='"+begain+"' and chozhi_data<'"+down+"'";
		return db.executeQuery(sql, this);
	}
	public boolean insert(final Chozhi cz) throws SQLException{
		String sql="insert into chozhi(chozhi_jiaoyifangxiang,chozhi_jiaoyilaiyuan,chozhi_money,chozhi_data,chozhi_caozuoren,chozhi_tel) values(?,?,?,?,?,?)";
		return db.executeUpdate(sql, new save(cz))>0;
	}
	private class save implements ParamHandler{
		Chozhi cz;
		public save(Chozhi cz){
			this.cz=cz;
		}
		@Override
		public void doHander(PreparedStatement pStatement) throws SQLException {
			pStatement.setString(1, cz.getChozhijiaoyifangxiang());
			pStatement.setString(2, cz.getChozhijiaoyilaiyuan());
			pStatement.setString(3, cz.getChozhimoney());
			pStatement.setObject(4, cz.getChozhidata());
			pStatement.setString(5, cz.getChozhicaozuoren());
			pStatement.setString(6, cz.getChozhitel());
		}
		
	}
	@Override
	public Chozhi doHander(Map<String, Object> row) {
		Chozhi cz=new Chozhi();
		cz.setChozhijiaoyifangxiang((String) row.get("chozhi_jiaoyifangxiang"));
		cz.setChozhijiaoyilaiyuan((String) row.get("chozhi_jiaoyilaiyuan"));
		cz.setChozhimoney((String) row.get("chozhi_money"));
		cz.setChozhidata((Object) row.get("chozhi_data"));
		cz.setChozhicaozuoren((String) row.get("chozhi_caozuoren"));
		cz.setChozhitel((String) row.get("chozhi_tel"));;
		return cz;
	}	

}
