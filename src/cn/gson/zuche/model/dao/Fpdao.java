package cn.gson.zuche.model.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

import cn.gson.zuche.model.bean.Fapiao;
import cn.gson.zuche.model.jdbc.MySqlDb;
import cn.gson.zuche.model.jdbc.ParamHandler;
import cn.gson.zuche.model.jdbc.ResultHandler;

public class Fpdao implements ResultHandler<Fapiao> {
	
	private MySqlDb db = MySqlDb.getInstance();
	
	
	/**
	 * 插入发票
	 */
	public boolean insertfapiao(final Fapiao fapiao) throws SQLException{
		String sql = "INSERT into fapiao(fptt,fp_user,fp_phone,fp_yb,fp_address,fp_user_id) VALUES(?,?,?,?,?,?)";
		return db.executeUpdate(sql,new save(fapiao))>0;
}

	@Override
	public Fapiao doHander(Map<String, Object> row) {
		Fapiao fapiao = new Fapiao();
		fapiao.setFpid(Integer.parseInt(row.get("fp_id") + ""));
		fapiao.setFptt(row.get("fptt") + "");
		fapiao.setFpuser(row.get("fp_user") + "");
		fapiao.setFpphone(row.get("fp_phone") + "");
		fapiao.setFpyb(row.get("fp_yb") + "");
		fapiao.setFpaddress(row.get("fp_addres") + "");
		fapiao.setFpuserid(Integer.parseInt(row.get("fp_user_id") + ""));
		return fapiao;
	}

	private class save implements ParamHandler {
		Fapiao fapiao = new Fapiao();

		save(Fapiao fapiao) {
			this.fapiao = fapiao;
		}

		public void doHander(PreparedStatement pStatement) throws SQLException {
			pStatement.setString(1, fapiao.getFptt());
			pStatement.setString(2, fapiao.getFpuser());
			pStatement.setString(3, fapiao.getFpphone());	
			pStatement.setString(4, fapiao.getFpyb());
			pStatement.setString(5, fapiao.getFpaddress());
			pStatement.setInt(6,fapiao.getFpuserid());	
		}

	}
}
