package cn.gson.zuche.model.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import cn.gson.zuche.model.bean.Guanliyuantype;
import cn.gson.zuche.model.jdbc.MySqlDb;
import cn.gson.zuche.model.jdbc.ParamHandler;
import cn.gson.zuche.model.jdbc.ResultHandler;

public class Guanliyuantypedao implements ResultHandler<Guanliyuantype>{
private MySqlDb db=MySqlDb.getInstance();
/**
 * 插入
 * @param t
 * @return
 * @throws SQLException
 */
	public boolean insert(final Guanliyuantype t) throws SQLException{
		String sql="insert into manager_quanxian(employee_type,web_system_id) values(?,?)";
		return db.executeUpdate(sql, new save(t))>0;	
	}
	private class save implements ParamHandler{
		Guanliyuantype t;
		public save(Guanliyuantype t){
			this.t=t;
		}
		@Override
		public void doHander(PreparedStatement pStatement) throws SQLException {
			pStatement.setString(1, t.getEmployee_type());
			pStatement.setString(2, t.getWeb_system_id());			
		}
		
	}
	/**
	 * 删除
	 * @param t
	 * @return
	 * @throws SQLException
	 */
	public boolean delete(String  t) throws SQLException{
		String sql="delete from manager_quanxian where employee_type='"+t+"'";
		return db.executeUpdate(sql)>0;	
	}
	/**
	 * 查询
	 */
	public List<Guanliyuantype> select(String  t) throws SQLException{
		String sql="select * from manager_quanxian where employee_type="+t+"";
		return db.executeQuery(sql, this);	
	}
	@Override
	public Guanliyuantype doHander(Map<String, Object> row) {
		Guanliyuantype g=new Guanliyuantype();
		g.setEmployee_type((String) row.get("employee_type"));
		g.setWeb_system_id(row.get("web_system_id")+"");
		return g;
	}

}
