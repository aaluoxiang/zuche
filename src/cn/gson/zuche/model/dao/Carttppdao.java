package cn.gson.zuche.model.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import cn.gson.zuche.model.bean.Carttpp;
import cn.gson.zuche.model.bean.Cartype;
import cn.gson.zuche.model.jdbc.MySqlDb;
import cn.gson.zuche.model.jdbc.ResultHandler;

public class Carttppdao implements ResultHandler<Carttpp>{
	private MySqlDb db = MySqlDb.getInstance();
	/**
	 * 获取车的销量
	 * @author Seven
	 *
	 */ 
	 public List<Carttpp> huoquxiaoliang() throws SQLException{
			String sql="SELECT car_type_name,car_xiaoliang from car_type";	
			return db.executeQuery(sql, this);
	}
	public Carttpp doHander(Map<String, Object> row) {
		Carttpp t=new Carttpp();
		t.setCartypename((String) row.get("car_type_name"));
		t.setNum((Integer) row.get("car_xiaoliang"));
		return t;
	}

}
