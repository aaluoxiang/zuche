package cn.gson.zuche.model.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import cn.gson.zuche.model.bean.Orderzt;
import cn.gson.zuche.model.bean.UserYhq;
import cn.gson.zuche.model.jdbc.MySqlDb;
import cn.gson.zuche.model.jdbc.ResultHandler;

public class OrderztDao implements ResultHandler<Orderzt> {
		
	private MySqlDb db = MySqlDb.getInstance();
	
	
	public List<Orderzt> findztname(Integer id) throws SQLException {
		String sql = "select order_zt_name from user_order_zt where order_zt_id="+id+"";
		return db.executeQuery(sql, this);
	}
	
	
	
	@Override
	public Orderzt doHander(Map<String, Object> row) {
		Orderzt od = new Orderzt();
		od.setOrderztid((Integer)row.get("order_zt_id"));
		od.setOrderztname(row.get("order_zt_name")+"");
		
		return od;
	}

}
