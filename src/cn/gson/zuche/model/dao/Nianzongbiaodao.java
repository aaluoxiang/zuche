package cn.gson.zuche.model.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import cn.gson.zuche.model.bean.Nianzongbiao;
import cn.gson.zuche.model.jdbc.MySqlDb;
import cn.gson.zuche.model.jdbc.ParamHandler;
import cn.gson.zuche.model.jdbc.ResultHandler;

public class Nianzongbiaodao implements ResultHandler<Nianzongbiao>{
	private MySqlDb db=MySqlDb.getInstance();
	public List<Nianzongbiao> select() throws SQLException{
		String sql="select * from nianzongbiao";
		return db.executeQuery(sql, this);
	}
	
	public List<Nianzongbiao> selectbydata(String data) throws SQLException{
		String sql="select * from nianzongbiao where data='"+data+"'";
		return db.executeQuery(sql, this);
	}
	
	public boolean insert(final Nianzongbiao n) throws SQLException{
		String sql="insert into nianzongbiao(data,money) values(?,?)";
		return db.executeUpdate(sql,new save(n))>0;
	}	
	public List<Nianzongbiao> selectqian(String data) throws SQLException{
		String sql="select money from nianzongbiao where data='"+data+"'";
		return db.executeQuery(sql, this);
	}
	
	public boolean update(String money,String data) throws SQLException{
		String sql="update nianzongbiao set money='"+money+"' where data='"+data+"'";
		return db.executeUpdate(sql)>0;
	}	
	private class save implements ParamHandler{
		Nianzongbiao n;
		 public save(Nianzongbiao n){
			this.n=n;
		}
		@Override
		public void doHander(PreparedStatement pStatement) throws SQLException {
		pStatement.setString(1, n.getData());
		pStatement.setString(2, n.getMoney());
		}
		
	}
	@Override
	public Nianzongbiao doHander(Map<String, Object> row) {
		Nianzongbiao n=new Nianzongbiao();
		n.setData((String) row.get("data"));
		n.setMoney((String) row.get("money"));
		return n;
	}

}
