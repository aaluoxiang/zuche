package cn.gson.zuche.model.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import cn.gson.zuche.model.bean.Wcpz;
import cn.gson.zuche.model.jdbc.MySqlDb;
import cn.gson.zuche.model.jdbc.ParamHandler;
import cn.gson.zuche.model.jdbc.ResultHandler;

public class Wcpzdao implements ResultHandler<Wcpz>{
	private MySqlDb db = MySqlDb.getInstance();

	public List<Wcpz> selectall() throws SQLException{
		String sql="select * from wcpz";
		return db.executeQuery(sql, this);
	}
	
	public boolean insertAll(Wcpz wcpz) throws SQLException{
		String sql = "insert into wcpz(damage_level,maintain_money,orderid,quityjmoney) values(?,?,?,?)";
		return db.executeUpdate(sql, new Save(wcpz))>0;
		
	}
	@Override
	public Wcpz doHander(Map<String, Object> row) {
		Wcpz wcpz = new Wcpz();
		wcpz.setWcpzid(row.get("wcpzid")+"");
		wcpz.setDamagelevel(row.get("damage_level")+"");
		wcpz.setMaintainmoney(row.get("maintain_money")+"");
		wcpz.setOrderid(row.get("orderid")+"");
		wcpz.setQuityjmoney(row.get("quityjmoney")+"");
		return wcpz;
	}

	private class Save implements ParamHandler{
		Wcpz wcpz = new Wcpz();
		
		public Save(Wcpz wcpz) {
			this.wcpz = wcpz;
		}
		@Override
		public void doHander(PreparedStatement pStatement) throws SQLException {
			// TODO Auto-generated method stub
			pStatement.setString(1, wcpz.getDamagelevel());
			pStatement.setString(2, wcpz.getMaintainmoney());
			pStatement.setString(3, wcpz.getOrderid());
			pStatement.setString(4, wcpz.getQuityjmoney());
		}
		
	}
}
