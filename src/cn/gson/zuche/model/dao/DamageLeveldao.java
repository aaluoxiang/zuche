package cn.gson.zuche.model.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import cn.gson.zuche.model.bean.DamageLevel;
import cn.gson.zuche.model.jdbc.MySqlDb;
import cn.gson.zuche.model.jdbc.ParamHandler;
import cn.gson.zuche.model.jdbc.ResultHandler;

public class DamageLeveldao implements ResultHandler<DamageLevel>{
	private MySqlDb db = MySqlDb.getInstance();

	public List<DamageLevel> selectall() throws SQLException{
		String sql="select * from damagelevel";
		return db.executeQuery(sql, this);
	}
	public List<DamageLevel> selectid(String value) throws SQLException{
		String sql="select lever_id from damagelevel where add_maintain_money='"+value+"'";
		return db.executeQuery(sql, this);
	}
	
	@Override
	public DamageLevel doHander(Map<String, Object> row) {
		DamageLevel daml = new DamageLevel();
		daml.setLeverid(row.get("lever_id")+"");
		daml.setLevername(row.get("damage_lever_name")+"");
		daml.setAddmaintainmoney(row.get("add_maintain_money")+"");
		return daml;
	}
}
