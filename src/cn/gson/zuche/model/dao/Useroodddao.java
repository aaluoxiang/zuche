package cn.gson.zuche.model.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import cn.gson.zuche.model.bean.Useroodd;
import cn.gson.zuche.model.bean.Userorder;
import cn.gson.zuche.model.jdbc.MySqlDb;
import cn.gson.zuche.model.jdbc.ResultHandler;

public class Useroodddao implements ResultHandler<Useroodd>{
	private MySqlDb db = MySqlDb.getInstance();
	/**
	 * 查找已完成订单数量
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public List<Useroodd> chazhaoyiwcshuliang() throws SQLException {
		String sql = "SELECT  order_zt_name as name,COUNT(user_order_zt) as num from user_order_zt,user_order WHERE user_order_zt=1 and order_zt_id=user_order_zt";
		return db.executeQuery(sql, this);
	}
	/**
	 * 查找已经退单的数量
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public List<Useroodd> chazhaoytdshuliang( String zt) throws SQLException {
		String sql = "SELECT  order_zt_name as name,COUNT(user_order_zt) as num from user_order_zt,user_order WHERE user_order_zt='"+zt+"' and order_zt_id=user_order_zt";
		return db.executeQuery(sql, this);
	}
	/**
	 * 按照时间查询所有订单的状态
	 */
	public List<Useroodd> chaxunbytime( String begin, String down,String zt) throws SQLException {
		String sql = "SELECT  order_zt_name as name,COUNT(user_order_zt) as num from user_order_zt,user_order WHERE (order_single_time BETWEEN '"+begin+"' and '"+down+"') and user_order_zt='"+zt+"' and order_zt_id=user_order_zt";
		return db.executeQuery(sql, this);
	}
	@Override
	public Useroodd doHander(Map<String, Object> row) {
		Useroodd d=new Useroodd();
		d.setName((String) row.get("name"));
		d.setNum(row.get("num")+"");
		return d;
	}

}
