package cn.gson.zuche.model.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import cn.gson.zuche.model.bean.Sijixiangche;
import cn.gson.zuche.model.bean.Weburl;
import cn.gson.zuche.model.jdbc.MySqlDb;
import cn.gson.zuche.model.jdbc.ParamHandler;
import cn.gson.zuche.model.jdbc.ResultHandler;

public class Sijixiangchedao implements ResultHandler<Sijixiangche>{
	MySqlDb db=MySqlDb.getInstance();
	public List<Sijixiangche> select(String name) throws SQLException{
		String sql="select * from siji_xiangche where siji_name='"+name+"'";
		return db.executeQuery(sql, this);
	}
	/*
	 * 删除图片
	 */
	public boolean delete(String name) throws SQLException{
		String sql="delete from siji_xiangche where siji_url='"+name+"'";
		return db.executeUpdate(sql)>0;
	}
	public boolean insert(final Sijixiangche siji) throws SQLException{
		String sql="insert  into siji_xiangche(siji_name,siji_url) values(?,?)";
		return db.executeUpdate(sql, new save(siji))>0;
	}
	public class save implements ParamHandler {
		Sijixiangche siji;
		save(Sijixiangche siji) {
			this.siji = siji;
		}
		public void doHander(PreparedStatement pStatement) throws SQLException {
			pStatement.setString(1,siji.getName());
			pStatement.setString(2,siji.getUrl());		
		}
	}
	@Override
	public Sijixiangche doHander(Map<String, Object> row) {
		Sijixiangche siji=new Sijixiangche();
		siji.setName((String) row.get("siji_name"));
		siji.setUrl((String) row.get("siji_url"));
		return siji;
	}
}
