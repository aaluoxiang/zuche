package cn.gson.zuche.model.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import cn.gson.zuche.model.bean.Count;
import cn.gson.zuche.model.jdbc.MySqlDb;
import cn.gson.zuche.model.jdbc.ResultHandler;

public class Countdao implements ResultHandler<Count>{
	private MySqlDb db = MySqlDb.getInstance();

	
	
	/**
	 * 根据用户id查询取消订单数
	 * @author 
	 *
	 */
	public List<Count> selectdingdan(Long userId) throws SQLException{
		String sql = "SELECT COUNT(*) counter FROM user_order where user_order_zt=3 AND  user_id="+userId+"";
		return db.executeQuery(sql, this);
	}



	@Override
	public Count doHander(Map<String, Object> row) {
		Count c=new Count();
		c.setCounter(Integer.parseInt(row.get("counter")+""));
		return c;
	}

	
	

}
