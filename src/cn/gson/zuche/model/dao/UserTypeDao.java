package cn.gson.zuche.model.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import cn.gson.zuche.model.bean.User;
import cn.gson.zuche.model.bean.UserType;
import cn.gson.zuche.model.jdbc.MySqlDb;
import cn.gson.zuche.model.jdbc.ParamHandler;
import cn.gson.zuche.model.jdbc.ResultHandler;

public class UserTypeDao implements ResultHandler<UserType> {
	private MySqlDb db = MySqlDb.getInstance();
	
	/**
	 * 查询所需升级积分
	 * @throws SQLException 
	 */
	public List<UserType> findAll() throws SQLException{
		String sql = "SELECT * from user_type";
		return db.executeQuery(sql, this);
	}
	/**
	 * 删除
	 */
	public boolean delete(String id) throws SQLException{
		String sql="delete from user_type where user_type_id="+id+"";
		return db.executeUpdate(sql)>0;
	}
	/**
	 * 根据名字查找id
	 */
	public List<UserType> findid(String id) throws SQLException{
		String sql = "SELECT user_type_id from user_type where user_type_name='"+id+"'";
		return db.executeQuery(sql, this);
	}
	/**
	 * 插入
	 * @param usertypename
	 * @return
	 * @throws SQLException
	 */
	public boolean insert(final UserType t) throws SQLException{
		String sql="insert into user_type(user_type_name,user_type_upjf,user_type_cut) values(?,?,?)";
		return db.executeUpdate(sql, new save(t))>0;
	}
	private class save implements ParamHandler{
		UserType t;
		public save(UserType t){
			this.t=t;
		}
		public void doHander(PreparedStatement pStatement) throws SQLException {
			pStatement.setString(1,t.getUsertypename());
			pStatement.setString(2,t.getUsertypeupjf());
			pStatement.setString(3,t.getUsertypecut()+"");
		}
		
	}
	public List findupjf(String usertypename) throws SQLException{
		String sql = "SELECT user_type_upjf from user_type where user_type_name='"+usertypename+"'";
		return db.executeQuery(sql, new ResultHandler() {
			@Override
			public Object doHander(Map row) {
				return row.get("user_type_upjf");
			}
		});
	
	}
	/**
	 * 查询会员信息
	 */
	public List<UserType> findZk(String name) throws SQLException{
		String sql = "SELECT * from user_type where user_type_name='"+name+"'";
		return db.executeQuery(sql, this);
	}
	@Override
	public UserType doHander(Map<String, Object> row) {
		UserType u = new UserType();
		u.setUsertypeId(Long.parseLong(row.get("user_type_id")+""));
		u.setUsertypename((String) row.get("user_type_name"));
		u.setUsertypecut((Double) row.get("user_type_cut"));
		u.setUsertypeupjf((String) row.get("user_type_upjf"));
		return u;
	}

	
	
}
