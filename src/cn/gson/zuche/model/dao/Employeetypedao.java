package cn.gson.zuche.model.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import cn.gson.zuche.model.bean.Employeetype;
import cn.gson.zuche.model.jdbc.MySqlDb;
import cn.gson.zuche.model.jdbc.ResultHandler;

public class Employeetypedao implements ResultHandler<Employeetype>{
private MySqlDb db=MySqlDb.getInstance();
	public List<Employeetype> select() throws SQLException{
		String sql="select * from employee_type where employee_type_id>1";
		return db.executeQuery(sql, this);
	}
	
	public List<Employeetype> findid(String name) throws SQLException{
		String sql="select employee_type_id from employee_type where employee_type_name='"+name+"'";
		return db.executeQuery(sql, this);
	}
	@Override
	public Employeetype doHander(Map<String, Object> row) {
		Employeetype em=new Employeetype();
		em.setEmployeetypeid((String) row.get("employee_type_id"));
		em.setEmployeetypename((String) row.get("employee_type_name"));
		return em;
	}

}
