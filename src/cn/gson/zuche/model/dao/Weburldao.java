package cn.gson.zuche.model.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;


import cn.gson.zuche.model.bean.Weburl;
import cn.gson.zuche.model.jdbc.MySqlDb;
import cn.gson.zuche.model.jdbc.ParamHandler;
import cn.gson.zuche.model.jdbc.ResultHandler;

public class Weburldao implements ResultHandler<Weburl>{
	private MySqlDb db=MySqlDb.getInstance();
	public List<Weburl> findall() throws SQLException{
		String sql="select * from web_url";
		return db.executeQuery(sql, this);
	}
	public boolean sava(final Weburl w ) throws SQLException{
		String sal="INSERT into web_url(web_url_dns,web_url_name,web_url_img) VALUES(?,?,?)";
		return db.executeUpdate(sal, new SaveParamHandler(w))>0;	
	}
	public int delete(String a) throws SQLException{
		String sql="delete from  web_url where web_url_type_id="+a+"";
		return db.executeUpdate(sql);	
	}
	public int update(){	
		return 0;	
	}
	public class SaveParamHandler implements ParamHandler {
		Weburl w;
		SaveParamHandler(Weburl user) {
			this.w = user;
		}
		public void doHander(PreparedStatement pStatement) throws SQLException {
			pStatement.setString(1,w.getWeburldns() );
			pStatement.setString(2, w.getWeburlname());
			pStatement.setString(3, w.getWeburlimg());
		}
	}
	public Weburl doHander(Map<String, Object> row) {
		Weburl web=new Weburl();
		web.setWeburldns((String) row.get("web_url_dns"));
		web.setWeburlimg((String)row.get("web_url_img"));
		web.setWeburlname((String) row.get("web_url_name"));
		web.setWeburltypeid((Integer) row.get("web_url_type_id"));
		return web;
	}

}
