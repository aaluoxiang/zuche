package cn.gson.zuche.model.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import cn.gson.zuche.model.bean.Systemmk;
import cn.gson.zuche.model.jdbc.MySqlDb;
import cn.gson.zuche.model.jdbc.ParamHandler;
import cn.gson.zuche.model.jdbc.ResultHandler;


public class Systemmkdao implements ResultHandler<Systemmk>{
private MySqlDb db=MySqlDb.getInstance();
public List<Systemmk> findall() throws SQLException{
	String sql="SELECT * from web_system";
	return db.executeQuery(sql,this);
}

/**
 * 查询最大序号
 * @return
 * @throws SQLException
 */
public List<Systemmk> selectmax() throws SQLException{
	String sql="select max(web_son_id) as web_model_paixu from web_system";
	return db.executeQuery(sql,this);
}
/**
 * 查询
 * @author Seven
 *
 */
public List<Systemmk> selectBysonid(String sonid) throws SQLException{
	String sql="SELECT * from web_system where web_son_id="+sonid+"";
	return db.executeQuery(sql,this);
}
/**
 * 删除
 */
public boolean delete(String mkid) throws SQLException{
	String sql="delete from web_system where web_son_id='"+mkid+"'";
	return db.executeUpdate(sql)>0;
}
/**
 * 插入
 * @author Seven
 * @throws SQLException 
 *
 */
public boolean insert(final Systemmk mk) throws SQLException{
	String sql="insert into web_system(web_model_name,web_model_url,web_son_id,web_father_id) values(?,?,?,?)";
	return db.executeUpdate(sql,new saveprm(mk))>0;
}
private class saveprm implements ParamHandler{
	Systemmk sys;
	saveprm(Systemmk sys){
		this.sys=sys;
	}
	public void doHander(PreparedStatement pStatement) throws SQLException {	
		pStatement.setString(1, sys.getWebmodelname());
		pStatement.setString(2, sys.getWebmodelurl());		
		pStatement.setInt(3, sys.getWebsonid());
		pStatement.setInt(4, sys.getWebfatherid());
	}
	
}
	public Systemmk doHander(Map<String, Object> row) {
		Systemmk s=new Systemmk();
		s.setWebsonid((Integer) row.get("web_son_id"));
		s.setWebmodelname((String)row.get("web_model_name"));
		s.setWebmodeltype((String) row.get("web_model_type"));
		s.setWebmodelpaixu(row.get("web_model_paixu")+"");
		s.setWebmodelurl((String) row.get("web_model_url"));
		s.setWebfatherid((Integer)  row.get("web_father_id"));
		
		return s;
	}

}
