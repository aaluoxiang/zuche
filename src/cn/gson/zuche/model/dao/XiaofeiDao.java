package cn.gson.zuche.model.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import cn.gson.zuche.model.bean.Xiaofei;
import cn.gson.zuche.model.jdbc.MySqlDb;
import cn.gson.zuche.model.jdbc.ResultHandler;

public class XiaofeiDao implements ResultHandler<Xiaofei> {

	private MySqlDb db = MySqlDb.getInstance();

	/**
	 * 用户中心余额模块消费记录查询
	 * 
	 * @param orderids
	 * @return
	 * @throws SQLException
	 */
	public List<Xiaofei> findAllxfjl(List orderids) throws SQLException {
		String sql = "SELECT order_id,order_jiaoyifaxiang,order_jiaoyilaiyuan,order_money,order_data "
				+ "FROM order_xiaofeijilu WHERE ";
		// order_id='ksice2019' or order_id='ksice2016'
		for (int i = 0; i < orderids.size(); i++) {
			if (i == orderids.size() - 1) {
				sql += "order_id='" + orderids.get(i) + "'";
				break;
			}
			sql += "order_id='" + orderids.get(i) + "' or ";

		}
		sql += " ORDER BY `order_data` DESC";
		System.out.println(sql);
		System.out.println(orderids);
		return db.executeQuery(sql, this);
	}

	public List<Xiaofei> findsumsfjl(List orderids) throws SQLException {
		String sql = "SELECT order_id,order_jiaoyifaxiang,order_jiaoyilaiyuan,SUM(order_money) as order_money ,MAX(order_data) as order_data "
				+ "FROM order_xiaofeijilu WHERE ";
		for (int i = 0; i < orderids.size(); i++) {
			if (i == orderids.size() - 1) {
				sql += "order_id='" + orderids.get(i) + "'";
				break;
			}
			sql += "order_id='" + orderids.get(i) + "' or ";

		}
		sql += " GROUP BY order_id ORDER BY `order_data` DESC";
		return db.executeQuery(sql, this);
	}

	@Override
	public Xiaofei doHander(Map<String, Object> row) {
		Xiaofei xf = new Xiaofei();
		xf.setOrderid(row.get("order_id") + "");
		xf.setOrderxffx(row.get("order_jiaoyifaxiang") + "");
		xf.setOrderxfly(row.get("order_jiaoyilaiyuan") + "");
		xf.setOrdermoney(row.get("order_money") + "");
		xf.setOrderxftime(row.get("order_data"));
		return xf;
	}
}
