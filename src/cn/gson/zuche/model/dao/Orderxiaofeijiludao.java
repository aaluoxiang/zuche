package cn.gson.zuche.model.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import cn.gson.zuche.model.bean.Orderxiaofeijilu;
import cn.gson.zuche.model.jdbc.MySqlDb;
import cn.gson.zuche.model.jdbc.ParamHandler;
import cn.gson.zuche.model.jdbc.ResultHandler;

public class Orderxiaofeijiludao implements ResultHandler<Orderxiaofeijilu>{
private MySqlDb db=MySqlDb.getInstance();
	public List<Orderxiaofeijilu> select(String id) throws SQLException{
		String sql="select * from order_xiaofeijilu where order_id='"+id+"'";
		return db.executeQuery(sql, this);
	}
	public List<Orderxiaofeijilu> selectshifou(String id) throws SQLException{
		String sql="select * from order_xiaofeijilu where order_id='"+id+"'";
		return db.executeQuery(sql, this);
	}
	public boolean insert(final Orderxiaofeijilu jilu) throws SQLException{
		String sql="insert into order_xiaofeijilu values(?,?,?,?,?)";
		return db.executeUpdate(sql, new save(jilu))>0;
	}
	public List<Orderxiaofeijilu> selectsum(String id) throws SQLException{
		String sql="select sum(order_money ) as order_money  from order_xiaofeijilu where order_id='"+id+"' ";
		return db.executeQuery(sql, this);
	}
	/**
	 * 查询每天订单的总金额
	 * @param jilu
	 * @return
	 * @throws SQLException
	 */
	public List<Orderxiaofeijilu> selectdaymoney(String data,String data2) throws SQLException{
		String sql="select sum(order_money) as order_money  from order_xiaofeijilu WHERE order_data BETWEEN '"+data+"' and '"+data2+"'";
		return db.executeQuery(sql, this);
	}
	//插入退款数据
	
	public boolean inserttk(final Orderxiaofeijilu jilu) throws SQLException{
		String sql="insert into order_xiaofeijilu values(?,?,?,?,?)";
		return db.executeUpdate(sql, new sv(jilu))>0;
	}
	private class sv implements ParamHandler{
		Orderxiaofeijilu jilu;
		sv(Orderxiaofeijilu jilu){
			this.jilu=jilu;
		}
		public void doHander(PreparedStatement pStatement) throws SQLException {
			pStatement.setString(1, jilu.getOrderid());
			pStatement.setString(2, "退款");
			pStatement.setString(3, jilu.getOrderjiaoyilaiyuan());
			pStatement.setString(4, jilu.getOrdermoney());
			pStatement.setString(5,	(String) jilu.getOrderdata());		
		}
	}
		
	
	/**
	 * 根据订单号总结订单消费金额
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public List<Orderxiaofeijilu> selectmoney(String id) throws SQLException{
		String sql="select sum(order_money) as order_money FROM order_xiaofeijilu WHERE  order_id='"+id+"'";
		return db.executeQuery(sql, this);
	}
	private class save implements ParamHandler{
		Orderxiaofeijilu jilu;
		save(Orderxiaofeijilu jilu){
			this.jilu=jilu;
		}
		public void doHander(PreparedStatement pStatement) throws SQLException {
			pStatement.setString(1, jilu.getOrderid());
			pStatement.setString(2, "消费");
			pStatement.setString(3, jilu.getOrderjiaoyilaiyuan());
			pStatement.setString(4, jilu.getOrdermoney());
			pStatement.setString(5,	(String) jilu.getOrderdata());		
		}
	}
	@Override
	public Orderxiaofeijilu doHander(Map<String, Object> row) {
		Orderxiaofeijilu jilu=new Orderxiaofeijilu();
		jilu.setOrderdata(row.get("order_data"));
		jilu.setOrderid((String) row.get("order_id"));
		jilu.setOrderjiaoyifaxiang((String) row.get("order_jiaoyifaxiang"));
		jilu.setOrderjiaoyilaiyuan((String) row.get("order_jiaoyilaiyuan"));
		jilu.setOrdermoney(row.get("order_money")+"");
		return jilu;
	}

}
