package cn.gson.zuche.model.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import cn.gson.zuche.model.bean.Qcpz;
import cn.gson.zuche.model.jdbc.MySqlDb;
import cn.gson.zuche.model.jdbc.ParamHandler;
import cn.gson.zuche.model.jdbc.ResultHandler;

public class Qcpzdao implements ResultHandler<Qcpz>{
	private MySqlDb db = MySqlDb.getInstance();

	public List<Qcpz> selectall() throws SQLException{
		String sql="select * from qcpz";
		return db.executeQuery(sql, this);
	}
	
	public List<Qcpz> selectbyid(String id) throws SQLException{
		String sql="select * from qcpz where orderid='"+id+"'";
		return db.executeQuery(sql, this);
	}
	
	public boolean insertAll(Qcpz qcpz) throws SQLException{
		String sql = "insert into qcpz(lxrname,jznumber,orderid,jzpicture,yjmoney) values(?,?,?,?,?)";
		return db.executeUpdate(sql, new Save(qcpz))>0;
		
	}
	
	@Override
	public Qcpz doHander(Map<String, Object> row) {
		Qcpz qcpz = new Qcpz();
		qcpz.setQcpzid(row.get("qcpzid")+"");
		qcpz.setLxrname(row.get("lxrname")+"");
		qcpz.setJznumber(row.get("jznumber")+"");
		qcpz.setOrderid(row.get("orderid")+"");
		qcpz.setJzpicture(row.get("jzpicture")+"");
		qcpz.setYjmoney(row.get("yjmoney")+"");
		return qcpz;
	}

	private class Save implements ParamHandler{
		Qcpz qcpz = new Qcpz();
		
		public Save(Qcpz qcpz) {
			this.qcpz = qcpz;
		}
		@Override
		public void doHander(PreparedStatement pStatement) throws SQLException {
			// TODO Auto-generated method stub
			pStatement.setString(1, qcpz.getLxrname());
			pStatement.setString(2, qcpz.getJznumber());
			pStatement.setString(3, qcpz.getOrderid());
			pStatement.setString(4, qcpz.getJzpicture());
			pStatement.setString(5, qcpz.getYjmoney());
		}
		
	}
}
