package cn.gson.zuche.model.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import cn.gson.zuche.model.GlobalParamHandler;
import cn.gson.zuche.model.bean.Employee;
import cn.gson.zuche.model.jdbc.MySqlDb;
import cn.gson.zuche.model.jdbc.ParamHandler;
import cn.gson.zuche.model.jdbc.ResultHandler;

public class Employeedao implements ResultHandler<Employee>{
	private MySqlDb db=MySqlDb.getInstance();
	public List<Employee> select() throws SQLException{
		String sql="select employee_id,employee_name,employee_tel,employee_sex,employee_age,employee_sfz,employee_jzbh,employee_zt_mc as employee_zt  from employee,employee_ztb where employee_type_id=1 and employee.employee_zt=employee_ztb.employee_zt_id";
		return db.executeQuery(sql, this);
	}
	/**
	 * 通过姓名查找员工
	 * @param em
	 * @return
	 * @throws SQLException
	 */
	public List<Employee> selectByname(String name) throws SQLException{
		String sql="select employee_id,employee_name,employee_tel,employee_sex,employee_age,employee_sfz,employee_jzbh,employee_zt_mc as employee_zt  from employee,employee_ztb where employee_type_id=1 and  employee_name='"+name+"' and  employee.employee_zt=employee_ztb.employee_zt_id";
		return db.executeQuery(sql, this);
	}
	/**
	 * 判断登陆
	 * @param em
	 * @return
	 * @throws SQLException
	 */
	public List<Employee> pddl(String name,String password) throws SQLException{
		String sql="select employee_type_id from employee where employee_name='"+name+"' and employee_password='"+password+"'";
		return db.executeQuery(sql, this);
	}
	public boolean updata(Employee em) throws SQLException{
		String sql="UPDATE employee SET	employee_name='"+em.getEmployeename()+"',employee_yonhuming='admin',employee_type_id='"+em.getEmployeetypeid()+"',employee_sex='"+em.getEmployeesex()+"',employee_qq='"+em.getEmployeeqq()+"',employee_password='"+em.getEmployeepassword()+"' WHERE employee_id="+em.getEmployeeid()+"";
		System.out.println(sql);
		return db.executeUpdate(sql)>0;
	}
	public List<Employee> selectgly() throws SQLException{
		String sql="select employee_password,employee_id,employee_name,employee_yonhuming,employee_sex,employee_type_name as employee_type_id,employee_qq FROM employee,employee_type WHERE  employee.employee_type_id>1 and  employee.employee_type_id=employee_type.employee_type_id";
		return db.executeQuery(sql, this);
	}
	public boolean update(String zt,String id) throws SQLException{
		String sql="update employee set employee_zt="+zt+" where employee_id="+id+"";
		return db.executeUpdate(sql)>0;
	}
	public boolean save(final Employee em) throws SQLException{
		String sql="INSERT INTO employee (employee_type_id,employee_name,employee_tel,employee_sex,employee_age,employee_sfz,employee_jzbh,employee_zt) VALUES(1,?,?,?,?,?,?,?)";
		return db.executeUpdate(sql, new SaveParamHandler(em))>0;
	}
	public boolean savegly(final Employee em) throws SQLException{
		String sql="INSERT INTO employee (employee_name,employee_yonhuming,employee_sex,employee_type_id,employee_qq,employee_password) VALUES(?,?,?,?,?,?)";
		return db.executeUpdate(sql, new SaveParam(em))>0;
	}
	private class SaveParam implements ParamHandler {
		Employee em;
		SaveParam(Employee em) {
			this.em = em;
		}
		public void doHander(PreparedStatement pStatement) throws SQLException {
			// 处理sql参数
			pStatement.setString(1, em.getEmployeename());
			pStatement.setString(2, em.getEmployeeyonhuming());
			pStatement.setString(3, em.getEmployeesex());
			pStatement.setString(4, em.getEmployeetypeid());
			pStatement.setString(5, em.getEmployeeqq());
			pStatement.setString(6, em.getEmployeepassword());
		}
	}
	public boolean delete(String id) throws SQLException{
		String sql="delete from  employee where employee_id="+id+"";
			return db.executeUpdate(sql)>0;
	}
	private class SaveParamHandler implements ParamHandler {
		Employee em;
		SaveParamHandler(Employee em) {
			this.em = em;
		}
		public void doHander(PreparedStatement pStatement) throws SQLException {
			// 处理sql参数
			pStatement.setString(1, em.getEmployeename());
			pStatement.setString(2, em.getEmployeetel());
			pStatement.setString(3, em.getEmployeesex());
			pStatement.setInt(4, em.getEmployeeage());
			pStatement.setString(5, em.getEmployeesfz());
			pStatement.setString(6, em.getEmployeejzbh());
			pStatement.setString(7, em.getEmployeezt());
		}
	}
	@Override
	public Employee doHander(Map<String, Object> row) {
		Employee em=new Employee();
		em.setEmployeeid((Integer) row.get("employee_id"));
		em.setEmployeetypeid((String) row.get("employee_type_id"));
		em.setEmployeename((String) row.get("employee_name"));
		em.setEmployeetel((String) row.get("employee_tel"));
		em.setEmployeesex((String) row.get("employee_sex"));
		em.setEmployeeage((Integer) row.get("employee_age"));
		em.setEmployeesfz((String) row.get("employee_sfz"));
		em.setEmployeejzbh((String) row.get("employee_jzbh"));
		em.setEmployeezt((String) row.get("employee_zt"));
		em.setEmployeepassword((String) row.get("employee_password"));
		em.setEmployeeqq((String)row.get("employee_qq"));
		em.setEmployeeyonhuming((String)row.get("employee_yonhuming"));;
		return em;
	}

}
