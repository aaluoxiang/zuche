package cn.gson.zuche.model.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import cn.gson.zuche.model.bean.Userorderzt;
import cn.gson.zuche.model.jdbc.MySqlDb;
import cn.gson.zuche.model.jdbc.ResultHandler;

public class Userorderztdao implements ResultHandler<Userorderzt> {
private MySqlDb db=MySqlDb.getInstance();
	public List<Userorderzt> select() throws SQLException{
		String sql="select * from user_order_zt";
		return db.executeQuery(sql, this);
	}
	@Override
	public Userorderzt doHander(Map<String, Object> row) {
		Userorderzt zt=new Userorderzt();
		zt.setOrderztid(row.get("order_zt_id")+"");
		zt.setOrderztname((String) row.get("order_zt_name"));
		return zt;
	}

}
