package cn.gson.zuche.model.bean;

public class Chozhi {
private String chozhitel;
private String chozhijiaoyifangxiang;
private String chozhijiaoyilaiyuan;
private String chozhimoney;
private Object chozhidata;
private String chozhicaozuoren;
public Chozhi(){
	
}
public String getChozhitel() {
	return chozhitel;
}
public void setChozhitel(String chozhitel) {
	this.chozhitel = chozhitel;
}
public String getChozhijiaoyifangxiang() {
	return chozhijiaoyifangxiang;
}
public void setChozhijiaoyifangxiang(String chozhijiaoyifangxiang) {
	this.chozhijiaoyifangxiang = chozhijiaoyifangxiang;
}
public String getChozhijiaoyilaiyuan() {
	return chozhijiaoyilaiyuan;
}
public void setChozhijiaoyilaiyuan(String chozhijiaoyilaiyuan) {
	this.chozhijiaoyilaiyuan = chozhijiaoyilaiyuan;
}
public String getChozhimoney() {
	return chozhimoney;
}
public void setChozhimoney(String chozhimoney) {
	this.chozhimoney = chozhimoney;
}
public Object getChozhidata() {
	return chozhidata;
}
public void setChozhidata(Object chozhidata) {
	this.chozhidata = chozhidata;
}

public String getChozhicaozuoren() {
	return chozhicaozuoren;
}
public void setChozhicaozuoren(String chozhicaozuoren) {
	this.chozhicaozuoren = chozhicaozuoren;
}
@Override
public String toString() {
	return "Chozhi [chozhitel=" + chozhitel + ", chozhijiaoyifangxiang=" + chozhijiaoyifangxiang
			+ ", chozhijiaoyilaiyuan=" + chozhijiaoyilaiyuan + ", chozhimoney=" + chozhimoney + ", chozhidata="
			+ chozhidata + ", chozhicaozuoren=" + chozhicaozuoren + "]";
}

}
