package cn.gson.zuche.model.bean;

/**
 * 车辆损坏评级实体类
 * 
 * @author huanxu
 *
 */
public class DamageLevel {

	// 损坏等级表主键id
	private String leverid;

	// 损坏等级名称
	private String levername;

	// 增加的维修费用
	private String addmaintainmoney;

	public DamageLevel() {
	}

	public String getLeverid() {
		return leverid;
	}

	public void setLeverid(String leverid) {
		this.leverid = leverid;
	}

	public String getLevername() {
		return levername;
	}

	public void setLevername(String levername) {
		this.levername = levername;
	}

	public String getAddmaintainmoney() {
		return addmaintainmoney;
	}

	public void setAddmaintainmoney(String addmaintainmoney) {
		this.addmaintainmoney = addmaintainmoney;
	}

	@Override
	public String toString() {
		return "DamageLevel [leverid=" + leverid + ", levername=" + levername + ", addmaintainmoney=" + addmaintainmoney
				+ "]";
	}
	
	
}
