package cn.gson.zuche.model.bean;

public class Employee {
private Integer employeeid;
private String employeetypeid;
private String employeename;
private String employeetel;
private String employeesex;
private Integer employeeage;
private String employeesfz;
private String employeejzbh;
private String employeezt;
private String employeepassword;
private String employeeqq;
private String employeeyonhuming;
public Employee(){}
public Integer getEmployeeid() {
	return employeeid;
}
public void setEmployeeid(Integer employeeid) {
	this.employeeid = employeeid;
}
public String getEmployeeyonhuming() {
	return employeeyonhuming;
}
public void setEmployeeyonhuming(String employeeyonhuming) {
	this.employeeyonhuming = employeeyonhuming;
}
public String getEmployeeqq() {
	return employeeqq;
}
public void setEmployeeqq(String employeeqq) {
	this.employeeqq = employeeqq;
}
public String getEmployeetypeid() {
	return employeetypeid;
}
public void setEmployeetypeid(String employeetypeid) {
	this.employeetypeid = employeetypeid;
}
public String getEmployeename() {
	return employeename;
}
public void setEmployeename(String employeename) {
	this.employeename = employeename;
}
public String getEmployeetel() {
	return employeetel;
}
public void setEmployeetel(String employeetel) {
	this.employeetel = employeetel;
}
public String getEmployeesex() {
	return employeesex;
}
public void setEmployeesex(String employeesex) {
	this.employeesex = employeesex;
}
public Integer getEmployeeage() {
	return employeeage;
}
public void setEmployeeage(Integer employeeage) {
	this.employeeage = employeeage;
}
public String getEmployeesfz() {
	return employeesfz;
}
public void setEmployeesfz(String employeesfz) {
	this.employeesfz = employeesfz;
}
public String getEmployeejzbh() {
	return employeejzbh;
}
public void setEmployeejzbh(String employeejzbh) {
	this.employeejzbh = employeejzbh;
}
public String getEmployeepassword() {
	return employeepassword;
}
public void setEmployeepassword(String employeepassword) {
	this.employeepassword = employeepassword;
}

public String getEmployeezt() {
	return employeezt;
}
public void setEmployeezt(String employeezt) {
	this.employeezt = employeezt;
}
@Override
public String toString() {
	return "Employee [employeeid=" + employeeid + ", employeetypeid=" + employeetypeid + ", employeename="
			+ employeename + ", employeetel=" + employeetel + ", employeesex=" + employeesex + ", employeeage="
			+ employeeage + ", employeesfz=" + employeesfz + ", employeejzbh=" + employeejzbh + ", employeezt="
			+ employeezt + ", employeepassword=" + employeepassword + ", employeeqq=" + employeeqq
			+ ", employeeyonhuming=" + employeeyonhuming + "]";
}
}
