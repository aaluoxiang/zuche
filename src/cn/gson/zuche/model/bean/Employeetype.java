package cn.gson.zuche.model.bean;

public class Employeetype {
private String employeetypeid;
private String employeetypename;
public Employeetype(){
	
}
public String getEmployeetypeid() {
	return employeetypeid;
}
public void setEmployeetypeid(String employeetypeid) {
	this.employeetypeid = employeetypeid;
}
public String getEmployeetypename() {
	return employeetypename;
}
public void setEmployeetypename(String employeetypename) {
	this.employeetypename = employeetypename;
}
@Override
public String toString() {
	return "Employeetype [employeetypeid=" + employeetypeid + ", employeetypename=" + employeetypename + "]";
}

}
