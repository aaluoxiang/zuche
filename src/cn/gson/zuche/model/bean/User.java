package cn.gson.zuche.model.bean;

public class User {
	
	
	// 用户ID
	 
	private Long userId;
	
	
	// 用户姓名
	 
	private String userName;
	
	
	// 用户密码
	private String password;
	
	//用户密码MD5
	private String passwdmd5;

	// 用户邮箱
	private String userEmail;
	
	 //用户性别
	private String userSex;
	
	//用户证件类型
	private String userZjlx;
	
	//用户身份证
	private String userSfz;
	
	 //用户电话
	private String userTel;
	
	// 用户驾照
	private Integer userJz;
	
	//用户类型
	private Object userType;

	//用户状态
	private Object userZt;

	//用户余额
	private Double userMoney;

	//用户优惠券数量
	private Integer userYhq;

	//用户消费记录
	private Integer userXfnum; 

	//用户积分
	private Integer userJf;

	
	public User() {

	}

	
	
	public String getUserZjlx() {
		return userZjlx;
	}



	public void setUserZjlx(String userZjlx) {
		this.userZjlx = userZjlx;
	}



	public Long getUserId() {
		return userId;
	}

	public String getPasswdmd5() {
		return passwdmd5;
	}



	public void setPasswdmd5(String passwdmd5) {
		this.passwdmd5 = passwdmd5;
	}



	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getUserSex() {
		return userSex;
	}

	public void setUserSex(String userSex) {
		this.userSex = userSex;
	}

	public String getUserSfz() {
		return userSfz;
	}

	public void setUserSfz(String userSfz) {
		this.userSfz = userSfz;
	}

	public String getUserTel() {
		return userTel;
	}

	public void setUserTel(String userTel) {
		this.userTel = userTel;
	}

	public Integer getUserJz() {
		return userJz;
	}

	public void setUserJz(Integer userJz) {
		this.userJz = userJz;
	}

	public Object getUserType() {
		return userType;
	}

	public void setUserType(Object userType) {
		this.userType = userType;
	}

	public Object getUserZt() {
		return userZt;
	}

	public void setUserZt(Object userZt) {
		this.userZt = userZt;
	}

	public Double getUserMoney() {
		return userMoney;
	}

	public void setUserMoney(Double userMoney) {
		this.userMoney = userMoney;
	}

	public Integer getUserYhq() {
		return userYhq;
	}

	public void setUserYhq(Integer userYhq) {
		this.userYhq = userYhq;
	}

	public Integer getUserXfnum() {
		return userXfnum;
	}

	public void setUserXfnum(Integer userXfnum) {
		this.userXfnum = userXfnum;
	}

	public Integer getUserJf() {
		return userJf;
	}

	public void setUserJf(Integer userJf) {
		this.userJf = userJf;
	}



	@Override
	public String toString() {
		return "User [userId=" + userId + ", userName=" + userName + ", password=" + password + ", passwdmd5="
				+ passwdmd5 + ", userEmail=" + userEmail + ", userSex=" + userSex + ", userZjlx=" + userZjlx
				+ ", userSfz=" + userSfz + ", userTel=" + userTel + ", userJz=" + userJz + ", userType=" + userType
				+ ", userZt=" + userZt + ", userMoney=" + userMoney + ", userYhq=" + userYhq + ", userXfnum="
				+ userXfnum + ", userJf=" + userJf + "]";
	}








	
	
}
