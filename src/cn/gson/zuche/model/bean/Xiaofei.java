package cn.gson.zuche.model.bean;

public class Xiaofei {
	
	//消费记录表的订单号
	private String orderid;
	
	//消费方向
	private String orderxffx;
	
	//消费来源
	private String orderxfly;
	
	//消费金额
	private String ordermoney;
	
	//消费时间
	private Object orderxftime;

	public Xiaofei() {
		
	}

	public String getOrderid() {
		return orderid;
	}

	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}

	public String getOrderxffx() {
		return orderxffx;
	}

	public void setOrderxffx(String orderxffx) {
		this.orderxffx = orderxffx;
	}

	public String getOrderxfly() {
		return orderxfly;
	}

	public void setOrderxfly(String orderxfly) {
		this.orderxfly = orderxfly;
	}

	public String getOrdermoney() {
		return ordermoney;
	}

	public void setOrdermoney(String ordermoney) {
		this.ordermoney = ordermoney;
	}

	public Object getOrderxftime() {
		return orderxftime;
	}

	public void setOrderxftime(Object orderxftime) {
		this.orderxftime = orderxftime;
	}

	@Override
	public String toString() {
		return "Xiaofei [orderid=" + orderid + ", orderxffx=" + orderxffx + ", orderxfly=" + orderxfly + ", ordermoney="
				+ ordermoney + ", orderxftime=" + orderxftime + "]";
	}
	
}
