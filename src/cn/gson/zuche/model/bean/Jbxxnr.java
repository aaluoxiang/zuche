package cn.gson.zuche.model.bean;

public class Jbxxnr {
	private String webpoint;
	private String webdes;
	private String webcompanyname;
	private String webbq;
	private String webintro;
	private String webtel;
	private String webbt;
	public Jbxxnr(){}
	public String getWebpoint() {
		return webpoint;
	}
	public void setWebpoint(String webpoint) {
		this.webpoint = webpoint;
	}
	public String getWebdes() {
		return webdes;
	}
	public void setWebdes(String webdes) {
		this.webdes = webdes;
	}
	public String getWebcompanyname() {
		return webcompanyname;
	}
	public void setWebcompanyname(String webcompanyname) {
		this.webcompanyname = webcompanyname;
	}
	public String getWebbq() {
		return webbq;
	}
	public void setWebbq(String webbq) {
		this.webbq = webbq;
	}
	public String getWebintro() {
		return webintro;
	}
	public void setWebintro(String webintro) {
		this.webintro = webintro;
	}
	public String getWebtel() {
		return webtel;
	}
	public void setWebtel(String webtel) {
		this.webtel = webtel;
	}
	public String getWebbt() {
		return webbt;
	}
	public void setWebbt(String webbt) {
		this.webbt = webbt;
	}
	@Override
	public String toString() {
		return "Jbxxnr [webpoint=" + webpoint + ", webdes=" + webdes + ", webcompanyname=" + webcompanyname + ", webbq="
				+ webbq + ", webintro=" + webintro + ", webtel=" + webtel + ", webbt=" + webbt + "]";
	}
	
}
