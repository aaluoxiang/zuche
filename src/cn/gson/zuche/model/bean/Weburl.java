package cn.gson.zuche.model.bean;

public class Weburl {
private String weburldns;
private String weburlname;
private String weburlimg;
private Integer weburltypeid;
public Integer getWeburltypeid() {
	return weburltypeid;
}
public void setWeburltypeid(Integer weburltypeid) {
	this.weburltypeid = weburltypeid;
}
public Weburl(){
	
}
public String getWeburldns() {
	return weburldns;
}
public void setWeburldns(String weburldns) {
	this.weburldns = weburldns;
}
public String getWeburlname() {
	return weburlname;
}
public void setWeburlname(String weburlname) {
	this.weburlname = weburlname;
}
public String getWeburlimg() {
	return weburlimg;
}
public void setWeburlimg(String weburlimg) {
	this.weburlimg = weburlimg;
}
@Override
public String toString() {
	return "" + weburldns + "," + weburlname + "," + weburlimg
			+ "," + weburltypeid + "";
}
}
