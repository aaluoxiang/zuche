package cn.gson.zuche.model.bean;

public class Nianzongbiao {
private String data;
private String money;
public String getData() {
	return data;
}
public void setData(String data) {
	this.data = data;
}
public String getMoney() {
	return money;
}
public void setMoney(String money) {
	this.money = money;
}
@Override
public String toString() {
	return "Nianzongbiao [data=" + data + ", money=" + money + "]";
}

}
