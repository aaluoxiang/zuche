package cn.gson.zuche.model.bean;

public class Huizongbiao {
private String name;
private String money;
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getMoney() {
	return money;
}
public void setMoney(String money) {
	this.money = money;
}
@Override
public String toString() {
	return "{name:'" + name + "', data:[" + money + "]}";
}

}
