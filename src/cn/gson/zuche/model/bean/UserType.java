package cn.gson.zuche.model.bean;

public class UserType {
	
	private Long usertypeId;
	
	private String usertypename;
	
	private Double usertypecut;
	
	private String usertypeupjf;

	
	public UserType() {
		
	}

	public Long getUsertypeId() {
		return usertypeId;
	}

	public void setUsertypeId(Long usertypeId) {
		this.usertypeId = usertypeId;
	}

	public String getUsertypename() {
		return usertypename;
	}

	public void setUsertypename(String usertypename) {
		this.usertypename = usertypename;
	}

	public Double getUsertypecut() {
		return usertypecut;
	}

	public void setUsertypecut(Double usertypecut) {
		this.usertypecut = usertypecut;
	}

	public String getUsertypeupjf() {
		return usertypeupjf;
	}

	public void setUsertypeupjf(String usertypeupjf) {
		this.usertypeupjf = usertypeupjf;
	}

	@Override
	public String toString() {
		return "UserType [usertypeId=" + usertypeId + ", usertypename=" + usertypename + ", usertypecut=" + usertypecut
				+ ", usertypeupjf=" + usertypeupjf + "]";
	}
	
}
