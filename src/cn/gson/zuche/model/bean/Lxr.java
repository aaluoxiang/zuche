package cn.gson.zuche.model.bean;

public class Lxr {
	private Integer lxrid;
	private String  lxrname;
	private String lxremail;
	private String lxrsfz;
	private String lxrphone;
	private Integer lxruserid;
	
	public Lxr(){}
	
	
	public Integer getLxrid() {
		return lxrid;
	}


	public void setLxrid(Integer lxrid) {
		this.lxrid = lxrid;
	}


	public String getLxrname() {
		return lxrname;
	}


	public void setLxrname(String lxrname) {
		this.lxrname = lxrname;
	}


	public String getLxremail() {
		return lxremail;
	}


	public void setLxremail(String lxremail) {
		this.lxremail = lxremail;
	}


	public String getLxrsfz() {
		return lxrsfz;
	}


	public void setLxrsfz(String lxrsfz) {
		this.lxrsfz = lxrsfz;
	}


	public String getLxrphone() {
		return lxrphone;
	}


	public void setLxrphone(String lxrphone) {
		this.lxrphone = lxrphone;
	}


	public Integer getLxruserid() {
		return lxruserid;
	}


	public void setLxruserid(Integer lxruserid) {
		this.lxruserid = lxruserid;
	}


	@Override
	public String toString() {
		return "Lxr [lxrid=" + lxrid + ", lxrname=" + lxrname + ", lxremail=" + lxremail + ", lxrsfz=" + lxrsfz
				+ ", lxrphone=" + lxrphone + ", lxruserid=" + lxruserid + "]";
	}
	
	

}
