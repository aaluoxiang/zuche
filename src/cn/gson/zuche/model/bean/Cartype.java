package cn.gson.zuche.model.bean;

public class Cartype {
		private Integer carTypeId;
		private String carTypeName;
		private String carPrice;
		private Integer carSeatNum;
		private Integer carDoorNum;
		private String carOil;
		private String  carBsx;
		private String  carPail;
		private String  carDrive;
		private String  carOilBox;
		private String  carGps;
		private String  carSkyWindow;
		private String  carGasbag;
		private String  carBackld;
		private String  carImageUrl;
		private String  carPinpai;
		private String  carXinxi;
		private String  carXinche;
		private String  carRemen;
		private Integer carXiaoliang;
		private Integer carZtId;
		private String  carZtName;
		private Integer carZtIds;
		private Integer carTypeIds;
		private String num;
		private String carlicenseid;
		private String cardes;
		private String carkucun;
		private String  carDv;
		private String dindan;
		public String getDindan() {
			return dindan;
		}
		public void setDindan(String dindan) {
			this.dindan = dindan;
		}
		public Integer getCarTypeId() {
			return carTypeId;
		}
		public void setCarTypeId(Integer carTypeId) {
			this.carTypeId = carTypeId;
		}
		public String getCarTypeName() {
			return carTypeName;
		}
		public void setCarTypeName(String carTypeName) {
			this.carTypeName = carTypeName;
		}
		public String getCarPrice() {
			return carPrice;
		}
		public void setCarPrice(String carPrice) {
			this.carPrice = carPrice;
		}
		public Integer getCarSeatNum() {
			return carSeatNum;
		}
		public void setCarSeatNum(Integer carSeatNum) {
			this.carSeatNum = carSeatNum;
		}
		public Integer getCarDoorNum() {
			return carDoorNum;
		}
		public void setCarDoorNum(Integer carDoorNum) {
			this.carDoorNum = carDoorNum;
		}
		public String getCarOil() {
			return carOil;
		}
		public void setCarOil(String carOil) {
			this.carOil = carOil;
		}
		public String getCarBsx() {
			return carBsx;
		}
		public void setCarBsx(String carBsx) {
			this.carBsx = carBsx;
		}
		public String getCarPail() {
			return carPail;
			
		}
		public void setCarPail(String carPail) {
			this.carPail = carPail;
		}
		public String getCarDrive() {
			return carDrive;
		}
		public void setCarDrive(String carDrive) {
			this.carDrive = carDrive;
		}
		public String getCarOilBox() {
			return carOilBox;
		}
		public void setCarOilBox(String carOilBox) {
			this.carOilBox = carOilBox;
		}
		public String getCarGps() {
			return carGps;
		}
		public void setCarGps(String carGps) {
			this.carGps = carGps;
		}
		public String getCarSkyWindow() {
			return carSkyWindow;
		}
		public void setCarSkyWindow(String carSkyWindow) {
			this.carSkyWindow = carSkyWindow;
		}
		public String getCarGasbag() {
			return carGasbag;
		}
		public void setCarGasbag(String carGasbag) {
			this.carGasbag = carGasbag;
		}
		public String getCarBackld() {
			return carBackld;
		}
		public void setCarBackld(String carBackld) {
			this.carBackld = carBackld;
		}
		public String getCarImageUrl() {
			return carImageUrl;
		}
		public void setCarImageUrl(String carImageUrl) {
			this.carImageUrl = carImageUrl;
		}
		public String getCarPinpai() {
			return carPinpai;
		}
		public void setCarPinpai(String carPinpai) {
			this.carPinpai = carPinpai;
		}
		public String getCarXinxi() {
			return carXinxi;
		}
		public void setCarXinxi(String carXinxi) {
			this.carXinxi = carXinxi;
		}
		public String getCarXinche() {
			return carXinche;
		}
		public void setCarXinche(String carXinche) {
			this.carXinche = carXinche;
		}
		public String getCarRemen() {
			return carRemen;
		}
		public void setCarRemen(String carRemen) {
			this.carRemen = carRemen;
		}
		public Integer getCarXiaoliang() {
			return carXiaoliang;
		}
		public void setCarXiaoliang(Integer carXiaoliang) {
			this.carXiaoliang = carXiaoliang;
		}
		public Integer getCarZtId() {
			return carZtId;
		}
		public void setCarZtId(Integer carZtId) {
			this.carZtId = carZtId;
		}
		public String getCarZtName() {
			return carZtName;
		}
		public void setCarZtName(String carZtName) {
			this.carZtName = carZtName;
		}
		public Integer getCarZtIds() {
			return carZtIds;
		}
		public void setCarZtIds(Integer carZtIds) {
			this.carZtIds = carZtIds;
		}
		public Integer getCarTypeIds() {
			return carTypeIds;
		}
		public void setCarTypeIds(Integer carTypeIds) {
			this.carTypeIds = carTypeIds;
		}
		public String getNum() {
			return num;
		}
		public void setNum(String num) {
			this.num = num;
		}
		public String getCarlicenseid() {
			return carlicenseid;
		}
		public void setCarlicenseid(String carlicenseid) {
			this.carlicenseid = carlicenseid;
		}
		public String getCardes() {
			return cardes;
		}
		public void setCardes(String cardes) {
			this.cardes = cardes;
		}
		public String getCarkucun() {
			return carkucun;
		}
		public void setCarkucun(String carkucun) {
			this.carkucun = carkucun;
		}
		public String getCarDv() {
			return carDv;
		}
		public void setCarDv(String carDv) {
			this.carDv = carDv;
		}
		@Override
		public String toString() {
			return "Cartype [carTypeId=" + carTypeId + ", carTypeName=" + carTypeName + ", carPrice=" + carPrice
					+ ", carSeatNum=" + carSeatNum + ", carDoorNum=" + carDoorNum + ", carOil=" + carOil + ", carBsx="
					+ carBsx + ", carPail=" + carPail + ", carDrive=" + carDrive + ", carOilBox=" + carOilBox
					+ ", carGps=" + carGps + ", carSkyWindow=" + carSkyWindow + ", carGasbag=" + carGasbag
					+ ", carBackld=" + carBackld + ", carImageUrl=" + carImageUrl + ", carPinpai=" + carPinpai
					+ ", carXinxi=" + carXinxi + ", carXinche=" + carXinche + ", carRemen=" + carRemen
					+ ", carXiaoliang=" + carXiaoliang + ", carZtId=" + carZtId + ", carZtName=" + carZtName
					+ ", carZtIds=" + carZtIds + ", carTypeIds=" + carTypeIds + ", num=" + num + ", carlicenseid="
					+ carlicenseid + ", cardes=" + cardes + ", carkucun=" + carkucun + ", carDv=" + carDv + ", dindan="
					+ dindan + "]";
		}
		




		
		
	
		
}