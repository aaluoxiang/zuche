package cn.gson.zuche.model.bean;

public class Userorder {
	private String orderid;
	private String userorderzt;
	private String odermoney;
	private String usertel;
	private String usersfz;
	private String userid;
	private Object gettime;
	private	Object backtime;
	private String ordergetaddress;
	private String orderbackaddress;
	private String orderdriverid;
	private String ordercarid;
	private String orderstoreid;
	private Integer orderyhqid;
	private Integer ordercommentid;
	private Integer orderdelayid;
	private String ordercartype;
	private String price;
	private String ordercarsiji;
	private String ordermianpei;
	private Object ordersingletime;
	private String orderlxrname;
	private String orderlxrphone;
	private String orderfp;

	
	public Userorder(){}

	public String getOrderid() {
		return orderid;
	}

	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}

	public String getUserorderzt() {
		return userorderzt;
	}

	public void setUserorderzt(String userorderzt) {
		this.userorderzt = userorderzt;
	}

	public String getOdermoney() {
		return odermoney;
	}

	public void setOdermoney(String odermoney) {
		this.odermoney = odermoney;
	}

	public String getUsertel() {
		return usertel;
	}

	public void setUsertel(String usertel) {
		this.usertel = usertel;
	}

	public String getUsersfz() {
		return usersfz;
	}

	public void setUsersfz(String usersfz) {
		this.usersfz = usersfz;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public Object getGettime() {
		return gettime;
	}

	public void setGettime(Object gettime) {
		this.gettime = gettime;
	}

	public Object getBacktime() {
		return backtime;
	}

	public void setBacktime(Object backtime) {
		this.backtime = backtime;
	}

	public String getOrdergetaddress() {
		return ordergetaddress;
	}

	public void setOrdergetaddress(String ordergetaddress) {
		this.ordergetaddress = ordergetaddress;
	}

	public String getOrderbackaddress() {
		return orderbackaddress;
	}

	public void setOrderbackaddress(String orderbackaddress) {
		this.orderbackaddress = orderbackaddress;
	}

	public String getOrderdriverid() {
		return orderdriverid;
	}

	public void setOrderdriverid(String orderdriverid) {
		this.orderdriverid = orderdriverid;
	}

	public String getOrdercarid() {
		return ordercarid;
	}

	public void setOrdercarid(String ordercarid) {
		this.ordercarid = ordercarid;
	}

	public String getOrderstoreid() {
		return orderstoreid;
	}

	public void setOrderstoreid(String orderstoreid) {
		this.orderstoreid = orderstoreid;
	}

	public Integer getOrderyhqid() {
		return orderyhqid;
	}

	public void setOrderyhqid(Integer orderyhqid) {
		this.orderyhqid = orderyhqid;
	}

	public Integer getOrdercommentid() {
		return ordercommentid;
	}

	public void setOrdercommentid(Integer ordercommentid) {
		this.ordercommentid = ordercommentid;
	}

	public Integer getOrderdelayid() {
		return orderdelayid;
	}

	public void setOrderdelayid(Integer orderdelayid) {
		this.orderdelayid = orderdelayid;
	}

	public String getOrdercartype() {
		return ordercartype;
	}

	public void setOrdercartype(String ordercartype) {
		this.ordercartype = ordercartype;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getOrdercarsiji() {
		return ordercarsiji;
	}

	public void setOrdercarsiji(String ordercarsiji) {
		this.ordercarsiji = ordercarsiji;
	}

	public String getOrdermianpei() {
		return ordermianpei;
	}

	public void setOrdermianpei(String ordermianpei) {
		this.ordermianpei = ordermianpei;
	}

	public Object getOrdersingletime() {
		return ordersingletime;
	}

	public void setOrdersingletime(Object ordersingletime) {
		this.ordersingletime = ordersingletime;
	}

	public String getOrderlxrname() {
		return orderlxrname;
	}

	public void setOrderlxrname(String orderlxrname) {
		this.orderlxrname = orderlxrname;
	}

	public String getOrderlxrphone() {
		return orderlxrphone;
	}

	public void setOrderlxrphone(String orderlxrphone) {
		this.orderlxrphone = orderlxrphone;
	}

	public String getOrderfp() {
		return orderfp;
	}

	public void setOrderfp(String orderfp) {
		this.orderfp = orderfp;
	}


	public Userorder(String orderid, String userorderzt, String odermoney, String usertel, String usersfz,
			String userid, Object gettime, Object backtime, String ordergetaddress, String orderbackaddress,
			String orderdriverid, String ordercarid, String orderstoreid, Integer orderyhqid, Integer ordercommentid,
			Integer orderdelayid, String ordercartype, String price, String ordercarsiji, String ordermianpei,
			Object ordersingletime, String orderlxrname, String orderlxrphone, String orderfp) {
		super();
		this.orderid = orderid;
		this.userorderzt = userorderzt;
		this.odermoney = odermoney;
		this.usertel = usertel;
		this.usersfz = usersfz;
		this.userid = userid;
		this.gettime = gettime;
		this.backtime = backtime;
		this.ordergetaddress = ordergetaddress;
		this.orderbackaddress = orderbackaddress;
		this.orderdriverid = orderdriverid;
		this.ordercarid = ordercarid;
		this.orderstoreid = orderstoreid;
		this.orderyhqid = orderyhqid;
		this.ordercommentid = ordercommentid;
		this.orderdelayid = orderdelayid;
		this.ordercartype = ordercartype;
		this.price = price;
		this.ordercarsiji = ordercarsiji;
		this.ordermianpei = ordermianpei;
		this.ordersingletime = ordersingletime;
		this.orderlxrname = orderlxrname;
		this.orderlxrphone = orderlxrphone;
		this.orderfp = orderfp;
		
	}

	@Override
	public String toString() {
		return "Userorder [orderid=" + orderid + ", userorderzt=" + userorderzt + ", odermoney=" + odermoney
				+ ", usertel=" + usertel + ", usersfz=" + usersfz + ", userid=" + userid + ", gettime=" + gettime
				+ ", backtime=" + backtime + ", ordergetaddress=" + ordergetaddress + ", orderbackaddress="
				+ orderbackaddress + ", orderdriverid=" + orderdriverid + ", ordercarid=" + ordercarid
				+ ", orderstoreid=" + orderstoreid + ", orderyhqid=" + orderyhqid + ", ordercommentid=" + ordercommentid
				+ ", orderdelayid=" + orderdelayid + ", ordercartype=" + ordercartype + ", price=" + price
				+ ", ordercarsiji=" + ordercarsiji + ", ordermianpei=" + ordermianpei + ", ordersingletime="
				+ ordersingletime + ", orderlxrname=" + orderlxrname + ", orderlxrphone=" + orderlxrphone + ", orderfp="
				+ orderfp + "]";
	}

	
	
	
}