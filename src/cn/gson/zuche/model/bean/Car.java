package cn.gson.zuche.model.bean;

public class Car {
	private String carlicenseid;
	private String cartypeids;
	private String carstoreid;
	private String carztids;
	private Integer num;

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public String getCarlicenseid() {
		return carlicenseid;
	}

	public void setCarlicenseid(String carlicenseid) {
		this.carlicenseid = carlicenseid;
	}

	public String getCartypeids() {
		return cartypeids;
	}

	public void setCartypeids(String cartypeids) {
		this.cartypeids = cartypeids;
	}

	public String getCarstoreid() {
		return carstoreid;
	}

	public void setCarstoreid(String carstoreid) {
		this.carstoreid = carstoreid;
	}

	public String getCarztids() {
		return carztids;
	}

	public void setCarztids(String carztids) {
		this.carztids = carztids;
	}

	@Override
	public String toString() {
		return "Car [carlicenseid=" + carlicenseid + ", cartypeids=" + cartypeids + ", carstoreid=" + carstoreid
				+ ", carztids=" + carztids + ", num=" + num + "]";
	}

}
