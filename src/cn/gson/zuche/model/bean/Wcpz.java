package cn.gson.zuche.model.bean;

/**
 * 订单完成凭证实体类
 * 
 * @author huanxu
 *
 */
public class Wcpz {
	// 完成凭证表主键id
	private String wcpzid;

	// 车辆损坏等级
	private String damagelevel;

	// 维修费用
	private String maintainmoney;

	// 订单号
	private String orderid;

	// 退还定金数目
	private String quityjmoney;

	public Wcpz() {

	}

	public String getWcpzid() {
		return wcpzid;
	}

	public void setWcpzid(String wcpzid) {
		this.wcpzid = wcpzid;
	}

	public String getDamagelevel() {
		return damagelevel;
	}

	public void setDamagelevel(String damagelevel) {
		this.damagelevel = damagelevel;
	}

	public String getMaintainmoney() {
		return maintainmoney;
	}

	public void setMaintainmoney(String maintainmoney) {
		this.maintainmoney = maintainmoney;
	}

	public String getOrderid() {
		return orderid;
	}

	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}

	public String getQuityjmoney() {
		return quityjmoney;
	}

	public void setQuityjmoney(String quityjmoney) {
		this.quityjmoney = quityjmoney;
	}

	@Override
	public String toString() {
		return "Wcpz [wcpzid=" + wcpzid + ", damagelevel=" + damagelevel + ", maintainmoney=" + maintainmoney
				+ ", orderid=" + orderid + ", quityjmoney=" + quityjmoney + "]";
	}
	
}
