package cn.gson.zuche.model.bean;

public class Systemmk {

private Integer websonid;
private String webmodelname;
private String webmodeltype;
private String webmodelurl;
private String webmodelpaixu;
private Integer webfatherid;
public Systemmk(){
	
}

public Systemmk(Integer websonid, String webmodelname, String webmodelurl,
		Integer webfatherid) {
	
	this.websonid = websonid;
	this.webmodelname = webmodelname;
	this.webmodelurl = webmodelurl;
	this.webfatherid = webfatherid;
}

public Integer getWebsonid() {
	return websonid;
}
public void setWebsonid(Integer websonid) {
	this.websonid = websonid;
}
public String getWebmodelname() {
	return webmodelname;
}
public void setWebmodelname(String webmodelname) {
	this.webmodelname = webmodelname;
}
public String getWebmodeltype() {
	return webmodeltype;
}
public void setWebmodeltype(String webmodeltype) {
	this.webmodeltype = webmodeltype;
}
public String getWebmodelurl() {
	return webmodelurl;
}
public void setWebmodelurl(String webmodelurl) {
	this.webmodelurl = webmodelurl;
}
public String getWebmodelpaixu() {
	return webmodelpaixu;
}
public void setWebmodelpaixu(String webmodelpaixu) {
	this.webmodelpaixu = webmodelpaixu;
}
public Integer getWebfatherid() {
	return webfatherid;
}
public void setWebfatherid(Integer webfatherid) {
	this.webfatherid = webfatherid;
}

@Override
public String toString() {
	return "Systemmk [websonid=" + websonid
			+ ", webmodelname=" + webmodelname + ", webmodeltype=" + webmodeltype + ", webmodelurl=" + webmodelurl
			+ ", webmodelpaixu=" + webmodelpaixu + ", webfatherid=" + webfatherid + "]";
}

}
