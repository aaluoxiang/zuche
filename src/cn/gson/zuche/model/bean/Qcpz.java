package cn.gson.zuche.model.bean;

/**
 * 取车凭证 实体类
 * 
 * @author huanxu
 *
 */
public class Qcpz {

	// 取车凭证表里面的主键id
	private String qcpzid;

	// 取车人姓名
	private String lxrname;

	// 取车人驾照编号
	private String jznumber;

	// 订单号
	private String orderid;

	// 取车人驾照复印件图片地址
	private String jzpicture;

	// 所交押金
	private String yjmoney;

	public Qcpz() {
	}

	public String getQcpzid() {
		return qcpzid;
	}

	public void setQcpzid(String qcpzid) {
		this.qcpzid = qcpzid;
	}

	public String getLxrname() {
		return lxrname;
	}

	public void setLxrname(String lxrname) {
		this.lxrname = lxrname;
	}

	public String getJznumber() {
		return jznumber;
	}

	public void setJznumber(String jznumber) {
		this.jznumber = jznumber;
	}

	public String getOrderid() {
		return orderid;
	}

	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}

	public String getJzpicture() {
		return jzpicture;
	}

	public void setJzpicture(String jzpicture) {
		this.jzpicture = jzpicture;
	}

	public String getYjmoney() {
		return yjmoney;
	}

	public void setYjmoney(String yjmoney) {
		this.yjmoney = yjmoney;
	}

	@Override
	public String toString() {
		return "Qcpz [qcpzid=" + qcpzid + ", lxrname=" + lxrname + ", jznumber=" + jznumber + ", orderid=" + orderid
				+ ", jzpicture=" + jzpicture + ", yjmoney=" + yjmoney + "]";
	}
	
	
}
