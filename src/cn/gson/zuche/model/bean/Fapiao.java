package cn.gson.zuche.model.bean;

public class Fapiao {
	//发票id
	private Integer fpid;
	//发票抬头
	private String fptt;
	//发票的收票人
	private String fpuser;
	//发票电话
	private String fpphone;
	//发票邮编
	private String fpyb;
	//发票地址
	private String fpaddress;
	//发票连接用户表的外键
	private Integer fpuserid;
	
	public Fapiao(){}
	
	public Integer getFpid() {
		return fpid;
	}
	public void setFpid(Integer fpid) {
		this.fpid = fpid;
	}
	public String getFptt() {
		return fptt;
	}
	public void setFptt(String fptt) {
		this.fptt = fptt;
	}
	public String getFpuser() {
		return fpuser;
	}
	public void setFpuser(String fpuser) {
		this.fpuser = fpuser;
	}
	public String getFpphone() {
		return fpphone;
	}
	public void setFpphone(String fpphone) {
		this.fpphone = fpphone;
	}
	public String getFpyb() {
		return fpyb;
	}
	public void setFpyb(String fpyb) {
		this.fpyb = fpyb;
	}
	public String getFpaddress() {
		return fpaddress;
	}
	public void setFpaddress(String fpaddress) {
		this.fpaddress = fpaddress;
	}
	public Integer getFpuserid() {
		return fpuserid;
	}
	public void setFpuserid(Integer fpuserid) {
		this.fpuserid = fpuserid;
	}
	public Fapiao(Integer fpid, String fptt, String fpuser, String fpphone, String fpyb, String fpaddress,
			Integer fpuserid) {
		super();
		this.fpid = fpid;
		this.fptt = fptt;
		this.fpuser = fpuser;
		this.fpphone = fpphone;
		this.fpyb = fpyb;
		this.fpaddress = fpaddress;
		this.fpuserid = fpuserid;
	}
	@Override
	public String toString() {
		return "Fapiao [fpid=" + fpid + ", fptt=" + fptt + ", fpuser=" + fpuser + ", fpphone=" + fpphone + ", fpyb="
				+ fpyb + ", fpaddress=" + fpaddress + ", fpuserid=" + fpuserid + "]";
	}
	
	
	

}
