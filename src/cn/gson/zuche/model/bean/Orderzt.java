package cn.gson.zuche.model.bean;

public class Orderzt {
   private Integer orderztid;
   private String orderztname;
   
   public Orderzt(){}
   
public Integer getOrderztid() {
	return orderztid;
}
public void setOrderztid(Integer orderztid) {
	this.orderztid = orderztid;
}
public String getOrderztname() {
	return orderztname;
}
public void setOrderztname(String orderztname) {
	this.orderztname = orderztname;
}
@Override
public String toString() {
	return "Orderzt [orderztid=" + orderztid + ", orderztname=" + orderztname + "]";
}
public Orderzt(Integer orderztid, String orderztname) {
	super();
	this.orderztid = orderztid;
	this.orderztname = orderztname;
}
   
   
}
