package cn.gson.zuche.model.bean;

public class Ordercomment {

	private String commentid;

	private String commentcon;

	private Object commenttime;

	private String commentdriver;

	private String userodid;

	private String userid;

	private String commenttype;
	
	private String commentcar;

	public Ordercomment() {

	}

	public String getCommentcar() {
		return commentcar;
	}

	public void setCommentcar(String commentcar) {
		this.commentcar = commentcar;
	}

	public String getCommentid() {
		return commentid;
	}

	public void setCommentid(String commentid) {
		this.commentid = commentid;
	}

	public String getCommentcon() {
		return commentcon;
	}

	public void setCommentcon(String commentcon) {
		this.commentcon = commentcon;
	}

	public Object getCommenttime() {
		return commenttime;
	}

	public void setCommenttime(Object commenttime) {
		this.commenttime = commenttime;
	}

	public String getCommentdriver() {
		return commentdriver;
	}

	public void setCommentdriver(String commentdriver) {
		this.commentdriver = commentdriver;
	}

	public String getUserodid() {
		return userodid;
	}

	public void setUserodid(String userodid) {
		this.userodid = userodid;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getCommenttype() {
		return commenttype;
	}

	public void setCommenttype(String commenttype) {
		this.commenttype = commenttype;
	}

	@Override
	public String toString() {
		return "Ordercomment [commentid=" + commentid + ", commentcon=" + commentcon + ", commenttime=" + commenttime
				+ ", commentdriver=" + commentdriver + ", userodid=" + userodid + ", userid=" + userid
				+ ", commenttype=" + commenttype + ", commentcar=" + commentcar + "]";
	}

	

}
