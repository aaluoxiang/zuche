package cn.gson.zuche.model.bean;

public class Count {
		//计数
	private Integer counter;
	
	public Count(){}

	public Integer getCounter() {
		return counter;
	}

	public void setCounter(Integer counter) {
		this.counter = counter;
	}

	public Count(Integer counter) {
		super();
		this.counter = counter;
	}

	@Override
	public String toString() {
		return "Counter [counter=" + counter + "]";
	}
	
	
}
