package cn.gson.zuche.model.bean;

public class UserYhq {
	
	//用户 user_yhq_user 中user优惠券的id
	private String useryhqid;
	
	//用户id
	private Long userid;
	
	//优惠券库中的优惠券id
	private Integer yhqiduser;
	
	//用户拥有此类优惠券数量
	private Integer yhqnumber;
	
	//优惠券状态
	private String yhqzt;
	
	//优惠券库中的优惠券id
	private Integer yhqid;
	
	//优惠券库中yhq_type 优惠金额
	private Integer yhqtype;
	
	//优惠券库中的优惠券说明
	private String yhqsm;
	
	//优惠券过期时间
	private Object outtime;

	public UserYhq() {
		
	}

	public String getUseryhqid() {
		return useryhqid;
	}

	public void setUseryhqid(String useryhqid) {
		this.useryhqid = useryhqid;
	}

	public Long getUserid() {
		return userid;
	}

	public void setUserid(Long userid) {
		this.userid = userid;
	}

	public Integer getYhqiduser() {
		return yhqiduser;
	}

	public void setYhqiduser(Integer yhqiduser) {
		this.yhqiduser = yhqiduser;
	}

	public Integer getYhqnumber() {
		return yhqnumber;
	}

	public void setYhqnumber(Integer yhqnumber) {
		this.yhqnumber = yhqnumber;
	}

	public String getYhqzt() {
		return yhqzt;
	}

	public void setYhqzt(String yhqzt) {
		this.yhqzt = yhqzt;
	}

	public Integer getYhqid() {
		return yhqid;
	}

	public void setYhqid(Integer yhqid) {
		this.yhqid = yhqid;
	}

	public Integer getYhqtype() {
		return yhqtype;
	}

	public void setYhqtype(Integer yhqtype) {
		this.yhqtype = yhqtype;
	}

	public String getYhqsm() {
		return yhqsm;
	}

	public void setYhqsm(String yhqsm) {
		this.yhqsm = yhqsm;
	}

	public Object getOuttime() {
		return outtime;
	}

	public void setOuttime(Object outtime) {
		this.outtime = outtime;
	}

	@Override
	public String toString() {
		return "UserYhq [useryhqid=" + useryhqid + ", userid=" + userid + ", yhqiduser=" + yhqiduser + ", yhqnumber="
				+ yhqnumber + ", yhqzt=" + yhqzt + ", yhqid=" + yhqid + ", yhqtype=" + yhqtype + ", yhqsm=" + yhqsm
				+ ", outtime=" + outtime + "]";
	}
}
