package cn.gson.zuche.model.bean;

public class Userorderzt {
private String orderztid;
private String orderztname;
public Userorderzt(){
	
}
public String getOrderztid() {
	return orderztid;
}
public void setOrderztid(String orderztid) {
	this.orderztid = orderztid;
}
public String getOrderztname() {
	return orderztname;
}
public void setOrderztname(String orderztname) {
	this.orderztname = orderztname;
}
@Override
public String toString() {
	return "Userorderzt [orderztid=" + orderztid + ", orderztname=" + orderztname + "]";
}
}
