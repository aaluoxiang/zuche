package cn.gson.zuche.controller.admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;

import cn.gson.zuche.model.bean.Qcpz;
import cn.gson.zuche.model.dao.Qcpzdao;

/**
 * Servlet implementation class Chakanpzxx
 */
@WebServlet("/chakanpzxx")
public class Chakanpzxx extends HttpServlet {
	
private Qcpzdao dao=new Qcpzdao();
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id=request.getParameter("orderid");
		try {
			List<Qcpz> qcpz=dao.selectbyid(id);
			PrintWriter out=response.getWriter();
			String json=JSON.toJSONString(qcpz);
			out.print(json);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
