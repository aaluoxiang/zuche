package cn.gson.zuche.controller.admin;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import cn.gson.zuche.model.bean.Weburl;
import cn.gson.zuche.model.dao.Weburldao;

/**
 * Servlet implementation class tianjiaSeverlet
 */
@WebServlet({ "/tianjia", "/tianjia/*" })
@MultipartConfig(maxFileSize = 10 * 1024 * 1024)
public class tianjiaSeverlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
			String value = null;
		if (null != request.getParameter("value")) {
			String[] xiugaizhi = request.getParameter("value").split(",");
			request.setAttribute("webbtt", xiugaizhi[1]);
			request.setAttribute("weburll", xiugaizhi[0]);
			request.setAttribute("webimgg", xiugaizhi[2]);
			request.setAttribute("weburltypeid", xiugaizhi[3]);
		}
		request.getRequestDispatcher("/WEB-INF/jsp/adminjsp/tianjia.jsp").forward(request, response);
	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Weburl url = new Weburl();
		Weburldao dao = new Weburldao();
		Uploadpohto up=new Uploadpohto();
		req.setCharacterEncoding("UTF-8");
		resp.setContentType("text/html; charset=UTF-8");
		Part part = req.getPart("file");
		try {
			String newFileName=up.updaload(part, resp);
			url.setWeburlimg(newFileName);
		} catch (Exception e) {
			url.setWeburlimg(req.getParameter("aa"));			
		}
		url.setWeburldns(req.getParameter("weburl"));
		url.setWeburlname(req.getParameter("webbt"));
		String aa=null;
		aa=req.getParameter("aa");
		String id=null;		
		id=req.getParameter("id");
		try {	
			if(null!=aa&&null!=id){
				dao.sava(url);	
			}else{		
				
			}	
		} catch (SQLException e) {
			e.printStackTrace();
		}
		doGet(req, resp);
	}
}
