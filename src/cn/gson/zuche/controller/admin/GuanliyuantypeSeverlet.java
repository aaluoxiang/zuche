package cn.gson.zuche.controller.admin;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.gson.zuche.model.bean.Employeetype;
import cn.gson.zuche.model.bean.Guanliyuantype;
import cn.gson.zuche.model.bean.Systemmk;
import cn.gson.zuche.model.dao.Employeetypedao;
import cn.gson.zuche.model.dao.Guanliyuantypedao;
import cn.gson.zuche.model.dao.Systemmkdao;

/**
 * Servlet implementation class GuanliyuantypeSeverlet
 */
@WebServlet("/guanliyuantype")
public class GuanliyuantypeSeverlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private Systemmkdao dao=new Systemmkdao();
    private Employeetypedao tdao=new Employeetypedao();
    private Guanliyuantype guanliyuan=new Guanliyuantype();
    private Guanliyuantypedao gdao=new Guanliyuantypedao();
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		String type=null;
		type=request.getParameter("type");		
		if(type!=null&&!"".equals(type)){			
			try {
				List<Employeetype> t=tdao.findid(type);
				String typeid=t.get(0).getEmployeetypeid();
				gdao.delete(typeid);
				String[] id=request.getParameter("id").split(",");
				for (int i = 0; i < id.length; i++) {
				guanliyuan.setEmployee_type(typeid);
				guanliyuan.setWeb_system_id(id[i]);
				gdao.insert(guanliyuan);
				}			
			} catch (SQLException e){
				e.printStackTrace();
			}		
		}
		try {
			List<Systemmk> list=dao.findall();
			request.setAttribute("list", list);
			List<Employeetype> tlist=tdao.select();
			request.setAttribute("tlist", tlist);
		} catch (SQLException e) {			
			e.printStackTrace();
		}
		request.getRequestDispatcher("WEB-INF/jsp/adminjsp/guanliyuantype.jsp").forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		doGet(request, response);
	}

}
