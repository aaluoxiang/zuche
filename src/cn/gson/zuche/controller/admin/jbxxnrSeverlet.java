package cn.gson.zuche.controller.admin;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cn.gson.zuche.model.bean.Jbxxnr;
import cn.gson.zuche.model.dao.Jbxxnrdao;

/**
 * Servlet implementation class jbxxnrSeverlet
 */
@WebServlet("/jbxxnr")
public class jbxxnrSeverlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
  
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.removeAttribute("dd");
		HttpSession session = request.getSession();
		Jbxxnrdao dao = new Jbxxnrdao();
		request.setAttribute("dd", "jbxxnr");
		try {
			List<Jbxxnr> list = dao.findall();
			request.setAttribute("Webbq", list.get(0).getWebbq());
			request.setAttribute("Webbt", list.get(0).getWebbt());
			request.setAttribute("Webcompanyname", list.get(0).getWebcompanyname());
			request.setAttribute("Webdes", list.get(0).getWebdes());
			request.setAttribute("Webintro", list.get(0).getWebintro());
			request.setAttribute("Webpoint", list.get(0).getWebpoint());
			request.setAttribute("Webtel", list.get(0).getWebtel());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		request.getRequestDispatcher("/WEB-INF/jsp/adminjsp/jbxxnr.jsp").forward(request, response);

	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		Jbxxnrdao dao=new Jbxxnrdao();
				
		
		List list=new ArrayList<>();
		list.add(request.getParameter("webpoint"));
		list.add(request.getParameter("webdes"));
		list.add(request.getParameter("webcompanyname"));
		list.add(request.getParameter("webbq"));
		list.add(request.getParameter("webintro"));
		list.add(request.getParameter("webtel"));
		list.add(request.getParameter("webbt"));

		try {
			if(dao.sava(list)){
				request.setAttribute("alert","true");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		doGet(request, response);
	}

}
