package cn.gson.zuche.controller.admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONArray;

import cn.gson.zuche.model.bean.Chozhi;
import cn.gson.zuche.model.dao.Chozhidao;

/**
 * Servlet implementation class ChaxunjiluSeverlet
 */
@WebServlet("/chaxunjilu")
public class ChaxunjiluSeverlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Chozhidao dao=new Chozhidao();
    
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		String begain=request.getParameter("begain");
		String down=request.getParameter("down");
		try {
			List<Chozhi> list=dao.selectbydata(begain, down);
			PrintWriter out = response.getWriter();
			String jsonString = JSONArray.toJSONString(list);
			out.print(jsonString);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
