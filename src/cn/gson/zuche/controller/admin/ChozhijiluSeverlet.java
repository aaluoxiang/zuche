package cn.gson.zuche.controller.admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import java.util.StringJoiner;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONArray;

import cn.gson.zuche.model.bean.Chozhi;
import cn.gson.zuche.model.dao.Chozhidao;

/**
 * Servlet implementation class ChozhijiluSeverlet
 */
@WebServlet("/chozhijilu")
public class ChozhijiluSeverlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
   private Chozhidao dao=new Chozhidao();

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		String tel=request.getParameter("tel");
		try {
			List<Chozhi> list=dao.select(tel);
			PrintWriter out=response.getWriter();
			String jsonstring=JSONArray.toJSONString(list);
			out.print(jsonstring);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
