package cn.gson.zuche.controller.admin;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class AdminLoginoutSeverlet
 */
@WebServlet("/adminloginout")
public class AdminLoginoutSeverlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getSession().invalidate();
	/*	HttpSession session=request.getSession();
		session.removeAttribute("zhanghao");*/
		response.sendRedirect("adminlogin");
	}

}
