package cn.gson.zuche.controller.admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.alibaba.fastjson.JSONArray;

import cn.gson.zuche.model.bean.Useroodd;
import cn.gson.zuche.model.bean.Userorderzt;
import cn.gson.zuche.model.dao.Useroodddao;
import cn.gson.zuche.model.dao.Userorderztdao;

/**
 * Servlet implementation class Dindantongjichaxun
 */
@WebServlet("/dindantongjichaxun")
public class Dindantongjichaxun extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Useroodddao dddao=new Useroodddao();
	private Userorderztdao ztdao=new Userorderztdao();
	private Useroodddao oodao = new Useroodddao();
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		HttpSession session=request.getSession();
		
		String begin=request.getParameter("begin");	
		String down=request.getParameter("down");	
		try {
			List<Userorderzt> zt = ztdao.select();
			List<Useroodd> list=new ArrayList<Useroodd>();
			for (int i = 0; i < zt.size(); i++) {			
				List<Useroodd> l=dddao.chaxunbytime(begin, down,zt.get(i).getOrderztid());									
				list.add(new Useroodd(l.get(0).getName(),l.get(0).getNum()));
			}	
			session.setAttribute("jj",list);
			//List<Useroodd> l=dddao.chaxunbytime(begin, down,"3");	
			PrintWriter out=response.getWriter();
			String jsonstring=JSONArray.toJSONString(list);		
			out.print(jsonstring);
			
		} catch (SQLException e) {			
			e.printStackTrace();
		}
		
		
		
	}

}
