package cn.gson.zuche.controller.admin;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.gson.zuche.model.bean.Employee;
import cn.gson.zuche.model.dao.Employeedao;

/**
 * Servlet implementation class tianjiayuangonSeverlet
 */
@WebServlet("/tianjiayuangon")
public class tianjiayuangonSeverlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    private Employeedao dao=new Employeedao();
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		request.getRequestDispatcher("/WEB-INF/jsp/adminjsp/tianjiayuangon.jsp").forward(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Employee em=new Employee();
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		String name=request.getParameter("employeename");
		String employeetel=request.getParameter("employeetel");
		String employeesex=request.getParameter("employeesex");
		String employeeage=request.getParameter("employeeage");
		String employeesfz=request.getParameter("employeesfz");
		String employeejzbh=request.getParameter("employeejzbh");
		String employeeztai=request.getParameter("employeeztai");
		em.setEmployeename(name);
		em.setEmployeetel(employeetel);
		em.setEmployeesex(employeesex);
		em.setEmployeeage(Integer.parseInt(employeeage));
		em.setEmployeesfz(employeesfz);
		em.setEmployeejzbh(employeejzbh);
		if(employeeztai.equals("签订合约")){			
			em.setEmployeezt("3");
		}else if(employeeztai.equals("不合适的")){
			em.setEmployeezt("2");
		}else if(employeeztai.equals("合适的")){
			em.setEmployeezt("1");
		}
		try {
			dao.save(em);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		doGet(request, response);
	}

}
