package cn.gson.zuche.controller.admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONArray;

import cn.gson.zuche.model.bean.Car;
import cn.gson.zuche.model.bean.Cartype;
import cn.gson.zuche.model.dao.Cardao;
import cn.gson.zuche.model.dao.CartypeDao;
import cn.gson.zuche.model.dao.Userorderdao;

/**
 * Servlet implementation class Charuchepaihao
 */
@WebServlet("/charuchepaihao")
public class Charuchepaihao extends HttpServlet {
	private static final long serialVersionUID = 1L;
   private Userorderdao dao=new Userorderdao();
   private Cardao cdao=new Cardao();
   private CartypeDao typedao=new CartypeDao();
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String dindanhao=request.getParameter("dindanid");
		String chepaihao=request.getParameter("chepaihao");
		String cartypename=request.getParameter("cartypename");
		try {
			//分配车辆
			dao.fengche(dindanhao, chepaihao);
			
			//修改车状态
			cdao.updatecheztai(chepaihao, "2");
			List<Cartype> list=typedao.shuaixin(cartypename);		
			PrintWriter out = response.getWriter();
			String jsonString = JSONArray.toJSONString(list);		
			out.print(jsonString);
		
			//修改订单状态
			dao.updatedindanztai(dindanhao);				
			//修改车的库存
			List<Car> c= cdao.huoqukucun(cartypename);
			typedao.fcgaikucun(cartypename, c.get(0).getNum());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		doGet(request, response);
	}

}
