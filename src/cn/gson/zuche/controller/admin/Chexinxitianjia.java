package cn.gson.zuche.controller.admin;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.gson.zuche.model.bean.Cartype;
import cn.gson.zuche.model.bean.Sijixiangche;
import cn.gson.zuche.model.dao.CartypeDao;
import cn.gson.zuche.model.dao.Sijixiangchedao;

/**
 * Servlet implementation class Chexinxitianjia
 */
@WebServlet("/chexinxi")
public class Chexinxitianjia extends HttpServlet {
	private CartypeDao dao=new CartypeDao();
	private Cartype t=new Cartype();
	private Sijixiangchedao sj=new Sijixiangchedao();
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		String id=null;
		id=request.getParameter("id");
		String mc=request.getParameter("mc");
		if(id!=null){
		try {
			List<Cartype> list= dao.selectbyname(id);
			request.setAttribute("cartypeid", list.get(0).getCarTypeId());
			request.setAttribute("chexinxijihe", list.get(0).getCarTypeName());
			request.setAttribute("carprice", list.get(0).getCarPrice());
			request.setAttribute("carxinche", list.get(0).getCarXinche());
			request.setAttribute("cardoornum", list.get(0).getCarDoorNum());
			request.setAttribute("carseatnum", list.get(0).getCarSeatNum());
			request.setAttribute("caroil", list.get(0).getCarOil());
			request.setAttribute("carbsx", list.get(0).getCarBsx());
			request.setAttribute("carpail", list.get(0).getCarPail());
			request.setAttribute("cardrive", list.get(0).getCarDrive());
			request.setAttribute("caroilbox", list.get(0).getCarOilBox());
			request.setAttribute("cargps", list.get(0).getCarGps());
			request.setAttribute("carskywindow", list.get(0).getCarSkyWindow());
			request.setAttribute("carbackld", list.get(0).getCarBackld());
			request.setAttribute("cargasbag", list.get(0).getCarGasbag());					
			request.setAttribute("chem", list.get(0).getCarTypeName());			
			List<Sijixiangche> xc=sj.select(mc);
			request.setAttribute("xc",xc);							
			request.setAttribute("miaoshu", list.get(0).getCardes());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		}
		request.getRequestDispatcher("/WEB-INF/jsp/adminjsp/tianjiachexinxi.jsp").forward(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		String typeid=request.getParameter("ycangid");
		
		String shangpingname=request.getParameter("shangpingname");
		String danjia=request.getParameter("danjia");
		String shifouxinping=request.getParameter("shifouxinping");
		String door=request.getParameter("door");
		String chezuodianshu=request.getParameter("chezuodianshu");
		String rangyouleixing=request.getParameter("rangyouleixing");
		String biansuxiang=request.getParameter("biansuxiang");
		String pailiang=request.getParameter("pailiang");
		String qudon=request.getParameter("qudon");
		String youxiangronliang=request.getParameter("youxiangronliang");
		String gps=request.getParameter("gps");
		String tianchuang=request.getParameter("tianchuang");
		String leida=request.getParameter("leida");
		String anquanqinang=request.getParameter("anquanqinang");
		
		request.setAttribute("chem",shangpingname);	
		
		t.setCarTypeName(shangpingname);
		t.setCarPrice(danjia);
		t.setCarSeatNum(Integer.parseInt(chezuodianshu));
		t.setCarDoorNum(Integer.parseInt(door));
		t.setCarOil(rangyouleixing);
		t.setCarBsx(biansuxiang);
		t.setCarPail(pailiang);
		t.setCarDrive(qudon);
		t.setCarOilBox(youxiangronliang);
		t.setCarGps(gps); 
		t.setCarSkyWindow(tianchuang);
		t.setCarGasbag(anquanqinang);
		t.setCarBackld(leida);
		t.setCarXinche(shifouxinping);
			
		try {
			if(!typeid.isEmpty()){				
				t.setCarTypeId(Integer.parseInt(typeid));	
				dao.updatachexinxi(t);
			}else{
			request.setAttribute("chem",shangpingname);
			dao.insertxinche(t);
			List<Cartype> xincheid=dao.huoquxincheid(shangpingname);
			request.setAttribute("cartypeid",xincheid.get(0).getCarTypeId());
			//通过姓名获取这个新品的id
			//然后封装到cartypeid中使得描述界面的按钮和图片上传界面图片获取到新插入车的id
			//
			//request.setAttribute("cartypeid", );
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		doGet(request, response);
	}
}
