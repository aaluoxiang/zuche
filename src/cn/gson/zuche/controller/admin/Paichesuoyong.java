package cn.gson.zuche.controller.admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONArray;

import cn.gson.zuche.model.bean.Cartype;
import cn.gson.zuche.model.dao.CartypeDao;

/**
 * Servlet implementation class Paichesuoyong
 */
@WebServlet("/paichesuoyong")
public class Paichesuoyong extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private CartypeDao dao = new CartypeDao();
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		String cheleixing=request.getParameter("cheleixing");
		String dindanid=request.getParameter("dindanid");
		try {
			List<Cartype> list=dao.selectbychexing(cheleixing,dindanid);
		
			PrintWriter out = response.getWriter();
			String jsonString = JSONArray.toJSONString(list);
			out.print(jsonString);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
