package cn.gson.zuche.controller.admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.alibaba.fastjson.JSONArray;

import cn.gson.zuche.model.bean.Sijixiangche;
import cn.gson.zuche.model.dao.Sijixiangchedao;

/**
 * Servlet implementation class Xiangcheshangchuang
 */

@WebServlet({"/xiangche", "/xiangche/*" })
public class Xiangcheshangchuang extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		String name=request.getParameter("name");
		Sijixiangchedao dao=new Sijixiangchedao();		
		Sijixiangche xiangche=new Sijixiangche();	
		Uploadpohto up=new Uploadpohto();
		
		try {
			List<Sijixiangche> list=dao.select(name);
			 PrintWriter out = response.getWriter();
			  String jsonString = JSONArray.toJSONString(list);
			  out.print(jsonString);
		} catch (SQLException e) {
			e.printStackTrace();	
		}
			
}
}
