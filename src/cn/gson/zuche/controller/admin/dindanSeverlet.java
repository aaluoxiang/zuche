package cn.gson.zuche.controller.admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.alibaba.fastjson.JSONArray;

import cn.gson.zuche.model.bean.Car;
import cn.gson.zuche.model.bean.Cartype;
import cn.gson.zuche.model.bean.DamageLevel;
import cn.gson.zuche.model.bean.Orderxiaofeijilu;
import cn.gson.zuche.model.bean.Qcpz;
import cn.gson.zuche.model.bean.User;
import cn.gson.zuche.model.bean.Userorder;
import cn.gson.zuche.model.bean.Wcpz;
import cn.gson.zuche.model.dao.Cardao;
import cn.gson.zuche.model.dao.CartypeDao;
import cn.gson.zuche.model.dao.DamageLeveldao;
import cn.gson.zuche.model.dao.Orderxiaofeijiludao;
import cn.gson.zuche.model.dao.Qcpzdao;
import cn.gson.zuche.model.dao.UserDao;
import cn.gson.zuche.model.dao.Userorderdao;
import cn.gson.zuche.model.dao.Wcpzdao;

/**
 * Servlet implementation class dindanSeverlet
 */
@WebServlet("/dindan")
@MultipartConfig(maxFileSize = 10 * 1024 * 1024)
public class dindanSeverlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Userorderdao dao = new Userorderdao();
	private Cardao cdao = new Cardao();
	private CartypeDao typedao = new CartypeDao();
	private UserDao udao = new UserDao();
	private Orderxiaofeijiludao odao = new Orderxiaofeijiludao();
	private Orderxiaofeijilu jl = new Orderxiaofeijilu();
	private Qcpzdao qcdao=new Qcpzdao();
	private DamageLeveldao damagedao=new DamageLeveldao();
	private Wcpz wcpz=new Wcpz();
	private Wcpzdao wcdao=new Wcpzdao();
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		String dindanhao = null;
		dindanhao = request.getParameter("dindanid");
		String chepaihao = request.getParameter("chepaihao");
		System.out.println(chepaihao);
		String cartypename = request.getParameter("cartypename");
		String money = request.getParameter("money");
		String dindanbianhao = request.getParameter(" dindanbianhao");
		String tuikuangtel = request.getParameter("tel");

		if (money != null) {
			try {
				dao.updatetuikuanzt(dindanbianhao, "3");
				List<User> tkmoney = udao.selectmoney(tuikuangtel);
				String zonmoeny = Float.parseFloat(money) + tkmoney.get(0).getUserMoney() + "";
				udao.updatemoney(tuikuangtel, zonmoeny);
				Date d = new Date();
				SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String time = f.format(d);
				jl.setOrderdata(time);
				jl.setOrderid(dindanbianhao);
				jl.setOrderjiaoyilaiyuan("余额");
				jl.setOrdermoney(money);
				odao.inserttk(jl);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if (dindanhao != null) {
			try {
				// 分配车辆
				dao.fengche(dindanhao, chepaihao);
				// 修改车状态
				cdao.updatecheztai(chepaihao, "2");
				List<Cartype> list = typedao.shuaixin(cartypename);
				PrintWriter out = response.getWriter();
				String jsonString = JSONArray.toJSONString(list);
				out.print(jsonString);
				// 修改订单状态
				dao.updatedindanztai(dindanhao);
				// 修改车的库存
				List<Car> c = cdao.huoqukucun(cartypename);
				typedao.fcgaikucun(cartypename, c.get(0).getNum());
				List<Cartype> xiaoliang = typedao.selectxiaoliang(cartypename);
				typedao.zengjiaxiaoliang(cartypename, xiaoliang.get(0).getCarXiaoliang() + 1);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		String id = request.getParameter("id");
		
		String chaxunid = null;
		chaxunid = id;
		System.out.println(request.getParameter("ztxuanze"));
		try {
			if (chaxunid != null || request.getParameter("ztxuanze") != null) {
				int page = 1;
				if (request.getParameter("page") != null) {
					page = Integer.parseInt(request.getParameter("page"));		
				}
				Integer count = (page - 1) * 4;
				String zt = null;
				zt = request.getParameter("ztxuanze");
				if (zt.equals("--状态选择--")) {
					request.setAttribute("ztxuanze", "$('.ztxuanze').val('--状态选择--')");
					zt = null;
				} else {
					request.setAttribute("ztxuanze", "$('.ztxuanze').val('" + zt + "')");
				}
				List<Userorder> list = dao.selectyige(chaxunid, count, zt);
				List<DamageLevel> d=damagedao.selectall();
				request.setAttribute("listuserorder", list);
				request.setAttribute("damage", d);
				System.out.println(d);
				List<Userorder> selectcount = dao.selectdaicangshumu(chaxunid, page, zt);
				this.find(list, selectcount, page, request, response);
			} else {
				int page = 1;
				if (request.getParameter("page") != null) {
					page = Integer.parseInt(request.getParameter("page"));
				}
				String zt = null;
				List<DamageLevel> d=damagedao.selectall();
				request.setAttribute("damage", d);
				zt = request.getParameter("ztxuanze");
				if (zt == null || "".equals(zt)) {
					request.setAttribute("ztxuanze", "$('.ztxuanze').val('--状态选择--')");
				} else {
					request.setAttribute("ztxuanze", "$('.ztxuanze').val('" + zt + "')");
				}
				String count = (page - 1) * 4 + "";
				List<Userorder> list = dao.select(count);
				List<Userorder> selectcount = dao.selectcount();
				this.find(list, selectcount, page, request, response);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		request.getRequestDispatcher("/WEB-INF/jsp/adminjsp/dindan.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		Uploadpohto up = new Uploadpohto();
		// 上传图片
		Qcpz qc=new Qcpz();
		String quchename = null;	
		String dindanhao = request.getParameter("dindanid");
		quchename = request.getParameter("quchename");
		String qucheyajin = request.getParameter("qucheyajin");	
		if (quchename != null && !quchename.equals("")){
			Part part = request.getPart("file");
			String newFileName = up.updaload(part, response);
			qc.setJzpicture(newFileName);
			qc.setOrderid(dindanhao);
			qc.setYjmoney(qucheyajin);
			qc.setLxrname(quchename);
			try {
				qcdao.insertAll(qc);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		Orderxiaofeijiludao odao = new Orderxiaofeijiludao();
		Userorderdao orderdao = new Userorderdao();
		Orderxiaofeijilu jilu = new Orderxiaofeijilu();
		String id = request.getParameter("ad");
		String a = null;
		a = id;
		if (a != null) {
			String money = request.getParameter("fukuanjine");
			String fashi = request.getParameter("fukuanfanshi");
			String shoukuantel = request.getParameter("shoukuantel");
			Date date = new Date();
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String time = format.format(date);
			jilu.setOrderid(id);
			jilu.setOrderjiaoyilaiyuan(fashi);
			jilu.setOrderdata(time);
			try {
				if (fashi.equals("余额")) {
					List<User> usermoney = udao.selectmoney(shoukuantel);
					double zonhemoney = usermoney.get(0).getUserMoney() - Float.parseFloat(money);
					if (zonhemoney >= 0) {
						udao.updateusermoney(zonhemoney + "", shoukuantel);
						if (odao.selectshifou(id).size() == 0) {
							String shoukuangmoney = request.getParameter("shoukuanmoney");
							jilu.setOrdermoney(shoukuangmoney);
							odao.insert(jilu);
						}
						jilu.setOrdermoney(money);
						boolean bool = odao.insert(jilu);
						List<Orderxiaofeijilu> summoney = odao.selectmoney(id);
						orderdao.updata(id, summoney.get(0).getOrdermoney());
					} else {
						request.setAttribute("error", "余额不足");
					}
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else {
			String ap = request.getParameter("ap");
			System.out.println(ap);
			String yishoujine = request.getParameter("jine");
			String shishoujine = null;
			shishoujine = request.getParameter("shishoujine");
			String tel = request.getParameter("tel");
			String shifouhaiche = null;
			shifouhaiche = request.getParameter("haiche");
			String carid = request.getParameter("carid");
			String xiufujine=request.getParameter("xiufujine");
			String yingtuiyajin=request.getParameter("yingtuiyajin");
			String dxk=request.getParameter("dxk");
			
			try {
				
				if (shifouhaiche != null && !shifouhaiche.equals("")){
					List<DamageLevel> leavelid=damagedao.selectid(dxk);
					wcpz.setDamagelevel(leavelid.get(0).getLeverid());
					wcpz.setMaintainmoney(xiufujine);
					wcpz.setOrderid(ap);
					wcpz.setQuityjmoney(yingtuiyajin);
					wcdao.insertAll(wcpz);
					leavelid.get(0).getLeverid();
					if (shifouhaiche.equals("已还车")) {
						cdao.updatecheztai(carid, "1");
						List<Car> l = cdao.nakucun(carid);
						typedao.huankucun(carid, l.get(0).getNum() + "");
					} else {
						dao.updatedindanztai(ap);
					}
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			if (shishoujine != null && !shishoujine.equals("")) {
				String zhi = (Float.parseFloat(yishoujine) - Float.parseFloat(shishoujine)) + "";
				if (Float.parseFloat(zhi) == 0 || Float.parseFloat(zhi) < 0) {
					try {
						orderdao.updatezt(ap);
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				try {
					orderdao.updata(ap, zhi);
					List<User> usejf = udao.selectuserjf(tel);
					Integer jf = usejf.get(0).getUserJf();
					String zonjf = Float.parseFloat(yishoujine) + jf + "";
					udao.updateuserjf(zonjf, tel);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		doGet(request, response);
	}

	private void find(List<Userorder> list, List<Userorder> selectcount, Integer page, HttpServletRequest request,
			HttpServletResponse response) {
		request.setAttribute("page", page);
		double s = Double.parseDouble(selectcount.get(0).getOrderid()) / 4;
		request.setAttribute("suoyoujilu", selectcount.get(0).getOrderid());
		request.setAttribute("suoyoushu", (int) Math.ceil(s));
		request.setAttribute("listuserorder", list);
		System.out.println(list);
	}
}
