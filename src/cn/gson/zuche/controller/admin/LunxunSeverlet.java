package cn.gson.zuche.controller.admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.gson.zuche.model.bean.Lingshibiao;
import cn.gson.zuche.model.dao.Lingshibiaodao;

/**
 * Servlet implementation class LunxunSeverlet
 */
@WebServlet("/lunxun")
public class LunxunSeverlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Lingshibiaodao ls=new Lingshibiaodao();
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		try {
			List<Lingshibiao> list=ls.select();			
			if(list.size()>0){
				PrintWriter out=response.getWriter();
				out.print(list.size()); 	
				ls.delete();
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
