package cn.gson.zuche.controller.admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONArray;

import cn.gson.zuche.model.bean.Cartype;
import cn.gson.zuche.model.dao.CartypeDao;

/**
 * Servlet implementation class Chepeizhixinxi
 */
@WebServlet("/chepeizhixinxi")
public class Chepeizhixinxi extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private CartypeDao dao=new CartypeDao();
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");	
		String name=request.getParameter("name");	
			try {
				List<Cartype> list= dao.selectbyname(name);
			
				PrintWriter out=response.getWriter();
				String jsonstring=JSONArray.toJSONString(list);
				out.print(jsonstring);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}

}
