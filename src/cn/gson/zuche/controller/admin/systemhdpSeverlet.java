package cn.gson.zuche.controller.admin;

import java.io.Console;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.gson.zuche.model.bean.Weburl;
import cn.gson.zuche.model.dao.Weburldao;

/**
 * Servlet implementation class systemhdpSeverlet
 */
@WebServlet("/systemhdp")
public class systemhdpSeverlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private	Weburldao dao=new Weburldao();
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try {
			List< Weburl> list=dao.findall();
			request.setAttribute("weburl", list);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		request.getRequestDispatcher("/WEB-INF/jsp/adminjsp/systemhdp.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getParameter("tupianid")!=null){
			String[] a=new String[100];
			a=request.getParameter("tupianid").split(",");
			for (int i = 0; i < a.length; i++) {
				try {
					dao.delete(a[i]);
				} catch (SQLException e) {					
					e.printStackTrace();
				}
			}
		}	
		doGet(request, response);
	}

}
