package cn.gson.zuche.controller.admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONArray;

import cn.gson.zuche.model.bean.User;
import cn.gson.zuche.model.dao.UserDao;

/**
 * Servlet implementation class UserxiugaiSeverlet
 */
@WebServlet("/userxiugai")
public class UserxiugaiSeverlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
   private UserDao dao=new UserDao();
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	String id=request.getParameter("id");
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
	try {
		List<User> list=dao.xiugaiuser(id);
		PrintWriter out=response.getWriter();
		String jsonString = JSONArray.toJSONString(list);
		out.print(jsonString);	
	} catch (SQLException e) {
		e.printStackTrace();
	}
	}

}
