package cn.gson.zuche.controller.admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONArray;

import cn.gson.zuche.model.bean.Orderxiaofeijilu;
import cn.gson.zuche.model.dao.Orderxiaofeijiludao;

/**
 * Servlet implementation class JudgeSeverlet
 */
@WebServlet("/judge")
public class JudgeSeverlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
        
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id=request.getParameter("id");
		response.setCharacterEncoding("utf-8");
		Orderxiaofeijiludao dao=new Orderxiaofeijiludao();
		try {
			List<Orderxiaofeijilu> list=dao.select(id);	
			  PrintWriter out = response.getWriter();
			  String jsonString = JSONArray.toJSONString(list);
			  out.print(jsonString);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
