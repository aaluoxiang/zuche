package cn.gson.zuche.controller.admin;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.gson.zuche.model.bean.Systemmk;
import cn.gson.zuche.model.bean.Weburl;
import cn.gson.zuche.model.dao.Systemmkdao;
import cn.gson.zuche.model.dao.Weburldao;

/**
 * Servlet implementation class SystemmkServelet
 */
@WebServlet("/systemmk")
public class SystemmkServelet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       private Systemmkdao sdao=new Systemmkdao();
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	String mkid=null;
    	mkid=request.getParameter("sonid");
    	
    	try {
    		if(mkid!=null&&!mkid.equals("")){
        		String []sonid=mkid.split(",");
        		for (int i = 0; i < sonid.length; i++) {				
    					sdao.delete(sonid[i]);	
    			}
        	}
			List<Systemmk> list=sdao.findall();
			request.setAttribute("weburl", list);
			List<Systemmk> max=sdao.selectmax();
			int a=1;
			request.setAttribute("max", Integer.parseInt(max.get(0).getWebmodelpaixu())+a);
		} catch (SQLException e) {
			e.printStackTrace();
		}
    	request.getRequestDispatcher("WEB-INF/jsp/adminjsp/systemmk.jsp").forward(request, response);;
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name=null;
		name=request.getParameter("name");
		String url=request.getParameter("url");
		String paixu=request.getParameter("paixu");
		//一级模块添加		
		Systemmk mk=new Systemmk();
		mk.setWebmodelname(name);
		mk.setWebmodelurl(url);
		if(null!=paixu&&!("").equals(paixu)){
			mk.setWebsonid(Integer.parseInt(paixu));
		}
		mk.setWebfatherid(0);
		
		//子模块添加
		String sonname=null;
		sonname=request.getParameter("sonname");
		String sonURL=request.getParameter("sonURL");
		String sonpaixu=request.getParameter("sonpaixu");
		String fatherid=request.getParameter("webfatherid");
		System.out.println(fatherid);
		System.out.println(sonURL);
		System.out.println(sonpaixu);
		System.out.println(sonname);
		try {
			if(name!=null&&!name.equals("")){
				boolean b=sdao.insert(mk);
				request.setAttribute("ss",b);	
			}
			if(sonname!=null&&!sonname.equals("")){	
				mk.setWebfatherid(Integer.parseInt(fatherid));
				mk.setWebmodelname(sonname);
				mk.setWebmodelurl(sonURL);
				mk.setWebsonid(Integer.parseInt(sonpaixu));
				sdao.insert(mk);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		doGet(request, response);
	}

}
