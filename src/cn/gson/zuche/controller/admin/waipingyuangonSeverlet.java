package cn.gson.zuche.controller.admin;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.gson.zuche.model.bean.Employee;
import cn.gson.zuche.model.dao.Employeedao;

/**
 * Servlet implementation class waipingyuangonSeverlet
 */
@WebServlet("/waipingyuangon")
public class waipingyuangonSeverlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       private Employeedao dao=new Employeedao();
  
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {	
		
		try {
			List<Employee> list=dao.select();
			request.setAttribute("list", list);
		} catch (SQLException e) {		
			e.printStackTrace();
		}
		request.getRequestDispatcher("/WEB-INF/jsp/adminjsp/waipingyuangon.jsp").forward(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		if(request.getParameter("tupianid") != null){
			try {
				dao.delete(request.getParameter("tupianid"));
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		String employeeztai=request.getParameter("gaibianztai");
		String id=request.getParameter("id");
		try {
		if(employeeztai.equals("签订合约")){			
			dao.update("3", id);
		}else if(employeeztai.equals("不合适的")){
			dao.update("2", id);
		}else if(employeeztai.equals("合适的")){
				dao.update("1", id);	
		}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		doGet(request, response);
	}

}
