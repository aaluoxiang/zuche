package cn.gson.zuche.controller.admin;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.gson.zuche.model.bean.Cartype;
import cn.gson.zuche.model.bean.Cartypee;
import cn.gson.zuche.model.dao.CartypeDao;

/**
 * Servlet implementation class shangpingunaliSeverlet
 */
@WebServlet("/shangpingunali")
public class shangpingunaliSeverlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private CartypeDao dao=new CartypeDao();
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		String id=null;	
		id=request.getParameter("id");	
		
		if(id!=null){
			try {
				String chexing=null;
				chexing=request.getParameter("chexing");
			if(chexing.equals("新车型")){
				dao.updata(id,"非新车");
			}else{
				dao.updata(id,"新车型");
			}					
			} catch (SQLException e) {				
				e.printStackTrace();
			}
		}
		try {
			List<Cartype> bb=dao.selectsuoyou();
			request.setAttribute("shangpinglist", bb);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		request.getRequestDispatcher("WEB-INF/jsp/adminjsp/shangpinguanli.jsp").forward(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String tupianid=request.getParameter("tupianid");
		try {
			String[] s=tupianid.split(",");
			for (int i = 0; i < s.length; i++) {
				dao.delete(s[i]);
			}	
		} catch (SQLException e) {
			e.printStackTrace();
		}
		doGet(request, response);
	}

}
