package cn.gson.zuche.controller.admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONArray;

import cn.gson.zuche.model.bean.Car;
import cn.gson.zuche.model.bean.Cartype;
import cn.gson.zuche.model.dao.Cardao;
import cn.gson.zuche.model.dao.CartypeDao;

/**
 * Servlet implementation class XinzengSeverlet
 */
@WebServlet("/xinzeng")
public class XinzengSeverlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Cardao dao = new Cardao();
	private Car c = new Car();
	private CartypeDao cardao = new CartypeDao();

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		String typeid = request.getParameter("typeid");
		String chepaihao = request.getParameter("chepaihao");
	
		c.setCarlicenseid(chepaihao);
		c.setCartypeids(typeid);
		c.setCarztids("1");
		try {
			dao.insert(c);
			List<Car> count=dao.selectcount(typeid);	
			
			cardao.updatakucun(typeid, count.get(0).getCartypeids());			
			List<Cartype> list = cardao.selectchexiangxi(typeid);
			PrintWriter out = response.getWriter();
			String jsonstring = JSONArray.toJSONString(list);
			out.print(jsonstring);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
