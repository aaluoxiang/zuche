package cn.gson.zuche.controller.admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONArray;

import cn.gson.zuche.model.bean.Employee;
import cn.gson.zuche.model.dao.Employeedao;

/**
 * Servlet implementation class Chaxunwaipingyuangon
 */
@WebServlet("/chaxunwaipingyuangon")
public class Chaxunwaipingyuangon extends HttpServlet {
	private static final long serialVersionUID = 1L;
	 private Employeedao dao=new Employeedao();
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		String name=request.getParameter("xingming");
		try {
			List<Employee> list=dao.selectByname(name);
			PrintWriter out =response.getWriter();
			String jsonstring=JSONArray.toJSONString(list);
			out.print(jsonstring);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		
	}

}
