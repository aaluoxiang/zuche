package cn.gson.zuche.controller.admin;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.gson.zuche.model.bean.Employee;
import cn.gson.zuche.model.dao.Employeedao;

/**
 * Servlet implementation class guanliyuanguanliSeverlet
 */
@WebServlet("/guanliyuanguanli")
public class guanliyuanguanliSeverlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Employeedao dao=new Employeedao(); 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
			if(request.getParameter("id")!=null){
				try {
					dao.delete(request.getParameter("id"));
				} catch (SQLException e) {			
					e.printStackTrace();
				}
			}
		try {
			List<Employee> glylist=dao.selectgly();
			request.setAttribute("glylist",glylist);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	request.getRequestDispatcher("/WEB-INF/jsp/adminjsp/guanliyuanguanli.jsp").forward(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		String yonhu=request.getParameter("yonhu");
		String xinming=request.getParameter("xingming");
		String sex=request.getParameter("sex");
		String guanli=request.getParameter("guanli");
		String qq=request.getParameter("qq");	
		String password=request.getParameter("password");
		Employee em=new Employee();
		em.setEmployeeyonhuming(xinming);
		em.setEmployeename(yonhu);
		em.setEmployeesex(sex);
		em.setEmployeeqq(qq);
		em.setEmployeepassword(password);
		if(guanli.equals("管理员")){	
			em.setEmployeetypeid("2");	
		}else if(guanli.equals("外部销售")){			
			em.setEmployeetypeid("3");
		}else if(guanli.equals("超级管理员")){			
			em.setEmployeetypeid("4");
		}		
		if(!request.getParameter("yonhuid").equals("aa")){	
			em.setEmployeeid(Integer.parseInt(request.getParameter("yonhuid")));
			try {
				dao.updata(em);
			} catch (SQLException e) {		
				e.printStackTrace();
			}
		}else{
			try {
				dao.savegly(em);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}	
		
		doGet(request, response);
	}

}
