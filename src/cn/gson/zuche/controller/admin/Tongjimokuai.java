package cn.gson.zuche.controller.admin;

import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.gson.zuche.model.bean.Carttpp;
import cn.gson.zuche.model.bean.Cartype;
import cn.gson.zuche.model.bean.Huizongbiao;
import cn.gson.zuche.model.bean.Nianzongbiao;
import cn.gson.zuche.model.bean.Orderxiaofeijilu;
import cn.gson.zuche.model.bean.Useroodd;
import cn.gson.zuche.model.bean.Userorder;
import cn.gson.zuche.model.bean.Userorderzt;
import cn.gson.zuche.model.dao.Carttppdao;
import cn.gson.zuche.model.dao.Huizongdao;
import cn.gson.zuche.model.dao.Nianzongbiaodao;
import cn.gson.zuche.model.dao.Orderxiaofeijiludao;
import cn.gson.zuche.model.dao.Useroodddao;
import cn.gson.zuche.model.dao.Userorderdao;
import cn.gson.zuche.model.dao.Userorderztdao;

/**
 * Servlet implementation class Tongjimokuai
 */
@WebServlet("/tongjimokuai")
public class Tongjimokuai extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Carttppdao d = new Carttppdao();
	private Userorderdao udao = new Userorderdao();
	private Useroodddao oodao = new Useroodddao();
	private Orderxiaofeijiludao ordao = new Orderxiaofeijiludao();
	private Huizongdao hdao = new Huizongdao();
	private Userorderztdao ztdao=new Userorderztdao();
	private Nianzongbiao nian = new Nianzongbiao();
	private Nianzongbiaodao ndao = new Nianzongbiaodao();
	private Huizongbiao hb = new Huizongbiao();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String n = null;
		n = request.getParameter("name");

		try {
			List<Huizongbiao> symoney = hdao.select();
			Date d = new Date();
			Calendar date = Calendar.getInstance();
			date.setTime(d);
			date.set(Calendar.DATE, date.get(Calendar.DATE) - 1);
			SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
			Date endate = f.parse(f.format(date.getTime()));
			String end = f.format(endate);
			String begain = f.format(d);		
			List<Huizongbiao> summoney = hdao.selectsummoney((date.get(Calendar.MONTH) + 1) + "");
			System.out.println(summoney.get(0).getMoney());

			nian.setData(date.get(date.get(Calendar.MONTH) + 1) + "");
			nian.setMoney(summoney.get(0).getMoney());
			
			System.out.println(date.get(date.get(Calendar.MONTH) + 1));
			if (ndao.selectbydata(date.get(date.get(Calendar.MONTH) + 1) + "").size() == 0) {
				ndao.insert(nian);
				
			}

			List<Nianzongbiao> qian = ndao.selectqian(date.get(date.get(Calendar.MONTH) + 1) + "");
			System.out.println(qian.get(0).getMoney());
			if (ndao.selectbydata(date.get(date.get(Calendar.MONTH) + 1) + "").size()!=0) {

				ndao.update(summoney.get(0).getMoney(), date.get(date.get(Calendar.MONTH) + 1) + "");
			}

			List<Orderxiaofeijilu> zonmoney = ordao.selectdaymoney(end, begain);
			
			if(zonmoney.get(0).getOrdermoney()!=null&&!zonmoney.get(0).getOrdermoney().equals("")){
				hb.setMoney(zonmoney.get(0).getOrdermoney() + "");
			}else{
				hb.setMoney("0");
			}
			//System.out.println("总计钱"+zonmoney.get(0).getOrdermoney() );
			hb.setName(end);
			// 查询是否已经插入
			List<Huizongbiao> ok = hdao.select(end);
			if (ok.size() == 0) {
				hdao.insert(hb);
			}
			List<Huizongbiao> suoyou = hdao.select();
			String s[] = new String[1000];
			StringBuffer a = new StringBuffer();
			StringBuffer b = new StringBuffer();
			
			if ("年度".equals(n)) {				
				List<Nianzongbiao> nianzho = ndao.select();
				for (int i = 0; i < nianzho.size(); i++) {
					String name = nianzho.get(i).getData();
					s[i] = "" + name + "";
					a.append("'" + s[i] + "月',");
				}
				for (int i = 0; i < nianzho.size(); i++){
					String money = nianzho.get(i).getMoney();
					s[i] = "" + money + "";
					b.append(s[i] + ",");
				}
				List aa = new ArrayList();
				aa.add("{name:'2017年',data:[" + b.substring(0, b.length() - 1) + "]}");
				request.setAttribute("aa", aa);
				request.setAttribute("s", "[" + a.substring(0, a.length() - 1) + "]");
			} else {
				for (int i = 0; i < suoyou.size(); i++) {
					String name = suoyou.get(i).getName();
					s[i] = "" + name + "";
					a.append("'" + s[i] + "',");
				}
				for (int i = 0; i < suoyou.size(); i++) {

					String money = suoyou.get(i).getMoney();
					s[i] = "" + money + "";
					b.append(s[i] + ",");
				}
				List aa = new ArrayList();
				aa.add("{name:'2017年',data:[" + b.substring(0, b.length() - 1) + "]}");
				request.setAttribute("aa", aa);
				request.setAttribute("s", "[" + a.substring(0, a.length() - 1) + "]");
			}
			// System.out.println(aa);
			request.setAttribute("suoyou", suoyou);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// ordao.selectdaymoney(data, data2);
		try {
			List list = new ArrayList();
			List<Carttpp> l = d.huoquxiaoliang();
			request.setAttribute("xiaoliang", l);
			List<Userorderzt> zt=ztdao.select();
			for (int i = 0; i < zt.size(); i++) {
				//oodao.chazhaoytdshuliang(zt.get(i).getOrderztid());
				list.add(oodao.chazhaoytdshuliang(zt.get(i).getOrderztid()).get(0));
			}
			/*List<Useroodd> ywc = oodao.chazhaoyiwcshuliang();
			List<Useroodd> ytd = oodao.chazhaoytdshuliang("3");
			List<Useroodd> dfk = oodao.chazhaoytdshuliang("4");
			List<Useroodd> yfk = oodao.chazhaoytdshuliang("5");
			List<Useroodd> zlz = oodao.chazhaoytdshuliang("6");
			List<Useroodd> hcsh = oodao.chazhaoytdshuliang("7");
			List<Useroodd> tkz = oodao.chazhaoytdshuliang("8");
			list.add(ywc.get(0));
			list.add(ytd.get(0));
			list.add(dfk.get(0));
			list.add(yfk.get(0));
			list.add(zlz.get(0));
			list.add(hcsh.get(0));
			list.add(tkz.get(0));*/
			request.setAttribute("dindanshuliang", list);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		request.getRequestDispatcher("WEB-INF/jsp/adminjsp/tonjimokuai.jsp").forward(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doGet(request, response);
	}

}
