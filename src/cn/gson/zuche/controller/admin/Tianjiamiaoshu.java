package cn.gson.zuche.controller.admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONArray;

import cn.gson.zuche.model.dao.CartypeDao;

/**
 * Servlet implementation class Tianjiamiaoshu
 */
@WebServlet("/tianjiamiaoshu")
public class Tianjiamiaoshu extends HttpServlet {
	private static final long serialVersionUID = 1L;
       private CartypeDao dao=new CartypeDao();
       
   
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String des=request.getParameter("miaoshu");
		String id=request.getParameter("miaosuid");
		try {
			boolean s=dao.updatades(id, des);
			PrintWriter out=response.getWriter();
			String jsonstring=JSONArray.toJSONString(s);
			out.print(jsonstring);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
