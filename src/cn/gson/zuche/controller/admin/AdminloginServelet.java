package cn.gson.zuche.controller.admin;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cn.gson.zuche.common.StringUtils;
import cn.gson.zuche.controller.user.CaptchaServlet;
import cn.gson.zuche.model.bean.Employee;
import cn.gson.zuche.model.dao.Employeedao;

/**
 * Servlet implementation class AdminloginServelet
 */
@WebServlet("/adminlogin")
public class AdminloginServelet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private Employeedao dao=new Employeedao();
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("WEB-INF/jsp/adminjsp/adminlogin.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		HttpSession session=request.getSession();		
		String zhanghao=request.getParameter("logintel");		
		String passwd=request.getParameter("login-passwd");	
		String code=request.getParameter("login-code");
		String sesionCode = (String) request.getSession().getAttribute(CaptchaServlet.CAPTCHA_KEY);
		
		if(StringUtils.isEmpty(zhanghao)){
			request.setAttribute("error_m_emp", "$('.t_m_emp').css('display','block')");
			doGet(request, response);
			return;
		}
		if(StringUtils.isEmpty(passwd)){
			request.setAttribute("error_w_emp", "$('.t_w_emp').css('display','block')");
			doGet(request, response);
			return;
		}
		if(StringUtils.isEmpty(code)){
			request.setAttribute("error_code_emp", "$('.t_code_emp').css('display','block')");
			doGet(request, response);
			return;
		}
		if(!sesionCode.equalsIgnoreCase(code)){
			request.setAttribute("error_code", "$('.t_code').css('display','block')");
			doGet(request, response);
			return;
		}
		
		try {
			List<Employee> list=dao.pddl(zhanghao,passwd);			
			if(list.size()==0){
				request.setAttribute("error_m", "$('.t_m').css('display','block')");
				doGet(request, response);
				return;
			}
			session.setAttribute("zhanghao",zhanghao);
			session.setAttribute("quanxian", list.get(0).getEmployeetypeid());
			response.sendRedirect(request.getContextPath()+"/index");
			
			/*if(list.get(0).getEmployeepassword().equals(passwd)&&list.size()>0){
				session.setAttribute("zhanghao",zhanghao);
				response.sendRedirect(request.getContextPath()+"/index");
				
			}else{
				doGet(request, response);
			}*/
		} catch (SQLException e) {			
			e.printStackTrace();
		}
	}

}
