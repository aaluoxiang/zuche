package cn.gson.zuche.controller.admin;

import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.jws.soap.SOAPBinding.Use;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cn.gson.zuche.model.bean.Chozhi;
import cn.gson.zuche.model.bean.User;
import cn.gson.zuche.model.bean.UserType;
import cn.gson.zuche.model.dao.Chozhidao;
import cn.gson.zuche.model.dao.UserDao;
import cn.gson.zuche.model.dao.UserTypeDao;
import javafx.scene.input.DataFormat;
import sun.misc.Perf.GetPerfAction;

/**
 * Servlet implementation class huiyuanguanliSeverlet
 */
@WebServlet("/huiyuanliebiao")
public class huiyuanguanliSeverlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
   private UserDao dao=new UserDao();
   private User u=new User();
   private Chozhidao chozhi=new Chozhidao();
   private UserTypeDao td=new UserTypeDao();
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String tel=null;
		tel=request.getParameter("value");
		try {
			List<UserType> t=td.findAll();
			request.setAttribute("leixingjihe", t);
		if(tel!=null){		
				List<User> list=dao.selectbytel(tel);
				request.setAttribute("userlist", list);			
		}else{
			List<User> list=dao.select();
			request.setAttribute("userlist", list);	
		}
			} catch (SQLException e) {
				e.printStackTrace();
			}	
		request.getRequestDispatcher("/WEB-INF/jsp/adminjsp/huiyuanliebiao.jsp").forward(request, response);	
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		HttpSession session=request.getSession();
		String money=null;
		String te=request.getParameter("chozhitel");
		money=request.getParameter("chochozhizhi");		
		//充值
		try {
			if(!money.isEmpty()){
				//获取当前时间
				Date d=new Date();
				SimpleDateFormat f=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String date=f.format(d);
				System.out.println(date);
				Chozhi cz= new Chozhi();
				String gly=(String) session.getAttribute("zhanghao");
				//充值记录表
				cz.setChozhitel(te);
				cz.setChozhicaozuoren(gly);
				cz.setChozhidata(date);
				cz.setChozhijiaoyifangxiang("充值");
				cz.setChozhijiaoyilaiyuan("余额");
				cz.setChozhimoney(money);
				chozhi.insert(cz);			
				
			//更新余额
			List<User> qian=dao.selectmoney(te);
			String nummoney=(Float.parseFloat(money)+qian.get(0).getUserMoney())+"";	
			dao.updatemoney(te, nummoney);
			}
		} catch (SQLException e) {	
			e.printStackTrace();
			request.setAttribute("erorr", "请输入电话");
		} 
		String tianjianame=null;
		tianjianame=request.getParameter("tianjiayonhuming");
	if(!tianjianame.isEmpty()){
		String tianjiamima=request.getParameter("tianjiamima");
		String juese=request.getParameter("juese");
		String ztai=request.getParameter("ztai");
		String tianjiadianhua=request.getParameter("tianjiadianhua");
		String tianjiaemail=request.getParameter("tianjiaemail");
		String tinajiayue=request.getParameter("tinajiayue");
		String tianjiajifen=request.getParameter("tianjiajifen");
		u.setUserName(tianjianame);
		u.setPassword(tianjiamima);
		try {
			List<UserType> tlist=td.findid(juese);		
			u.setUserType(tlist.get(0).getUsertypeId());
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		/*if(juese.equals("普卡会员")){
			u.setUserType("1");
		}else if(juese.equals("银卡会员")){
			u.setUserType("2");
		}else if(juese.equals("黄金会员")){
			u.setUserType("3");
		}else if(juese.equals("白金会员")){
			u.setUserType("4");
		}else if(juese.equals("钻石会员")){
			u.setUserType("5");
		}*/
		if(ztai.equals("正常")){
			u.setUserZt("1");
		}else if(ztai.equals("冻结")){
			u.setUserZt("2");
		}
		u.setUserTel(tianjiadianhua);
		u.setUserEmail(tianjiaemail);
		if(tinajiayue.isEmpty()){
			u.setUserMoney(0.0);
		}else{
			u.setUserMoney(Double.parseDouble(tinajiayue));
		}
		
		
		
		u.setUserJf(Integer.parseInt(tianjiajifen+""));
		String xiugaiid=null;
		xiugaiid=request.getParameter("ss");
		try {
			if(!xiugaiid.isEmpty()){
				//修改用户信息
				dao.updateuser(u,xiugaiid);
			}else{
				//插入用户
				dao.insertxinyonhu(u);
			}		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
		doGet(request, response);
	}

}
