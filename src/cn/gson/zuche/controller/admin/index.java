package cn.gson.zuche.controller.admin;

import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cn.gson.zuche.model.bean.Guanliyuantype;
import cn.gson.zuche.model.bean.Systemmk;
import cn.gson.zuche.model.dao.Guanliyuantypedao;
import cn.gson.zuche.model.dao.Systemmkdao;



/**
 * Servlet implementation class index
 */
@WebServlet("/index")
public class index extends HttpServlet {
	private static final long serialVersionUID = 1L; 
	private Guanliyuantypedao Gdao=new Guanliyuantypedao();
	private Systemmkdao Sdao=new Systemmkdao();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {	
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		HttpSession session=request.getSession();
		Systemmkdao dao=new Systemmkdao();
		List list;
		Date time=new Date();
		SimpleDateFormat form=new SimpleDateFormat("yyyy年MM月dd日");
		String data=form.format(time);	
		String []s={"星期一","星期二","星期三","星期四","星期五","星期六","星期日"};
		String sj;
		if(time.getDay()==0){
			sj=""+data+""+s[6]+"";
		}else{
			sj=""+data+""+s[time.getDay()-1]+"";
		}		
		request.setAttribute("time",sj);	
		request.setAttribute("dd","iframe");
		try {
			List<Systemmk> guanliyuan= new ArrayList<Systemmk>();
			list = dao.findall();
			request.setAttribute("as",list);
			String quanxian=(String) session.getAttribute("quanxian");
			List<Guanliyuantype> t=Gdao.select(quanxian);
			for (int i = 0; i <t.size(); i++) {							
			List<Systemmk> system=Sdao.selectBysonid(t.get(i).getWeb_system_id());			
			/*mk.setWebsonid(system.get(0).getWebsonid());
			mk.setWebmodelname(system.get(0).getWebmodelname());
			mk.setWebmodelurl(system.get(0).getWebmodelurl());
			mk.setWebfatherid(system.get(0).getWebfatherid());
			System.out.println(mk);*/
			guanliyuan.add(new Systemmk(system.get(0).getWebsonid(),system.get(0).getWebmodelname(),system.get(0).getWebmodelurl(),system.get(0).getWebfatherid()));		
			}	
			System.out.println(guanliyuan);
			request.setAttribute("bs",guanliyuan);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		request.getRequestDispatcher("/WEB-INF/jsp/adminjsp/houtai.jsp").forward(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
		doGet(request, response);
	}
}
