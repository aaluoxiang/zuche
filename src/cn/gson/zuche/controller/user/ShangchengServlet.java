package cn.gson.zuche.controller.user;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONArray;

import cn.gson.zuche.model.bean.Cartype;
import cn.gson.zuche.model.dao.CartypeDao;


/**
 * Servlet implementation class ShangchengServlet
 */
@WebServlet("/shangcheng")
public class ShangchengServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private CartypeDao  cdao=new  CartypeDao();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {	
		
		String stime1=request.getParameter("stime1");
		String stime2=request.getParameter("stime2");
		String quche1=request.getParameter("quche1");
		String quche2=request.getParameter("quche2");
		String btime1=request.getParameter("btime1");
		String btime2=request.getParameter("btime2");
		String huanche1=request.getParameter("huanche1");
		String huanche2=request.getParameter("huanche2");
		
		request.setAttribute("stime1", stime1);
		request.setAttribute("stime2", stime2);
		request.setAttribute("quche1", quche1);
		request.setAttribute("quche2", quche2);
		request.setAttribute("btime1", btime1);
		request.setAttribute("btime2", btime2);
		request.setAttribute("huanche1", huanche1);
		request.setAttribute("huanche2", huanche2);
		
		try {
			List<Cartype> che= cdao.findPinpai();
			List<Cartype> chexinxi=cdao.findche();
		
			request.setAttribute("chexinxi", chexinxi);
			request.setAttribute("che", che);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		request.getRequestDispatcher("/WEB-INF/jsp/shangcheng.jsp").forward(request, response);
	}
	 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			System.out.println("ssss");
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		String max=null;
		String chexing=request.getParameter("chexing");
		String jiage=request.getParameter("jiage");
		String pinpai=request.getParameter("pinpai");
		String paixu=request.getParameter("paixu");	
		if("不限".equals(pinpai)){
			pinpai=null;
		}
	
		if("0".equals(jiage)){
			 max="100";	
		}else if("100".equals(jiage)){
			max="300";
		}else if("300".equals(jiage)){
			max="500";
		}else if("500".equals(jiage)){
			max="2000";
		}
		
		PrintWriter out = response.getWriter();
	    try {
	    	List<Cartype> xinxi=cdao.findXinxi(chexing,jiage, max, pinpai,paixu);
	    	out.print(JSONArray.toJSONString(xinxi)); 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    out.flush();
    	out.close();
	  
		
	}

}
