package cn.gson.zuche.controller.user;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cn.gson.zuche.common.StringUtils;
import cn.gson.zuche.model.bean.User;
import cn.gson.zuche.model.dao.UserDao;
import cn.gson.zuche.model.dao.UserTypeDao;

/**
 * Servlet implementation class MyinforController
 */
@WebServlet("/updatepassword")
public class UpdatePasswordController extends HttpServlet {
	
	UserDao udao = new UserDao();
	UserTypeDao utypedao = new UserTypeDao();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			HttpSession session = request.getSession();
			Long userId = Long.parseLong(session.getAttribute("userId")+"");
			List<User> users = udao.findAllById(userId);
			User user = users.get(0);
			String upjf = (String) utypedao.findupjf(user.getUserType()+"").get(0);
			request.setAttribute("user", users.get(0));
			request.setAttribute("upjf", upjf);
			request.getRequestDispatcher("/WEB-INF/jsp/updatepassword.jsp").forward(request, response);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			request.setCharacterEncoding("UTF-8");
			HttpSession session = request.getSession();
			String userid = session.getAttribute("userId")+"";
			User user = udao.selectxinxi(userid).get(0);
			
			String oldpassword = request.getParameter("oldpassword");
			String newpassword = request.getParameter("newpassword");
			String passwordagain = request.getParameter("passwordagain");

			if (StringUtils.isEmpty(newpassword)) {
				request.setAttribute("error", "新密码不能为空");
				setreqatu(oldpassword, newpassword, passwordagain, request, response);
				doGet(request, response);
				return;
			}
			if (!passwordagain.equals(newpassword)) {
				request.setAttribute("error", "两次密码不一致");
				setreqatu(oldpassword, newpassword, passwordagain, request, response);
				doGet(request, response);
				return;
			}
			String md5passed=StringUtils.md5Encode(newpassword);
			String md5passold=StringUtils.md5Encode(oldpassword);
			System.out.println(user);
			System.out.println("md5passold:"+md5passold);
			System.out.println("user.getPasswdmd5"+user.getPasswdmd5());
			if (!md5passold.equals(user.getPasswdmd5())) {
				request.setAttribute("error", "输入旧密码有误");
				setreqatu(oldpassword, newpassword, passwordagain, request, response);
				doGet(request, response);
				return;
			}
			
			System.out.println(newpassword);
			System.out.println(user.getUserId());
			if (udao.updatepassword(newpassword, user.getUserId(),md5passed)) {
				request.setAttribute("updatemessage", "true");
				doGet(request, response);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	public void setreqatu(String oldpassword, String newpassword, String passwordagain, HttpServletRequest request,
			HttpServletResponse response) {
		request.setAttribute("oldpassword", oldpassword);
		request.setAttribute("newpassword", newpassword);
		request.setAttribute("passwordagain", passwordagain);
	}

}
