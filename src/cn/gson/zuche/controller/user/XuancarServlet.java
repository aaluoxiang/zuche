package cn.gson.zuche.controller.user;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cn.gson.zuche.common.StringUtils;
import cn.gson.zuche.model.bean.Cartype;
import cn.gson.zuche.model.dao.CartypeDao;

@WebServlet("/mainjump")
public class XuancarServlet extends HttpServlet{
	private CartypeDao  cdao=new  CartypeDao();

@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	List<Cartype> chexinxi=null;
	HttpSession session=req.getSession();
	Object userId=session.getAttribute("userId");
	String quche1=(String) session.getAttribute("quSelect");
	String quche2=(String) session.getAttribute("send");
	String huanche1=(String) session.getAttribute("haiSelect");
	String huanche2=(String) session.getAttribute("repay");
	String stime1=(String) session.getAttribute("sendDate");
	String stime2=session.getAttribute("sendTime")+"";
	String btime2=(String) session.getAttribute("repayDate");
	String btime1=session.getAttribute("repayTime")+"";
	String carname=req.getParameter("carname");
	
	
	
	
	if(StringUtils.isEmpty(quche2)||StringUtils.isEmpty(huanche2)){
		req.setAttribute("quche2","天元区湖南工业大学");
		req.setAttribute("huanche2", "天元区湖南工业大学");
	}
	else{
		req.setAttribute("userId", userId);
		req.setAttribute("qudate", "$('.senttime_2').val('"+stime1+"')");
		req.setAttribute("haidate", "$('.backtime_2').val('"+btime2+"')");
		req.setAttribute("qucity", "$('.quche_2').val('"+quche1+"')");
		req.setAttribute("haicity","$('.huanche_2').val('"+huanche1+"')");
		req.setAttribute("qutime", "$('.senttime_3').val('"+stime2+"')");
		req.setAttribute("haitime", "$('.backtime_3').val('"+btime1+"')");
		req.setAttribute("quche2", quche2);
		req.setAttribute("huanche2", huanche2);
	}
	
	try {
		List<Cartype> che= cdao.findPinpai();
		if(!StringUtils.isEmpty(carname)){
		chexinxi=cdao.findchexinxi(carname);
		req.setAttribute("chexinxi", chexinxi);
		}else{
		chexinxi=cdao.findche();
		req.setAttribute("chexinxi", chexinxi);
		req.setAttribute("che", che);
		}
	} catch (SQLException e) {
		e.printStackTrace();
	}
	req.getRequestDispatcher("/WEB-INF/jsp/shangcheng.jsp").forward(req, resp);
}
	
@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	
		doGet(req, resp);
	}
}
