package cn.gson.zuche.controller.user;

import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cn.gson.zuche.model.bean.Cartype;
import cn.gson.zuche.model.bean.Ordercomment;
import cn.gson.zuche.model.bean.Userorder;
import cn.gson.zuche.model.dao.CartypeDao;
import cn.gson.zuche.model.dao.OrdercommentDao;
import cn.gson.zuche.model.dao.Userorderdao;

/**
 * Servlet implementation class OrdercommentController
 */
@WebServlet("/ordercomment")
public class OrdercommentController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	Userorderdao uoddao = new Userorderdao();
	CartypeDao cartpdao = new CartypeDao();
	OrdercommentDao odcomdao = new OrdercommentDao();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			HttpSession session = request.getSession();
			String userid = session.getAttribute("userId") + "";
			String orderid = request.getParameter("orderid");
			Userorder userorder = uoddao.findAllByorderId(orderid).get(0);
			Cartype cartype = cartpdao.findallbyname(userorder.getOrdercartype()).get(0);
			request.setAttribute("cartype", cartype);
			request.setAttribute("userid", userid);
			request.setAttribute("userorder", userorder);
			// System.out.println(orderid);
			// System.out.println(userid);
			// System.out.println(userorder);
			// System.out.println(cartype);
			request.getRequestDispatcher("/WEB-INF/jsp/ordercomment.jsp").forward(request, response);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			request.setCharacterEncoding("UTF-8");
			response.setCharacterEncoding("UTF-8");
			String pldj = request.getParameter("pldj");
			String cartypename = request.getParameter("cartypename");
			String orderid = request.getParameter("orderid");
			String plcon = request.getParameter("plcon");
			String userid = request.getParameter("userid");
			SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String commenttime = s.format(new Date());
			Ordercomment odcom = new Ordercomment();
			odcom.setCommenttime(commenttime);
			odcom.setCommentcar(cartypename);
			odcom.setCommentcon(plcon);
			if(!"请选择".equals(pldj)){
				odcom.setCommenttype(pldj);
			}else {
				odcom.setCommenttype(5+"");
			}
			odcom.setUserodid(orderid);
			odcom.setUserid(userid);
			System.out.println(odcom.getUserid());
			if (odcomdao.insertcomment(odcom)) {
				request.setAttribute("plok", "评论成功！~");
				doGet(request, response);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
