package cn.gson.zuche.controller.user;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cn.gson.zuche.model.bean.User;
import cn.gson.zuche.model.bean.UserType;
import cn.gson.zuche.model.dao.UserDao;
import cn.gson.zuche.model.dao.UserTypeDao;

/**
 * Servlet implementation class MyLevelController
 */
@WebServlet("/mylevel")
public class MyLevelController extends HttpServlet {

	UserTypeDao usertpdao = new UserTypeDao();
	UserDao udao = new UserDao();
	UserTypeDao utypedao = new UserTypeDao();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			HttpSession session = request.getSession();
			Long userid = Long.parseLong( session.getAttribute("userId")+"");
			List<User> users = udao.findAllById(userid);
			User user = users.get(0);
			List<UserType> usertypes = usertpdao.findAll();
			String upjf = (String) utypedao.findupjf(user.getUserType()+"").get(0);
			request.setAttribute("upjf", upjf);
			request.setAttribute("user", user);
			request.setAttribute("usertypes", usertypes);
			request.setAttribute("userType", user.getUserType());
			request.getRequestDispatcher("/WEB-INF/jsp/mylevel.jsp").forward(request, response);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
