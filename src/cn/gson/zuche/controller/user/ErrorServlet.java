package cn.gson.zuche.controller.user;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.gson.zuche.common.Check;
import cn.gson.zuche.common.IDCardValidate;
import cn.gson.zuche.common.StringUtils;
import cn.gson.zuche.model.bean.Lxr;
import cn.gson.zuche.model.dao.Lxrdao;

/**
 * Servlet implementation class ErrorServlet
 */
@WebServlet("/error")
public class ErrorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Lxrdao  ldao=new Lxrdao();
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		PrintWriter out=response.getWriter();
		String id = request.getParameter("id");
		String xingming=request.getParameter("xingming");
		String dianhua=request.getParameter("dianhua");
		String zhengjian=request.getParameter("zhengjian");
		String youxiang=request.getParameter("youxiang");
		if("checkname".equals(id)) {
			if(StringUtils.isEmpty(xingming)){
				out.print("请输入真实姓名！");
			}else if(StringUtils.isNumeric(xingming)){
				out.print("姓名不能为数字！");
			}else{
				out.print("success");
				
			}
		}

		if("checkphone".equals(id)) {
			if(StringUtils.isEmpty(dianhua)){
				out.print("请输入联系电话!");
			}else if(StringUtils.isMobileNO(dianhua)==false){
			
				out.print("请输入正确的电话!");
			}else{
				out.print("success");
				
			}
		}
		try {
			List<Lxr>   lxr=ldao.finallbylxrsfz(zhengjian);
			if("checkcard".equals(id)) {
				if(StringUtils.isEmpty(zhengjian)){
					out.print("请输入证件号码！");
				}else if(IDCardValidate.validate(zhengjian)==false){
					out.print("请输入正确的身份证号！");
				}else if(lxr.size()>0){
					System.out.println("fewfw");
					out.print("此身份证号已被占用!");
				}else{
					out.print("success");
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		if("checkemail".equals(id)) {
		 if(Check.checkEmail(youxiang)==false){
				out.print("请输入正确的邮箱！");
			}else{
				out.print("success");
			}
		}
		
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
