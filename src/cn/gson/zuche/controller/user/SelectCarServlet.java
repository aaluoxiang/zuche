package cn.gson.zuche.controller.user;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.gson.zuche.common.StringUtils;

@WebServlet("/selectcar")
public class SelectCarServlet extends HttpServlet {
		@Override
		protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
			req.getRequestDispatcher("/WEB-INF/jsp/main.jsp").forward(req, resp);
		}
		@Override
		protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
			req.setCharacterEncoding("utf-8");
			resp.setCharacterEncoding("utf-8");
			
			String quSelect=req.getParameter("quSelect");
			String haiSelect=req.getParameter("haiSelect");
			String send=req.getParameter("send");
			String repay=req.getParameter("repay");
			
			String sendDate=req.getParameter("d422");
			String repayDate=req.getParameter("d423");
			Integer sendTime=Integer.parseInt(req.getParameter("qutimSelect"));
			Integer repayTime=Integer.parseInt(req.getParameter("haitimSelect"));
			
			if(StringUtils.isEmpty(send)){
				System.out.println("???");
				req.setAttribute("error_qu", "$('.t_map_qu').css('display','block')");
				doGet(req, resp);
				return;
			}
			if(StringUtils.isEmpty(repay)){
				req.setAttribute("error_hai", "$('.t_map_hai').css('display','block')");
				doGet(req, resp);
				return;
			}
			
			if(sendDate.equals(repayDate)){
				if(sendTime>=repayTime){
					req.setAttribute("error_time_less", "$('.t_time_more').css('display','none');$('.t_time_day').css('display','none');$('.t_time_less').css('display','block');$('.t_time_hour').css('display','none');");
					doGet(req, resp);
					return;
				}
			}
			/*
			System.out.println(quSelect);
			System.out.println(haiSelect);
			System.out.println(send);
			System.out.println(repay);
			System.out.println(sendDate);
			System.out.println(repayDate);
			System.out.println(sendTime);
			System.out.println(repayTime);*/
			
			resp.sendRedirect(req.getContextPath()+"/shangcheng");			
		}
}
