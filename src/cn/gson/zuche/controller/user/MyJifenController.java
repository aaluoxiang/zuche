package cn.gson.zuche.controller.user;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cn.gson.zuche.model.bean.User;
import cn.gson.zuche.model.bean.Xiaofei;
import cn.gson.zuche.model.dao.UserDao;
import cn.gson.zuche.model.dao.UserTypeDao;
import cn.gson.zuche.model.dao.Userorderdao;
import cn.gson.zuche.model.dao.XiaofeiDao;

/**
 * Servlet implementation class MyLevelController
 */
@WebServlet("/myjifen")
public class MyJifenController extends HttpServlet {

	UserDao udao = new UserDao();
	UserTypeDao utypedao = new UserTypeDao();
	Userorderdao uorderdao = new Userorderdao();
	XiaofeiDao xfdao= new XiaofeiDao();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			HttpSession session = request.getSession();
			Long userid = Long.parseLong(session.getAttribute("userId") + "");
			User user = udao.findAllById(userid).get(0);
			String upjf = (String) utypedao.findupjf(user.getUserType()+"").get(0);
			List list = uorderdao.findokorderbyuserid(userid);
			System.out.println(list);
			List<Xiaofei> sfjls = null;
			if (list .size()!=0 ) {
				System.out.println(1);
				sfjls=xfdao.findAllxfjl(list);
			}
			System.out.println(sfjls);
			request.setAttribute("sfjls", sfjls);
			request.setAttribute("upjf", upjf);
			request.setAttribute("user", user);
			request.getRequestDispatcher("/WEB-INF/jsp/myjifen.jsp").forward(request, response);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}



}
