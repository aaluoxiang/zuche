package cn.gson.zuche.controller.user;

import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cn.gson.zuche.common.StringUtils;
import cn.gson.zuche.model.bean.Cartype;
import cn.gson.zuche.model.bean.Orderxiaofeijilu;
import cn.gson.zuche.model.bean.Orderzt;
import cn.gson.zuche.model.bean.User;
import cn.gson.zuche.model.bean.UserType;
import cn.gson.zuche.model.bean.Userorder;
import cn.gson.zuche.model.dao.CartypeDao;
import cn.gson.zuche.model.dao.Orderxiaofeijiludao;
import cn.gson.zuche.model.dao.OrderztDao;
import cn.gson.zuche.model.dao.UserDao;
import cn.gson.zuche.model.dao.UserTypeDao;
import cn.gson.zuche.model.dao.Userorderdao;

/**
 * Servlet implementation class YdchenggongServlet
 */
@WebServlet("/chenggong")
public class YdchenggongServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private CartypeDao  cdao=new  CartypeDao();
	private UserDao  udao=new  UserDao();
	private UserTypeDao  utdao=new UserTypeDao();
	private Userorderdao  uddao=new Userorderdao();
	private OrderztDao  od=new OrderztDao();
	private Orderxiaofeijiludao xfdao=new Orderxiaofeijiludao();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		HttpSession session=request.getSession();
		Long userId=Long.parseLong(session.getAttribute("userId")+"");
		String ordernum=request.getParameter("ordernum");
		request.setAttribute("ordernum", ordernum);

		
		try {
			List<User> userxinxi=udao.selectxinxi(userId);
			List<Userorder> userorder=uddao.findxinxi(ordernum);
			List<Cartype> xinxi=cdao.findxinxi(userorder.get(0).getOrdercartype());
			List<User>   user=udao.findAllById(userId);
			List<UserType> zk=utdao.findZk(user.get(0).getUserType()+"");
			int  y= (int) (Integer.parseInt(xinxi.get(0).getCarPrice())*zk.get(0).getUsertypecut());
		
			
			String	name=userxinxi.get(0).getUserName();
			String glname=StringUtils.name(name);

			SimpleDateFormat s=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//先设置格式化
			String getcartime=userorder.get(0).getGettime()+"";//这个是数据库拿的带点的
			String backcartime=userorder.get(0).getBacktime()+"";
			String gshtime = (String) getcartime.subSequence(0, getcartime.length()-2);//把点去掉 
			String bshtime = (String) backcartime.subSequence(0, backcartime.length()-2);//把点去掉
		
			
			Long l=s.parse(bshtime).getTime()-s.parse(gshtime).getTime();
			
			int i=(int)(l/(3600*1000*24));//天数
			int d=(int)((l-i*3600*1000*24)/(3600*1000));//小时
			if(d>=4){
				i=i+1;
			}
			
			int  x= (int) (Integer.parseInt(xinxi.get(0).getCarPrice())*i*zk.get(0).getUsertypecut());
			String orderzt=userorder.get(0).getUserorderzt();
			Integer ztid=Integer.valueOf(orderzt);
			List<Orderzt> ztname=od.findztname(ztid);
			//获取总价根据状态封装
			if(ztname.get(0).getOrderztname().equals("已完成")){
				List<Orderxiaofeijilu> xiaofei=xfdao.selectsum(ordernum);			
				StringBuffer zongjia=new StringBuffer(xiaofei.get(0).getOrdermoney());
				request.setAttribute("zongjia", zongjia.subSequence(0, zongjia.length()-3));
			}else{
				String zongjia=userorder.get(0).getOdermoney();
				request.setAttribute("zongjia", zongjia);
				
			}

			request.setAttribute("ztname",ztname.get(0).getOrderztname());
			request.setAttribute("orderzt",orderzt);
			request.setAttribute("price",x);
			request.setAttribute("carprice", y);
			request.setAttribute("zuqi", i);
			request.setAttribute("gshtime", gshtime);
			request.setAttribute("bshtime", bshtime);
			request.setAttribute("youhuimoney", userorder.get(0).getOrderyhqid());	
			request.setAttribute("mianpei", userorder.get(0).getOrdermianpei());
			request.setAttribute("carname",xinxi.get(0).getCarTypeName());
			request.setAttribute("tupian", xinxi.get(0).getCarImageUrl());
			request.setAttribute("xinxi", xinxi);
			request.setAttribute("userorder", userorder);
			request.setAttribute("name", name);
			request.setAttribute("glname", glname);
			
		
			request.getRequestDispatcher("WEB-INF/jsp/yuding.jsp").forward(request, response);
		} catch (Exception e) {
			
			e.printStackTrace();
		}
	}

	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}		
}
