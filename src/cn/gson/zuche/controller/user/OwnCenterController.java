package cn.gson.zuche.controller.user;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cn.gson.zuche.common.IDCardValidate;
import cn.gson.zuche.common.StringUtils;
import cn.gson.zuche.model.bean.User;
import cn.gson.zuche.model.dao.UserDao;
import cn.gson.zuche.model.dao.UserTypeDao;
import cn.gson.zuche.model.dao.UserYhqDao;

/**
 * Servlet implementation class OwnCenterController
 */
@WebServlet("/owncenter")
public class OwnCenterController extends HttpServlet {

	UserDao udao = new UserDao();
	UserTypeDao utypedao = new UserTypeDao();
	UserYhqDao uyhqdao= new UserYhqDao();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		Long userId = Long.parseLong(session.getAttribute("userId")+"");
		try {
			List<User> users = udao.findAllById(userId);
			
			User user = users.get(0);
			int userjf = user.getUserJf();
			List yhqnums = uyhqdao.findyhqcount(userId);
			Integer yuqnum = Integer.parseInt(yhqnums.get(0)+"");
			udao.upyhqnum(userId, yuqnum);
			user.setUserYhq(yuqnum);
			System.out.println(userjf);
			System.out.println(user);
			if(userjf<2000){
				udao.upusertype(userId,1);
				user.setUserType("普卡会员");
			}else if (2000 <= userjf && userjf < 5000) {
				udao.upusertype(userId,2);
				user.setUserType("银卡会员");
			}else if (5000<= userjf && userjf<9000){
				udao.upusertype(userId,3);
				user.setUserType("黄金会员");
			}else if (9000<= userjf && userjf<15000){
				udao.upusertype(userId,4);
				user.setUserType("白金会员");
			}else if (15000<= userjf){
				udao.upusertype(userId,5);
				user.setUserType("钻石会员");
			}
			request.setAttribute("user", user);
			String upjf = utypedao.findupjf(user.getUserType()+"").get(0)+"";
			request.setAttribute("upjf", upjf);
			/*
			 * if(user.getUserSfz()!=null && user.getUserSfz().length()>0 ){
			 * String sfz = user.getUserSfz().substring(0,
			 * 6)+"****"+user.getUserSfz().substring(14);
			 * request.setAttribute("sfz", sfz); } String tel =
			 * user.getUserTel().substring(0,
			 * 3)+"****"+user.getUserTel().substring(7);
			 * request.setAttribute("tel", tel);
			 */
			request.getRequestDispatcher("/WEB-INF/jsp/owncenter.jsp").forward(request, response);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			request.setCharacterEncoding("UTF-8");
			HttpSession session = request.getSession();
			Long userid = Long.parseLong(session.getAttribute("userId") + "");
			String yhname = request.getParameter("yhname");
			String zjlxxz = request.getParameter("zjlxxz");
			String yhsfz = request.getParameter("yhsfz");
			String yhtel = request.getParameter("yhtel");
			String e_mail = request.getParameter("e_mail");
			if (StringUtils.isEmpty(yhname)) {
				request.setAttribute("error", "姓名不能为空");
				doGet(request, response);
				return;
			}

			if ("请选择有效证件".equals(zjlxxz)) {
				request.setAttribute("error", "请选择证件类型");
				doGet(request, response);
				return;
			}

			if (!IDCardValidate.validate(yhsfz)) {
				request.setAttribute("error", "错误身份证");
				doGet(request, response);
				return;
			}

			// 接下来进行数据封装 插入数据库
			User u = new User();
			u.setUserId(userid);
			u.setUserName(yhname);
			u.setUserZjlx(zjlxxz);
			u.setUserSfz(yhsfz);
			u.setUserTel(yhtel);
			u.setUserEmail(e_mail);
			// System.out.println(u);
			if (udao.updatemyinfor(u)) {
				request.setAttribute("updatemessage", "true");
				doGet(request, response);
				return;
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
