package cn.gson.zuche.controller.user;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.gson.zuche.model.bean.Cartype;
import cn.gson.zuche.model.bean.Ordercomment;
import cn.gson.zuche.model.dao.CartypeDao;
import cn.gson.zuche.model.dao.OrdercommentDao;

/**
 * Servlet implementation class CarinfoController
 */
@WebServlet("/carinfo")
public class CarinfoController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	CartypeDao cartpdao = new CartypeDao();
	OrdercommentDao odcomdao = new OrdercommentDao();
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			request.setCharacterEncoding("Utf-8");
			response.setCharacterEncoding("Utf-8");
			String cartpname = request.getParameter("cartpname");
			System.out.println(cartpname);
			Cartype cartype= cartpdao.findallbyname(cartpname).get(0);
			System.out.println(cartype);
			List<Ordercomment> carcoms=odcomdao.findbycartpname(cartpname);
			System.out.println(carcoms);
			request.setAttribute("cartype", cartype);
			request.setAttribute("carcoms", carcoms);
			request.getRequestDispatcher("/WEB-INF/jsp/carinfo.jsp").forward(request, response);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
