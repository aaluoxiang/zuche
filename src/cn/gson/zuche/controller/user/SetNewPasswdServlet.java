package cn.gson.zuche.controller.user;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.gson.zuche.common.StringUtils;
import cn.gson.zuche.model.bean.User;
import cn.gson.zuche.model.dao.UserDao;

@WebServlet("/newpasswd")
public class SetNewPasswdServlet extends HttpServlet {
	UserDao udao = new UserDao();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("utf-8");
		resp.setCharacterEncoding("utf-8");
		PrintWriter out = resp.getWriter();
		String newpasswd=req.getParameter("onepassword");
		String userTel=req.getParameter("usertel");
		
		try {
			String md5passwd=StringUtils.md5Encode(newpasswd);
			boolean users = udao.newpassword(newpasswd,md5passwd,userTel);
			if(users){
				System.out.println("修改密码成功");
				out.print("true");
			}else{
				System.out.println("修改密码失败sfasdsdfa");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
