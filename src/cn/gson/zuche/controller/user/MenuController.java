package cn.gson.zuche.controller.user;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class MenuController
 */
@WebServlet({"/menu","/menu/*"})
public class MenuController extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String datamenu = request.getParameter("datamenu");
		
		switch (datamenu) {
		case "1":
			response.sendRedirect(request.getContextPath()+"/owncenter");
			break;
		case "2":
			response.sendRedirect(request.getContextPath()+"/updatepassword");
			break;
		case "3":
			response.sendRedirect(request.getContextPath()+"/mylevel");
			break;
		case "4":
			response.sendRedirect(request.getContextPath()+"/mybalancemoney");
			break;
		case "5":
			response.sendRedirect(request.getContextPath()+"/myyhq");
			break;
		case "6":
			response.sendRedirect(request.getContextPath()+"/myjifen");
			break;
		case "7":
			response.sendRedirect(request.getContextPath()+"/myorder");
			break;
		case "8":
			response.sendRedirect(request.getContextPath()+"/myatten");
			break;

		default:
			break;
		}
	}
}
