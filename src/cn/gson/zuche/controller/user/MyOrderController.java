package cn.gson.zuche.controller.user;

import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cn.gson.zuche.common.StringUtils;
import cn.gson.zuche.model.bean.Orderxiaofeijilu;
import cn.gson.zuche.model.bean.User;
import cn.gson.zuche.model.bean.Userorder;
import cn.gson.zuche.model.dao.Orderxiaofeijiludao;
import cn.gson.zuche.model.dao.UserDao;
import cn.gson.zuche.model.dao.UserTypeDao;
import cn.gson.zuche.model.dao.Userorderdao;

/**
 * Servlet implementation class MyLevelController
 */
@WebServlet("/myorder")
public class MyOrderController extends HttpServlet {

	/*
	 * 需要 车型名 、车短名 、订单信息； 需要查询 car_type 、user_order
	 */
	Userorderdao uorderdao = new Userorderdao();
	UserDao udao = new UserDao();
	UserTypeDao utypedao = new UserTypeDao();
	Orderxiaofeijiludao xfjldao = new Orderxiaofeijiludao();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			HttpSession session = request.getSession();
			String userid = session.getAttribute("userId") + "";
			Long userId = Long.parseLong(userid);
			User user = udao.findAllById(userId).get(0);
			int userjf = user.getUserJf();
			System.out.println(userjf);
			if(userjf<2000){
				udao.upusertype(userId,1);
				user.setUserType("普卡会员");
			}else if (2000 <= userjf && userjf < 5000) {
				udao.upusertype(userId,2);
				user.setUserType("银卡会员");
			}else if (5000<= userjf && userjf<9000){
				udao.upusertype(userId,3);
				user.setUserType("黄金会员");
			}else if (9000<= userjf && userjf<15000){
				udao.upusertype(userId,4);
				user.setUserType("白金会员");
			}else if (15000<= userjf){
				udao.upusertype(userId,5);
				user.setUserType("钻石会员");
			}
			System.out.println(user.getUserType());
			
			Date date = new Date();
			Long nowcutodertime ;
			Long nowtime = date.getTime();
			Long oneday = (long) (24*3600*1000);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			List<Userorder> orderdfk = uorderdao.ownfinddfkorder(userid);
			for (Userorder userorder : orderdfk) {
				nowcutodertime =nowtime - sdf.parse(userorder.getOrdersingletime()+"").getTime();
				if (nowcutodertime>oneday) {
					uorderdao.centerQuitoeder(userorder.getOrderid());
				}
			}
			int page = 1;
			if (request.getParameter("page") != null) {
				page = Integer.parseInt(request.getParameter("page"));		
			}
			Integer count = (page - 1) * 4;
			List<Userorder> userorders = uorderdao.ownfindorder(userid,count+"");
			for (Userorder userorder : userorders) {
				if ("已完成".equals(userorder.getUserorderzt())) {
					Orderxiaofeijilu odxfji = xfjldao.selectmoney(userorder.getOrderid()).get(0);
					userorder.setOdermoney(odxfji.getOrdermoney().substring(0,odxfji.getOrdermoney().length()-2));
				}
			}
			List<Userorder> userdfkorders = uorderdao.ownfinddfkorder(userid);
			List<Userorder> userywcorders = uorderdao.ownfindywcorder(userid);
			for (Userorder userorder : userywcorders) {
				Orderxiaofeijilu odxfji = xfjldao.selectmoney(userorder.getOrderid()).get(0);
				userorder.setOdermoney(odxfji.getOrdermoney().substring(0,odxfji.getOrdermoney().length()-2));
			}
			List<Userorder> useryqxorders = uorderdao.ownfindYqxorder(userid);
			List<Userorder> userzlzorders = uorderdao.ownfindZlzorder(userid);
			List<Userorder> userclzorders = uorderdao.ownfindclzorder(userid);
			Object obj = utypedao.findupjf(user.getUserType() + "");
			System.out.println(obj);
			String upjf = (String) utypedao.findupjf(user.getUserType() + "").get(0);
			
			int ordercount = uorderdao.findAllorder(userid).size();
			int pagecount =  (int) Math.ceil((double)ordercount / 4);

			request.setAttribute("upjf", upjf);
			request.setAttribute("user", user);
			
			request.setAttribute("page", page);
			request.setAttribute("ordercount", ordercount);
			request.setAttribute("pagecount", pagecount);
			
			
			request.setAttribute("userorders", userorders);
			request.setAttribute("userzlzorders", userzlzorders);
			request.setAttribute("userclzorders", userclzorders);
			request.setAttribute("userdfkorders", userdfkorders);
			request.setAttribute("useryqxorders", useryqxorders);
			request.setAttribute("userywcorders", userywcorders);
			request.getRequestDispatcher("/WEB-INF/jsp/myorder.jsp").forward(request, response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			request.setCharacterEncoding("Utf-8");
			response.setCharacterEncoding("Utf-8");
			String orderid = request.getParameter("orderid");
			String orderzt = request.getParameter("orderzt");

			System.out.println(orderid);
			System.out.println(orderzt);
			if (!StringUtils.isEmpty(orderzt)) {
				if ("租赁中".equals(orderzt)) {
					uorderdao.hcbyorderid(orderid);
					doGet(request, response);
					return;
				}
				if ("已付款".equals(orderzt)) {
					SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					String gettime = request.getParameter("gettime");
					String gshtime = (String) gettime.subSequence(0, gettime.length() - 2);
					Calendar nowtime = Calendar.getInstance();
					Long tq = s.parse(gshtime).getTime() - nowtime.getTimeInMillis();
					int aa = (int) (tq / (1000 * 3600));
					System.out.println(aa);
					if (aa < 2) {
						request.setAttribute("error", "取车时间前两小时不能退单~如需退单请致电18890289825(熊管理)");
						doGet(request, response);
						return;
					}
					//
					// System.out.println(gett);
					uorderdao.quitmoney(orderid);
					doGet(request, response);
					return;
				}

			}
			if (uorderdao.centerQuitoeder(orderid)) {
				doGet(request, response);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
