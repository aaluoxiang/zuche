package cn.gson.zuche.controller.user;

import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cn.gson.zuche.model.bean.Orderxiaofeijilu;
import cn.gson.zuche.model.bean.User;
import cn.gson.zuche.model.bean.Userorder;
import cn.gson.zuche.model.dao.Lingshibiaodao;
import cn.gson.zuche.model.dao.Orderxiaofeijiludao;
import cn.gson.zuche.model.dao.UserDao;
import cn.gson.zuche.model.dao.Userorderdao;
import jdk.nashorn.internal.ir.RuntimeNode.Request;

/**
 * Servlet implementation class PayController
 */
@WebServlet("/pay")
public class PayController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	Userorderdao uorderdao = new Userorderdao();
	UserDao udao = new UserDao();
	Orderxiaofeijiludao odxfjldao = new Orderxiaofeijiludao();
	Lingshibiaodao lsbdao = new Lingshibiaodao();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			String orderid = request.getParameter("orderid");
			Userorder userorder = uorderdao.findAllByorderId(orderid).get(0);
			request.setAttribute("userorder", userorder);
			request.getRequestDispatcher("/WEB-INF/jsp/orderpay.jsp").forward(request, response);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			HttpSession session = request.getSession();
			Long userid = Long.parseLong(session.getAttribute("userId") + "");
			String orderid = request.getParameter("orderid");
			Double paymoney = Double.parseDouble(request.getParameter("paymoney"));
			String payfs = request.getParameter("payfs");
			if ("yue".equals(payfs)) {
				User user = udao.findAllById(userid).get(0);
				Double usermoney = user.getUserMoney();
				if (user.getUserMoney() < paymoney) {
					request.setAttribute("payerror", "用户余额不足请更换支付方式或者前往门店充值！~");
					doGet(request, response);
					return;
				}
				Double newmoney = usermoney - paymoney;
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

				// 封装
				Orderxiaofeijilu odxfjl = new Orderxiaofeijilu();
				odxfjl.setOrderid(orderid);
				odxfjl.setOrderjiaoyilaiyuan("余额");
				odxfjl.setOrdermoney(paymoney + "");
				odxfjl.setOrderdata(sdf.format(new Date()));
				System.out.println(odxfjl.getOrderdata());
				if (udao.updatemoneybyid(userid, newmoney) && uorderdao.payorderbyid(orderid)
						&& odxfjldao.insert(odxfjl) && lsbdao.insert(orderid)) {
					request.setAttribute("paymessage", "支付成功！~~~");
					doGet(request, response);
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
