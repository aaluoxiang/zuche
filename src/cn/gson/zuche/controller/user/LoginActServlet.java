package cn.gson.zuche.controller.user;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cn.gson.zuche.common.StringUtils;
import cn.gson.zuche.model.bean.Cartype;
import cn.gson.zuche.model.bean.User;
import cn.gson.zuche.model.dao.CartypeDao;
import cn.gson.zuche.model.dao.UserDao;
@WebServlet("/loginact")
public class LoginActServlet extends HttpServlet {
		
		@Override
		protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
			req.setAttribute("error_denglu", "$('.denglu').css('display','block')");
			req.setAttribute("error_fu", "$('.fu').css('display','block')");
			req.getRequestDispatcher("/WEB-INF/jsp/getyhq.jsp").forward(req,resp);

		}
		
		@Override
		protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
			req.setCharacterEncoding("utf-8");
			resp.setCharacterEncoding("utf-8");
			UserDao udao = new UserDao();
			HttpSession session = req.getSession();
			List<User> users = null;
			String userTel=req.getParameter("email");		//用户电话号码
			String password=req.getParameter("password");	//用户密码
			String logincode=req.getParameter("logincode"); //验证码
			String sesionCode = (String) req.getSession().getAttribute(CaptchaServlet.CAPTCHA_KEY);
			
//			System.out.println(sesionCode);
			/*电话号码判空*/
			if(StringUtils.isEmpty(userTel)){
				
				req.setAttribute("error_m_emp", "$('.t_m_emp').css('display','block')");
				doGet(req, resp);
				return;
			}
			if(userTel.length()!=11){
				req.setAttribute("error_m", "$('.t_m').css('display','block')");
				doGet(req, resp);
				return;
			}
			/*输入密码框判空*/
			if(StringUtils.isEmpty(password)){
				req.setAttribute("error_w_emp", "$('.t_w_emp').css('display','block')");
				doGet(req, resp);
				return;
			}
			/*输入验证码框判空*/
			if(StringUtils.isEmpty(logincode)){
				req.setAttribute("error_code_emp", "$('.t_code_emp').css('display','block')");
				doGet(req, resp);
				return;
			}
			/*输入验证码不正确*/
			if(!logincode.equalsIgnoreCase(sesionCode)){
				req.setAttribute("error_code", "$('.t_code').css('display','block')");
				doGet(req, resp);
				return;
			}
			
			try {
				String md5passwd=StringUtils.md5Encode(password);
				users = udao.findAllByPasswdmd5(userTel,md5passwd);
				if(users.size()==0){
					req.setAttribute("error_w", "$('.t_w').css('display','block')");
					doGet(req, resp);
					return;
				}
				
				String name=StringUtils.name(users.get(0).getUserName());
				session.setAttribute("userId", users.get(0).getUserId());
				session.setAttribute("userName",name);
			} catch (Exception e) {
				e.printStackTrace();
			}
			resp.sendRedirect(req.getContextPath()+"/getyhq");
		}
}
