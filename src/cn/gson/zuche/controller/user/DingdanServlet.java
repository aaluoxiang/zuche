package cn.gson.zuche.controller.user;

import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cn.gson.zuche.common.StringUtils;
import cn.gson.zuche.model.bean.Cartype;
import cn.gson.zuche.model.bean.Count;
import cn.gson.zuche.model.bean.Fapiao;
import cn.gson.zuche.model.bean.Lxr;
import cn.gson.zuche.model.bean.User;
import cn.gson.zuche.model.bean.UserType;
import cn.gson.zuche.model.bean.UserYhq;
import cn.gson.zuche.model.bean.Userorder;
import cn.gson.zuche.model.dao.CartypeDao;
import cn.gson.zuche.model.dao.Countdao;
import cn.gson.zuche.model.dao.Fpdao;
import cn.gson.zuche.model.dao.Lxrdao;
import cn.gson.zuche.model.dao.UserDao;
import cn.gson.zuche.model.dao.UserTypeDao;
import cn.gson.zuche.model.dao.UserYhqDao;
import cn.gson.zuche.model.dao.Userorderdao;


/**
 * Servlet implementation class DingdanServlet
 */
@WebServlet("/dingdan")
public class DingdanServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private CartypeDao  cdao=new  CartypeDao();
	private UserDao  udao=new  UserDao();
	private UserTypeDao  utdao=new UserTypeDao();
	private Userorderdao  uddao=new Userorderdao();
	private UserYhqDao  uydao=new UserYhqDao();
	private Lxrdao  ldao=new Lxrdao();
	private Fpdao fdao=new Fpdao();
	private Countdao ctdao=new Countdao();
	private Integer money;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		HttpSession session=request.getSession();
		
		
		Long userId=Long.parseLong(session.getAttribute("userId")+"");
		String name=request.getParameter("name");
		String quche1=request.getParameter("quche1");
		String huanche1=request.getParameter("huanche1");
		String senttime1=request.getParameter("senttime1");
		String backtime1=request.getParameter("backtime1");
		String quche2=request.getParameter("quche2");
		String huanche2=request.getParameter("huanche2");
		String senttime2=request.getParameter("senttime2");
		String backtime2=request.getParameter("backtime2");
		String zuqi=request.getParameter("zuqi");
		try {
			List<UserYhq>  youhui=uydao.findokYhqById(userId);
			List<Count>  counter=ctdao.selectdingdan(userId);
			request.setAttribute("counter", counter.get(0).getCounter());
			List<Lxr>  lxr=ldao.finallbyuserid(userId.toString());
			int x=lxr.size();
			request.setAttribute("l", x);
			request.setAttribute("lxr", lxr);
			request.setAttribute("youhui", youhui);
			if(youhui.size()>0){
			request.setAttribute("cut", youhui.get(0).getYhqtype());
		}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		request.setAttribute("userId", userId);
		request.setAttribute("name",name);
		request.setAttribute("zuqi",zuqi);
		request.setAttribute("quche1", quche1);
		request.setAttribute("huanche1", huanche1);
		request.setAttribute("senttime1", senttime1);
		request.setAttribute("backtime1", backtime1);
		request.setAttribute("quche2", quche2);
		request.setAttribute("huanche2", huanche2);
		request.setAttribute("senttime2", senttime2);
		request.setAttribute("backtime2", backtime2);
		
		try {
			List<Cartype> xinxi=cdao.findxinxi(name);
			List<User>   user=udao.findAllById(userId);
			List<UserType> zk=utdao.findZk(user.get(0).getUserType()+"");
			int  y= (int) (Integer.parseInt(xinxi.get(0).getCarPrice())*zk.get(0).getUsertypecut());
			String tel=user.get(0).getUserTel();
			request.setAttribute("userzt",user.get(0).getUserZt());
			request.setAttribute("userdh",tel);
			String  phone=StringUtils.phone(tel);
			request.setAttribute("userphone", phone);
			request.setAttribute("userphones", tel);
			String xm=user.get(0).getUserName();
			String glname=StringUtils.name(xm);
			request.setAttribute("userxm", xm);
			request.setAttribute("glname", glname);
			request.setAttribute("usersfz", user.get(0).getUserSfz());
			request.setAttribute("useremail", user.get(0).getUserEmail());
			request.setAttribute("carxinxi", xinxi.get(0).getCarXinxi());
			request.setAttribute("carprice", y);
			request.setAttribute("tupian",xinxi.get(0).getCarImageUrl());
			int  x= (int) (Integer.parseInt(xinxi.get(0).getCarPrice())*Integer.parseInt(zuqi)*zk.get(0).getUsertypecut());
			request.setAttribute("price",x);
			request.setAttribute("xinxi", xinxi);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		request.getRequestDispatcher("WEB-INF/jsp/dingdan.jsp").forward(request, response);
	}

	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		HttpSession session=request.getSession();
		
		String userId=request.getParameter("userId");
		String carname=request.getParameter("carname");
		String stime1=request.getParameter("stime1");
		String stime2=request.getParameter("stime2");
		String quche1=request.getParameter("quche1");
		String quche2=request.getParameter("quche2");
		String btime1=request.getParameter("btime1");
		String btime2=request.getParameter("btime2");
		String huanche1=request.getParameter("huanche1");
		String huanche2=request.getParameter("huanche2");
		String username=request.getParameter("username");
		String userphone=request.getParameter("userphone");
		String usercardnum=request.getParameter("usercardnum");
		String useremail=request.getParameter("useremail");
		String siji=request.getParameter("siji");
		String zongjia=request.getParameter("zongjia");
		String mianpei=request.getParameter("mianpei");
		String fptt=request.getParameter("fptt");
		String fpname=request.getParameter("fpname");
		String fpphone=request.getParameter("fpphone");
		String fpyb=request.getParameter("fpyb");
		String fpaddress=request.getParameter("fpaddress");
		String yhqid=request.getParameter("yhqid");
		System.out.println(mianpei);
		
		Date d=new Date();
		SimpleDateFormat f=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String ordernum=StringUtils.getOrdernum();
			
		User u=new User();
		u.setUserName(username);
		u.setUserSfz(usercardnum);
		u.setUserZjlx(userphone);
		u.setUserEmail(useremail);
		
		
		Lxr l=new Lxr();
		l.setLxrname(username);
		l.setLxrphone(userphone);
		l.setLxrsfz(usercardnum);
		l.setLxremail(useremail);
		l.setLxruserid(Integer.parseInt(userId));
		
		
		Fapiao fp=new Fapiao();
		fp.setFptt(fptt);
		fp.setFpuser(fpname);
		fp.setFpphone(fpphone);
		fp.setFpyb(fpyb);
		fp.setFpaddress(fpaddress);
		fp.setFpuserid(Integer.parseInt(userId));
		
		try {
			
			List<Lxr>   lxr=ldao.finallbylxrsfz(usercardnum);
			if(lxr.size()<=0){
				ldao.insertxinyonhu(l);
			}
			if(!fpaddress.equals("undefined")){
				if(fdao.insertfapiao(fp)){
					System.out.println("发票插入成功");
				}
			}
		} catch (NumberFormatException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	
		
		try {
			if(!yhqid.equals("undefined")){
				List<UserYhq> youhuiquan=uydao.findyhqmoney(yhqid);
				money=youhuiquan.get(0).getYhqtype();
			if(uydao.updatenum(yhqid)){
				System.out.println("优惠券更改成功！");
			}
			}
		
			 Userorder ud=new Userorder();
				ud.setUserid(userId);
				ud.setGettime(stime1+" "+stime2);
				ud.setBacktime(btime1+" "+btime2);
				ud.setOdermoney(zongjia);
				ud.setOrderbackaddress(huanche1+huanche2);
				ud.setOrdergetaddress(quche1+quche2);
				ud.setOrderid(ordernum);
				ud.setOrdercarsiji(siji);
				ud.setUserorderzt("4");
				ud.setOrdercartype(carname);
				
				if(!mianpei.equals("undefined") || StringUtils.isEmpty(mianpei)){
					System.out.println(mianpei);
				    ud.setOrdermianpei(mianpei);
				}else{
					ud.setOrdermianpei("");
				}
				ud.setOrdersingletime(f.format(d));
				if(money!=null){
					ud.setOrderyhqid(money);
				}
				ud.setOrderlxrname(username);
				ud.setOrderlxrphone(userphone);
				if(!fpaddress.equals("undefined")){
					ud.setOrderfp("需要发票");
				}else{
					ud.setOrderfp("");
				}
				
			
			if(uddao.saveorder(ud)){
				response.sendRedirect(request.getContextPath()+"/chenggong?ordernum="+ordernum+"");
			}
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
	}

}
