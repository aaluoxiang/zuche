package cn.gson.zuche.controller.user;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cn.gson.zuche.common.StringUtils;
import cn.gson.zuche.model.bean.User;
import cn.gson.zuche.model.bean.UserYhq;
import cn.gson.zuche.model.dao.UserDao;
import cn.gson.zuche.model.dao.UserYhqDao;

@WebServlet("/getyhq")
public class ActGetYhqServlet extends HttpServlet {
	UserDao udao = new UserDao();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session=req.getSession();
		Object userId=session.getAttribute("userId");
		session.setAttribute("yhqid","3");
		req.setAttribute("userId",userId);
		req.getRequestDispatcher("/WEB-INF/jsp/actgetyhq.jsp").forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("utf-8");
		resp.setCharacterEncoding("utf-8");
		
		HttpSession session = req.getSession();
		List<User> users = null;
		
		PrintWriter out = resp.getWriter();
		Object userId=session.getAttribute("userId"); 
		Object yhqid=session.getAttribute("yhqid");
		
		UserYhqDao yhq=new UserYhqDao();
		
		try {
			List<UserYhq> ishas=yhq.findHasYhq(userId,yhqid);
			System.out.println("数据库存在的数量为："+ishas.size());
			System.out.println("yhqID:"+yhqid);
			if(ishas.size()>0){
				System.out.println("存在该优惠券");
				out.print(1);
			}
			else{
				boolean addyhq=yhq.addUseryhq(userId, yhqid);
				if(addyhq){
					System.out.println("该用户不存在该优惠券");
					System.out.println("领取优惠卷成功；");
				}
				out.println(2);
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		/*resp.sendRedirect(req.getContextPath()+"/getyhq");*/
	}
}
