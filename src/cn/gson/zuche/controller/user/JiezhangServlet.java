package cn.gson.zuche.controller.user;

import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cn.gson.zuche.common.StringUtils;
import cn.gson.zuche.model.bean.Fapiao;
import cn.gson.zuche.model.bean.Lxr;
import cn.gson.zuche.model.bean.Orderxiaofeijilu;
import cn.gson.zuche.model.bean.User;
import cn.gson.zuche.model.bean.UserYhq;
import cn.gson.zuche.model.bean.Userorder;
import cn.gson.zuche.model.dao.CartypeDao;
import cn.gson.zuche.model.dao.Countdao;
import cn.gson.zuche.model.dao.Fpdao;
import cn.gson.zuche.model.dao.Lingshibiaodao;
import cn.gson.zuche.model.dao.Lxrdao;
import cn.gson.zuche.model.dao.Orderxiaofeijiludao;
import cn.gson.zuche.model.dao.UserDao;
import cn.gson.zuche.model.dao.UserTypeDao;
import cn.gson.zuche.model.dao.UserYhqDao;
import cn.gson.zuche.model.dao.Userorderdao;

/**
 * Servlet implementation class JiezhangServlet
 */
@WebServlet("/jiezhang")
public class JiezhangServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private CartypeDao cdao = new CartypeDao();
	private UserDao udao = new UserDao();
	private UserTypeDao utdao = new UserTypeDao();
	private Userorderdao uddao = new Userorderdao();
	private UserYhqDao uydao = new UserYhqDao();
	private Lxrdao ldao = new Lxrdao();
	private Fpdao fdao = new Fpdao();
	private Countdao ctdao = new Countdao();
	private Integer money;
	Orderxiaofeijiludao odxfjldao = new Orderxiaofeijiludao();
	Lingshibiaodao lsbdao = new Lingshibiaodao();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		String userId = request.getParameter("userId");
		String carname = request.getParameter("carname");
		String stime1 = request.getParameter("stime1");
		String stime2 = request.getParameter("stime2");

		String quche1 = request.getParameter("quche1");
		String quche2 = request.getParameter("quche2");
		String btime1 = request.getParameter("btime1");
		String btime2 = request.getParameter("btime2");
		String huanche1 = request.getParameter("huanche1");
		String huanche2 = request.getParameter("huanche2");
		String username = request.getParameter("username");
		String userphone = request.getParameter("userphone");
		String usercardnum = request.getParameter("usercardnum");
		String useremail = request.getParameter("useremail");
		String siji = request.getParameter("siji");
		String zongjia = request.getParameter("zongjia");
		String mianpei = request.getParameter("mianpei");
		String fptt = request.getParameter("fptt");
		String fpname = request.getParameter("fpname");
		String fpphone = request.getParameter("fpphone");
		String fpyb = request.getParameter("fpyb");
		String fpaddress = request.getParameter("fpaddress");
		String yhqid = request.getParameter("yhqid");

		Date d = new Date();
		SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		String ordernum = StringUtils.getOrdernum();

		User u = new User();
		u.setUserName(username);
		u.setUserSfz(usercardnum);
		u.setUserZjlx(userphone);
		u.setUserEmail(useremail);

		Lxr l = new Lxr();
		l.setLxrname(username);
		l.setLxrphone(userphone);
		l.setLxrsfz(usercardnum);
		l.setLxremail(useremail);
		l.setLxruserid(Integer.parseInt(userId));

		Fapiao fp = new Fapiao();
		fp.setFptt(fptt);
		fp.setFpuser(fpname);
		fp.setFpphone(fpphone);
		fp.setFpyb(fpyb);
		fp.setFpaddress(fpaddress);
		fp.setFpuserid(Integer.parseInt(userId));

		try {

			List<Lxr> lxr = ldao.finallbylxrsfz(usercardnum);
			if (lxr.size() <= 0) {
				ldao.insertxinyonhu(l);
			}
			if (!"undefined".equals(fpaddress)) {
				if (fdao.insertfapiao(fp)) {
					System.out.println("发票插入成功");
				}
			}
		} catch (NumberFormatException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			if (!yhqid.equals("undefined")) {
				List<UserYhq> youhuiquan = uydao.findyhqmoney(yhqid);
				money = youhuiquan.get(0).getYhqtype();
				if (uydao.updatenum(yhqid)) {
					System.out.println("优惠券更改成功！");
				}
			}

			Userorder ud = new Userorder();
			ud.setUserid(userId);
			ud.setGettime(stime1 + " " + stime2);
			ud.setBacktime(btime1 + " " + btime2);
			ud.setOdermoney(zongjia);
			ud.setOrderbackaddress(huanche1 + huanche2);
			ud.setOrdergetaddress(quche1 + quche2);
			ud.setOrderid(ordernum);
			ud.setOrdercarsiji(siji);
			ud.setUserorderzt("5");
			ud.setOrdercartype(carname);
			if (!mianpei.equals("undefined")) {
				ud.setOrdermianpei(mianpei);
			} else {
				ud.setOrdermianpei("");
			}
			ud.setOrdersingletime(f.format(d));
			if (money != null) {
				ud.setOrderyhqid(money);
			}
			ud.setOrderlxrname(username);
			ud.setOrderlxrphone(userphone);
			if (!"undefined".equals(fpaddress)) {
				ud.setOrderfp("需要发票");
			} else {
				ud.setOrderfp("");
			}
			request.setAttribute("userorder", ud);
			System.out.println(ud);
			request.getRequestDispatcher("/WEB-INF/jsp/orderqxpay.jsp").forward(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			String orderid = request.getParameter("orderid");
			String userorderzt = request.getParameter("userorderzt");
			String odermoney = request.getParameter("odermoney");
			String usertel = request.getParameter("usertel");
			String usersfz = request.getParameter("usersfz");
			String userid = request.getParameter("userid");
			String gettime = request.getParameter("gettime");
			String backtime = request.getParameter("backtime");
			String ordergetaddress = request.getParameter("ordergetaddress");
			String orderbackaddress = request.getParameter("orderbackaddress");
			String orderdriverid = request.getParameter("orderdriverid");
			String ordercarid = request.getParameter("ordercarid");
			String orderstoreid = request.getParameter("orderstoreid");
			String odid = request.getParameter("orderyhqid");
			Integer orderyhqid = null;
			if (!StringUtils.isEmpty(odid)) {
				orderyhqid = Integer.parseInt(request.getParameter("orderyhqid") + "");
			}
			String ordercommentid = request.getParameter("ordercommentid");
			String orderdelayid = request.getParameter("orderdelayid");
			String ordercartype = request.getParameter("ordercartype");
			String price = request.getParameter("price");
			String ordercarsiji = request.getParameter("ordercarsiji");
			String ordermianpei = request.getParameter("ordermianpei");
			String ordersingletime = request.getParameter("ordersingletime");
			String orderlxrname = request.getParameter("orderlxrname");
			String orderlxrphone = request.getParameter("orderlxrphone");
			String orderfp = request.getParameter("orderfp");

			Userorder userorder = new Userorder();
			userorder.setOrderid(orderid);
			userorder.setUserorderzt(userorderzt);
			userorder.setOdermoney(odermoney);
			userorder.setUsertel(usertel);
			userorder.setUsersfz(usersfz);
			userorder.setUserid(userid);
			userorder.setGettime(gettime);
			userorder.setBacktime(backtime);
			userorder.setOrdergetaddress(ordergetaddress);
			userorder.setOrderbackaddress(orderbackaddress);
			userorder.setOrderdriverid(orderdriverid);
			userorder.setOrdercarid(ordercarid);
			userorder.setOrderstoreid(orderstoreid);
			userorder.setOrderyhqid(orderyhqid);
			userorder.setOrdercartype(ordercartype);
			userorder.setOrdercarsiji(ordercarsiji);
			userorder.setOrdermianpei(ordermianpei);
			userorder.setOrdersingletime(ordersingletime);
			userorder.setOrderlxrname(orderlxrname);
			userorder.setOrderlxrphone(orderlxrphone);
			userorder.setOrderfp(orderfp);

			String payfs = request.getParameter("payfs");
			System.out.println(payfs);
			System.out.println(userorder);
			Long uid = Long.parseLong(userid);
			Double paymoney = Double.parseDouble(odermoney);
			if ("yue".equals(payfs)) {
				User user;
				user = udao.findAllById(uid).get(0);
				Double usermoney = user.getUserMoney();
				if (user.getUserMoney() < paymoney) {
					request.setAttribute("payerror", "用户余额不足请更换支付方式或者前往门店充值！~");
					doGet(request, response);
					return;
				}

				Double newmoney = usermoney - paymoney;
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

				// 封装
				Orderxiaofeijilu odxfjl = new Orderxiaofeijilu();
				odxfjl.setOrderid(orderid);
				odxfjl.setOrderjiaoyilaiyuan("余额");
				odxfjl.setOrdermoney(paymoney + "");
				odxfjl.setOrderdata(sdf.format(new Date()));
				System.out.println(odxfjl.getOrderdata());

				if (uddao.saveorder(userorder)) {
					if (udao.updatemoneybyid(uid, newmoney) && uddao.payorderbyid(orderid) && odxfjldao.insert(odxfjl)
							&& lsbdao.insert(orderid)) {
						request.setAttribute("paymessage", "支付成功！~~~");
						doGet(request, response);
						return;
					}
					request.setAttribute("payerror", "订单插入失败");
					doGet(request, response);
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
