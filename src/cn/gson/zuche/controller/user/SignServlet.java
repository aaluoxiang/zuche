package cn.gson.zuche.controller.user;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import java.util.List;
import java.util.regex.Matcher;  

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cn.gson.zuche.common.StringUtils;
import cn.gson.zuche.model.bean.User;
import cn.gson.zuche.model.dao.UserDao;

//import org.apache.jasper.tagplugins.jstl.core.Out;


@WebServlet("/sign")
public class SignServlet extends HttpServlet {
	UserDao udao = new UserDao();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getRequestDispatcher("/WEB-INF/jsp/sign.jsp").forward(req, resp);
	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("utf-8");
		resp.setCharacterEncoding("utf-8");
		PrintWriter out = resp.getWriter();
		String userName=req.getParameter("xname");
		String userTel=req.getParameter("xmobile");
		String old=req.getParameter("xpasswd");
		String now=req.getParameter("agpasswd");
		String captcha = req.getParameter("xcode");
		/*未实现功能，checkBox是否被选择*/
		String sesionCode = (String) req.getSession().getAttribute(CaptchaServlet.CAPTCHA_KEY);
		if(StringUtils.isEmpty(userName)){
			req.setAttribute("error_n", "$('.t_n').css('display','block')");
			doGet(req, resp);
			return;
		}
		if(StringUtils.isEmpty(userTel)){
			req.setAttribute("error_m_emp", "$('.t_m_emp').css('display','block')");
			doGet(req, resp);
			return;
		}
		if(userTel.length()!=11){
			req.setAttribute("error_m", "$('.t_m').css('display','block')");
			doGet(req, resp);
			return;
		}
		
		if(StringUtils.isEmpty(old)){
			 req.setAttribute("error_w_emp", "$('.t_w_emp').css('display','block')");
			 doGet(req, resp);
				return;
		 }
		if(StringUtils.isEmpty(captcha)){
			req.setAttribute("error_c_emp", "$('.t_c_emp').css('display','block')");
			doGet(req, resp);
			return;
		}
		if(old.length()<6){
			System.out.println("密码长度小于6");
			req.setAttribute("error_w_less", "$('.t_w_less').css('display','block')");
			 doGet(req, resp);
				return;
		}
		if(!old.equalsIgnoreCase(now)){
			 req.setAttribute("error_aw", "$('.t_aw').css('display','block')");
			 doGet(req, resp);
				return;
		 }
		
		if (!captcha.equalsIgnoreCase(sesionCode)) {
			req.setAttribute("error_c", "$('.t_c').css('display','block')");
			doGet(req, resp);
			return;
		}
		try {
			List<User> users = udao.findAllByTel(userTel);
			if(users.size()!=0){
				req.setAttribute("error_m_exist", "$('.t_m_exist').css('display','block')");
				doGet(req, resp);
				return;
			}
			String passmd5=StringUtils.md5Encode(old);
			Boolean success= udao.addUser(userName, userTel,old,passmd5);
			if(success){
				System.out.println("注册成功");
			}
			else{
				System.out.println("注册失败");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			
			
			HttpSession session = req.getSession();
			 List<User> users = udao.findAllByPasswd(userTel,old);
			if(users.size()==0){
				req.setAttribute("error_w", "$('.t_w').css('display','block')");
				doGet(req, resp);
				return;
			}
			session.setAttribute("userId", users.get(0).getUserId());
			session.setAttribute("userName", users.get(0).getUserName());
			boolean useryhq=udao.addUseryhq(users.get(0).getUserId());
			if(useryhq){
				System.out.println("新注册用户优惠卷分配成功");
			}
			else{
				System.out.println("新注册用户优惠卷分配失败");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		resp.sendRedirect(req.getContextPath() + "/fltk");
	}
	
	
}
