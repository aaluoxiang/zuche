package cn.gson.zuche.controller.user;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cn.gson.zuche.common.StringUtils;
import cn.gson.zuche.model.bean.Cartype;
import cn.gson.zuche.model.bean.Jbxxnr;
import cn.gson.zuche.model.bean.Weburl;
import cn.gson.zuche.model.dao.CartypeDao;
import cn.gson.zuche.model.dao.Jbxxnrdao;
import cn.gson.zuche.model.dao.Weburldao;

@WebServlet("/main")
public class MainServlet extends HttpServlet {
	private CartypeDao  cdao=new  CartypeDao();
	private Weburldao wdao=new Weburldao();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			List<Cartype> byid=cdao.carOrder("car_type_id");
			List<Cartype> byprice=cdao.carOrder("car_price");
			List<Cartype> byxiaoliang=cdao.carOrderDesc("car_xiaoliang");
			List<Cartype> bykucun=cdao.carOrderDesc("car_kucun");
			List<Weburl> bg=wdao.findall();
			
			
			req.setAttribute("bg", bg);
			req.setAttribute("byid", byid);
			req.setAttribute("byprice", byprice);
			req.setAttribute("byxiaoliang", byxiaoliang);
			req.setAttribute("bykucun", bykucun);
			
			HttpSession session = req.getSession();
			Jbxxnrdao dao = new Jbxxnrdao();
			req.setAttribute("dd", "jbxxnr");
			try {
				List<Jbxxnr> list = dao.findall();
				session.setAttribute("Webbq", list.get(0).getWebbq());
				session.setAttribute("Webbt", list.get(0).getWebbt());
				session.setAttribute("Webcompanyname", list.get(0).getWebcompanyname());
				session.setAttribute("Webdes", list.get(0).getWebdes());
				session.setAttribute("Webintro", list.get(0).getWebintro());
				session.setAttribute("Webpoint", list.get(0).getWebpoint());
				session.setAttribute("Webtel", list.get(0).getWebtel());
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		req.getRequestDispatcher("/WEB-INF/jsp/main.jsp").forward(req, resp);
	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session=req.getSession();
		
		req.setCharacterEncoding("utf-8");
		resp.setCharacterEncoding("utf-8");
		 
		String quSelect=req.getParameter("quSelect");
		String haiSelect=req.getParameter("haiSelect");
		String send=req.getParameter("send");
		String repay=req.getParameter("repay");
							
		String sendDate=req.getParameter("d422");
		String repayDate=req.getParameter("d423");
		Integer sendTime=Integer.parseInt(req.getParameter("qutimSelect"));
		Integer repayTime=Integer.parseInt(req.getParameter("haitimSelect"));
		
		String sendTimeText=req.getParameter("qutimSelect").trim();
		String repayTimeText=req.getParameter("haitimSelect").trim();
		
		System.out.println(sendTime);
		System.out.println(repayTime);
		
		session.setAttribute("quSelect", quSelect);
		session.setAttribute("haiSelect", haiSelect);
		session.setAttribute("send", send);
		session.setAttribute("repay", repay);
		session.setAttribute("sendDate", sendDate);
		session.setAttribute("repayDate", repayDate);
		session.setAttribute("sendTime", sendTime);
		session.setAttribute("repayTime", repayTime);
		
		
		if(StringUtils.isEmpty(send)){
			req.setAttribute("error_qu", "$('.t_map_qu ').css('display','block')");
			doGet(req, resp);
			return;
		}
		if(StringUtils.isEmpty(repay)){
			req.setAttribute("error_hai", "$('.t_map_hai').css('display','block')");
			doGet(req, resp);
			return;
		}
		
		if(sendDate.equals(repayDate)){
			if(sendTime>=repayTime){
				req.setAttribute("error_time_less", "$('.t_time_more').css('display','none');$('.t_time_day').css('display','none');$('.t_time_less').css('display','block');$('.t_time_hour').css('display','none');");
				doGet(req, resp);
				return;
			}
		}
		/*
		System.out.println(quSelect);
		System.out.println(haiSelect);
		System.out.println(send);
		System.out.println(repay);
		System.out.println(sendDate);
		System.out.println(repayDate);
		System.out.println(sendTime);
		System.out.println(repayTime);*/
		
		resp.sendRedirect(req.getContextPath()+"/mainjump");			
	}
}
