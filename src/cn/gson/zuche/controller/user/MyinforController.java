package cn.gson.zuche.controller.user;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cn.gson.zuche.model.bean.User;

/**
 * Servlet implementation class MyinforController
 */
@WebServlet("/myinfor")
public class MyinforController extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/*HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userId");*/
		/*User user = (User) session.getAttribute("user");
		System.out.println(user);*/
		request.getRequestDispatcher("/WEB-INF/jsp/myinfor.jsp").forward(request,response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
