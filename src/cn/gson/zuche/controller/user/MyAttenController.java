package cn.gson.zuche.controller.user;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cn.gson.zuche.common.IDCardValidate;
import cn.gson.zuche.common.StringUtils;
import cn.gson.zuche.model.bean.Lxr;
import cn.gson.zuche.model.bean.User;
import cn.gson.zuche.model.dao.Lxrdao;
import cn.gson.zuche.model.dao.UserDao;
import cn.gson.zuche.model.dao.UserTypeDao;

/**
 * Servlet implementation class MyAttenController
 */
@WebServlet("/myatten")
public class MyAttenController extends HttpServlet {

	UserDao udao = new UserDao();
	Lxrdao lxrdao = new Lxrdao();

	UserTypeDao utypedao = new UserTypeDao();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		Long userid = Long.parseLong(session.getAttribute("userId") + "");
		try {
			User user = udao.findAllById(userid).get(0);
			String upjf = (String) utypedao.findupjf(user.getUserType() + "").get(0);
			List<Lxr> lxrs = lxrdao.finallbyuserid(userid + "");
			request.setAttribute("upjf", upjf);
			request.setAttribute("lxrs", lxrs);
			request.setAttribute("user", user);
			request.getRequestDispatcher("/WEB-INF/jsp/myatten.jsp").forward(request, response);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			HttpSession session = request.getSession();
			String formtype = request.getParameter("formtype");
			if("tianjia".equals(formtype)){
				Integer userid = Integer.parseInt(session.getAttribute("userId") + "");
				String lxrname = request.getParameter("lxrname");
				String lxrsfz = request.getParameter("lxrsfz");
				String lxrtel = request.getParameter("lxrtel");
				String e_mail = request.getParameter("e_mail");
				System.out.println(lxrname + lxrsfz + lxrtel + e_mail);
				if (StringUtils.isEmpty(lxrname)) {
					request.setAttribute("error", "姓名不能为空");
					doGet(request, response);
					return;
				}
				if (!IDCardValidate.validate(lxrsfz)) {
					request.setAttribute("error", "错误身份证");
					doGet(request, response);
					return;
				}
				if (!StringUtils.isMobileNO(lxrtel)) {
					request.setAttribute("error", "错误电话");
					doGet(request, response);
					return;
				}
				Lxr lxr = new Lxr();
				lxr.setLxrname(lxrname);
				lxr.setLxrsfz(lxrsfz);
				lxr.setLxrphone(lxrtel);
				lxr.setLxremail(e_mail);
				lxr.setLxruserid(userid);
				if (lxrdao.insertLxr(lxr)) {
					doGet(request, response);
					return;
				}
				return;
			}
			if("shanchu".equals(formtype)){
				String lxrid= request.getParameter("lxrid");
				if (lxrdao.deleteLxr(lxrid)) {
					doGet(request, response);
					return;
				}
				return;
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
