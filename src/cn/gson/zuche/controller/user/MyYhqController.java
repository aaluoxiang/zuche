package cn.gson.zuche.controller.user;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cn.gson.zuche.model.bean.User;
import cn.gson.zuche.model.bean.UserYhq;
import cn.gson.zuche.model.dao.UserDao;
import cn.gson.zuche.model.dao.UserTypeDao;
import cn.gson.zuche.model.dao.UserYhqDao;

/**
 * Servlet implementation class MyLevelController
 */
@WebServlet("/myyhq")
public class MyYhqController extends HttpServlet {

	UserYhqDao uyhqdao = new UserYhqDao();
	UserTypeDao utypedao = new UserTypeDao();
	UserDao udao = new UserDao();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			HttpSession session = request.getSession();
			Long userid = Long.parseLong(session.getAttribute("userId")+"");
			User user = udao.findAllById(userid).get(0);
			List<UserYhq> userokyhqs = uyhqdao.findokYhqById(userid);
			List<UserYhq> usernoyhqs = uyhqdao.findnoYhqById(userid);
			String upjf = (String) utypedao.findupjf(user.getUserType()+"").get(0);
			
			request.setAttribute("upjf", upjf);
			request.setAttribute("user", user);
			request.setAttribute("userokyhqs", userokyhqs);
			request.setAttribute("usernoyhqs", usernoyhqs);
			request.getRequestDispatcher("/WEB-INF/jsp/myyhq.jsp").forward(request, response);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
