package cn.gson.zuche.controller.user;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cn.gson.zuche.model.bean.Chozhi;
import cn.gson.zuche.model.bean.User;
import cn.gson.zuche.model.bean.Xiaofei;
import cn.gson.zuche.model.dao.Chozhidao;
import cn.gson.zuche.model.dao.UserDao;
import cn.gson.zuche.model.dao.UserTypeDao;
import cn.gson.zuche.model.dao.Userorderdao;
import cn.gson.zuche.model.dao.XiaofeiDao;

/**
 * Servlet implementation class MyBalanceMoneyController
 */
@WebServlet("/mybalancemoney")
public class MyBalanceMoneyController extends HttpServlet {

	UserDao udao = new UserDao();
	Userorderdao uorderdao = new Userorderdao();
	XiaofeiDao xfdao= new XiaofeiDao();
	Chozhidao czdao= new Chozhidao();
	UserTypeDao utypedao = new UserTypeDao();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		Long userid = Long.parseLong(session.getAttribute("userId")+"");
		try {
			User user = udao.findAllById(userid).get(0);
			String tel = user.getUserTel();
			List list=uorderdao.findokorderbyuserid(userid);
			System.out.println(list);
			List<Xiaofei> xfjls = null;
			if (list .size()!=0 ) {
				System.out.println(1);
				xfjls=xfdao.findAllxfjl(list);
				
			}
			List<Chozhi> czjls= czdao.select(tel);
			String upjf = (String) utypedao.findupjf(user.getUserType()+"").get(0);
			request.setAttribute("upjf", upjf);
			request.setAttribute("user", user);
		//	System.out.println(czjls);
			request.setAttribute("xfjls", xfjls);
			request.setAttribute("czjls", czjls);
			
			request.getRequestDispatcher("/WEB-INF/jsp/mybalancemoney.jsp").forward(request, response);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
