package cn.gson.zuche.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class baseServlet
 */

public class BaseServlet extends HttpServlet {
	private HttpServletRequest request;
	private HttpServletResponse response;
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try{
		this.request=request;
		this.response=response;
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		super.service(request, response);
		}catch(Exception e){
			response.setStatus(500);
			e.printStackTrace();
		}
	}
	protected void forward(String page)throws ServletException, IOException {
		request.getRequestDispatcher("/WEB-INF/jsp/adminjsp"+page+".jsp").forward(request, response);
	}
	protected void redirect(String path)throws ServletException, IOException {
		response.sendRedirect(request.getContextPath()+"/"+path);
	}
}
