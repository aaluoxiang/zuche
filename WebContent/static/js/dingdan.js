/**
 * 自定义订单插件
 */

$(function(){
			/**
			 * 其他费用显示
			 */
			$(".opcl .fee-txt .qita").click(function(){
				$(this).toggleClass("open");
				$(" .newpg-conf .feeul > li.bluelist.shows").toggle();
			});
			/**
			 * 优惠券使用
			 */
			$(".opcl .fee-txt .youhui").click(function(){
				$(this).toggleClass("open");
				$(" .newpg-conf .feeul > li.bluelist.show").toggle();
			});
			/**
			 * 免赔服务
			 */
			$(".serviceOption .blue-ques").hover(function(){
				$(".otherul .blue-ques .bx-bubble").toggle();
			});
			
			$(".fee-txt .blue-ques").hover(function(){
				$(".fee-txt .bx-bubble").toggle();
			});
			
			/**
			 * 是否要发票
			 */
			$(".billul").hide();
            $(".pay-radio").click(function () {
              	$(".billul").toggle();
            });
            
            /**
              * 在线支付
              */
             $(".paydetail .blue-ques").hover(function(){
             	$(".paydetail .pay-bubble").toggle();
             });
              
              /**
               * 查看配置信息
               */
              $(".configbox").hide();
			$(".base-btn .look-config").click(function(){
				$(".configbox").toggle();
				$('.fu').css('display','block');
			});
			
			$(".cfclose").click(function(){
				if($(this).parent().hasClass("cftitle")){
					$(".configbox").toggle();
					$('.fu').css('display','none');
				}
				if($(this).parent().hasClass("yanzheng")){
					$(".yanzheng").toggle();
					$('.fu').css('display','none');
				}
				if($(this).parent().hasClass("dongjie")){
					$(".dongjie").toggle();
					$('.fu').css('display','none');
				}
				
			});
			
			$(".order-phonebox .phone-icon").hover(function(){
				$(".phone-shad").toggle();
			});
			
			$(".zjtype").click(function(){
				$(".card").toggle();	
			});
			$(".card").on("click","li",function(){
				var zhengjian=$(this).text();
				$(".zjtype").val(zhengjian);
				$(".card").css("display","none");	
			})
			
		});