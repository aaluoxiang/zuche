$(function(){
				$.yzfangfa("请输入您的手机号",-50,"error-tip",'phone_emp');
				$.yzfangfa("手机格式不正确",-50,"error-tip",'phone_error');
				$.yzfangfa("请输入验证码",-10,"error-tip",'code_emp');
				$.yzfangfa("验证码输入不正确",-10,"error-tip",'code_error');
				$.yzfangfa("请输入您的密码",-40,"error-tip",'password_emp');
				$.yzfangfa("两次密码输入不一致",-40,"error-tip",'password_error');
				
				$('.errorbox').css('display','none');
				
				$('.findback').on('click',function(){
					$('.errorbox').css('display','none');
					if($('#usertel').val()==""){
						$('.phone_emp').css('display','block');
						return false;
					}
					var myreg = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
					if(!myreg.test($(".usertel").val())){ 
							$('.phone_error').css('display','block');
						    return false; 
					}
					$('#youtel').html($(".usertel").val());
					$('#process2').attr('class','forget-process');
					$('.find-box1').css('display','none');
					$('.find-box2').css('display','block');
				});
				
				$('.change').on('click',function(){
					$('#process2').attr('class','');
					$('.find-box1').css('display','block');
					$('.find-box2').css('display','none');
				});
				
				$('.btn-get').on('click',function(){
					$('#phonetip').css('display','none');
					$('#sendtip').css('display','block');
					$('.btn-get').css('display','none');
					$('.btn-set').css('display','block');
					$.ajax({
						type:"get",
						url:"forgetcode",
						cache:false,
					});
					
					var time=59;
					var timer=null;
					timer=setInterval(function(){
						var lasttime=time+"秒后可重发";
						$('.btn-set').html(lasttime);
						time--;
						if(time==0){
							$('.btn-set').html("再次获取");
							clearInterval(timer);
						}
					},1000);
				});
				$('.btn-set').on('click',function(){
					var time=60;
					var timer=null;
					timer=setInterval(function(){
						var lasttime=time+"秒后可重发";
						$('.btn-set').html(lasttime);
						time--;
						if(time==0){
							$('.btn-set').html("再次获取");
							clearInterval(timer);
						}
					},1000);
				});
				
				$('.submit').on('click',function(){
					$('.errorbox').css('display','none');
					
					var mobilecode=$('#mobilecode').val();
					if($('#mobilecode').val()==""){
						$('.code_emp').css('display','block');
						return false;
					}
					$.ajax({
						type:"post",
						url:"forgetcode",
						cache:false,
						data:{"mobilecode":mobilecode},
						success:function(data){
							if("false"==data){
								console.log("错误")
								$('.code_error').css('display','block');
								return false;
							}
							if(!("false"==data)){
								 $('#process3').attr('class','forget-process');
								 $('.find-box2').css('display','none');
								 $('.find-box3').css('display','block');
							}
						}
					});
						
					/*此处进行后台模拟演练*/
					/*if($('#mobilecode').val()!="1234"){
						$('.code_error').css('display','block');
						return false;
					}*/
					
				});
				
				$('.save').on('click',function(){
					$('.errorbox').css('display','none');
					var usertel=$('#youtel').html();
					var onepassword=$('#onepassword').val();
					if($('#onepassword').val()==""||$('#agpassword').val()==""){
						$('.password_emp').css('display','block');
						return false;
					}
					if($('#onepassword').val()!=$('#agpassword').val()){
						$('.password_error').css('display','block');
						return false;
					}
					$.ajax({
						type:"get",
						url:"newpasswd",
						cache:false,
						data:{"onepassword":onepassword,"usertel":usertel},
						success:function(data){
							if("true"==data){
								$('#process4').attr('class','forget-process');
								$('.forget-find-text').css('display','none');
								$('.find-box3').css('display','none');
								$('.find-box4').css('display','block');
							}
						}
						
					});
					
				});
				
			});