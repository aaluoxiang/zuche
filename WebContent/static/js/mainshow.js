$(function() {
	var oUl = document.getElementById('main-search-ul')
	var aBg = oUl.getElementsByTagName('li');
	var oSelect = document.getElementById('main-search-ol');
	var aXuan = oSelect.getElementsByTagName('li');

	var oMainSerach = document.getElementById('main-search');
	var bDiv = document.getElementById('main-car-toprg');
	var aLi = bDiv.getElementsByTagName("a");
	var sDiv = document.getElementsByClassName('main-car-show');
	var n = 0;

	$.yzfangfa("取车地址不能为空",104,"error-tip",'t_map_qu');
	$.yzfangfa("还车地址不能为空",158,"error-tip",'t_map_hai');
	$.yzfangfa("还车时间必须大于取车时间",262,"error-tip",'t_time_less');
	$.yzfangfa("不足一天将按一天收费",262,"error-tip",'t_time_day');
	$.yzfangfa("每小时加收超时费￥30元起",262,"error-tip",'t_time_hour');
	$.yzfangfa("超过4小时按一天收费",262,"error-tip",'t_time_more');
	
	$('.errorbox').css('display','none');
	"${error_qu}";
	"${error_hai}";
	"${error_time_less}";
	for(var i = 0; i < aLi.length; i++) {
		aLi[i].index = i;
		aLi[i].onmouseover = function() {
			for(var i = 0; i < aLi.length; i++) {
				aLi[i].className = '';
				sDiv[i].style.display = 'none';
			}
			this.className = "active";
			sDiv[this.index].style.display = "block";
			this.color = "#f66";
		}
	}

	aXuan[0].className = 'att';
	aBg[0].style.display = 'block';
	
	for(var i = 0; i < aXuan.length; i++) {
		aXuan[i].index = i;
		aXuan[i].onclick = function() {
			for(var i = 0; i < aXuan.length; i++) {
				aXuan[i].className = '';
				aBg[i].style.display = 'none';
			}
			this.className = 'att';
			aBg[this.index].style.display = 'block';
		}
	}

	function move() {
		for(var i = 0; i < aXuan.length; i++) {
			aXuan[i].className = '';
			aBg[i].style.display = 'none';
		}
		aXuan[n].className = 'att';
		aBg[n].style.display = 'block';
		n++;
		if(n == aXuan.length) {
			n = 0;
		}
	}
	var timer = setInterval(move, 2000);
	oMainSerach.onmouseover = function() {
		clearInterval(timer);
	}
	oMainSerach.onmouseout = function() {
		timer = setInterval(move, 2000);
	}

	/*以下代码设置无缝滚动*/
	var oPerDiv = document.getElementById('main-person-show');
	var oPerUl = oPerDiv.getElementsByTagName('ul')[0];
	var oPerLi = oPerUl.getElementsByTagName('li');
	var bl = document.getElementById('show-left');
	var br = document.getElementById('show-right');
	var num = -2;

	oPerUl.innerHTML = oPerUl.innerHTML + oPerUl.innerHTML;
	oPerUl.style.width = (oPerLi[0].offsetWidth+80) * oPerLi.length + 'px';

	bl.onclick = function() {
		num = -2;
	}
	br.onclick = function() {
		num = 2;
	}

	function personMove() {
		if(oPerUl.offsetLeft < -oPerUl.offsetWidth / 2) {
			oPerUl.style.left = '0';
		}
		if(oUl.offsetLeft > 0) {
			oPerUl.style.left = -oPerUl.offsetWidth / 2 + 'px';
		}
		oPerUl.style.left = oPerUl.offsetLeft + num + 'px';
	}
	var personTimer = setInterval(personMove, 30);
	oPerDiv.onmouseover = function() {
		clearInterval(personTimer);
	}
	oPerDiv.onmouseout = function() {
		personTimer = setInterval(personMove, 30);
	}

	/*timeSet.js*/
	function addDate(date, days) {
		var d = new Date(date);
		d.setDate(d.getDate() + days);
		var m = d.getMonth() + 1;
		return d.getFullYear() + '-' + m + '-' + d.getDate();
	}

	function convertDateFromString(dateString) {
		if(dateString) {
			var date = new Date(dateString.replace(/-/, "/"))
			return date;
		}
	}

	Date.prototype.Format = function(fmt) { //author: meizz 
		var o = {
			"M+": this.getMonth() + 1, //月份 
			"d+": this.getDate(), //日 
			"h+": this.getHours(), //小时 
			"m+": this.getMinutes(), //分 
			"s+": this.getSeconds(), //秒 
			"q+": Math.floor((this.getMonth() + 3) / 3), //季度 
			"S": this.getMilliseconds() //毫秒 
		};
		if(/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
		for(var k in o)
			if(new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
		return fmt;
	}

	var oDate422 = document.getElementById('d422');
	var oDate423 = document.getElementById('d423');
	var	qutimSelect=document.getElementById('qutimSelect');
	var	haitimSelect=document.getElementById('haitimSelect');
	var weekdayTime=document.querySelector('.show-weekday-span');
	var longTime=document.querySelector('.show-longtime-span');
	var qutimValue=qutimValue=parseInt($('#qutimSelect option:selected').val());
	var haitimValue=parseInt($('#haitimSelect option:selected').val());
	/*console.log(qutimSelect);
	console.log(haitimSelect);*/
	var date = new Date();
	var nowDate = date.Format('yyyy-MM-dd');
	
	oDate422.value =addDate(nowDate, 1);
	oDate423.value = addDate(nowDate, 3);
	var weekday = new Array(7);
	weekday[0] = "周日";
	weekday[1] = "周一";
	weekday[2] = "周二";
	weekday[3] = "周三";
	weekday[4] = "周四";
	weekday[5] = "周五";
	weekday[6] = "周六";
	weekdayTime.innerText=weekday[new Date().getDay()];
	longTime.innerText=(convertDateFromString(oDate423.value) - convertDateFromString(oDate422.value)) / (24 * 60 * 60 * 1000);
	/*送车上门日期选取*/
	oDate422.onfocus = function() {
		WdatePicker({ minDate: addDate(nowDate,1), maxDate: addDate(nowDate, 40) });
//		console.log(oDate422.value);
		if(convertDateFromString(oDate422.value) > convertDateFromString(oDate423.value)) {
//			console.log("1 比2 大")
			oDate423.value = addDate(oDate422.value, 2);
		} else {
//			console.log("2 比1 大")
		}
		qutimValue=parseInt($('#qutimSelect option:selected').val());
		haitimValue=parseInt($('#haitimSelect option:selected').val());
		console.log(qutimValue);
		console.log(haitimValue);
		console.log(oDate422.value);
		console.log(oDate423.value);
		if((convertDateFromString(oDate423.value) - convertDateFromString(oDate422.value)) / (24 * 60 * 60 * 1000)!=0){
			longTime.innerText=(convertDateFromString(oDate423.value) - convertDateFromString(oDate422.value)) / (24 * 60 * 60 * 1000);
			if(haitimValue-qutimValue>=4){
				longTime.innerText=parseInt(longTime.innerText)+1;
			}
		}else{
			longTime.innerText=1;
		} 
		
		weekdayTime.innerText=weekday[new Date(oDate422.value).getDay()];
		/*longTime.innerText=(convertDateFromString(oDate423.value) - convertDateFromString(oDate422.value)) / (24 * 60 * 60 * 1000);*/

		
		/*后面的输入框的值减去前面输入框的值
		console.log((convertDateFromString(oDate423.value) - convertDateFromString(oDate422.value)) / (24 * 60 * 60 * 1000));
		根据第一个输入框的日期获取它是星期几
		console.log(weekday[new Date(oDate422.value).getDay()]);*/
	}
	/*还车日期选取，*/
	oDate423.onfocus = function() {
//		console.log(oDate422)
		WdatePicker({ minDate:oDate422.value, maxDate: addDate(oDate422.value, 80) });
		weekdayTime.innerText=weekday[new Date(oDate422.value).getDay()];
		qutimValue=parseInt($('#qutimSelect option:selected').val());
		haitimValue=parseInt($('#haitimSelect option:selected').val());
		if((convertDateFromString(oDate423.value) - convertDateFromString(oDate422.value)) / (24 * 60 * 60 * 1000)!=0){
			longTime.innerText=(convertDateFromString(oDate423.value) - convertDateFromString(oDate422.value)) / (24 * 60 * 60 * 1000);
			if(haitimValue-qutimValue>=4){
				longTime.innerText=parseInt(longTime.innerText)+1;
			}
			$('.errorbox').css('display','none');
		}else{
			longTime.innerText=1;
			$('.errorbox').css('display','none');
			$('.t_time_less').css('display','block');
		}
	}
	haitimSelect.onclick=function(){
		qutimValue=parseInt($('#qutimSelect option:selected').val());
		haitimValue=parseInt($('#haitimSelect option:selected').val());
		if((convertDateFromString(oDate423.value) - convertDateFromString(oDate422.value)) / (24 * 60 * 60 * 1000)==0){
			/*console.log(haitimValue);
			console.log(qutimValue);*/
			longTime.innerText=1;
			if(qutimValue>=haitimValue){
				$('.errorbox').css('display','none');
				$('.t_time_less').css('display','block');
			}else{
				$('.errorbox').css('display','none');
				$('.t_time_day').css('display','block');
			}
		}
		else{
			longTime.innerText=(convertDateFromString(oDate423.value) - convertDateFromString(oDate422.value)) / (24 * 60 * 60 * 1000);
			if(haitimValue-qutimValue>=4){
				longTime.innerText=parseInt(longTime.innerText)+1;
				$('.errorbox').css('display','none');
				$('.t_time_more').css('display','block');
			}else if(haitimValue>qutimValue){
				$('.errorbox').css('display','none');
				$('.t_time_hour').css('display','block');
			}
			else{
				$('.errorbox').css('display','none');
			}
			
		}
	}
		
		qutimSelect.onclick=function(){
			qutimValue=parseInt($('#qutimSelect option:selected').val());
			haitimValue=parseInt($('#haitimSelect option:selected').val());
		if((convertDateFromString(oDate423.value) - convertDateFromString(oDate422.value)) / (24 * 60 * 60 * 1000)==0){
			console.log(haitimValue);
			console.log(qutimValue);
			longTime.innerText=1;
			if(qutimValue>haitimValue){
				$('.errorbox').css('display','none');
				$('.t_time_less').css('display','block');
			}else{
				$('.errorbox').css('display','none');
				$('.t_time_day').css('display','block');
			}
		}
		else{
			longTime.innerText=(convertDateFromString(oDate423.value) - convertDateFromString(oDate422.value)) / (24 * 60 * 60 * 1000);
			if(haitimValue-qutimValue>=4){
				longTime.innerText=parseInt(longTime.innerText)+1;
				$('.errorbox').css('display','none');
				$('.t_time_more').css('display','block');
			}
		}
	}
		
	
	
		/*获得地图中需要的各标签class名称 */
		var send=document.querySelector('.send');
		var repay = document.querySelector('.repay');
		
		/* 获得送车上门与还车地址*/
		var sendValue=document.querySelector('.send').value;
		var repayValue = document.querySelector('.repay').value;
		
		var topRight=document.querySelector('.map-top-right');
		var topRighth=document.querySelector('.map-top-righth');
		var addrBtn=document.querySelector('.show-addr-btn');
		var addrBtnh=document.querySelector('.show-addr-btnh');
		
/*根据选取城市的id来跟踪地图  */
		function theLocation() {
			var city = $('#quSelect option:selected').val();
			if(city != "") {
				map.centerAndZoom(city, 14); // 用城市名设置地图中心点
			}
		}
		/*js设置送车上门地图*/
		var showAddr = document.getElementsByName('show-addr');
		var ss = document.querySelector('#show-ss')
		var map = new BMap.Map("allmap");
/*当前的经纬度为株洲  */
		var point = new BMap.Point(113.14, 27.83);
		map.centerAndZoom(point, 14);
		map.enableScrollWheelZoom(true); //地图可以放大缩小

		var geoc = new BMap.Geocoder();
		var addComp;
		var lastAdd
		map.addEventListener("click", function(e) {
			var pt = e.point;
			geoc.getLocation(pt, function(rs) {
				addComp = rs.addressComponents;
/*获得地址的详细信息*/
				lastAdd = addComp.district + "" + addComp.street + "" + addComp.streetNumber;
				/*获取详细地址*/
//				console.log( /*addComp.province + ", " + addComp.city + ", " +  */ lastAdd);
//				showAddr.innerText = lastAdd;
				ss.innerText = lastAdd;
			});
		});
		
/*与上同理*/
		/*js设置还车地址地图*/
		var showAddrh = document.getElementsByName('show-addh');
		var ssh = document.querySelector('#show-ssh')
		var maps = new BMap.Map("allmaph");
		var points = new BMap.Point(113.1, 27.8);
		maps.centerAndZoom(points, 14);
		maps.enableScrollWheelZoom(true); //地图可以放大缩小

		function theLocationh() {
			var city = $('#haiSelect option:selected').val();
			if(city != "") {
				maps.centerAndZoom(city, 14); // 用城市名设置地图中心点
			}
		}
		var geoch = new BMap.Geocoder();
		var addComph;
		var lastAddh; 
		maps.addEventListener("click", function(e) {
			var pt = e.point;
			geoch.getLocation(pt, function(rs) {
				addComph = rs.addressComponents;
				lastAddh = addComph.district + "" + addComph.street + "" + addComph.streetNumber;
				/*获取详细地址*/
//				console.log( /*addComp.province + ", " + addComp.city + ", " +  */ lastAdd);
//				showAddr.innerText = lastAddh;
				ssh.innerText = lastAddh;
			});
		});
		

		$('#quSelect').change(function(){
			document.querySelector('.send').value="";
		});
		$('#haiSelect').change(function(){
			document.querySelector('.repay').value="";
		});
		
		/*取车input输入框*/
		send.onfocus= function() {
//			mapKong();
			show('map-show');
			show("mbDIV");
			theLocation();
		}
		/*还车input输入框*/
		repay.onfocus = function() {
//			mapKong();
			show('map-showh');
			show("mbDIV");
			theLocationh();
		}
		/*送车地图中右上角的关闭按钮*/
		topRight.onclick=function(){
			hidden('map-show');
			hidden('mbDIV');
		}
		/*还车地图中右上角的关闭按钮*/
		topRighth.onclick=function(){
			hidden('map-showh');
			hidden('mbDIV');
		}
		/*送车地图中的确定按钮*/
		
		addrBtn.onclick=function(){
			hidden('map-show');
			hidden('mbDIV');
			if(typeof(lastAdd)=='undefined'){
					lastAdd='';
				}
			send.value=lastAdd;
		}
		/*还车地图中的确定按钮*/
		addrBtnh.onclick=function(){
			hidden('map-showh');
			hidden('mbDIV');
			if(typeof(lastAddh)=='undefined'){
					lastAddh='';
				}
			repay.value=lastAddh;
		}
		/*方法：传入某个标签的class或者id将其进行显示*/
		function show(ele) {
			document.querySelector("#"+ele).style.display='block';
		}
		/*方法：传入某个标签的class或者id将其进行隐藏*/
		function hidden(ele) {
			document.querySelector("#"+ele).style.display='none'
		}
	
	
});