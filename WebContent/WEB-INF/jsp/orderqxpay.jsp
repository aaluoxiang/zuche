<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=request.getContextPath()%>/">
<link rel="stylesheet" type="text/css" href="static/css/payorder.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>支付页面</title>
<script src="static/js/jquery-1.12.4.js" type="text/javascript"
	charset="utf-8"></script>
<script>
	function canSUbmit() {
		if ($('.payfs').val()) {
			return true;
		}
		return false;
	}
	$(function() {
		$('.pay_item').on('click', '.pay_item_li', function() {
			$(this).css('border', '2px solid #fabe00');
			$(this).siblings().css('border', '1.5px solid #e4e6e9');
			var payfs = $(this).attr('data-name');
			$('.payfs').val(payfs);
		});
		$('.payerrorinfo').on('click', '.yhqinfoclose', function() {
			$(this).parents('.payerror').remove();
		});
	});
</script>
</head>
<body style="background-color: #f2f3f5;">
	<jsp:include page="header-inc.jsp" flush="true"></jsp:include>
	<c:if test="${payerror!=null }">
		<div class="payerror">
			<div class="payerrorinfo">
				<a class="yhqinfoclose"></a>
				<div>${payerror }</div>
			</div>
		</div>
	</c:if>
	<c:if test="${paymessage!=null }">
		<div class="payerror">
			<div class="payerrorinfo">
				<div>
					${paymessage } <a href="myorder" class="tiaozhuan">返回我的订单></a>
				</div>
			</div>
		</div>
	</c:if>
	<div class="order_payinfor">
		<div class="orderpaytop">
			<div class="payinfocontent">
				<div class="content_left">
					<p>
						<span class="pay_title">待付款</span>
					</p>
					<p class="pay_tip">在线完成支付，快速取还车</p>
				</div>
				<div class="content_right">
					<p class="pay_tip">
						<span class="">全款租金</span> <span class="pay_money"> ¥ <i>${userorder.odermoney }</i>
						</span>
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="order_payinfor" style="margin-bottom: 30px;">
		<div class="orderpaybottom">
			<div class="pay_method">支付方式</div>
			<div class="pay_item">
				<ul>
					<li class="pay_item_li" data-name="zfb">
						<div class="pay_item_fs">
							<img
								src="https://image.zuchecdn.com/newversion/order/btn_zfb.png"
								alt="">
						</div>
					</li>
					<li class="pay_item_li" data-name="wx">
						<div class="pay_item_fs">
							<img src="https://image.zuchecdn.com/newversion/order/btn_wx.png"
								alt="">
						</div>
					</li>
					<li class="pay_item_li" data-name="yue">
						<div class="pay_item_fs"
							style="line-height: 55px; font-size: 20px;">余额支付</div>
					</li>
				</ul>
			</div>
			<div class="pay_button">
				<form method="post" onsubmit="return canSUbmit()">
					<input type="hidden" name="orderid" value="${userorder.orderid }">
					<input type="hidden" name="userorderzt" value="${userorder.userorderzt }">
					<input type="hidden" name="odermoney" value="${userorder.odermoney }"> 
					<input type="hidden" name="usertel" value="${userorder.usertel }"> 
					<input type="hidden" name="usersfz" value="${userorder.usersfz }">
					<input type="hidden" name="userid" value="${userorder.userid }">
					<input type="hidden" name="gettime" value="${userorder.gettime }">
					<input type="hidden" name="backtime" value="${userorder.backtime }">
					<input type="hidden" name="ordergetaddress" value="${userorder.ordergetaddress }">  
					<input type="hidden" name="orderbackaddress" value="${userorder.orderbackaddress }"> 
					<input type="hidden" name="orderdriverid" value="${userorder.orderdriverid }"> 
					<input type="hidden" name="ordercarid" value="${userorder.ordercarid }"> 
					<input type="hidden" name="orderstoreid" value="${userorder.orderstoreid }"> 
					<input type="hidden" name="orderyhqid" value="${userorder.orderyhqid }"> 
					<input type="hidden" name="ordercommentid" value="${userorder.ordercommentid }"> 
					<input type="hidden" name="orderdelayid" value="${userorder.orderdelayid }"> 
					<input type="hidden" name="ordercartype" value="${userorder.ordercartype }"> 
					<input type="hidden" name="price" value="${userorder.price }"> 
					<input type="hidden" name="ordercarsiji" value="${userorder.ordercarsiji }"> 
					<input type="hidden" name="ordermianpei" value="${userorder.ordermianpei }"> 
					<input type="hidden" name="ordersingletime" value="${userorder.ordersingletime }"> 
					<input type="hidden" name="orderlxrname" value="${userorder.orderlxrname }"> 
					<input type="hidden" name="orderlxrphone" value="${userorder.orderlxrphone }"> 
					<input type="hidden" name="orderfp" value="${userorder.orderfp }"> 
					<input type="hidden" name="payfs" class="payfs" value="">
					<button class="button_pay">确认支付</button>
				</form>
			</div>
		</div>
	</div>
	<jsp:include page="footer-inc.jsp" flush="true"></jsp:include>
</body>
</html>