<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<base href="<%request.getContextPath();%>" >
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>login</title>
<link rel="stylesheet" type="text/css" href="static/css/error.css"/>
<link rel="stylesheet" type="text/css" href="static/css/sign.css"/>
<link rel="stylesheet" type="text/css" href="static/css/loginmain.css"/>
<script type="text/javascript" src="static/js/jquery-1.12.4.js" ></script>
<script type="text/javascript" src="static/js/error-tip.js" ></script>
<script type="text/javascript" src="static/js/login.js" ></script>
  <script>
	    $(function() {
	    	${error_m};
	    	${error_w};
	    	${error_m_emp};
	    	${error_w_emp};
	    	${error_code};
	    	${error_code_emp};
	    });
    </script>
</head>
<body>
	<div class="zc_head_bd">
			<div class="zc_main">
				<a href="main" class="zc_logo" alt="租车公司"></a>
				<div class="zc_font">
					欢迎登录
				</div>
			</div>
			<!--head end-->
		</div>
<!-- begin -->
<div id="login" style="min-width: 1300px;position: relative;">
    <div class="wrapper" style="min-width: 1300px;height:500px;position: relative;background:url('static/img/web-bg2.jpg');margin: 0 auto;background-position:top center;">
        <div class="login" style="width: 1200px;position: relative;margin: 0 auto;">
        	
            <form action="" method="post" id="login-form" class="login-form">
			<div class="login-main">
				<span class="login-span">登录</span>
				<div class="error-tip">
					</div>
				<span class="email-img spanimg"></span>
				<input type="text" class="login-input email" name="email" id="email" value="${param.email}" placeholder="请输入手机号" autofocus="autofocus" />
				<span class="password-img spanimg"></span>
				<input type="password" class="login-input password" name="password" id="password" value="" placeholder="请输入密 码"  />
				<span class="logincode-img spanimg"></span>
				<input type="tel" name="logincode" id="logincode" value="" placeholder="请输入验证码" />
				<div class="login-img">
					<img onclick="this.src='captcha?r'+Date.now()" src="captcha" alt="验证码">
				</div>
				<div class="other-btn">
					<a href="sign" class="login-a">立即注册</a>
					<a href="forget" class="login-b">忘记密码？</a>
				</div>
				<div class="login-footer">
					<input type="submit" value="登录" class="login-btn" id="login-btn" />
				</div>
			</div>
			
		</form>
        </div>
    </div>
</div>
<!-- end -->
</body>
</html>