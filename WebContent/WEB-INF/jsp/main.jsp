<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
<!DOCTYPE html>
<html>
	<head>
	 <base href="<%request.getContextPath();%>" >
		<meta charset="UTF-8">
		<title>租车公司</title>
		<link rel="stylesheet" href="static/css/error.css" type='text/css'/> 
		<link rel="stylesheet" href="static/css/main.css" />
		<script type="text/javascript" src="static/js/jquery-1.12.4.js" ></script>
		<script type="text/javascript" src="static/js/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="static/js/mainshow.js" ></script>
		<script src="static/js/error-tip.js" type="text/javascript"></script>
		<script type="text/javascript" src="http://api.map.baidu.com/api?v=1.3"></script>
		<script type="text/javascript">
		$(function(){
			${error_qu};
			${error_hai};
			${error_time_less};
			$('.s').on('click',function(){
				var carname=$(this).attr('name');
				$(this).attr("href","mainjump?carname='"+carname+"'");
			})
			$('.moveimg').mouseover(function(){
				
				 $(this).css('margin-left','10px');
				 console.log( $(this).css('margin-left','10px'));
				 $(this).offset(function(n,c){
					newPos=new Object();
			        newPos.left=c.left+10;
			        return newPos;
				}); 
			});
			$('.moveimg').mouseout(function(){
				 $(this).children('img').css('margin-left','0px');
				 $(this).offset(function(n,c){
					newPos=new Object();
			        newPos.left=c.left-10;
			        return newPos;
				}); 
			});
		});
		</script>
	</head>
	<body>
	<jsp:include page="header-inc.jsp"></jsp:include>
		<div class="main">
			<div class="main-m">
				<div id="mbDIV" >/**设置弹出层的底部颜色，让底部文字不可见**/
				</div>
				<div id="map-show">
					<div class="map-show-top">
						<span class="map-top-left">送车上门详细地址</span>
						<span class="map-top-right"></span>
					</div>
					<div id="allmap"></div>
					<div id="r-result">
						<div class="show-addr" >
							<span class="addr-icon"></span>
							<div class="show-addr-mess">
								<span id="show-ss">
									请选择送车上门地址
								</span>
								<a class="show-addr-btn">确认地址</a>
							</div>
						</div>
					</div>
				</div>
				<div id="map-showh">
					<div class="map-show-top">
						<span class="map-top-left">huan上门详细地址</span>
						<span class="map-top-righth"></span>
					</div>
					<div id="allmaph"></div>
					<div id="r-resulth">
						<div class="show-addr" >
							<span class="addr-icon"></span>
							<div class="show-addr-mess">
								<span id="show-ssh">
									请选择还车上门地址
								</span>
								<a class="show-addr-btnh">确认地址</a>
							</div>
						</div>
					</div>
				</div>
				<div class="main-search" id="main-search">
					<ul  class="main-search-ul" id="main-search-ul">
						<c:forEach items="${bg}" var="bg">
							<li class="main-search-li"  style="background-image: url(static/img/${bg.weburlimg})"></li>
						</c:forEach>
						<!-- <li class="main-search-li"  style="display: block;background-image: url(static/img/web-bg1.jpg)"></li>
						<li class="main-search-li" style="background-image: url(static/img/web-bg2.jpg)"></li>
						<li class="main-search-li" style="background-image: url(static/img/web-bg3.jpg)"></li>
						<li class="main-search-li" style="background-image: url(static/img/web-bg4.jpg)"></li> -->
					</ul>
					<ol class="main-search-ol" id="main-search-ol">
					<c:forEach items="${bg}" var="bg">
							<li ></li>
						</c:forEach>
						<!-- <li class="att"></li>
						<li></li>
						<li></li>
						<li></li> -->
					</ol>
					<div style="border:0px solid red;width:1200px;margin:0 auto;">
					<div class="error-tip">
						
					</div>
					<div class="main-search-box" >
						<span class="search-box-s" >
							选择租车
						</span>
					<form name="form1" action="main" method="post">
						<div class="search-box-select">
							<ul>
								<li>
									<span >取车</span>
									<select id="quSelect" class="addrSelect" name="quSelect">
										<option value="株洲">株洲</option>
										<option value="武汉">武汉</option>
										<option value="长沙">长沙</option>
										<option value="北京">北京</option>
									</select>
									<input type="text" class="show-map send" name="send" placeholder="请输入送车上门地址" value="${param.send }" readonly="readonly"  />
								</li>
								<li>
									<span >还车</span>
									<select id="haiSelect"class="addrSelect" name="haiSelect">
										<option value="株洲">株洲</option>
										<option value="武汉">武汉</option>
										<option value="长沙">长沙</option>
										<option value="北京">北京</option>
									</select>
									<input type="text" class="show-map repay" name="repay" placeholder="请输入还车上门地址" value="${param.repay }" readonly="readonly"/>
								</li>
							</ul>
							<span class="how-long">
									租期
								</span>
							<div class="show-time-lf" >
								
								<input id="d422" class="Wdate" type="text" name="d422" readonly="readonly" value="${param.d222 }"/>
								<!--根据日期得到星期几-->
								<div class="show-weekday" >
									<span class="show-weekday-span">dfsa</span>
								</div>
								<select class="timSelect" id="qutimSelect" name="qutimSelect">
									<option value="8">8:00</option>
									<option value="9">9:00</option>
									<option value="10" selected="selected">10:00</option>
									<option value="11">11:00</option>
									<option value="12">12:00</option>
									<option value="13">13:00</option>
									<option value="14">14:00</option>
									<option value="15">15:00</option>
									<option value="16">16:00</option>
									<option value="17">17:00</option>
									<option value="18">18:00</option>
									<option value="19">19:00</option>
									<option value="20">20:00</option>
									<option value="21">21:00</option>
									<option value="22">22:00</option>
								</select>
							</div>
							<span class="show-center"></span>
							<div class="show-time-rg" >
								<div class="show-longtime">
									<span class="show-longtime-span"></span><span>天</span>
								</div>
								<input id="d423" class="Wdate" type="text" name="d423" readonly="readonly" value="${param.d223 }"/>
								<select class="timSelect" id="haitimSelect" name="haitimSelect">
									<option value="8">8:00</option>
									<option value="9">9:00</option>
									<option value="10" selected="selected">10:00</option>
									<option value="11">11:00</option>
									<option value="12">12:00</option>
									<option value="13">13:00</option>
									<option value="14">14:00</option>
									<option value="15">15:00</option>
									<option value="16">16:00</option>
									<option value="17">17:00</option>
									<option value="18">18:00</option>
									<option value="19">19:00</option>
									<option value="20">20:00</option>
									<option value="21">21:00</option>
									<option value="22">22:00</option>
								</select>
							</div>
						</div>
						</form>
							<a href="javascript:document.form1.submit();"class="select-btn" >立即选车</a>
					</div>
					</div>
				</div>
				<ul class="main-nav">
					<li>
						<img class="main-nav-left" src="static/img/discribe1.jpg"/>
						<a class="nav-left-a" href="javascript:void(0);">
							<h3>接送机场</h3>
							<p>无需任何手续，方便快捷提前到达指定地点等候</p>
						</a>
					</li>
					<li>
						<img class="main-nav-left" src="static/img/discribe2.jpg"/>
						<a class="nav-left-a" href="javascript:void(0);">
							<h3>企业服务</h3>
							<p>承接各种大型会议用车专业商务接待团队</p>
						</a>
					</li>
					<li>
						<img class="main-nav-left" src="static/img/discribe3.jpg"/>
						<a class="nav-left-a" href="javascript:void(0);">
							<h3>婚期豪车</h3>
							<p>多款豪华跑车供您选择高中低档搭配套餐</p>
						</a>
					</li>
					<li>
						<img class="main-nav-left" src="static/img/discribe4.jpg"/>
						<a class="nav-left-a" href="javascript:void(0);">
							<h3>旅游越野</h3>
							<p>为您量身打造私人线路领略不用旅游风景</p>
						</a>
					</li>
				</ul>
				<div class="main-car">
					<div class="show-a">
						<a href="shangcheng" class="show-more">+<br/>更<br/>多<br/>车<br/>型</a>
					</div>
					<div class="main-car-toplf">
						<label class="main-top-label">
							选车方式
							<br />
							<em>SELECT CAR WAY</em>
						</label>
						<ul class="main-car-toprg" id="main-car-toprg">
							<li><a href="javascript:void(0);" class="active">默认选车</a></li>
							<li><a href="javascript:void(0);">价格选车</a></li>
							<li><a href="javascript:void(0);">销量选车</a></li>
							<li><a href="javascript:void(0);">库存选车</a></li>
						</ul>
					</div>
					<!--第一个carshow面板-->
					<div class="main-car-show" style="display: block;">
						<ul class="main-show-list">
						<c:forEach items="${byid}" var="byid">
							<li>
								<a class="s" href="javascript:void(0);" name="${byid.carTypeName}">
									<div class="show-price">${byid.carPrice}元/天</div>
									<div class="show-img">
										<img class="moveimg" src="${byid.carImageUrl}"/>
									</div>	
									<div class="show-model">${byid.carTypeName}|${byid.carXinxi}</div>
								</a>
							</li>
						</c:forEach>
						</ul>
					</div>
					<!--第二个carshow面板-->
					<div class="main-car-show" >
						<ul class="main-show-list">
						<c:forEach items="${byprice}" var="byprice">
							<li>
								<a class="s" href="javascript:void(0);" name="${byprice.carTypeName}">
									<div class="show-price">${byprice.carPrice}元/天</div>
									<div class="show-img">
										<img class="moveimg" src="${byprice.carImageUrl}"/>
									</div>	
									<div class="show-model">${byprice.carTypeName}|${byprice.carXinxi}</div>
								</a>
							</li>
						</c:forEach>
						</ul>
					</div>
					<!--第三个carshow面板-->
					
					<div class="main-car-show">
						<ul class="main-show-list">
						<c:forEach items="${byxiaoliang}" var="byxiaoliang">
							<li>
								<a class="s" href="javascript:void(0);" name="${byxiaoliang.carTypeName}">
									<div class="show-price">${byxiaoliang.carPrice}元/天</div>
									<div class="show-img">
										<img class="moveimg" src="${byxiaoliang.carImageUrl}"/>
									</div>	
									<div class="show-model">${byxiaoliang.carTypeName}|${byxiaoliang.carXinxi}</div>
								</a>
							</li>
						</c:forEach>
						</ul>
					</div>
					<!--第四个carshow面板-->
					<div class="main-car-show">
						<ul class="main-show-list">
						<c:forEach items="${bykucun}" var="bykucun">
							<li>
								<a class="s" href="javascript:void(0);" name="${bykucun.carTypeName}">
									<div class="show-price">${bykucun.carPrice}元/天</div>
									<div class="show-img">
										<img class="moveimg" src="${bykucun.carImageUrl}"/>
									</div>	
									<div class="show-model">${bykucun.carTypeName}|${bykucun.carXinxi}</div>
								</a>
							</li>
						</c:forEach>
						</ul>
					</div>
				</div>
				<!--精彩活动模板-->
				<div class="main-act" >
					<div class="main-car-toplf">
						<label class="main-top-label">
								精彩活动
								<br />
								<em>ACTIVITIES</em>
						</label>
						<span class="main-top-span">
							多种优惠套餐 让你快乐出行
						</span>
					</div>
					<div class="main-act-show">
						<div class="act-show-more">
							<a href="actcenter">+查看更多</a>
						</div>
						<div class="act-show-text">
							<p class="show-text-c">新闻中心</p>
							<p class="show-text-e">NEWS CENTER</p>
						</div>
						<ul class="act-show-context" >
							<li>
								<a href="javascript:void(0);">
									<div class="show-context-left">
										<span>12.13</span>
										<br />
										2016
									</div>
									<div class="show-context-right"><a href="actnew" style="text-decoration:none;color:#666;">点击查看</a></div>
									<p class="show-context-title">新用户专享，领取150元优惠券</p>	
									<p class="show-context-info">
										活动规则：<br>
										1. 新用户注册后，即送50元电子代金券，有效期：注册日起6个月；<br>
										2. 新用户首次租车结算后，再送100元电子代金券，有效期：首单结算完成后3个月。<br>
									</p>
								</a>
							</li>
							<li>
								<a href="javascript:void(0);">
									<div class="show-context-left">
										<span>12.13</span>
										<br />
										2016
									</div>
									<div class="show-context-right"><a href="getyhq" style="text-decoration:none;color:#666;">点击查看</a></div>
									<p class="show-context-title">五一五一、租五免一</p>	
									<p class="show-context-info">
										1.实际租期2天及以上的订单可用，优惠券平日、周末、法定节假日均可使用;<br>
										2.不与顺风车、预付特价、其他优惠活动、优惠类电子券同享;<br>
										3.此优惠仅限每人享受1次,每单限用1张，限本人使用;<br>
										4.代金券不可转让、不开发票、不找零、不兑换现金；
									</p>
								</a>
							</li>
							<li>
								<a href="actfree">
									<div class="show-context-left">
										<span>12.13</span>
										<br />
										2016
									</div>
									<div class="show-context-right">点击查看</div>
									<p class="show-context-title">万辆新车来袭 69元起租</p>	
									<p class="show-context-info">
										神州租车万辆新车来袭，低至69元起租<br>
										适用于全国直营门店（北京、伊犁除外）<br>
										万辆新车雪佛兰科鲁兹、丰田致炫、起亚K2、别克英朗、大众...
									</p>
								</a>
							</li>
						</ul>
					</div>
				</div>
				<!--部分司机风采-->
				<div class="main-person">
					<div class="show-lf" style="display: none;">
						<a href="javascript:void(0);" class="show-more" id="show-left">向左</a>
					</div>
					<div class="show-rg"style="display: none;">
						<a href="javascript:void(0);" class="show-more" id="show-right">向右></a>
					</div>
					<div class="main-car-toplf">
						<label class="main-top-label">
								部分司机风采
								<br />
								<em>THE DRIVER</em>
						</label>
						<span class="main-top-span">
							 多年驾龄技巧，让您放心乘坐！
						</span>
					</div>
					<div class="main-person-show" id="main-person-show">
						<ul class="person-show-list">
							<li >
								<img src="static/img/person1.jpg"/>
								<div class="person-name">姓名：皮特先生</div>
								<div class="person-speak">
									倡导活力、环保与健康的生活态度，是广汽丰田与广州马拉松共有的理念
								</div>
								<div class="person-aaa">
									<img src="static/img/news_icon_x2.jpg"/>
									<img src="static/img/news_icon_x2.jpg"/>
									<img src="static/img/news_icon_x2.jpg"/>
									<img src="static/img/news_icon_x2.jpg"/>
									<img src="static/img/news_icon_x2.jpg"/>
								</div>
								<!-- <a href="javascript:void(0);" class="person-message">点击查看</a> -->
							</li>
							<li >
								<img src="static/img/person2.jpg"/>
								<div class="person-name">姓名:葛优</div>
								<div class="person-speak">
									倡导活力、环保与健康的生活态度，是广汽丰田与广州马拉松共有的理念
								</div>
								<div class="person-aaa">
									<img src="static/img/news_icon_x2.jpg"/>
									<img src="static/img/news_icon_x2.jpg"/>
									<img src="static/img/news_icon_x2.jpg"/>
									<img src="static/img/news_icon_x2.jpg"/>
									<img src="static/img/news_icon_x2.jpg"/>
								</div>
								<!-- <a href="javascript:void(0);" class="person-message">点击查看</a> -->
							</li>
							<li >
								<img src="static/img/person3.jpg"/>
								<div class="person-name">姓名:奥巴马</div>
								<div class="person-speak">
									让我们征服全世界都吧~
								</div>
								<div class="person-aaa">
									<img src="static/img/news_icon_x2.jpg"/>
									<img src="static/img/news_icon_x2.jpg"/>
									<img src="static/img/news_icon_x2.jpg"/>                                                                                                               
									<img src="static/img/news_icon_x2.jpg"/>
									<img src="static/img/news_icon_x2.jpg"/>
								</div>
								<!-- <a href="javascript:void(0);" class="person-message">点击查看</a> -->
							</li>
							<li >
								<img src="static/img/person4.png"/>
								<div class="person-name">姓名:Mr 李</div>
								<div class="person-speak">
									倡导活力、环保与健康的生活态度，是广汽丰田与广州马拉松共有的理念
								</div>
								<div class="person-aaa">
									<img src="static/img/news_icon_x2.jpg"/>
									<img src="static/img/news_icon_x2.jpg"/>
									<img src="static/img/news_icon_x2.jpg"/>
									<img src="static/img/news_icon_x2.jpg"/>
									<img src="static/img/news_icon_x2.jpg"/>
								</div>
								<!-- <a href="javascript:void(0);" class="person-message">点击查看</a> -->
							</li>
							<li >
								<img src="static/img/person5.jpg"/>
								<div class="person-name">姓名:姜文</div>
								<div class="person-speak">
									坚持‘质的增长高于量的增长’的发展哲学
								</div>
								<div class="person-aaa">
									<img src="static/img/news_icon_x2.jpg"/>
									<img src="static/img/news_icon_x2.jpg"/>
									<img src="static/img/news_icon_x2.jpg"/>
									<img src="static/img/news_icon_x2.jpg"/>
									<img src="static/img/news_icon_x2.jpg"/>
								</div>
								<!-- <a href="javascript:void(0);" class="person-message">点击查看</a> -->
							</li>
							<li >
								<img src="static/img/person6.jpeg"/>
								<div class="person-name">姓名:郭德纲</div>
								<div class="person-speak">
									倡导活力、环保与健康的生活态度，是广汽丰田与广州马拉松共有的理念
								</div>
								<div class="person-aaa">
									<img src="static/img/news_icon_x2.jpg"/>
									<img src="static/img/news_icon_x2.jpg"/>
									<img src="static/img/news_icon_x2.jpg"/>
									<img src="static/img/news_icon_x2.jpg"/>
									<img src="static/img/news_icon_x2.jpg"/>
								</div>
								<!-- <a href="javascript:void(0);" class="person-message">点击查看</a> -->
							</li>
						</ul>
					</div>
					<!--底层栏，流程展示-->
					<ul class="pross-show">
						<li>
							<span class="show-num">1</span>
							<p class="pross-show-mess">
								预定车辆
								<br />
								<font>提前为您预留</font>
							</p>
						</li>
						<li>
							<span class="show-num">2</span>
							<p class="pross-show-mess">
								签订合同
								<br />
								<font>双方共同验车</font>
							</p>
						</li>
						<li>
							<span class="show-num">3</span>
							<p class="pross-show-mess">
								开心旅途
								<br />
								<font>一路为您保驾护航</font>
							</p>
						</li>
						<li>
							<span class="show-num">4</span>
							<p class="pross-show-mess">
								退还车辆
								<br />
								<font>完成租车使用</font>
							</p>
						</li>
					</ul>
				</div>
				
			</div>
		</div>
		<jsp:include page="footer-inc.jsp"></jsp:include>
	</body>
		
</html>


