<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	 <base href="<%request.getContextPath();%>" >
	 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	 
	 <link rel="stylesheet" href="static/css/sign.css" type='text/css'/>
	 <link rel="stylesheet" href="static/css/error.css" type='text/css'/> 
	 <script src="static/js/jquery-1.12.4.js" type="text/javascript"></script>
	  <script src="static/js/error-tip.js" type="text/javascript"></script>
	 <script src="static/js/sign.js" type="text/javascript"></script>
	<script type="text/javascript">
		$(function(){
			${error_n};
			${error_c};
			${error_m};
			${error_c_emp};
			${error_m_emp};
			${error_m_exist};
			${error_w_emp};
			${error_aw}; 
			${error_w_less};
		});
	</script>
	<style type="text/css">
    	.cwb {
    		letter-spacing: 3px;
    		font-size: 28px;
    		font-weight: bold;
            animation-iteration-count: infinite;
            animation-name: bluePulse;
            animation-duration: 3s;
        }
		@keyframes bluePulse{
            from {
                color: #fff;
                text-shadow: 0 0 10px #ebebeb;
                margin-left: 0px;
            }
            50% {
                color: #FFEB3B;
                text-shadow: 0 0 28px #2daebf;
                margin-left: 80px;
            }
            to {
                color: #fff;
                text-shadow: 0 0 10px #ebebeb;
                margin-left: 00px;
            }
        }

    </style>
<title>sign</title>
</head>
<body>
		<!--head start-->
		<div class="zc_head_bd">
			<div class="zc_main">
				<a href="main" class="zc_logo" alt="租车公司"></a>
				<div class="zc_font">
					欢迎注册
				</div>
			</div>
			<!--head end-->
		</div>
		<!--body部分-->
		<div class="zc-wbody">
			<div class="zc-inbody">
				<div class="zc-bmain">
					<div class="error-tip">
						
					</div>
					<p class="zc-fontzc">注册新用户</p>
					<!--form表单-->
					
					<form method="post" id="myform" autocomplete="off">
						<ul>
							<!--第一个输入框，用户的姓名-->
							<li class="zc-iptbox">
								<span class="rstar">*</span>
								<label>
									<span class="zc-iuser"></span>
								</label>
								<input type="text" name="xname"  id="xname" class="zc-ipt t_val xname" value="${param.xname}" placeholder="请输入你的真实姓名" maxlength="15"/>
							</li>
							<!--第二个输入框，用户的手机号-->
							<li class="zc-iptbox ">
								<span class="rstar ">*</span>
								<label >
									<span class="zc-stel "></span>
								</label>
								<input type="number" name="xmobile" id="xmobile"class="zc-ipt t_val xmobile" value="${param.xmobile}" maxlength="11" placeholder="请输入你的手机号 "/> 
								<label>
									<span class="whitew" style="background: white;width: 68px;height: 36px;display: inline-block;margin-left: 274px;margin-top: -2px;"></span>
								</label>
							</li>
							<!--第三个输入框，用户的密码-->
							<li class="zc-iptbox ">
								<span class="rstar ">*</span>
								<label>
									<span class="zc-passwd "></span>
								</label>
								<input type="password" name="xpasswd" id="xpasswd" class="zc-ipt t_val xpasswd" value="" maxlength="18"  placeholder="请输入你的密码 "/>
							</li>
							<li class="zc-iptbox ">
								<span class="rstar ">*</span>
								<label>
									<span class="zc-passwd "></span>
								</label>
								<input type="password" name="agpasswd" id="agpasswd" class="zc-ipt t_val agpasswd" value="" maxlength="18" placeholder="请再次输入你的密码 "/>
							</li>
							<!--第四个输入框，输入验证码-->
							<li class="zc-iptbox">
								<span class="rstar">*</span>
								<label>
									<span class="zc-code"></span>
								</label>
								<input type="text" name="xcode" id="xcode" width="200px" class="zc-ipt t_val  xcode" placeholder="请输入验证码 " maxlength="4"/>
								<div class="zc-imgcode ">
								<img onclick="this.src='captcha?r'+Date.now()" src="captcha" alt="验证码">
								</div> 
							</li>
							<!--第五个输入框，输入可用的优惠码-->
							<li class="zc-iptbox" style="display:none;">
								<label>
									<span class="zc-free "></span>
								</label>
								<input type="text" name="xfree" id="xfree" class="zc-ipt t_val" placeholder="如有优惠码，请填写优惠码 [暂未实现]" maxlength="11"readonly="true"/>
							</li>
						</ul>
					</form>
					<!--必须同意服务条款才能注册成功-->
					<p class="fltk ">
						<input type="checkbox" checked="checked" value="has check"  name="ck" id="ck"/>
						我已阅读并同意
						<a href="fltk" class="zc-login">《韬睿租车会员服务条款》</a>
					</p>
					<!--在点击注册时必须要进行检验，四个输入框不允许为空，电话号码为11位，密码进行简单的提醒，验证码进行核对-->
					<a href="javascript:void(0); " class="zc-btn" id="next-btn" onclick="document.getElementById('myform').submit();">注册</a>
					<p class="zc_pl">已有账号<a href="login" class="zc-login"> 立即登录>></a></p>
				</div>
			</div>
		</div>
	</body>
</html>