<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.trkj.com/tags" prefix="Mytag"%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=request.getContextPath()%>/">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>我的订单</title>
<link rel="stylesheet" type="text/css" href="static/css/center.css">
<link rel="stylesheet" type="text/css" href="static/css/myinfor.css">
<link rel="stylesheet" type="text/css"
	href="static/css/owncenterordererror.css">
<link rel="stylesheet" type="text/css"
	href="static/css/owncentermyorder.css">
<link rel="stylesheet" type="text/css"
	href="static/css/owncentertimechaxun.css">
<link rel="stylesheet" type="text/css"
	href="static/css/owncentertabs.css">
<script src="static/js/jquery-1.12.4.js" type="text/javascript"
	charset="utf-8"></script>
<script src="static/js/errorclose.js" type="text/javascript"
	charset="utf-8"></script>
<script>
	$(function() {
		$(".tab").on(
				"click",
				".item",
				function() {
					var target = $(this);
					if (!target.hasClass('selected')) {
						var tab = this.dataset.tab;
						var nr = $(".tab-con .content[data-content=" + tab
								+ "]");
						var selectednr = nr.siblings('.selected');
						target.addClass('selected').siblings('.selected')
								.toggleClass('selected');
						nr.addClass('selected').siblings('.selected')
								.toggleClass('selected');
					}
				});
	});
</script>
</head>
<body>
	<jsp:include page="header-inc.jsp" flush="true"></jsp:include>
	<jsp:include page="tou.jsp" flush="true" />
	<div class="a_acon b">
		<jsp:include page="menu.jsp" flush="true" />
		<div class="container">
			<div data-nr="1" class="nr mymessage fl selected">
				<c:if test="${error!=null }">
					<div class="error">
						<div class="errorinfo">
							<a class="errorinfoclose"></a>
							<div>${error }</div>
						</div>
					</div>
				</c:if>
				<h2>订单详情</h2>

				<div class="mymessageform">
					<!-- style="border-top: 1px solid #e9ebee" -->
					<div class="tab">
						<div data-tab="1" class="item selected">全部</div>
						<div data-tab="2" class="item">待付款</div>
						<div data-tab="3" class="item">处理中</div>
						<div data-tab="4" class="item">租赁中</div>
						<div data-tab="5" class="item">已完成</div>
						<div data-tab="6" class="item">已取消</div>
					</div>
					<div class="tab-con">
						<div data-content="1" class="content selected">
							<table class="ordertable">
								<thead>
									<tr>
										<th>订单信息</th>
										<th>取车信息</th>
										<th>还车信息</th>
										<th>总计</th>
										<th>订单状态</th>
										<th>订单操作</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${userorders }" var="userorder">
										<tr>
											<td class="orderinfo"><b>${userorder.ordercartype }</b>
												<!-- <p>xxxxxx5人</p> -->
												<p>订单号：${userorder.orderid }</p></td>
											<td>
												<div class="qhcar_div">
													<p class="qhcar_p">
														<i class="zc_get_i"></i> ${userorder.ordergetaddress }
													</p>
													<p class="text_a_l">
														<fmt:formatDate value="${userorder.gettime }" type="both" />
													</p>
												</div>
											</td>
											<td>
												<div class="qhcar_div">
													<p class="qhcar_p">
														<i class="zc_back_i"></i> ${userorder.orderbackaddress }
													</p>

													<p class="text_a_l">
														<fmt:formatDate value="${userorder.backtime }" type="both" />
													</p>
												</div>
											</td>
											<td><div style="padding-top: 30px">¥${userorder.odermoney }</div></td>
											<td><div style="padding-top: 30px">${userorder.userorderzt }</div></td>
											<td>
												<div class="orderxg">
													<c:if test="${userorder.userorderzt=='待付款' }">
														<a href="pay?orderid=${userorder.orderid }" class="a_pay"
															target="_blank">立即付款</a>
														<form method="post">
															<input type="hidden" name="orderid"
																value="${userorder.orderid }">
															<button>取消订单</button>
														</form>
													</c:if>
													<c:if test="${userorder.userorderzt=='已付款' }">
														<form method="post">
															<input type="hidden" name="orderid"
																value="${userorder.orderid }"> <input
																type="hidden" name="orderzt"
																value="${userorder.userorderzt }"> <input
																type="hidden" name="gettime"
																value="${userorder.gettime }">
															<button>取消订单</button>
														</form>
													</c:if>
													<c:if test="${userorder.userorderzt=='租赁中' }">
														<a href="javascript:void(0);" class="a_pay">申请延期</a>
														<form method="post">
															<input type="hidden" name="orderid"
																value="${userorder.orderid }"> <input
																type="hidden" name="orderzt"
																value="${userorder.userorderzt }">
															<button>我已还车</button>
														</form>
													</c:if>
													<c:if test="${userorder.userorderzt=='已完成' }">
														<a href="ordercomment?orderid=${userorder.orderid }"
															class="a_pay">立即评论</a>
													</c:if>
													<a href="chenggong?ordernum=${userorder.orderid }">订单详情</a>
												</div>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
							<div style="text-align: center;margin-top: 10px;">
							<Mytag:page page="${page}" url="myorder" suoyoujilu="${ordercount }" totlepage="${pagecount}"/>
							</div>
						</div>
						<div data-content="2" class="content">
							<table class="ordertable">
								<thead>
									<tr>
										<th>订单信息</th>
										<th>取车信息</th>
										<th>还车信息</th>
										<th>总计</th>
										<th>订单状态</th>
										<th>订单操作</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${userdfkorders }" var="userorder">
										<tr>
											<td class="orderinfo"><b>${userorder.ordercartype }</b>
												<p>订单号：${userorder.orderid }</p></td>
											<td>
												<div class="qhcar_div">
													<p class="qhcar_p">
														<i class="zc_get_i"></i> ${userorder.ordergetaddress }
													</p>
													<p class="text_a_l">
														<fmt:formatDate value="${userorder.gettime }" type="both" />
													</p>
												</div>
											</td>
											<td>
												<div class="qhcar_div">
													<p class="qhcar_p">
														<i class="zc_back_i"></i> ${userorder.orderbackaddress }
													</p>

													<p class="text_a_l">
														<fmt:formatDate value="${userorder.backtime }" type="both" />
													</p>
												</div>
											</td>
											<td><div style="padding-top: 30px">¥${userorder.odermoney }</div></td>
											<td><div style="padding-top: 30px">${userorder.userorderzt }</div></td>
											<td>
												<div class="orderxg">
													<a href="pay?orderid=${userorder.orderid }" class="a_pay"
														target="_blank">立即付款</a>
													<form method="post">
														<input type="hidden" name="orderid"
															value="${userorder.orderid }">
														<button>取消订单</button>
													</form>
													<a href="chenggong?ordernum=${userorder.orderid }">订单详情</a>
												</div>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
						<div data-content="3" class="content">
							<table class="ordertable">
								<thead>
									<tr>
										<th>订单信息</th>
										<th>取车信息</th>
										<th>还车信息</th>
										<th>总计</th>
										<th>订单状态</th>
										<th>订单操作</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${userclzorders }" var="userorder">
										<tr>
											<td class="orderinfo"><b>${userorder.ordercartype }</b>
												<!-- <p>xxxxxx5人</p> -->
												<p>订单号：${userorder.orderid }</p></td>
											<td>
												<div class="qhcar_div">
													<p class="qhcar_p">
														<i class="zc_get_i"></i> ${userorder.ordergetaddress }
													</p>
													<p class="text_a_l">
														<fmt:formatDate value="${userorder.gettime }" type="both" />
													</p>
												</div>
											</td>
											<td>
												<div class="qhcar_div">
													<p class="qhcar_p">
														<i class="zc_back_i"></i> ${userorder.orderbackaddress }
													</p>

													<p class="text_a_l">
														<fmt:formatDate value="${userorder.backtime }" type="both" />
													</p>
												</div>
											</td>
											<td><div style="padding-top: 30px">¥${userorder.odermoney }</div></td>
											<td><div style="padding-top: 30px">${userorder.userorderzt }</div></td>
											<td>
												<div class="orderxg">
														<form method="post">
															<input type="hidden" name="orderid"
																value="${userorder.orderid }"> <input
																type="hidden" name="orderzt"
																value="${userorder.userorderzt }"> <input
																type="hidden" name="gettime"
																value="${userorder.gettime }">
															<button>取消订单</button>
														</form>
													<a href="chenggong?ordernum=${userorder.orderid }">订单详情</a>
												</div>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
						<div data-content="4" class="content">
							<table class="ordertable">
								<thead>
									<tr>
										<th>订单信息</th>
										<th>取车信息</th>
										<th>还车信息</th>
										<th>总计</th>
										<th>订单状态</th>
										<th>订单操作</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${userzlzorders }" var="userorder">
										<tr>
											<td class="orderinfo"><b>${userorder.ordercartype }</b>
												<p>订单号：${userorder.orderid }</p></td>
											<td>
												<div class="qhcar_div">
													<p class="qhcar_p">
														<i class="zc_get_i"></i> ${userorder.ordergetaddress }
													</p>
													<p class="text_a_l">
														<fmt:formatDate value="${userorder.gettime }" type="both" />
													</p>
												</div>
											</td>
											<td>
												<div class="qhcar_div">
													<p class="qhcar_p">
														<i class="zc_back_i"></i> ${userorder.orderbackaddress }
													</p>

													<p class="text_a_l">
														<fmt:formatDate value="${userorder.backtime }" type="both" />
													</p>
												</div>
											</td>
											<td><div style="padding-top: 30px">¥${userorder.odermoney }</div></td>
											<td><div style="padding-top: 30px">${userorder.userorderzt }</div></td>
											<td>
												<div class="orderxg">
														<a href="javascript:void(0);" class="a_pay">申请延期</a>
														<form method="post">
															<input type="hidden" name="orderid"
																value="${userorder.orderid }"> <input
																type="hidden" name="orderzt"
																value="${userorder.userorderzt }">
															<button>我已还车</button>
														</form>
													<a href="chenggong?ordernum=${userorder.orderid }">订单详情</a>
												</div>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
						<div data-content="5" class="content">
							<table class="ordertable">
								<thead>
									<tr>
										<th>订单信息</th>
										<th>取车信息</th>
										<th>还车信息</th>
										<th>总计</th>
										<th>订单状态</th>
										<th>订单操作</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${userywcorders }" var="userorder">
										<tr>
											<td class="orderinfo"><b>${userorder.ordercartype }</b>
												<p>订单号：${userorder.orderid }</p></td>
											<td>
												<div class="qhcar_div">
													<p class="qhcar_p">
														<i class="zc_get_i"></i> ${userorder.ordergetaddress }
													</p>
													<p class="text_a_l">
														<fmt:formatDate value="${userorder.gettime }" type="both" />
													</p>
												</div>
											</td>
											<td>
												<div class="qhcar_div">
													<p class="qhcar_p">
														<i class="zc_back_i"></i> ${userorder.orderbackaddress }
													</p>

													<p class="text_a_l">
														<fmt:formatDate value="${userorder.backtime }" type="both" />
													</p>
												</div>
											</td>
											<td><div style="padding-top: 30px">¥${userorder.odermoney }</div></td>
											<td><div style="padding-top: 30px">${userorder.userorderzt }</div></td>
											<td>
												<div class="orderxg">
														<a href="ordercomment?orderid=${userorder.orderid }"
															class="a_pay">立即评论</a>
													<a href="chenggong?ordernum=${userorder.orderid }">订单详情</a>
												</div>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
						<div data-content="6" class="content">
							<table class="ordertable">
								<thead>
									<tr>
										<th>订单信息</th>
										<th>取车信息</th>
										<th>还车信息</th>
										<th>总计</th>
										<th>订单状态</th>
										<th>订单操作</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${useryqxorders }" var="userorder">
										<tr>
											<td class="orderinfo"><b>${userorder.ordercartype }</b>
												<p>订单号：${userorder.orderid }</p></td>
											<td>
												<div class="qhcar_div">
													<p class="qhcar_p">
														<i class="zc_get_i"></i> ${userorder.ordergetaddress }
													</p>
													<p class="text_a_l">
														<fmt:formatDate value="${userorder.gettime }" type="both" />
													</p>
												</div>
											</td>
											<td>
												<div class="qhcar_div">
													<p class="qhcar_p">
														<i class="zc_back_i"></i> ${userorder.orderbackaddress }
													</p>
													<p class="text_a_l">
														<fmt:formatDate value="${userorder.backtime }" type="both" />
													</p>
												</div>
											</td>
											<td><div style="padding-top: 30px">¥${userorder.odermoney }</div></td>
											<td><div style="padding-top: 30px">${userorder.userorderzt }</div></td>
											<td>
												<div class="orderxg">
													<a href="chenggong?ordernum=${userorder.orderid }">订单详情</a>
												</div>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container orderztsm">
			<h3>订单状态说明</h3>
			<ul>
				<li><span class="s-green">预订成功</span>
					<p>订单已确认，尚未提车</p></li>
				<li><span class="s-orange">等待付款</span>
					<p>订单尚未付款，请在1小时内完成支付</p></li>
				<li><span class="s-blue">处理中</span>
					<p>您的订单已提交，正在处理中</p></li>
				<li><span class="s-blue">租赁中</span>
					<p>车辆正在租赁过程中</p></li>
				<li><span class="s-orange">待评价</span>
					<p>订单完成48小时内，可对我们做出评价</p></li>
				<li><span class="s-yellow">已完成</span>
					<p>已还车，订单结算完毕</p></li>
				<li><span class="s-gray">已取消</span>
					<p>订单已经取消</p></li>
			</ul>
		</div>
		<div class="container orderztsm ordernotice">
			<h3>注意事项</h3>
			<p>123</p>
			<p>123</p>
			<p>123</p>
		</div>
	</div>
	<jsp:include page="footer-inc.jsp" flush="true"></jsp:include>
</body>
</html>