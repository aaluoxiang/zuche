<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="<%=request.getContextPath()%>/">
		<meta charset="UTF-8">
		<script type="text/javascript" src="static/js/jquery-1.12.4.js" ></script>
		<script type="text/javascript" src="static/js/yuding.js" ></script>
		<link rel="stylesheet" href="static/css/yuding.css" />
<title>预定界面</title>


<style type="text/css">

			.zc-phone{
				background-image: url(static/img/icon-logo.png);
				background-repeat: no-repeat;
				
			}
			.zc-phone{
				width: 160px;
				height: 30px;
				background-position: 0 -572px;
			}
			.up.down {
			    background:url(static/img/order_sprite.png)no-repeat -295px -24px;
			}
			.up {
			    width: 12px;
			    height: 8px;
			    background:url(static/img/order_sprite.png) no-repeat -275px -24px;
			    overflow: hidden;
			    display: block;
			    position: absolute;
			    left: 83px;
			    top: 36px;
			}
			.cfclose {
			    cursor: pointer;
			    display: inline-block;
			    position: absolute;
			    top: 15px;
			    right: 7px;
			    width: 34px;
			    height: 34px;
			    background: url(static/img/close.png) no-repeat;
			    z-index: 2000019;
			}
			.btn_graywauto:hover , .btn_greenwauto:hover , .btn_orgwauto:hover , .btn_bluewauto:hover  {
			    background-image: url(static/img/butbgxhover.png);
			    background-repeat: repeat-x;
			  
			}
			.fu{
				position: absolute;
				width:100%;
				height: 1470px;
				left: 0;
				top: 0;
				background:darkgray;
				opacity: 0.60;
				z-index: 5;
				display: none;
			}
		
</style>

</head>
<body>
<div class="juzhong">
			<div class="dakuang">
				<div class="order-head">
					<div class="logo-zc">
						<a><img src="static/img/orlogo.png"></a>
						</div>
					<div class="xq-zc">
						<ul>
							<li>
								<div class="zc-phone"  style="cursor: pointer;"></div>
								<span class="xianshi"><img src="static/img/telDown2.png"></span>
								<img class="dianhua" src="static/img/telother2.png" />
							</li>
							<li class="zc-name">
								<span>欢迎您，${glname}</span>
							</li>
							<li class="zc-name">
								<a class="zc-dingdan" href="myorder">我的订单</a>
							</li>
							<li class="zc-name" style="border-right:none;">
								<a class="tuichu" href="logout">退出</a>
							</li>
						</ul>
					</div>
				</div>
		
				<div class="zc-yuding sz_succeedB yellow">
					<div class="successinfo">
						<span class="yuding">${ztname}</span>
						<span class="zongjia">订单总价:</span>
						<span> 
							<em class="rmb">￥</em>
							<em class="money">${zongjia}</em>
						</span>
						<c:if test='${ztname =="待付款"}'>
						<span style="color: #ff8560;"> 
							请及时付款，超过一天订单将自动取消！
						</span>
						</c:if>
					</div>
					<div class="f14 p10_0">
						<span>订单号：${ordernum}</span>
						<a>|</a>
						<span>租车人：${name}</span>
						<a>|</a>
						<span>租期：${zuqi}天</span>
						<a>|</a>
						<span>取车时需要刷取预授权：</span>
						<span class="carPrice sz_priceOrangeSmall ">
							<em class="rmb">￥</em>
							<em class="num">3000</em>
						</span>
					</div>
				</div>
				<div class="order_s_info index_bdb2 orDeBoxRight m20_0" style="background:#fff;">
					<h3>基本信息</h3>
					<table width="100%" class="f14" border="0" cellspacing="0" cellpadding="0">
						<tbody>
						<c:forEach items="${userorder}" var="userorder">
							<tr>
								<td width="10"></td>
								<td height="80" align="center" class="borderBlueB borderBlueR pre"
									valign="middle" rowspan="3" colspan="3">
									<img src="${tupian}" width="138" height="80"/>
									<span class="config-btn">配置信息</span>
								</td>
								<td width="260" height="66" align="center" class="quchesjian" rowspan="2">
									<strong>取车时间</strong>
									：${gshtime}
								</td>
								<td width="78" align="right" height="66" class="quchedidian " rowspan="2">
									<strong>取车地点</strong>
									：
								</td>
								<td width="230" class="didian " rowspan="2">${userorder.ordergetaddress}</td>
							</tr>
							<tr>
								<td></td>
							</tr>
							<tr>
								<td class="borderBlueB"></td>
								<td height="66" align="center" class=" haichesjian" valign="middle" rowspan="3">
									<strong>还车时间</strong>
									：${bshtime}
								</td>
								<td align="right" class="haichedidain" rowspan="2">
									<strong>还车地点</strong>
									：
								</td>
								<td align="left" class="didianhaiche" valign="middle" rowspan="2">
									${userorder.orderbackaddress}
								</td>
							</tr>
							</c:forEach>
							<c:forEach items="${xinxi}" var="xinxi">
							<tr class="f16 borderBlueB" style="font-family: '微软雅黑';">
								<td></td>
								<td height="40" align="center" class="borderBlueR" valign="middle">
									${xinxi.carTypeName}
								</td>
								<td align="center" class="borderBlueR" valign="middle">${xinxi.carXinxi}</td>
								<td align="center" class="borderBlueR" valign="middle">${xinxi.carDrive}</td>
							</tr>
							</c:forEach>
						</tbody>
					</table>
					<h3 class="pink">费用明细</h3>
					<div class="orderFeeDetails">
						<div class="feeItem borderBlueBda">
							<div class="feePrice">
								<span class="sz_OrderpOrangeM">
									<em class="rmb">￥</em>
									<em class="num">${price}</em>
								</span>
							</div>
							<div class="calculate">
								<span class="sz_priceGraySmall">
									<em class="num">${carprice}*${zuqi}</em>
									<em class="unit">=</em>
								</span>
							</div>
							<span class="feeName">车辆租赁及门店服务费</span>
						</div>
						<div class="feeItem borderBlueBda">
							<div class="feePrice">
								<span class="sz_OrderpOrangeM">
									<em class="rmb">￥</em>
									<em class="num">${40*zuqi}</em>
								</span>
							</div>
							<div class="calculate">
								<span class="sz_priceGraySmall">
									<em class="num">40*${zuqi}</em>
									<em class="unit">=</em>
								</span>
							</div>
							<span class="feeName">基本保险费</span>
						</div>
						<div class="feeItem borderBlueBda selectabletotal" style="overflow:inherit;">
							<div class="feePrice">
								<span class="sz_OrderpOrangeM">
									<em class="rmb">￥</em>
									<em class="num">${mianpei+60-youhuimoney}</em>
								</span>
							</div>
							<div class="calculate">
								<span class="sz_priceGraySmall">
									<em class="num">合计</em>
								</span>
							</div>
							<span class="feeName">
								其他费用
								<b class="up down" style="display: block;"></b>
							</span>
						</div>
						<div class="feeline50 discount borderBlueBda" style="display: none;position: relative;">
								<div class="feePrice">
								<span class="sz_OrderpOrangeM sa">
									<em class="rmb "></em>
									<em class="rmb ">￥</em>
									<em class="num ">${60}</em>
								</span>
							</div>

							<span class="feeName">手续费</span>
							<c:if test="${mianpei !=''}">
							<div class="feePrice">
								<span class="sz_OrderpOrangeM sa" style="position: relative;top:22px;left: 71px;">
									<em class="rmb "></em>
									<em class="rmb ">￥</em>
									<em class="num ">${mianpei}</em>
								</span>
							</div>
							<div class="calculate"style="position: relative;top:25px;left: 75px;">
								<span class="sz_priceGraySmall" style="position: relative;left: 17px;">
									<em class="num ">50*${zuqi}</em>
									<em class="unit ">=</em>
								</span>
							</div>
							<span class="feeName"style="position: relative;top: 25px;left: -90px;">不计免赔</span>
						</c:if>
							<c:if test="${mianpei ==''}">
							<div class="feePrice">
								<span class="sz_OrderpOrangeM sa" style="position: relative;top:22px;left: 71px;">
									<em class="rmb "></em>
									<em class="rmb ">￥</em>
									<em class="num ">0</em>
								</span>
							</div>
							<div class="calculate"style="position: relative;top:25px;left: 75px;">
								<span class="sz_priceGraySmall" style="position: relative;left: 17px;">
									<em class="num "></em>
									<em class="unit "></em>
								</span>
							</div>
							<span class="feeName"style="position: relative;top: 25px;left: -90px;">不计免赔</span>
						</c:if>
						<c:if test="${youhuimoney != 0}">
							<div class="feePrice you" >	
								<span class="sz_OrderpOrangeM sb" style="position: absolute;top:60px;left:889px;width:100px">
									<em class="rmb "></em>
									<em class="rmb ">￥</em>
									<em class="num ">${youhuimoney}</em>
								</span>
							</div>
							
							<span class="feeName youhui"style="position: relative;top: 45px;left: -190px;">优惠券</span>
						</c:if>
						</div>
					</div>
					<div class="orderFeeTotal">
						<div class="total102">
							<span class="yjtotal">订单总价：</span>
							<span class="sz_OrderpOrangeB">
								<em class="rmb">￥</em>
								<em class="num">${zongjia}</em>
							</span>
						</div>
					</div>
				</div>
				<div class="ac">
				<c:if test="${orderzt == 4 }">
					<div class="online-btn-p">
						<a href='pay?orderid=${ordernum}'><input class="btn_graywauto f16 newred" id="" type="button" value="支付租金"/></a>
						<div class="all-huitips">
							<i class="arrtop">◆</i>
							<i class="hui">惠</i>
							<span class="txt">支付全款，享到店快速取车</span>
						</div>
					</div>
					</c:if>
					<div class="top_new_tip_btn" id="orderModifyDiv">
						<input class="btn_graywauto gray f16" onclick="window.print()" id="orderModify"
							type="button" value="打印订单"/>
					</div>
					<a href="myorder"><input class="btn_graywauto gray f16" id="monitor_fourth_detail"
						type="button" value="订单中心"/></a>
				</div>
				<div class="index_bdb2 m20_0 p20 bg_white">
					<p class="f16 pb10">如何取车</p>
					<p class="f12 gray line_h24">
						1.取车时,出示以下证件的原件：本人二代身份证、本人国内有效驾驶证正副本、本人一张信用及可用额度均不低于3000元的国内有效信用卡，所有证件有效期须至少超过当次租期的一个月以上。
					</p>
					<p class="f12 gray line_h24">
						2.请您准时取车，超时取车，起租时间按预订取车时间起算。
					</p>
				</div>
				<div class="index_bdb2 m20_0 p20 bg_white">
					<p class="f16 pb10">退改规则</p>
					<p class="f12 gray line_h24"style="padding-left: 12px;">
						订单修改或取消：
						<br />
						a)取车时间距当前时间≥2个工作小时，请提前致电400 616 6666：取车时间在门店的营业时间内，请提前2小时申请；取车时间在门店营业时间之外，请您尽早致电申请。
						<br />
						b)取车时间距当前时间＜2个工作小时，不接受修改。
					</p>
					<font class="gray">
						（小贴士：如果您修改订单或取消订单重新预订，价格可能会发生变化。）
					</font>
				</div>
				<div class="sz_footer">
					<p>
						<a title="关于我们" href="aboutus">关于我们</a>
						<a title="精彩活动" href="actcenter" >精彩活动</a>
						<a title="帮助中心" href="helpcenter">帮助中心</a>
					</p>
					<p>Copyright©2008-2017 www.zuche.com All Rights Reserved.</p>
			<p>如果您对神州租车网站有任何意见,欢迎发送邮件到 web@zuche.com</p>
			<p>神州租车官网 京ICP备10005002号   |   京公网安备号 11010502026705</p>
				</div>
			</div>
			
			
		</div>
		<div class="configbox" style="display:none;">
			<div class="cftitle">
				<span>${carname}配置信息</span>
				<i class="cfclose"></i>
			</div>
			<div class="cflist">
				<ul>
				<c:forEach items="${xinxi}" var="xinxi">
					<li>
						<b class="zws"></b>
						座位数：
						<span>${xinxi.carSeatNum}</span>
					</li>
					<li>
						<b class="cms"></b>
						车门数：
						<span>${xinxi.carDoorNum}</span>
					</li>
					<li>
						<b class="rlls"></b>
						燃料类型：
						<span>${xinxi.carOil}</span>
					</li>
					<li>
						<b class="bsxlx"></b>
						变速箱类型：
						<span>${xinxi.carBsx}</span>
					</li>
					<li>
						<b class="pl"></b>
						排        量：
						<span>${xinxi.carPail}</span>
					</li>
					<li>
						<b class="rybh"></b>
						燃油标号：
						<span>92-93汽油</span>
					</li>
					<li>
						<b class="qdfs"></b>
						驱动方式：
						<span>${xinxi.carDrive}</span>
					</li>
					<li>
						<b class="fdjjqxs"></b>
						发动机进气形式：
						<span>自然吸气</span>
					</li>
					<li>
						<b class="tc"></b>
						天       窗：
						<span>${xinxi.carSkyWindow}</span>
					</li>
					<li>
						<b class="yxrl"></b>
						邮箱容量：
						<span>${xinxi.carOilBox}</span>
					</li>
					<li>
						<b class="yx"></b>
						音箱：
						<span>4</span>
					</li>
					<li>
						<b class="zy"></b>
						座      椅：
						<span>皮革座椅</span>
					</li>
					<li>
						<b class="dcld"></b>
						倒车雷达：
						<span>${xinxi.carGps}</span>
					</li>
					<li>
						<b class="qin"></b>
						气        囊：
						<span>${xinxi.carGasbag}</span>
					</li>
					<li class="nonebd">
						<b class="dvd"></b>
						DVD / CD：
						<span>${xinxi.carDv}</span>
					</li>
					<li class="nonebd">
						<b class="gps"></b>
						GPS导航：
						<span>${xinxi.carGps}</span>
					</li>
					</c:forEach>
				</ul>
			</div>
			
			
		</div>
			
		
		<div class="fu"></div>
	
</body>
</html>