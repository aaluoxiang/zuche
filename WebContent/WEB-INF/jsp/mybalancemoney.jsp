<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
<head>
<base href="<%=request.getContextPath()%>/">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>我的余额</title>
<link rel="stylesheet" type="text/css" href="static/css/center.css">
<link rel="stylesheet" type="text/css" href="static/css/myinfor.css">
<link rel="stylesheet" type="text/css" href="static/css/owncentermyyue.css">
</head>
<body>

	<jsp:include page="header-inc.jsp" flush="true"></jsp:include>
	<jsp:include page="tou.jsp" flush="true"/>
	<div class="a_acon b">
		<jsp:include page="menu.jsp" flush="true"/>
		<div class="container">
			<div data-nr="1" class="nr mymessage fl selected">
				<h2>我的余额</h2>
				<div class="mymessageform" style="border-top:1px solid #e9ebee" >
					<div>
						<h3 style="padding-top: 20px;">消费记录</h3>
						<table>
							<thead>
								<tr>
									<th>订单号</th>
									<th>金额</th>
									<th>消费方向</th>
									<th>消费时间</th>
								</tr>
							</thead>	
							<tbody>
								<c:forEach items="${xfjls }" var="xfjl">
									<tr>
										<td>${xfjl.orderid }</td>
										<td>${xfjl.ordermoney }</td>
										<td>${xfjl.orderxffx }</td>
										<td><fmt:formatDate value="${xfjl.orderxftime }" type="both"/></td>
									</tr>
								</c:forEach>
							</tbody>	
						</table>
					</div>
					<div>
						<h3>充值记录</h3>
						<table>
							<thead>
								<th>金额</th>
								<th>消费方向</th>
								<th>消费时间</th>
							</thead>
							<tbody>
								<c:forEach items="${czjls }" var="czjl">
									<tr>
										<td>${czjl.chozhimoney }</td>
										<td>${czjl.chozhijiaoyifangxiang }</td>
										<td><fmt:formatDate value="${czjl.chozhidata }" type="both"/></td>
									</tr>
								</c:forEach>
							</tbody>				
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="footer-inc.jsp" flush="true"></jsp:include>
</body>
</html>