<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title></title>
		<base href="<%request.getContextPath();%>" >
		<link rel="stylesheet" href="static/css/actfree.css" type='text/css'/>
	</head>
	<body>
		<jsp:include page="header-inc.jsp"></jsp:include>
		<div class="wrap">
			<div class="ban b1"></div>
			<div class="ban b2"></div>
			<div class="ban b3"></div>
			<div class="ban b4"></div>
			<div class="ban b5"></div>
			<div class="ban b6">
				<a href="mainjump" class="btn_wrap book">
	            <img src="static/img/web_sub_go_font.jpg" class="className animated">
		        </a>
		        <p>※  实际价格请以预订时价格为准（价格会根据预订时间和车辆库存等原因浮动）</p>
			</div>
			<div class="newcar_list_wrap">
		        <h2>经济车型69元起租</h2>
		        <ul>
		            <li><img src="static/img/web_list_1.jpg" ></li>
		            <li><img src="static/img/web_list_2.jpg" ></li>
		            <li><img src="static/img/web_list_3.jpg" ></li>
		        </ul>
		    </div>
		</div>
	</body>
</html>
