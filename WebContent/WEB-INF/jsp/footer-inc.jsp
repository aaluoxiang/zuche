<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <link rel="stylesheet" href="<%request.getContextPath(); %>static/css/foot.css" type='text/css'/>
<div class="foot">
			<ul class="foot_ul">
				<li><span>租车预定</span>
					<ul class="foot_2ul">
						<li><a href="helpcenter">服务时间</a></li>
						<li><a href="helpcenter">服务预定</a></li>
					</ul>
				</li>
				<li><span>会员服务</span>
					<ul class="foot_2ul">
						<li><a href="helpcenter">租车价格</a></li>
						<li><a href="helpcenter">会员积分</a></li>
					</ul>
					
				</li>
				<li><span>常见问题</span>
					<ul class="foot_2ul">
						<li><a href="helpcenter">结算流程</a></li>
						<li><a href="helpcenter">租期计费</a></li>
					</ul>
				</li>
				<li><span>我们的服务</span>
					<ul class="foot_2ul">
						<li><a href="helpcenter">保险责任</a></li>
						<li><a href="helpcenter">救援及备用</a></li>
					</ul>
				</li>
				<ul>
				<li class="list_img"><img class="showimg" src="static/img/share_icon_gz.jpg" width="100px" height="100px"alt="111"/>
					<br />
					<span>关注微信公众号</span>
				</li>
					
				<li class="list_img"><img class="showimg" src="static/img/share_icon_gz.jpg" width="100px" height="100px" alt="222"/>
				<br />
					<span>关注微信公众号</span>
				</li>
				</ul>
			</ul>
			
			<ul class="foot_address">
				<li>
					<p>${Webintro}</p>
				</li>
				<li>
					<p>24小时免费热线电话:</p>
					<!--<br />-->
					<span class="phone">
						${Webtel}
					</span>
				</li>
			</ul>
			<div class="foot_link">
				<span class="f_left">
					友情链接:
				</span>
				<ul class="f_right">
					<li>
						<a href="">aaluoxiang</a>
					</li>
					<li>
						<a href="">aaluoxiang</a>
					</li>
				</ul>
			</div>
			<div class="foot_end">
				<p class="end_left">${Webdes}</p>
				<p class="end_right">如果您对本汽车租赁有限公司有任何意见，欢迎发送联系电话:${Webtel}</p>
				
			</div>
		</div>