<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=request.getContextPath()%>/">
<meta charset="UTF-8">
<script type="text/javascript" src="static/js/jquery-1.12.4.js"></script>
<script type="text/javascript" src="static/js/dingdan.js"></script>
<script type="text/javascript" src="static/js/city.js"></script>
<link rel="stylesheet" href="static/css/dingdan.css" />
<title></title>

<style>
.newpg-head .order-logo {
	display: inline-block;
	position: relative;
	width: 213px;
	height: 58px;
	background: url(static/img/order_toplogo.png) no-repeat;
}

.newpg-head .phone-icon {
	width: 161px;
	height: 22px;
	cursor: pointer;
	display: inline-block;
	background: url(static/img/order_sprite.png) no-repeat;
}

.ifr-qc, .ifr-hc, .ifr-time, .ifr-local, .tan-icon, .blue-ques,
	.blue-downarr, .blue-uparr, .blue-yes, .white-no, .billclose, .litar-l,
	.litar-t {
	display: inline-block;
	background: url(static/img/order_sprite.png) no-repeat;
}

.cfclose {
	cursor: pointer;
	display: inline-block;
	position: absolute;
	top: 7px;
	right: 7px;
	width: 34px;
	height: 34px;
	background: url(static/img/close-g.png) no-repeat;
	z-index: 2000019;
}

.ifr-qc {
	width: 16px;
	height: 16px;
	background-position: -326px 0;
}

.ifr-time {
	width: 14px;
	height: 14px;
	background-position: -380px 0;
}

.ifr-local {
	width: 12px;
	height: 14px;
	background-position: -380px -22px;
}

.ifr-hc {
	width: 16px;
	height: 16px;
	background-position: -355px 0;
}

.tan-icon {
	width: 16px;
	height: 16px;
	background-position: -355px -22px;
	position: absolute;
	top: 12px;
	left: 10px;
}

.blue-ques {
	width: 16px;
	height: 16px;
	background-position: -329px -23px;
	vertical-align: middle;
	cursor: pointer;
}

.litar-l {
	width: 6px;
	height: 10px;
	background-position: -394px -67px;
}

.blue-downarr.open {
	background-position: -275px -24px;
}

.blue-downarr {
	width: 13px;
	height: 8px;
	background-position: -295px -24px;
}

.blue-yes {
	width: 14px;
	height: 14px;
	background-position: -358px -49px;
}

.radio-w {
	width: 12px;
	height: 12px;
	background-position: -294px 0;
}

.newpg-conf .radio-w.curr {
	background-position: -267px 0;
}

.radio-w {
	width: 12px;
	height: 12px;
	background-position: -294px 0;
}

.litar-t {
	width: 10px;
	height: 6px;
	background-position: -375px -67px;
}

.zc-close, .zc-retan {
	display: inline-block;
	background: url(static/img/icon.png);
}

.zc-retan {
	width: 17px;
	height: 17px;
	background-position: -440px 0px;
	vertical-align: middle;
}

.zc-close {
	width: 12px;
	height: 12px;
	background-position: -480px 0px;
}

.m1, .m2, .order-erpos300 {
	display: none;
}

table {
	border-collapse: collapse;
}

table th {
	height: 30px;
	line-height: 30px;
	background-color: #f8f8fa;
	font-size: 14px;
	color: #93939e;
	font-weight: 400;
	border-right: 1px solid #e9ebee;
}

table tr {
	border: 1px solid #e9ebee;
}

table td {
	text-align: center;
	font-size: 13px;
	padding: 14px;
	color: #60606c;
	border-right: 1px solid #e9ebee;
	width: 50px;
}

.pay-bubble dl dd{
    line-height: 24px;
    margin-bottom: 5px;
    font-size: 14px;
    color: #60606c;
    font-style: normal;
}
</style>
<script>

	$(function() {
		
		var cityAll=city.citylist;
		$.each(cityAll,function(i,n){
		    $("#province").append('<option value="'+ n.p + '">' + n.p + '</option>');
		}); 
		$("#province,#city").change(function(){
		    if($(this).attr("id") == "province"){
		        $("#city").html("").append('<option value="市（区）">市（区）</option>').next().html("").append('<option value="县、镇">县、镇</option>');
		    }else{
		        $("#county").html("").append('<option value="县、镇">县、镇</option>');
		    }
		    $.each(cityAll,function(j,k){
		        if($("#province").val() == k.p){
		            $.each(k.c,function(l,m){
		                $("#city").append('<option value="'+ m.n + '">' + m.n + '</option>');
		                if(m.a){
		                    $("#county").show();
		                    if($("#city").val() == m.n){
		                        $.each(m.a,function(e,f){
		                            $("#county").append('<option value="'+ f.s + '">' + f.s + '</option>');
		                        });
		                    }    
		                }else{
		                    $("#county").hide();
		                        
		                }
		            });
		        }
		    });
		});
		
		
		$(".look-change").click(
				function() {
					var stime1 = $(".stime1").html();
					var stime2 = $(".stime2").html();
					var quche1 = $(".quche1").html();
					var quche2 = $(".quche2").html();
					var btime1 = $(".btime1").html();
					var btime2 = $(".btime2").html();
					var huanche1 = $(".huanche1").html();
					var huanche2 = $(".huanche2").html();

					$(this).attr(
							"href",
							"shangcheng?stime1=" + stime1 + "&stime2=" + stime2
									+ "&quche1=" + quche1 + "&quche2=" + quche2
									+ "&btime1=" + btime1 + "&btime2=" + btime2
									+ "&huanche1=" + huanche1 + "&huanche2="
									+ huanche2 + "");
				});

		$("#name").blur(
				function() {
					var xingming = $(this).val();

					$.get("error", {
						xingming : xingming,
						id : "checkname"
					}, function(msg) {

						if ("success") {
							$(".m1").css("display", "none");

						}
						if (msg == "请输入真实姓名！" || msg == "姓名不能为数字！") {

							$(".m1").css("display", "block")
									.children(".zc-con").text(msg);

						}
					});

				});
		$("#xphone").blur(
				function() {
					var dianhua = $(this).val();

					$.get("error", {
						dianhua : dianhua,
						id : "checkphone"
					}, function(msg) {

						if ("success") {
							$(".m3").css("display", "none");
						}

						if (msg == "请输入联系电话!" || msg == "请输入正确的电话!") {
							
							$(".m3").css("display", "block")
									.children(".zc-con").text(msg);
						}

					});

				});

		$("#xidentitycard").blur(
				function() {
					var zhengjian = $(this).val();

					$.get("error", {
						zhengjian : zhengjian,
						id : "checkcard"
					}, function(msg) {

						if ("success") {
							$(".m2").css("display", "none");
							$(".gopay-blue").attr('disabled', "false");

						}
						if (msg == "请输入证件号码！" || msg == "请输入正确的身份证号！"
							|| msg == "此身份证号已被占用!") {	
							$(".m2").css("display", "block").children(
									".err-con").text(msg);

						}
					});

				});

		$("#memberEmail").blur(
				function() {
					var youxiang = $(this).val();

					$.get("error", {
						youxiang : youxiang,
						id : "checkemail"
					}, function(msg) {

						if ("success") {
							$(".order-erpos300").css("display", "none");

						}
						if (msg == "请输入正确的邮箱！") {

							$(".order-erpos300").css("display", "block")
									.children(".err-con").text(msg);

						}
					});

				});
		$(".zc-close").click(function() {
			if ($(this).parent().hasClass("m1")) {
				$(".m1").css("display", "none");
			}

			if ($(this).parent().hasClass("m2")) {
				$(".m2").css("display", "none");
			}

			if ($(this).parent().hasClass("order-erpos300")) {
				$(".order-erpos300").css("display", "none");
			}
			if ($(this).parent().hasClass("m3")) {
				$(".m3").css("display", "none");
			}

		});

		$(".gopay-blue").click(function() {
							var name = $("#name").val();
							var card = $("#xidentitycard").val();
							if (name == "" || card == "") {
								$(".yanzheng").css('display', 'block');
								$('.fu').css('display', 'block');

							} else if ($(".userzt").attr("value") == "冻结") {
								$(".dongjie").css('display', 'block');
								$('.fu').css('display', 'block');
							} else {
								$(".gopay-blue").css("background-color",
										"#2b99ff");
								var stime1 = $(".stime1").html();
								var stime2 = $(".stime2").html();
								var quche1 = $(".quche1").html();
								var quche2 = $(".quche2").html();
								var btime1 = $(".btime1").html();
								var btime2 = $(".btime2").html();
								var huanche1 = $(".huanche1").html();
								var huanche2 = $(".huanche2").html();
								var lp1 = $('.lp1').text();

								var username = $("#name").val();
								var userphone = $("#xphone").val();
								var usercardnum = $("#xidentitycard").val();
								var useremail = $("#memberEmail").val();
								var zongjia = $(".orderTotalPrice").text();
								if ($("#siji").is(':checked')) {
									var siji = "是"
								} else {
									var siji = "否"

								}
								if ($("#need-recepit").is(':checked')) {
									var fptt = $(".invoice_select").val();
									var fpname = $("#receipt_user").val();
									var fpphone = $("#receipt_user_mobile")
											.val();
									var fpyb = $("#receipt_address_zip").val();
									var fpaddress = $("#province").val()
											+ $("#city").val() + $("#county").val()
											+ $("#receipt_address").val();
								}
								var yhqid = $('input:radio[name="mpservice"]:checked').attr('sname');
								var mianpei = $('input:checkbox[name="mpservices"]:checked').attr('names');
								alert(mianpei);
								var form = $("<form></form>");
								form.attr("action", "dingdan");
								form.attr("method", "post");
								form.append($("<input type='text' value="+yhqid+" name='yhqid'/>"));
								form.append($("<input type='text' value="+${userId}+" name='userId'/>"));
								form.append($("<input type='text' value="+lp1+" name='carname'/>"));
								form.append($("<input type='text' value="+stime1+" name='stime1'/>"));
								form.append($("<input type='text' value="+stime2+" name='stime2'/>"));
								form.append($("<input type='text' value="+quche1+" name='quche1'/>"));
								form.append($("<input type='text' value="+quche2+" name='quche2'/>"));
								form.append($("<input type='text' value="+btime1+" name='btime1'/>"));
								form.append($("<input type='text' value="+btime2+" name='btime2'/>"));
								form.append($("<input type='text' value="+huanche1+" name='huanche1'/>"));
								form.append($("<input type='text' value="+huanche2+" name='huanche2'/>"));
								form.append($("<input type='text' value="+username+" name='username'/>"));
								form.append($("<input type='text' value="+userphone+" name='userphone'/>"));
								form.append($("<input type='text' value="+usercardnum+" name='usercardnum'/>"));
								form.append($("<input type='text' value="+useremail+" name='useremail'/>"));
								form.append($("<input type='text' value="+siji+" name='siji'/>"));
								form.append($("<input type='text' value="+zongjia+" name='zongjia'/>"));
								form.append($("<input type='text' value="+fptt+" name='fptt'/>"));
								form.append($("<input type='text' value="+fpname+" name='fpname'/>"));
								form.append($("<input type='text' value="+fpphone+" name='fpphone'/>"));
								form.append($("<input type='text' value="+fpyb+" name='fpyb'/>"));
								form.append($("<input type='text' value="+fpaddress+" name='fpaddress'/>"));
								form.append($("<input type='text' value="+mianpei+" name='mianpei'/>"));
								form.appendTo("body");
								form.css('display', 'none');
								form.submit();

							}

						});
		$(".gopay-red").click(function() {
			var name = $("#name").val();
			var card = $("#xidentitycard").val();
			if (name == "" || card == "") {
				$(".yanzheng").css('display', 'block');
				$('.fu').css('display', 'block');

			} else if ($(".userzt").attr("value") == "冻结") {
				$(".dongjie").css('display', 'block');
				$('.fu').css('display', 'block');
			} else {
				$(".gopay-blue").css("background-color","#2b99ff");
				var stime1 = $(".stime1").html();
				var stime2 = $(".stime2").html();
				var quche1 = $(".quche1").html();
				var quche2 = $(".quche2").html();
				var btime1 = $(".btime1").html();
				var btime2 = $(".btime2").html();
				var huanche1 = $(".huanche1").html();
				var huanche2 = $(".huanche2").html();
				var lp1 = $('.lp1').text();

				var username = $("#name").val();
				var userphone = $("#xphone").val();
				var usercardnum = $("#xidentitycard").val();
				var useremail = $("#memberEmail").val();
				var zongjia = $(".orderTotalPrice").text();
				if ($("#siji").is(':checked')) {
					var siji = "是"
				} else {
					var siji = "否"

				}
				if ($("#need-recepit").is(':checked')) {
					var fptt = $(".invoice_select").val();
					var fpname = $("#receipt_user").val();
					var fpphone = $("#receipt_user_mobile")
							.val();
					var fpyb = $("#receipt_address_zip").val();
					var fpaddress = $("#province").val()
							+ $("#city").val() + $("#county").val()
							+ $("#receipt_address").val();
				}
				var yhqid = $(
						'input:radio[name="mpservice"]:checked')
						.attr('sname');
				var mianpei = $(
						'input:checkbox[name="mpservices"]:checked')
						.attr('names');

				var form = $("<form></form>");
				form.attr("action", "jiezhang");
				form.attr("method", "get");
				form.append($("<input type='text' value="+yhqid+" name='yhqid'/>"));
				form.append($("<input type='text' value="+${userId}+" name='userId'/>"));
				form.append($("<input type='text' value="+lp1+" name='carname'/>"));
				form.append($("<input type='text' value="+stime1+" name='stime1'/>"));
				form.append($("<input type='text' value="+stime2+" name='stime2'/>"));
				form.append($("<input type='text' value="+quche1+" name='quche1'/>"));
				form.append($("<input type='text' value="+quche2+" name='quche2'/>"));
				form.append($("<input type='text' value="+btime1+" name='btime1'/>"));
				form.append($("<input type='text' value="+btime2+" name='btime2'/>"));
				form.append($("<input type='text' value="+huanche1+" name='huanche1'/>"));
				form.append($("<input type='text' value="+huanche2+" name='huanche2'/>"));
				form.append($("<input type='text' value="+username+" name='username'/>"));
				form.append($("<input type='text' value="+userphone+" name='userphone'/>"));
				form.append($("<input type='text' value="+usercardnum+" name='usercardnum'/>"));
				form.append($("<input type='text' value="+useremail+" name='useremail'/>"));
				form.append($("<input type='text' value="+siji+" name='siji'/>"));
				form.append($("<input type='text' value="+zongjia+" name='zongjia'/>"));
				form.append($("<input type='text' value="+fptt+" name='fptt'/>"));
				form.append($("<input type='text' value="+fpname+" name='fpname'/>"));
				form.append($("<input type='text' value="+fpphone+" name='fpphone'/>"));
				form.append($("<input type='text' value="+fpyb+" name='fpyb'/>"));
				form.append($("<input type='text' value="+fpaddress+" name='fpaddress'/>"));
				form.append($("<input type='text' value="+mianpei+" name='mianpei'/>"));
				form.appendTo("body");
				form.css('display', 'none');
				form.submit();

			}

		});

		$(".oth-che").click(function() {
			if ($(this).is(':checked')) {
				//	$("[class='need-recepit']:checkbox:checked").length > 0
				if($(".oth-youhui").is(':checked')){
					var feiyong = ${zuqi * 50};
					var ymoney=$(".orderTotalPrice").text();
					var yymoney=parseInt(ymoney) + parseInt(feiyong);
					$("#otherPriceTotal ").text(feiyong);
					$(".orderTotalPrice").text(yymoney);
				}else{
					var feiyong = ${zuqi * 50 + 60};
					var heji = ${zuqi * 40 + price + zuqi * 50 + 60};
					$("#otherPriceTotal ").text(feiyong);
					$(".orderTotalPrice").text(heji);
				}
			} else if($(".oth-youhui").is(':checked')){
				var smoney=$(".oth-che").attr("name");
				var feiyong = ${zuqi * 50};
				var yysmoney=parseInt(smoney) - parseInt(feiyong);
				$(".orderTotalPrice").text(yysmoney);
			}else {
				var zongjia = ${zuqi * 40 + price + 60};
				$("#otherPriceTotal ").text("60");
				$(".orderTotalPrice").text("");
				$(".orderTotalPrice").text(zongjia);
			}
		});
		<!--优惠券-->
		$(".oth-youhui").click(
				function() {
					var chefei = $(".chefei").text();
					var baoxian = $(".baoxian").text();
					var heji = $("#otherPriceTotal").text();
					//var zongjia=parseInt(chefei)+parseInt(baoxian)+parseInt(heji);
					if ($(this).is(':checked')) {
						var zongjia = parseInt(chefei) + parseInt(baoxian) + parseInt(heji);
						var money = $(this).attr("value");
						$("#otherPriceTotals").text(money);
						var j = parseInt(zongjia) - parseInt(money);
						$(".oth-che").attr("name",j);
						$(".orderTotalPrice").text("");
						$(".orderTotalPrice").text(j);
					}
				});
		<!--选择联系人-->
		$(".zcr").click(
				function() {
					$(".lxr").toggle();
					$(':checkbox[name=lxr]').each(
							function() {
								$(this).click(
										function() {
											if ($(this).is(':checked')) {
												$(this).parent().parent().siblings().children().children().attr("checked",false);
												$(this).attr('checked', true);
												var name = $(this).parent().siblings(".name").text();
												var phone = $(this).parent().siblings(".phone").text();
												var sfz = $(this).parent().siblings(".sfz").text();
												var email = $(this).parent().siblings(".email").text();
												$("#name").val(name);
												$("#xphone").val(phone);
												$("#xidentitycard").val(sfz);
												$("#memberEmail").val(email);
											}

										});
							});

				});

	});
</script>
</head>
<body>

	<div class="newpg-head" style="position: relative;">
		<div class="top-center"
			style="border: 0px solid red; width: 1200px; height: 60px; margin: 0 auto; position: relative;">
			<div class="newpg-con" style="left: 110px;">
				<input type="hidden" class="userzt" value="${userzt}" /> <a
					class="order-logo " href="" target="_blank" alt="神州租车"></a>
				<div class="head-r">
					<div class="order-phonebox">
						<span class="phone-icon"></span>
						<div class="phone-shad">
							<img alt="" src="static/img/tel.png" />
						</div>
					</div>
					<ul class="head-info">
						<li><a id="memberName"> 欢迎您，<i>${glname}</i>
						</a></li>
						<li><a href="owncenter">我的订单</a></li>
						<li class="nobrd"><a href="logout">退出</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="da">
		<div class="daCenter"
			style="border: 0px solid red; width: 1000px; margin: 0 auto;">
			<div class="newpg-proce">
				<div class="newpg-con2">
					<img class="order-proce" alt="" src="static/img/order_proce.png" />
				</div>
				<div class="car-info">
					<div class="ifl">
						<img alt="" src="${tupian}" />
						<div class="lin">
							<p class="lp1">${name}</p>
							<p class="lp2">${carxinxi}</p>
							<div class="base-btn">
								<a class="look-config">查看配置信息</a> <a class="look-change"
									href="shangcheng">修改订单</a>
							</div>
						</div>
					</div>
					<ul class="ifr">
						<li class="bdr">
							<p class="le line1">
								<i class="ifr-qc"></i> <span>取车</span>
							</p>
							<p class="le line2">
								<i class="ifr-time"></i> <span> <i class="stime1">${senttime1}</i>
									<i class="stime2">${senttime2}</i>
								</span>
							</p>
							<p class="le line3">
								<i class="ifr-local"></i> <span> <i class="quche1">${quche1}</i>
									<i class="quche2">${quche2}</i>
								</span>
							</p>
						</li>
						<li>
							<p class="le line1">
								<i class="ifr-hc"></i> <span>还车</span>
							</p>
							<p class="le line2">
								<i class="ifr-time"></i> <span> <i class="btime1">${backtime1}</i>
									<i class="btime2">${backtime2}</i>
								</span>
							</p>
							<p class="le line3">
								<i class="ifr-local"></i> <span> <i class="huanche1">${huanche1}</i>
									<i class="huanche2">${huanche2}</i>
								</span>
							</p>
						</li>
					</ul>
				</div>
				<c:if test="${youhui.size()>0 }">
				<div class="newbg-wxts">
					<i class="tan-icon"></i>
					<div class="tipdiv">
						<span class="wxleft">温馨提示:</span>
						<ul class="wxright">
							<li>还车结算时，可使用您账户中的优惠券</li>
						</ul>
					</div>
				</div>
				</c:if>
			</div>

			<!--
        	作者：1057245260@163.com
        	时间：2017-04-09
        	描述：基本信息
        -->
			<div class="newpg-conf">

				<div class="memberInfo">
					<h3>
						租车人信息 <span class="ifred">请认真填写一下信息，取车时需要现场核对！</span> 
						<c:if test="${l!=0 }">
						<span class="zcr">常用租车人</span>
						</c:if>
					</h3>
					<ul class="h3h infoul">
						<li class="memberXname"><label> <i>*</i>
								姓&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;名
						</label> <input name="xname " id="name"
							style="border: 1px solid rgb(228, 230, 233); border-image: none; color: black;"
							type="text" maxlength="50" required="required"
							placeholder="请输入您的姓名" value="${userxm}" />
							<div class="order-errorbox order-erpos m1">
								<span class="arout"> ◆ <span class="arin">◆</span>
								</span> <span class="zc-retan"></span> <i class="zc-con" id="nameInfo"></i>
								<span class="zc-close"></span>
							</div></li>
						<li><label> <i>*</i> 电话号码
						</label> <input name="xphone" class="downarr_ipt sz_papers zjtype"
							id="xphone"
							style="border: 1px solid rgb(228, 230, 233); border-image: none; color: rgb(47, 47, 57);"
							type="text" placeholder="请输入电话号码" value="${userphones}" />
							<div class="order-errorbox order-erpos m3">
								<span class="arout"> ◆ <span class="arin">◆</span>
								</span> <span class="zc-retan"></span> <i class="zc-con" id="nameInfo"></i>
								<span class="zc-close"></span>
							</div></li>
						<li><label> <i>*</i> 证件号码
						</label> <input name="xidentitycard " id="xidentitycard"
							class="xidentitycard" maxlength="20"
							style="border: 1px solid rgb(228, 230, 233); border-image: none; color: black;"
							type="text" placeholder="请输入证件号码" value="${usersfz }"/ >
							<div class="order-errorbox order-erpos m2">
								<span class="arout"> ◆ <span class="arin">◆</span>
								</span> <span class="zc-retan"></span> <i class="err-con"
									id="xidentitycardInfo"></i> <span class="zc-close"></span>
							</div></li>
						<li><label> <i class="none">&nbsp;</i>
								邮&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;箱
						</label> <input name="xemail " id="memberEmail" maxlength="50"
							style="border: 1px solid rgb(228, 230, 233); border-image: none; color: black;"
							type="text" placeholder="请输入邮箱" value="${useremail}" />

							<div class="order-errorbox order-erpos300">
								<span class="arout"> ◆ <span class="arin">◆</span>
								</span> <span class="zc-retan"></span> <i class="err-con"
									id="emailInfo">您输入的邮箱地址有误，请认真核实！</i> <span class="zc-close"></span>
							</div></li>
					</ul>
					<ul class="siji btnuls">

						<li><i class="radio-w "></i> <input name="siji" id="siji"
							class="pay-siji need-recepit yes" type="checkbox" /> <span>需要司机</span>
						</li>
					</ul>
					<div class="lxr">
						<table>
							<thead>
								<tr>
									<td></td>
									<td>姓名</td>
									<td>电话</td>
									<td>身份证号</td>
									<td>邮箱</td>
								</tr>

							</thead>
							<tbody>
								<c:forEach items="${lxr}" var="lxr">
									<tr>
										<td><input type="checkbox" name="lxr" class="lxrxinxi"
											id="lxrxinxi"></td>
										<td class="name">${lxr.lxrname}</td>
										<td class="phone">${lxr.lxrphone}</td>
										<td class="sfz">${lxr.lxrsfz}</td>	
										<c:choose>								
											<c:when test='${lxr.lxremail=="null"}'>
												<td class="email"></td>
											</c:when>																			
											<c:otherwise>
												<td class="email">${lxr.lxremail}</td>
											</c:otherwise>
										</c:choose>
									</tr>
								</c:forEach>
							</tbody>
						</table>

					</div>
				</div>

				<div class="newpg-con3">
					<input name="xmobile" id="xmobile" type="hidden"
						value="电话号码（从数据库里取）" /> <input name="xemail" id="xemail"
						type="hidden" value="" />
					<!--
                	作者：1057245260@163.com
                	时间：2017-04-09
                	描述：用户信息补全和短信提醒
                -->
					<div class="info-msg">
						<p>
							下单成功后，提醒信息会发送至手机 <i id="mmid">${userphone}</i>
						</p>
					</div>
					<!--
                	作者：1057245260@163.com
                	时间：2017-04-09
                	描述：短信提醒完毕
                -->
					<div class="papers-tip">
						<img src="static/img/zc_papers_tip.gif" />
						<p>取车时,出示以下证件的原件：本人二代身份证 、本人国内有效驾驶证正副本 、本人一张信用及可用额度
							均不低于3000元的国内有效信用卡，所有证件有效期须至少超过当次租期的一个月以上。</p>
					</div>
					<!--
                	作者：1057245260@163.com
                	时间：2017-04-09
                	描述：修改手机号开始
                -->
					<div class="zhezhao" id="zhezhao"></div>
					<div class="modify-pass" id="login"></div>
					<!--
                	作者：1057245260@163.com
                	时间：2017-04-09
                	描述：修改手机号结束
                -->
				</div>

				<!--
            	作者：1057245260@163.com
            	时间：2017-04-09
            	描述：费用明细
            -->
				<div class="feiyong">
					<h3>费用明细</h3>
					<ul class="h3h feeul">
						<li><span class="fee-txt">车辆租赁及门店服务费</span> <span
							class="fee-res"> <i>¥</i> <i class="chefei">${price}</i>
						</span> <span class="fee-pro">${carprice}*${zuqi}</span>
							<div class="clear"></div></li>
						<li><span class="fee-txt">基本保险费 <i class="blue-ques">
									<div class="bx-bubble">
										<i class="litar-l"></i>
										<dl>
											<dd>必购产品：如车辆租赁期间出险，保险理赔范围内的损失，客户仅需承担1500元以内的车辆损失部分，不用承担其他损失。</dd>
											<dd>承保范围：车辆损失险（1500元以内的损失由客户方承担）；第三者责任险
												20万元；车上人员责任险（驾驶员）5万元；全车盗抢险；玻璃单独爆裂险；自燃损失险。</dd>
											<dd>本服务按天计费，超时4小时以上按1天计费，超时4小时以内不另计费。</dd>
										</dl>
									</div>
							</i>
						</span> <span class="fee-res"> <i>¥</i> <i class="baoxian">${zuqi*40}</i>
						</span> <span class="fee-pro">40*${zuqi}</span>
							<div class="clear"></div></li>
						<li class="opcl"><span class="fee-txt">其他费用 <i
								class="blue-downarr open qita"></i>
						</span> <span class="fee-res"> <i>¥</i> <i id="otherPriceTotal">60</i>
						</span> <span class="fee-pro">合计</span>
							<div class="clear"></div></li>
						<li class="bluelist shows">
							<ul class="otherul">
								<li class="serviceOption"><span class="fee-txt serviceName"
									servicename="不计免赔服务"> 不计免赔服务 <i class="blue-yes"></i> <input
										name="mpservices" class="oth-che" type="checkbox"
										names="${zuqi*50}" />
								</span> <span class="fee-res"> <i>¥</i> ${zuqi*50}
								</span> <span class="fee-pro servicePrice" serviceprice="50">50*${zuqi}</span>
									<i class="blue-ques">
										<div class="bx-bubble">
											<i class="lital-l"></i>
											<dl>
												<dd>
													如车辆租赁期间出险，客户无需承担保险理赔范围内的任何损失,以及轮胎损失。 <br />
													本服务按天计费，超时4小时以上按1天计费，超时4小时以内不另计费。
												</dd>
											</dl>
										</div>
								</i></li>
								<li class="discount defaultFeeIten2000"><span
									class="fee-txt">手续费</span> <span class="fee-res"> <i>￥</i>
										60
								</span>
									<div class="clear"></div></li>
							</ul>
						</li>
						<!-- enowjgnaer -->
						<c:if test="${youhui.size()>0 }">
							<li class="opcl"><span class="fee-txt">优惠券使用 <i
									class="blue-downarr open youhui"></i>
							</span> <span class="fee-res"> <i>¥</i> <i id="otherPriceTotals"></i>
							</span> <span class="fee-pro">减去</span>
								<div class="clear"></div></li>
							<c:forEach items="${youhui}" var="youhui">
								<li class="bluelist show">
									<ul class="otherul">
										<li class="serviceOption"><span
											class="fee-txt serviceName"> 优惠券 <input
												name="mpservice" class="oth-youhui" type="radio"
												style="position: relative; left: -60px; top: 3px;"
												value="${youhui.yhqtype}" sname="${youhui.useryhqid}" />
										</span> <span class="fee-res"> <i>¥</i> ${youhui.yhqtype}
										</span></li>
									</ul>
							</c:forEach>
						</c:if>
					</ul>
				</div>
				<!--
            	作者：1057245260@163.com
            	时间：2017-04-09
            	描述：发票类型
            -->
				<div class="fapaio">
					<h3>发票类型</h3>
					<ul class="h3h btnul">

						<li><input name="feetype" id="need-recepit"
							class="pay-radio  need-recepit yes" type="checkbox" /> <span>需要发票</span>
						</li>
					</ul>
					<ul class="billul orderReceipt">
						<li class="h30"><span class="spleft">常用收件人</span></li>
						<li><span class="spleft">发票抬头</span> <input
							name="receipt_title"
							class="blipt invoice_select t_val downarr_ipt"
							style="color: black;" id="receipt_title" type="text"
							placeholder="个人/单位" /></li>
						<li><span class="spleft">收件人</span> <input
							name="receipt_user" class="t_val blipt" id="receipt_user"
							style="color: rgb(47, 47, 57);" placeholder="请输入收件人"
							value="${userxm}" /></li>
						<li><span class="spleft">手机号码</span> <input
							name="receipt_user_mobile" class="t_val blipt"
							id="receipt_user_mobile" style="color: rgb(47, 47, 57);"
							type="text" placeholder="请输入手机号码" value="${userdh}" /></li>
						<li><span class="spleft">邮政编码</span> <input
							name="receipt_address_zip" class="t_val blipt"
							id="receipt_address_zip" style="color: black;" type="text"
							placeholder="邮政编码" /></li>
						<li><span class="spleft">收件地址</span> <select name="province"
							class="address_select t_val blipt-s downarr_ipt" id="province"style="color:black">
							<option value="省份（市）">省份（市）</option>
							 </select>
							 <select name="city"
							class="address_select t_val blipt-s downarr_ipt" id="city" style="color:black">
							<option value="市（区）">市（区）</option>
							</select>
							 <select  class="address_select t_val blipt-s downarr_ipt"
							id="county" style="color:black">
							<option value="县、镇">县、镇</option>
							</select>
							
							 <input name="receipt_address" class="t_val blipt-l" id="receipt_address"
							type="text" style="color: black;" placeholder="我们将邮寄发票到您所填写的地址" />
							<div class="clear"></div></li>
						<li class="billtip">
							<div class="emjc">费用结算后为您寄出，如您选择上门取车，租车费用发票至少3个工作日后寄出。</div>
							<div class="emsm">自2015年7月1日起，您的消费金额将开具两份发票，一份租车费发票（含车辆租赁费、基本保险费），一份用车服务费发票（其他费用）。</div>
						</li>
					</ul>
				</div>
				
				<div class="gopay orderFeeTotal">
				<c:if test='${counter > "2" }'>
					<div class="paydetail">
						<span class="ddzj">
						需在线支付全款
							<i class="blue-ques">
								<div class="pay-bubble">
									<i class="litar-t"></i>
									<dl>
									  <dd>
									  	尊敬的用户，由于您多次取消订单，所以现在必须支付全款才能租车！
									  </dd>
									</dl>
								</div>
							</i>
						</span> 				
					</div>
					</c:if>
					<div class="paydetail">
						<span class="ddzj">订单总价</span> 
						<span class="ddall"> 
							<i>￥</i>
							<i class="orderTotalPrice all">${zuqi*40 +price+60}</i>
						</span>
					</div>
					<c:choose>
					<c:when test='${counter > "2" }'>
					<a class="gopay-red" id="monitor_fourth_next">支付全款</a>
					</c:when>																			
					<c:otherwise>
					<a class="gopay-blue" id="monitor_fourth_next">提交订单</a>
					</c:otherwise>
					</c:choose>
				</div>
			</div>
			<div class="newpg-tggz">

				<div class="newpg-con2">
					<div class="top">退改规则</div>
					<dl>
						<dt>1.订单取消</dt>
						<dd>非神州租车原因导致订单取消，预付款中的基本保险、手续费及可选服务费等将退还客户，车辆租赁费及门店服务费不退。</dd>
						<dt>2.订单修改</dt>
						<dd>
							<br /> a)取车时间距当前时间≥2个工作小时，如需修改订单，请在门店营业时间前2个工作小时以上致电400 616
							6666。 <br /> 若神州租车车辆可以满足更改订单的需求，可以为您修改订单，否则不予修改。 <br />
							b)取车时间距当前时间＜2个工作小时，不接受修改。
						</dd>
						<dd>（小贴士：如果您修改订单或取消订单重新预订，价格可能会发生变化。）</dd>
						<dd>3.提前还车：允许提前还车，但已预付的租金不退。</dd>
					</dl>
				</div>

			</div>
			<div class="sz_footer">
				<p>
					<a title="关于我们" href="aboutus">关于我们</a> <a title="精彩活动"
						href="actcenter">精彩活动</a> <a title="帮助中心" href="helpcenter">帮助中心</a>
				</p>
				<p>Copyright©2008-2017 www.zuche.com All Rights Reserved.</p>
				<p>如果您对神州租车网站有任何意见,欢迎发送邮件到 web@zuche.com</p>
				<p>神州租车官网 京ICP备10005002号   |   京公网安备号 11010502026705</p>
			</div>
		</div>
		<div class="configbox" style="display: block;">
			<div class="cftitle">
				<span>${name}配置信息</span> <i class="cfclose"></i>
			</div>
			<div class="cflist">
				<ul>
					<c:forEach items="${xinxi}" var="xinxi">
						<li><b class="zws"></b> 座位数： <span>${xinxi.carSeatNum}</span>
						</li>
						<li><b class="cms"></b> 车门数： <span>${xinxi.carDoorNum}</span>
						</li>
						<li><b class="rlls"></b> 燃料类型： <span>${xinxi.carOil}</span></li>
						<li><b class="bsxlx"></b> 变速箱类型： <span>${xinxi.carBsx}</span>
						</li>
						<li><b class="pl"></b> 排 量： <span>${xinxi.carPail}</span></li>
						<li><b class="rybh"></b> 燃油标号： <span>92-93汽油</span></li>
						<li><b class="qdfs"></b> 驱动方式： <span>${xinxi.carDrive}</span>
						</li>
						<li><b class="fdjjqxs"></b> 发动机进气形式： <span>自然吸气</span></li>
						<li><b class="tc"></b> 天 窗： <span>${xinxi.carSkyWindow}</span>
						</li>
						<li><b class="yxrl"></b> 邮箱容量： <span>${xinxi.carOilBox}</span>
						</li>
						<li><b class="yx"></b> 音箱： <span>4</span></li>
						<li><b class="zy"></b> 座 椅： <span>皮革座椅</span></li>
						<li><b class="dcld"></b> 倒车雷达： <span>${xinxi.carGps}</span></li>
						<li><b class="qin"></b> 气 囊： <span>${xinxi.carGasbag}</span>
						</li>
						<li class="nonebd"><b class="dvd"></b> DVD / CD： <span>${xinxi.carDv}</span>
						</li>
						<li class="nonebd"><b class="gps"></b> GPS导航： <span>${xinxi.carGps}</span>
						</li>
					</c:forEach>
				</ul>
			</div>
		</div>
		<div class="yanzheng">
			<i class="cfclose"></i>
			<div class="yanzheng-xin">
				<p>请完整租车人信息！</p>
			</div>
		</div>
		<div class="dongjie">
			<i class="cfclose"></i>
			<div class="dongjie-xin">
				<p>此用户已经被冻结，不能租车！</p>
			</div>
		</div>

	</div>
	<div class="fu"></div>
</body>
</html>

