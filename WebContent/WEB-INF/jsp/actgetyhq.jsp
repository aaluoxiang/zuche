<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="<%request.getContextPath();%>static/css/getyhq.css"/>
		<link rel="stylesheet" type="text/css" href="<%request.getContextPath();%>static/css/logincar.css"/>
		<link rel="stylesheet" type="text/css" href="static/css/error.css"/>
				<script src="static/js/jquery-1.12.4.js" type="text/javascript" charset="utf-8"></script>
		<script type="text/javascript" src="static/js/error-tip.js" ></script>
		<script type="text/javascript" src="static/js/login.js" ></script>

		<title></title>
		<script type="text/javascript">
			$(function(){
				$.ajax({
					type:"post",
					url:"getyhq",
					cache:false,
					success:function(data){
						console.log(data);
						console.log(data==1);
						console.log(data==2);
						if(1==data){
							console.log("该用户存在该优惠卷");
							$('.getyhq').css('display','none');
							$('.hasyhq').css('display','block');
							
						}
						if(2==data){
							console.log("该用户没有该优惠卷,已领取成功");
							$('.getyhq').css('display','block');
							$('.hasyhq').css('display','none');
						}
					}
				});
				
				$(".getyhq").click(function(){
					if(${userId==null}){
						$(".denglu").css("display","block");
						$(".fu").css("display","block");
						return false;
					} 
					$.ajax({
						type:"post",
						url:"getyhq",
						cache:false,
						success:function(data){
							console.log(data);
							console.log(data==1);
							console.log(data==2);
							if(1==data){
								console.log("该用户存在该优惠卷");
								$('.getyhq').css('display','none');
								$('.hasyhq').css('display','block');
								
							}
							if(2==data){
								console.log("该用户没有该优惠卷,已领取成功");
								$('.getyhq').css('display','block');
								$('.hasyhq').css('display','none');
							}
						}
					});
					
				});
				$('.close-cf').on('click',function(){
					$(".denglu").css("display","none");
					$(".fu").css("display","none");
				});
				${error_denglu};
				${error_fu};
				${error_m_emp};
				${error_m};
				${error_w_emp};
				${error_code_emp};
				${error_code};
				${error_w};
			
			});
		</script>
		<style type="text/css">
			.fu{
			    width: 100%;
   				height: 100%;
			}
			.denglu{
			    margin:-40% 42%;
			}
			.close-cf{
				cursor: pointer;
				position: relative;
				display: block;
				float: right;
				right:10px;
				top:5px;
			}
		</style>
	</head>
	<body>
	<jsp:include page="header-inc.jsp"></jsp:include>
		<div class="wrap" style="position:relative;">
		
			<div class="ban b1"></div>
			<div class="ban b2"></div>
			<div class="ban b3"></div>
			<div class="ban b4">
				<div class="rule_wrap">
					<div class="white_box fl">
						<h2>
							<img src="static/img/time.png" width="38px" height="38px" />
							活动时间
						</h2>
						<p>
							即日起至<br />
							2017年5月1日<br />
							<span style="color: #428bdd;font-size: 22px;">(领取30元优惠卷)</span>
						</p>
					</div>
					<div class="white_box fr">
						<h2>
							<img src="static/img/shuoming.png" width="38px" height="38px" />
							活动时间
						</h2>
						<p class="pd">
							<b>使用规则</b>
							1.实际租期2天及以上的订单可用，优惠券平日、周末、法定节假日均可使用;<br>
							2.不与顺风车、预付特价、其他优惠活动、优惠类电子券同享;<br>
							3.此优惠仅限每人享受1次,每单限用1张，限本人使用;<br>
							4.代金券不可转让、不开发票、不找零、不兑换现金；<br>
							<b>使用范围</b> 全国各城市直营门店、不限制车型;
						</p>
					</div>
					<a class="yhq getyhq" >领取优惠卷</a>
					<a class="yhq hasyhq" style="display: none;">已领取</a>
				</div>
			</div>
			<div class="denglu">
		<form action="loginact" method="post" id="login-form" class="login-form">
			<div class="login-main">
				<span class="close-cf"><img src="static/img/close-g.png"/></span>
				<span class="login-span">登录</span>
				<div class="error-tip">
					</div>
				<span class="email-img spanimg"></span>
				<input type="text" class="login-input email" name="email" id="email" value="${param.email}" placeholder="请输入手机号" autofocus="autofocus" />
				<span class="password-img spanimg"></span>
				<input type="password" class="login-input password" name="password" id="password"placeholder="请输入密 码"  />
				<span class="logincode-img spanimg"></span>
				<input type="tel" name="logincode" id="logincode" value="" placeholder="请输入验证码" />
				<div class="login-img">
					<img onclick="this.src='captcha?r'+Date.now()" src="captcha" alt="验证码">
				</div>
			<div class="other-btn">
					<a href="sign" class="login-a" style="position:relative;top:10px;left:-220px">立即注册</a>
					<a href="forget" class="login-b" style="position:relative;top:-25px;left:143px;">忘记密码？</a>
				</div>
				<div class="login-footer">
					<input type="submit" value="登录" class="login-btn" id="login-btn" style="position:relative;top:-20px"/>
				</div>
		</form>
	</div>
	</div>
		
	</body>
	<div class="fu"></div>
</html>
