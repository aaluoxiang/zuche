<%@ page language="java" contentType="text/html; charset=utf-8"  isErrorPage="true" pageEncoding="utf-8"%>
<%response.setStatus(HttpServletResponse.SC_OK);%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>该页面异常。。</title>
		<style type="text/css">
			.errorfour{
				border: 0px solid red;
				min-width: 1000px;
				height: 462px;
				background:#f1f1f1;
			}
			.errorfour-main{
				width: 1000px;
				height: 460px;
				margin: 0 auto;
				background:#f1f1f1 url(static/img/sz_errorbg.png) left center no-repeat;
				border: 0px solid #dbe4eb;
			    position: relative;
			}
			.errorfour-main h2{
				position: absolute;
			    color: #be1522;
			    top: 130px;
			    left: 445px;
			    font-size: 56px;
			    font-weight: normal;
			    font-family: "微软雅黑", arial, Simsun, sans-serif;
			}
			.errorfour-main h3{
				position: absolute;
			    color: #6f6f6f;
			    top: 220px;
			    left: 445px;
			    font-size: 36px;
			    font-weight: normal;
			    font-family: "微软雅黑", arial, Simsun, sans-serif;
			}
		</style>
	</head>
	<body>
	<jsp:include page="header-inc.jsp"></jsp:include>
		<div class="errorfour">
			<div class="errorfour-main">
				<h2>抱歉</h2>
				<h3>您访问的连接过期或无效！</h3>
			</div>
		</div>
	</body>
</html>
