<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=request.getContextPath()%>/">
<meta charset="UTF-8">
<title>个人中心(除页眉页脚)</title>

<script src="static/js/jquery-1.12.4.js" type="text/javascript" charset="utf-8"></script>
<script src="static/js/owncentererrorclose.js" type="text/javascript" charset="utf-8"></script>

<link rel="stylesheet" type="text/css" href="static/css/center.css">
<link rel="stylesheet" type="text/css" href="static/css/myinfor.css">
<link rel="stylesheet" type="text/css" href="static/css/owncentererror.css">
<!-- <link rel="stylesheet" type="text/css" href="static/css/head.css">
<link rel="stylesheet" type="text/css" href="static/css/foot.css"> -->
</head>

<body bgcolor="#f2f3f5">
	<jsp:include page="header-inc.jsp" flush="true"></jsp:include>
	<jsp:include page="tou.jsp" flush="true"/>
	<div class="a_acon b">
		<jsp:include page="menu.jsp" flush="true"/>
		<div class="container">
			<div data-nr="1" class="nr mymessage fl selected">
				<h2>我的信息</h2>
				<div class="mymessageform">
					<form method="post">
						<ul>
							<li>
								<div class="zjerrordiv" >
									<c:if test="${error=='姓名不能为空' }">
										<div class="errorbox" style="top: -40px;">
											<span class="xxbox">◆</span>
											<span class="errorimg"></span>
											<span class="errorinfor">姓名不能为空</span>
											<span class="errorclose"></span>
										</div>
									</c:if>
									<c:if test="${error=='错误身份证' }">
										<div class="errorbox" style="top: 60px;">
											<span class="xxbox">◆</span>
											<span class="errorimg"></span>
											<span class="errorinfor">错误身份证</span>
											<span class="errorclose"></span>
										</div>
									</c:if>
									<c:if test="${error=='请选择证件类型' }">
										<div class="errorbox" style="top: 30px;">
											<span class="xxbox">◆</span>
											<span class="errorimg"></span>
											<span class="errorinfor">请选择证件类型</span>
											<span class="errorclose"></span>
										</div>
									</c:if>
								</div>
								<label class="fl">姓名</label> 
									<span class="namespan fl">
										<input type="text" name="yhname" value="${user.userName }" />
									</span>
									<div class="fr">真实姓名，方便租车时核对身份</div>
							</li>
							<c:if test="${user.userZjlx!=null && user.userZjlx!='' }">
								<li class="sfzli">
									<label class="fl">证件</label> 
										<span class="namespan fl sfzspan"> 
											<input type="text" name="zjlxxz" value="${user.userZjlx }" readOnly="true" style="border-top: 1px solid #e9ebee;" /> 
											<input type="text" name="yhsfz" class="sfz" value="${user.userSfz }" readOnly="true"/>
										</span>
										<div class="fr">如需修改请联系客服</div>
								</li>
								<%-- <li>
									<label class="fl">证件</label>
									<label class="fl">${user.userZjlx }:${sfz }</label> 
									<span class="namespan fl">
										<input type="text" class="zdsfz" value="${user.userZjlx }:${user.userSfz }" readOnly="true">
									</span>
									<div class="fr">如需修改请联系管理员</div>
								</li> --%>
							</c:if>
							<c:if test="${user.userZjlx==null ||  user.userZjlx=='' }">
								<li class="sfzli">
									<label class="fl">证件</label> 
										<span class="namespan fl sfzspan"> 
											<select class="zjxz" name="zjlxxz">
												<option value="请选择有效证件">请选择有效证件</option>
												<option value="身份证">身份证</option>
											</select> 
											<input type="text" name="yhsfz" class="sfz" value="" />
										</span>
										<div class="fr">有效证件，方便租车时核对身份</div>
								</li>
							</c:if>
							<li>
								<label class="fl">电话</label> 
								<span class="namespan fl">
									<input type="text" name="yhtel" class="tel" value="${user.userTel }" />
								</span>
								<div class="fr">有效电话，方便派车时及时联系</div>
							</li>
							<li>
								<label class="fl">e-mail</label> 
									<span class="namespan fl"> 
										<input type="text" name="e_mail" class="e_mail" value="${user.userEmail }" />
									</span>
									<div class="fr">有效邮件</div>
							</li>
						</ul>
						<div class = "savebtn">
							<button>保存</button>
						</div>
					</form>
					
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="footer-inc.jsp" flush="true"></jsp:include>
	<c:if test="${updatemessage=='true' }">
		<script src="static/js/owncenterupdatemessage.js" type="text/javascript" charset="utf-8"></script>
	</c:if>
</body>
</html>