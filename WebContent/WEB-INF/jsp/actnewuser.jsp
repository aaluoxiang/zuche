<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="<%request.getContextPath();%>static/css/actnewuser.css"/>
		<title></title>
	</head>
	<body>
	<jsp:include page="header-inc.jsp"></jsp:include>
		<div class="action-main">
			<div class="ban1"></div>
			<div class="ban2"></div>
			<div class="ban3"></div>
			<div class="ban4"></div>
			<div class="ban5">
				<div class="ban-zc">
					<a href="sign" class="btn-zc"></a>
				</div>
			</div>
			<div class="ban6"></div>
			<div class="ban7"></div>
			<div class="ban8"></div>
			<div class="action-text">
				<span class="show-car"></span>
				<div class="inbox">
					<div class="mid">
						<p class="hds">活动时间：</p>
						<p class="hdb">即日起至2017年6月30日</p>
						<p class="lines"></p>
						<dl>
							<dt>活动规则：</dt>
							<dd>1. 新用户注册后，即送50元电子代金券，有效期：注册日起6个月；</dd>
							<dd>2. 新用户首次租车结算后，再送100元电子代金券，有效期：首单结算完成后3个月。</dd>
						</dl>
						<p class="lineb"></p>
						<dl>
							<dt>注意事项：</dt>
							<dd>1. 新用户专享券适用于全国所有直营门店、全部车型，节假日通用；</dd>
							<dd>2. 新用户首次下单需先完善个人信息，每个新用户仅可享受一次，限新用户本人使用；</dd>
							<dd>3. 本活动仅适用于短租自驾产品，国际租车不能参与本活动；</dd>
							<dd>4. 新用户专享券可用于日租及工作日、周租、月租套餐，不适用于顺风车、预付特价活动,<br/>
								且不与其他优惠同时享受；
							</dd>
							<dd>5. 新用户100元专享券，限首单且实际租期大于等于2天时使用。</dd>
						</dl>
						<p class="lineall">
							<span><a href="sign">马上注册 领取见面礼</a></span>
						</p>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
