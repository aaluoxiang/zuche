<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html >
<html>
<head>
<base href="<%=request.getContextPath()%>/">
<link rel="stylesheet" type="text/css" href="static/css/jbxxnr.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<style>
.anniu {
	float: left;
    zoom: 1;
    padding: 0px 37px;
    font-weight: 600;
    line-height: 35px;
    height: 35px;
    margin: 23px 0 0px 159px;
    border: none;
    background: #f60;
    color: #fff;
    font-size: 14px;
    font-weight: 600;
    border-radius: 4px;
    cursor: pointer;
    box-shadow: 2px 1px 4px 1px #666;
}
.nr {
	margin-left: 355px;
    margin-top: 30px;
}
</style>
<body>
	<div>
		<form method="post" action="jbxxnr">
			<div>
				<div class="jbxxnr">
					<span>基本信息内容</span>
				</div>
				<div class="jbxx">
					<div class="nr">
						<table>
							<tr>
								<td>标题:</td>
								<td><input class="var_1" type="text" value="${Webbt}"
									name="webbt" /></td>
							</tr>
							<tr>
								<td>关键字:</td>
								<td><textarea class="area" type="text" name="webpoint">${Webpoint }</textarea></td>
							</tr>
							<tr>
								<td>描述:</td>
								<td><textarea class="area" type="text" name="webdes" />${Webdes }</textarea></td>
							</tr>
							<tr>
								<td>公司名称:</td>
								<td><input class="var_1" type="text"
									value="${Webcompanyname}" name="webcompanyname" /></td>
							</tr>
							<tr>
								<td>版权:</td>
								<td><input class="var_1" type="text" value="${Webbq}"
									name="webbq" /></td>
							</tr>
							<tr>
								<td>简介:</td>
								<td><input class="var_1" type="text" value="${Webintro}"
									name="webintro" /></td>
							</tr>
							<tr>
								<td>电话:</td>
								<td><input class="var_1" type="text" value="${Webtel}"
									name="webtel" /></td>
							</tr>
						</table>
						<input class="anniu" type="submit" value="提交" />
					</div>
				</div>												
				<c:if test="${alert}">
					<script>alert("提交成功");</script>
				</c:if>
			</div>
		</form>

	</div>

</body>
</html>