<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<%@taglib uri="http://www.trkj.com/tags" prefix="myTag" %>
	<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html >
<html>
<head>
<base href="<%=request.getContextPath()%>/">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="styLesheet" type="text/css" href="static/css/xuanxiangka.css">
<script src="static/js/jquery-1.12.4.js"></script>
</head>
<script>
	$(function() {
		$(".tab").on(
				'click',
				'.item',
				function() {
					var target = $(this);
					if (!target.hasClass('selected')) {
						var tab = this.dataset.tab;
						var content = $(".container .content[data-content="
								+ tab + "]");
						var selectedContent = content.siblings('.selected');
						target.addClass('selected').siblings('.selected')
								.toggleClass('selected');
						selectedContent.toggleClass('selected');
						content.toggleClass('selected');
					}
				});
	})
</script>
<style>
th, td {
	font-family: "Microsoft YaHei";
	border: 1px solid #E2E2E2;
	text-align: center;
	padding: 7px 18px;
    font-size: 12px;
}

table {
	border-collapse: collapse;
}
 span{
 margin-left:50px;
 }

.systemhd {
	position: relative;
	width: 100%;
	background: url(static/img/title_b.png) 10px 9px no-repeat;
	line-height: 40px;
	height: 40px;
	box-shadow: 0 1px 3px #c3c3c3;
	font-family: "Microsoft YaHei";
}

.systemhdp .zhuti {
	padding-left: 50px;
}
.tabs{
width:95%;
}
.container{
    top: 4%;
    position: absolute;
}
</style>
<body>
	<div class="systemhd">
		<span class="zhuti">首页</span>
		<div class="tabs">
			<div class="tab">
				<div data-tab="1" class="item selected">已完成(${ywcgs})</div>
				<div data-tab="2" class="item">已付款(${yfkgs})</div>
				<div data-tab="3" class="item">待付款(${dfkgs})</div>
				<div data-tab="4" class="item">租赁中(${zlzgs})</div>
				<div data-tab="5" class="item">已退单(${ytdgs})</div>
			</div>
			<div class="container">
				<div data-content="1" class="content selected">
					<table>
						<tr>
							<th width="200px">编号</th>
							<th width="152px">状态</th>
							<th width="150px">金额</th>
							<th width="200px">电话</th>
							<th width="160px">名称</th>
							<th width="300px">订单时间</th>
						</tr>
						<c:forEach items="${ywc}" var="ywc">
						<tr>						
							<td>${ywc.orderid } </td>
							<td style="color: #49bf67">${ywc.userorderzt} </td>
							<td>${ywc.odermoney }</td>
							<td>${ywc.usertel }</td>
							<td>${ywc.userid }</td>
							<td><fmt:formatDate value="${ywc.ordersingletime}" type="both"/> </td>
						</tr>
						</c:forEach>
					</table>
					<div>
					<Mytag:page page="${page}" url="dindan" suoyoujilu="${suoyoujilu}" totlepage="${suoyoushu}"/>
					</div>
				</div>				
				<div data-content="2" class="content">
					<table>
						<tr>
							<th width="200px">编号</th>
							<th width="152px">状态</th>
							<th width="150px">金额</th>
							<th width="200px">电话</th>
							<th width="160px">名称</th>
							<th width="300px">订单时间</th>
						</tr>
						<c:forEach items="${yfk}" var="yfk">
						<tr>						
							<td>${yfk.orderid } </td>
							<td style="color: #49bf67">${yfk.userorderzt} </td>
							<td>${yfk.odermoney }</td>
							<td>${yfk.usertel }</td>
							<td>${yfk.userid }</td>
							<td><fmt:formatDate value="${yfk.ordersingletime }" type="both"/></td>
						</tr>
						</c:forEach>
					</table>
				</div>
				
				<div data-content="3" class="content">
					<table>
						<tr>
							<th width="200px">编号</th>
							<th width="152px">状态</th>
							<th width="150px">金额</th>
							<th width="200px">电话</th>
							<th width="160px">名称</th>
							<th width="300px">订单时间</th>
						</tr>
						<c:forEach items="${dfk}" var="dfk">
						<tr>						
							<td>${dfk.orderid } </td>
							<td style="color: #49bf67">${dfk.userorderzt} </td>
							<td>${dfk.odermoney }</td>
							<td>${dfk.usertel }</td>
							<td>${dfk.userid }</td>
							<td><fmt:formatDate value="${dfk.ordersingletime }" type="both"/></td>
						</tr>
						</c:forEach>
					</table>
				</div>
				
				<div data-content="4" class="content">
					<table>
						<tr>
							<th width="200px">编号</th>
							<th width="152px">状态</th>
							<th width="150px">金额</th>
							<th width="200px">电话</th>
							<th width="160px">名称</th>
							<th width="300px">订单时间</th>
						</tr>
						<c:forEach items="${zlz}" var="zlz">
						<tr>						
							<td>${zlz.orderid } </td>
							<td style="color: #49bf67">${zlz.userorderzt} </td>
							<td>${zlz.odermoney }</td>
							<td>${zlz.usertel }</td>
							<td>${zlz.userid }</td>
							<td><fmt:formatDate value="${zlz.ordersingletime }" type="both"/></td>
						</tr>
						</c:forEach>
					</table>
				</div>
				
				<div data-content="5" class="content">
					<table>
						<tr>
							<th width="200px">编号</th>
							<th width="152px">状态</th>
							<th width="150px">金额</th>
							<th width="200px">电话</th>
							<th width="160px">名称</th>
							<th width="300px">订单时间</th>
						</tr>
						<c:forEach items="${ytd}" var="ytd">
						<tr>						
							<td>${ytd.orderid } </td>
							<td style="color: #49bf67">${ytd.userorderzt} </td>
							<td>${ytd.odermoney }</td>
							<td>${ytd.usertel }</td>
							<td>${ytd.userid }</td>
							<td><fmt:formatDate value="${ytd.ordersingletime }" type="both"/></td>
						</tr>
						</c:forEach>
					</table>
				</div>
			</div>

		</div>
</body>
</html>