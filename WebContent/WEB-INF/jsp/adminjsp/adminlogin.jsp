<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=request.getContextPath()%>/">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="static/css/error.css" type='text/css'/> 
<link rel="stylesheet" href="static/css/adminlogin.css" type='text/css'/> 
<script type="text/javascript" src="static/js/jquery-1.12.4.js" ></script>
<script src="static/js/error-tip.js" type="text/javascript"></script>
<title>租车管理系统</title>
<style>
body{
background-image: url(static/img/houtaibj.jpg);
}
</style>
</head>
<script type="text/javascript">
$(function(){
	$.yzfangfa("请输入账号",2,"error-tip",'t_m_emp');
	$.yzfangfa("账号或密码错误,请重新输入",2,"error-tip",'t_m');

	$.yzfangfa("请输入密码",52,"error-tip",'t_w_emp');
	$.yzfangfa("请输入验证码",102,"error-tip",'t_code_emp');
	$.yzfangfa("验证码错误,请重新输入",102,"error-tip",'t_code');
	
	$('.errorbox').css('display','none');
	
	$('#login-tel').on('blur',function(){
		var logintel=$('#login-tel').val();
		if(logintel==""){
			$('.t_m_emp').css('display','block');
			return false;
		}
	});	
	$('#login-passwd').on('blur',function(){
		var loginpasswd=$('#login-passwd').val();
		if(loginpasswd==""){
			$('.t_w_emp').css('display','block');
			return false;
		}
	});
	
	$('#login-code').on('blur',function(){
		var logincode=$('#login-code').val();
		if(logincode==""){
			$('.t_code_emp').css('display','block');
			return false;
		}
	});
});
</script>
<script type="text/javascript">
		$(function(){
			${error_m};
	    	${error_m_emp};
	    	${error_w_emp};
	    	${error_code};
	    	${error_code_emp};
		});
</script>
<body>
<form  method="post" id="login-form" style="margin:14% auto">
			<div class="login-main">
				<span class="login-span">管理员登录</span>
				<div class="error-tip">
				</div>
				<span class="login-tel-img spanimg"></span>
				<input type="text" class="login-input" name="logintel" id="login-tel" placeholder="请输入账 号"  value="${param.logintel}"/>
				<span class="login-passwd-img spanimg"></span>
				<input type="password" class="login-input" name="login-passwd" id="login-passwd" value="" placeholder="请输入密 码" />
				<span class="login-code-img spanimg"></span>
				<input type="tel" name="login-code" id="login-code" placeholder="请输入验证码" />
				<div class="login-img">
					<img onclick="this.src='captcha?r'+Date.now()" src="captcha" alt="验证码">
				</div>
			</div>
			<div class="login-footer">
				<!--<a href="" class="login-a">立即注册</a>-->
				<input type="submit" value="登录" class="login-btn" id="login-btn" />
			</div>
		</form>
</body>
</html>