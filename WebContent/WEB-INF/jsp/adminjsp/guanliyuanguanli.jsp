<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=request.getContextPath()%>/">
<link rel="stylesheet" type="text/css"
	href="static/css/guanliyuanguanli.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script src="static/js/jquery-1.12.4.js" type="text/javascript"></script>
<style>
.bj {
	background: black;
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    opacity: 0.8;
    z-index: 2;
    display: none;
}
</style>
<script>
	$(function() {
		$('.anniu3').on('click',function(){
			$(".bj").css("display","block");
			$(".tianjiaxiugai").css("display","block");
			$(".xingming").val($(this).attr("name"));
			$(".yonhu").val($(this).attr("yonhuming"));
			$(".sex").val($(this).attr("sex"));
			$(".guanli").val($(this).attr("leixing"));
			$(".qq").val($(this).attr("qq"));
			$(".yonhuid").val($(this).attr("id"));
			$(".password").val($(this).attr("pwd"));
			$(".tjxg").text("修改");
			$(".yonhu").attr("readOnly",true);
		});
		$(".cha").on("click",function(){
			$(".bj").css("display","none");
			$(".tianjiaxiugai").css("display","none");
		});
		$(".button1").on("click",function(){
			$(".bj").css("display","block");
			$(".tianjiaxiugai").css("display","block");
			$(".xingming").val("");
			$(".yonhu").val("");
			$(".sex").val("");
			$(".guanli").val("");
			$(".password").val("");
			$(".qq").val("");
			$(".yonhuid").val("aa");
			$(".yonhu").attr("readOnly",false);
			$(".tjxg").text("添加");
		});
	})
</script>
</head>
<style>
.yonhu, .xingming, .qq,.password {
	padding: 6px;
	margin: 0px;
	border: 1px solid #c1d3d5;
	border-radius: 2px;
	box-shadow: 1px 1px 3px 1px #e2e2e2;
	width: 300px;
}
th{
	background: #f1f1f1;
	line-height: 2em;
	font-size: 12px;
	border: 1px solid #e2e2e2;
	padding: 14px;
}
td{
	text-align:center;
	border: 1px solid #e2e2e2;
	font-size:12px;
	padding: 6px;
}
.hdpnr tr:NTH-CHILD(odd) {
	background: #f5f4f4;
}

.hdpnr tr:HOVER {
	background: #dcdcdc;
}
.xinxi {
	margin: 21px 112px;
	width: 384px;
}
.hdpnr {
	position: absolute;
	margin-left: 10px;
	margin-top: 70px;
	box-shadow: 0 1px 3px #c3c3c3;
	margin-right: 8px;
}
.xinxitiaojiao {
	border: none;
	border-radius: 4px;
	padding: 10px 50px;
    margin: 14px 0 0px 127px;
	background: #f60;
	font-size: 14px;
	color: white;
}
</style>
<body>
	<form method="post" action="guanliyuanguanli">
		<div class="bj"></div>
		<div class="systemhdp">
			<div class="tianjiaxiugai">
				<div class="logo">
					<span class="tjxg"></span> <img class="cha" src="static/img/close.png" />
				</div>
				<div class="xinxi">
					<table>
						<tr>
							<td width="50px">用户名:</td>
							<td><input style="background: white" name="yonhu" type="text" class="yonhu" /></td>
						<tr>
							<td width="50px">姓名:</td>
							<td><input name="xingming" type="text" class="xingming" />
							</td>
						<tr>
						<tr>
							<td width="50px">密码:</td>
							<td><input name="password" type="text" class="password" />
							</td>
						<tr>
						<tr>
							<td width="50px">性别</td>
							<td style="text-align: left">
							<select class="sex" name="sex">
									<option value="">--请选择--</option>
									<option value="男">男</option>
									<option value="女">女</option>
							</select></td>
						<tr>
						<tr>
							<td width="50px">角色</td>
							<td style="text-align: left"><select class="guanli" name="guanli">
									<option value="">--请选择--</option>
									
									<option value="管理员">管理员</option>
									<option value="外部销售">外部销售</option>
									<option value="超级管理员">超级管理员</option>
							</select></td>
						<tr>
						<tr>
							<td width="50px">qq:</td>
							<td><input name="qq" type="text" class="qq" /></td>
						<tr>
					</table>
					<input type="submit" class="xinxitiaojiao" value="提交" />
				<input  style="display:none;width:0px" width=0px type="text" class="yonhuid" name="yonhuid"/>
				</div>
			</div>
			<span>管理员管理</span>
			<div class="hdpanniu">
				<input class="button1" type="button" value="+添加" /> <input class="tupianid" type="text" name="tupianid" />
			</div>
			<div class="hdpnr">
				<table border="1">
					<tr>
						<th style="padding: 8px" width="150px">名称</th>
						<th width="250px">用户名</th>
						<th width="150px">性别</th>
						<th width="200px">角色</th>
						<th width="200px">qq</th>
						<th width="300px">操作</th>
					</tr>
					<c:forEach var="employee" items="${glylist}">
						<tr>
							<td>${employee.employeename }</td>
							<td>${employee.employeeyonhuming }</td>
							<td>${employee.employeesex}</td>
							<td>${employee.employeetypeid}</td>
							<td>${employee.employeeqq}</td>
							<td><a style="text-decoration: none" href="javascript:void(0)" class="anniu3" pwd="${employee.employeepassword }" id="${employee.employeeid }" name="${employee.employeename }" yonhuming="${employee.employeeyonhuming }" sex="${employee.employeesex}" leixing="${employee.employeetypeid}" qq="${employee.employeeqq}"> 修改</a> <a class="anniu5"
								style="text-decoration: none" href="guanliyuanguanli?id=${employee.employeeid}" > 删除</a>
						</tr>
					</c:forEach>
				</table>
			</div>
		</div>
	</form>
</body>
</html>