<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html >
<html>
<head>
<base href="<%=request.getContextPath()%>/">
<link rel="stylesheet" type="text/css"
	href="static/css/tianjiayuangon.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<script src="static/js/jquery-1.12.4.js"></script>
<script>
	$(function() {
		$(".tupiananniu").on('click', function() {
			$(".bj").css("display", "block");
			$(".mk").css("display", "block");
		});
	})
</script>
<body>
	<div class="bj"></div>
	<div class="mk">
		<form>
			<div class="logo">
				<span>上传logo</span>
			</div>
			<div class="tupiankuang"></div>
			<input class="file1" type="file" /> <input type="submit"
				class="anniu4" value="提交" />
		</form>
	</div>
	<div>
		<form method="post">
			<div>
				<div class="jbxxnr">
					<span>添加员工</span>
				</div>
				<div class="jbxxwz">
					<div class="nr">
						<table>
							<tr>
								<td>员工姓名:</td>
								<td><input class="var_1" type="text" name="employeename" id="bt"
									placeholder="请输入姓名" /></td>
							</tr>
							<tr>
								<td>电话号码:</td>
								<td><input class="var_1" type="text" value="" name="employeetel" /></td>
							</tr>
							<tr>
								<td>性别:</td>
								<td><select name="employeesex">
										<option>--请选择性别--</option>
										<option value="男">男</option>
										<option value="女">女</option>
								</select></td>
							</tr>
							<tr>
								<td>年龄:</td>
								<td><input class="var_1" type="text" value="" name="employeeage" /></td>
							</tr>
							<tr>
								<td>身份证编号:</td>
								<td><input class="var_1" type="text" value="" name="employeesfz" />
								</td>
							</tr>
							<tr>
								<td>驾照编号:</td>
								<td><input class="var_1" type="text" value="" name="employeejzbh" />
								</td>
							</tr>
							<tr>
								<td>员工状态:</td>
								<td><select name="employeeztai">
										<option>--请选择--</option>
										<option value="签订合约">签订合约</option>
										<option value="合适的">合适的</option>
										<option value="不合适的">不合适的</option>
								</select>
								
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="anniuwz">
				<input class="anniu1" type="submit" value="提交" />
			</div>
		</form>
	</div>
</body>
</html>