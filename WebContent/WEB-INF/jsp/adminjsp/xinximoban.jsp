<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html >
<html>
<head>
<base href="<%=request.getContextPath()%>/">
<link rel="stylesheet" type="text/css" href="static/css/xinximoban.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<style>
th {
	background: #f1f1f1;
	line-height: 2em;
	padding:8px;
	font-size:12px;
	border: 1px solid #e2e2e2;
}
td {
	text-align: center;
	padding: 8px;
    line-height: 3em;
    border: 1px solid #e2e2e2;
    font-size: 12px;
}
.hdpnr {
	position: absolute;
	margin-left: 84px;
	margin-top: 20px;
	box-shadow: 0 1px 3px #c3c3c3;
	
}


</style>
<body>
<form method="post">
	<div class="systemhdp">
		<span>信息模板</span>
		<div class="hdpnr">
			<table>
				<tr>
					
					<th width="250px">模块</th>
					<th width="250px">标题</th>
					<th width="250px">描述</th>
					<th width="250px">操作</th>
					
				</tr>
				<c:forEach var="web" items="${weburl}">
					
					<tr>
						<td>邮件/手机验证(用于注册和申请)</td>
						<td>验证信息</td>
						<td>你的验证码:#</td>
						<td><input type="button" class="anniu3" value="修改"></td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</div>
	</form>
</body>
</html>