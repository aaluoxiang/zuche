<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE >
<html>
<head>
<base href="<%=request.getContextPath()%>/">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script src="static/js/jquery-1.12.4.js" type="text/javascript"></script>
<script>
	$(function() {
		$(".fplj").on('click',function(){
			var type=$('.type').val();	
			var s=[];
			$("input:checked").each(function(){
				var aa=$(this).attr("mkid");
				if(aa!=""&&aa!=null){
					s.push(aa);
				}				
			})
			
			if(type!="--请选择--"&&s.length>0){						
				$(this).attr("href","guanliyuantype?type="+type+"&&id="+s+"");
			}else{
				alert("所选信息不能为空！！！");
			}
	
		});
		
		$(".button8").on('click', function() {
			$(".nr ul").css("display", "block");
		});
		$(".tianjiaanniu").on('click', function() {
			$(".nr ul").css("display", "none");
		});
		$("#zoncheck").on('click', function() {
			if (this.checked) {
				$("input[type=checkbox]").attr("checked", true);
			}
			if (this.checked == false) {
				$("input[type=checkbox]").attr("checked", false);
			}
		});
		$(".nr").on('click','#zhuspan',function(){
			var dd=$(this).next();
			var dds=dd.siblings('ul:visible');
			if(dd.hasClass('a')){
				dds=dd;	
			}else{
				dd.slideDown().addClass('a');
			}
			dds.slideUp(function(){
				$(this).removeClass('a');
			})
		})
	})
</script>
</head>

<style>
* {
	font-family: "Microsoft YaHei";
}

ul {
	font-size: 14px;
	color: #666;
}

.jbxxnr {
	position: relative;
	width: 100%;
	background: url(static/img/title_b.png) 10px 9px no-repeat;
	line-height: 40px;
	height: 40px;
	box-shadow: 0 1px 3px #c3c3c3;
	font-family: "Microsoft YaHei";
}

.jbxxnr span {
	margin-left: 50px;
}

.jbxxwz {
	position: absolute;
	margin-top: 10px;
	width: 99%;
}

.nr {
	position: absolute;
	width: 99%;
	margin-top: 20px;
	margin-left: 5px;
	box-shadow: 0 1px 3px #c3c3c3;
}

.button8, .tianjiaanniu, .fp {
	padding: 8px 30px;
	border: none;
	background: rgb(54, 156, 218);
	border-radius: 4px;
	color: white;
	margin-left: 5px;
	cursor: pointer;
}

.nr span {
	font-size: 14px;
	color: #666;
	padding-left: 10px;
	line-height: 3em;
	cursor: pointer;
}

.nr input[type="checkbox"] {
	width: 15px;
	height: 15px;
	cursor: pointer;
	margin-left: 8px;
	vertical-align: middle;
}

.hdpanniu select {
	padding-left: 5px;
	height: 33px;
	box-shadow: 1px 1px 3px 1px #e2e2e2;
}

.hdpanniu {
	margin-left: 5px;
}

.nr ul {
	display: none;
}
</style>
<body>
	<div>
		<div class="jbxxnr">
			<span>角色分配</span>
		</div>
		<div class="jbxxwz">
			<div class="hdpanniu">
				<select name="type" class="type">
					<option>--请选择--</option>
					<c:forEach var="tlist" items="${tlist}">
						<option value="${tlist.employeetypename}">${tlist.employeetypename}</option>
					</c:forEach>
				</select>
				<a class="fplj"><input class="fp" type="button" value="分配" /></a>
				<input class="button8" type="button" value="全部打开" /> 
				<input class="tianjiaanniu" type="button" value="全部关闭" />
			</div>
			<div class="nr">
				<c:forEach var="a" items="${list}">
					<c:if test="${a.websonid<100}">
						<div>
							<input type="checkbox" id="zoncheck" /> 
							<span id="zhuspan">[+]    ${a.webmodelname}</span>
							<ul>
								<c:forEach var="b" items="${list}">
									<c:if test="${a.websonid==b.webfatherid}">
										<div>
											<input type="checkbox" mkid="${b.websonid}" /> <span>${b.webmodelname}</span>
										</div>
									</c:if>
								</c:forEach>
							</ul>
						</div>
					</c:if>
				</c:forEach>
			</div>
		</div>
	</div>
</body>
</html>