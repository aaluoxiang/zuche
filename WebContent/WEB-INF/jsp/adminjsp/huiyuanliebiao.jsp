<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html >
<html>
<head>
<base href="<%=request.getContextPath()%>/">
<link rel="stylesheet" type="text/css" href="static/css/huiyuanliebiao.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script src="static/js/jquery-1.12.4.js" type="text/javascript"></script>
<script src="static/js/My97DatePicker/WdatePicker.js" type="text/javascript" charset="utf-8"></script>
</head>
<script>
$(function(){
	$('.button8').on('click',function(){
		var tel=$(this).attr("tel");
		$('.bj').css('display', 'block');
		$('.shoukuan').css('display', 'block');	
		$('.fukuanjine').val(tel);
	});
	//关闭按钮
	$('.shoukuanbiaozhi').on('click', function() {
		$('.bj').css('display', 'none');
		$('.shoukuan').css('display', 'none');
		$('.tianjiamokuai').css('display', 'none');	
		$('.chozhijilu').css('display', 'none');
		$('.jiluchozhi').html("");	
		$('.begain').val("");
		$('.down').val("");
		$('.fukuanjine').val("");
		$('.chozhijine').val("");
		$('.fukuanjine').val("");
	});
	//添加
	$('.tianjiaanniu').on('click', function() {
		$('.bj').css('display', 'block');
		$('.tianjiamokuai').css('display', 'block');
		$('.biaozhi').text("添加");
		$('#tianjiayonhuming').val("");
		$('#tianjiamima').val("");
		$('.juese').val("");
		$('.ztai').val("");
		$('#tianjiadianhua').val("");
		$('#tianjiaemail').val("");
		$('#tinajiayue').val("");
		$('#tianjiajifen').val("");
		//$('.chozhijine1').css('readonly',"");
	});
	//查询记录
	$('.chaxunchozhijilu').on('click',function(){
		var begain=$('.begain').val();
		var down=$('.down').val();
		$('.jiluchozhi').html("");
		$.ajax({
				type:"post",
				url:"chaxunjilu",
				cache:false,
				data:{"begain":begain,"down":down},
				dataType:'json',
				success:function(data){
				$.each(data,function(i,item){						
					var d=new Date(item.chozhidata);
					var data=d.toLocaleString();
					var td1=$("<td>"+item.chozhijiaoyifangxiang+"</td>");
					var td2=$("<td>"+item.chozhijiaoyilaiyuan+"</td>");
					var td3=$("<td>"+item.chozhimoney+"</td>");
					var td4=$("<td>"+data+"</td>");
					var td5=$("<td>"+item.chozhicaozuoren+"</td>");
					var tr=$("<tr></tr>");
					tr.append(td1);
					tr.append(td2);
					tr.append(td3);
					tr.append(td4);
					tr.append(td5);
				$('.jiluchozhi').append(tr);
				num+=parseFloat(item.chozhimoney);
				});
				$('.zonqian').text(num+"元");
			}
		})
	})
	//查看充值记录
	$('.anniu5').on('click',function(){
		var num=0;
		$('.bj').css('display', 'block');
		$('.chozhijilu').css('display', 'block');
		var tel=$(this).attr("tel");
		$.ajax({
			type:"Get",
			url:"chozhijilu",
			cache:false,
			data:{"tel":tel},
			dataType:'json',
			success:function(data){
			$.each(data,function(i,item){
				var d=new Date(item.chozhidata);
				var data=d.toLocaleString();
					var td1=$("<td>"+item.chozhijiaoyifangxiang+"</td>");
					var td2=$("<td>"+item.chozhijiaoyilaiyuan+"</td>");
					var td3=$("<td>"+item.chozhimoney+"</td>");
					var td4=$("<td>"+data+"</td>");
					var td5=$("<td>"+item.chozhicaozuoren+"</td>");
					var tr=$("<tr></tr>");
					tr.append(td1);
					tr.append(td2);
					tr.append(td3);
					tr.append(td4);
					tr.append(td5);
				$('.jiluchozhi').append(tr);
				num+=parseFloat(item.chozhimoney);
				});
			$('.zonqian').text(num+"元");
			}
		})
	})
	$('.anniu3').on('click', function() {
		$('.bj').css('display', 'block');
		$('.biaozhi').text("修改");
		$('.tianjiamokuai').css('display', 'block');
		var id=$(this).attr('userid');
		$('.hide').attr('value',id);
		
		$.ajax({
			type:"post",
			url:"userxiugai",
			cache:false,
			data:{"id":id},
			dataType:'json',
			success:function(data){
				$.each(data,function(i,item){		
					$('#tianjiayonhuming').val(item.userName);
					$('#tianjiamima').val(item.password);
					$('.juese').val(item.userType);
					$('.ztai').val(item.userZt);
					$('#tianjiadianhua').val(item.userTel);
					$('#tianjiaemail').val(item.userEmail);
					$('#tinajiayue').val(item.userMoney);
					$('#tianjiajifen').val(item.userJf);
				})
			}
		})
	});
})
function chaxun(taget){
	var value=$('.shurudindan').val();
	if(value==null){
		alert("查询信息不能为空！！！");
	}else{
		$('.chaxun').attr('href',"huiyuanliebiao?value="+$('.shurudindan').val()+"");
	}
}
</script>
<style>
.button1,.tianjiaanniu,.chaxunchozhijilu {
	padding: 8px 30px;
	border: none;
	background: rgb(54, 156, 218);
	border-radius: 4px;
	color: white;
	margin-left:5px;
}
.chaxunchozhijilu{
margin-left:17px;
box-shadow: 0px 1px 3px 1px #ccc;
}
.anniu3 {
	background: red;
	border-radius: 4px;
	border: none;
	padding: 4px 8px;
	font-size: 14px;
	outline: none;
	color: white;
	cursor: pointer;
}
.anniu5,.anniu7,.button8{
	   background: #3eb05b;
	   line-height: 10px;
	padding: 4px 8px;
    border-radius: 4px;
    margin-bottom: 3px;
    font-size: 14px;
    color: white;
    cursor: pointer;
}
.button8{
background:#369CDA;
}
td {
	text-align: center;
	padding:8px;
	font-size:12px;
	    border: 1px solid #e2e2e2;
}
th {
	background: #f1f1f1;
	line-height: 2em;
	font-size: 12px;
	padding:8px;
	    border: 1px solid #e2e2e2;
}
.shoukuanbiaozhi{
	float: right;
	cursor: pointer;
	padding-top: 8px;
	padding-right: 8px;
}
.jiesudindan td, .jiesudindan tr {
	border: none;
}
.fukuanjine,.chozhijine,.fukuanjine1,.chozhijine1 {
	box-shadow: 1px 1px 3px 1px #e2e2e2;
	width: 200px;
	padding: 6px;
	border-radius: 2px;
	font-size: 14px;
	outline: none;
	border: 1px solid #D9D9D9;
}
.fukuanjine1,.chozhijine1{
width:300px;
}
span {
	padding-left: 50px;
}
.shoukuan,.tianjiamokuai,.chozhijilu{
	width: 450px;
	height: 301px;
	font-family: "Microsoft YaHei";
	position: absolute;
	border-radius: 2px;
	background: white;
	z-index: 3;
	box-shadow: 1px 1px 1px 1px #e2e2e2;
	margin: 130px 0 0 350px;
	display: none;
}
.chozhijilu{
width: 700px;
height: 400px;
display:none;
}
.tianjiamokuai{
width:600px;
height:400px;
}

.logo {
	border-bottom: 1px solid #e2e2e2;
	height: 42px;
	line-height: 42px;
	padding: 0 0 0 10px;
	background-color: #F8F8F8;
	font-family: "Microsoft YaHei";
	font-size: 14px;
}
.bottom {
	float: left;
	width: 100%;
	height: 50px;
	overflow: hidden;
	position: absolute;
	bottom: 0;
	left: 0;
	background: #f0f0f0;
	border-top: 1px solid #ccc;
}
.tijiaoo{
	padding: 0px 50px;
	background: #f60;
	margin: 8px 0 0px 20px;
	line-height: 35px;
	box-shadow: 2px 1px 4px 1px #666;
	border-radius: 4px;
	border: none;
	font-size: 14px;
	color: white;
}
.shoukuanmoney,.ad{
display:none;
width:0px;
}
.bj {
	background: black;
	position: absolute;
	width: 100%;
	height: 100%;
	top: 0;
	left: 0;
	opacity: 0.8;
	z-index: 2;
	display: none;
}
.shurudindan,.begain,.down{	
	box-shadow: 1px 1px 3px 1px #e2e2e2;
    font-size: 14px;
    border: 1px solid #D9D9D9;
    outline: none; 
}
.shurudindan,.begain,.down{
padding: 8px;
}
.shurudindan{
 margin-left: 20px;
}

.nr{
height:300px;
overflow-y:auto; 
margin-left:84px;
}
.zonjine{
width:94%;
line-height:30px;
border-bottom:1px solid #ccc;
padding: 4px 21px;
}
li{
float:left;
list-style-type:none;
margin-right:15px;
}
ul{
clear:both;
}
.chozhifashi{
height:34px;
}
table {
	border-collapse: collapse;
}
.chozhijilubiao th,.chozhijilubiao td{
border:1px solid #B5AFAF;
}
.chozhisjian{
margin-bottom:77px;
}
.jilubiao{
	width:614px;
    margin-left: 43px;
    margin-right: 148px;
        height: 231px;
    overflow-y: auto;
}
.jilubiao{
box-shadow: 0 1px 3px #c3c3c3;
}
.zonqian{
    color: #f00;
    padding: 0 6px;
    font-weight: 600;
}
</style>
<body>
<div class="bj"></div>
	<form method="post">
	<div class="chozhijilu">
		<div class="logo">
				<span class="biaozhi">充值记录</span> 
				<img class="shoukuanbiaozhi"  src="static/img/close-g.png" />
		</div>
		<div class="chozhisjian">
		<div class="zonjine">
			<label>总金额:</label>
			<span class="zonqian"></span>
		</div>
		<ul>
		<li>
		<input  class="begain" type="text" placeholder="开始时间" onfocus="WdatePicker({})" />
		<input type="text" class="down" placeholder="结束时间" onfocus="WdatePicker({})" />
		</li>
		<li>
		<input type="button" value="查询" class="chaxunchozhijilu"/>
		</li>
		</ul>
		</div>
		<div class="jilubiao">
		<table class="chozhijilubiao">
				<thead>
					<th width="110px">交易方向</th>
					<th width="110px">交易来源</th>
					<th width="110px">金额</th>
					<th width="90px">日期</th>
					<th width="110px">操作人</th>
				</thead>
				<tbody class="jiluchozhi">
				
				</tbody>
		</table>
		</div>
	</div>
	<div class="tianjiamokuai">
			<div class="logo">
				<span class="biaozhi"></span> 
				<img class="shoukuanbiaozhi" src="static/img/close-g.png" />
			</div>
			<div class="nr">
			<table class="tianjia">
				<tr>
					<td><label> 用户名: </label></td>
					<td>
						<input class="fukuanjine1" id="tianjiayonhuming" name="tianjiayonhuming" type="text" />
					</td>
				</tr>	
					<tr>
					<td><label> 密码:</label></td>
					<td>
						<input class="chozhijine1" id="tianjiamima" name="tianjiamima" type="text" />
					</td>
				</tr>	
				<tr>
					<td><label> 角色:</label></td>
					<td style="text-align: left">
						<select class="juese" name="juese">
						<option value="">--请选择--</option>		
						<c:forEach var="lx" items="${ leixingjihe}">	
						<option value="${lx.usertypename}">${lx.usertypename}</option>
						</c:forEach>	
						<!-- <option value="普卡会员">普卡会员</option>					
						<option value="银卡会员">银卡会员</option>
						<option value="黄金会员">黄金会员</option>
						<option value="白金会员">白金会员</option>
						<option value="钻石会员">钻石会员</option> -->
						</select>
					</td>
				</tr>	
				<tr>
					<td><label>状态:</label></td>
					<td style="text-align: left">
						<select class="ztai" name="ztai">
						<option value="">--请选择--</option>
						<option value="正常">正常</option>
						<option value="冻结">冻结</option>				
						</select>
					</td>
				</tr>	
				<tr>
					<td><label>电话:</label></td>
					<td>
						<input class="chozhijine1" id="tianjiadianhua" name="tianjiadianhua" type="text" />
					</td>
				</tr>	
				<tr>
					<td><label> email:</label></td>
					<td>
						<input class="chozhijine1" id="tianjiaemail" name="tianjiaemail" type="text" />
					</td>
				</tr>	
				<tr>
					<td><label> 余额:</label></td>
					<td>
						<input readonly="readonly" class="chozhijine1" id="tinajiayue" name="tinajiayue" type="text" />
					</td>
				
				</tr>	
				<tr>	
					<td><label>积分:</label></td>
					<td>
						<input class="chozhijine1" id="tianjiajifen" name="tianjiajifen" type="text" />
					</td>
				</tr>			
			</table>
			</div>
			<div class="bottom">
				<input type="submit" class="tijiaoo" value="提交" />				
			<input type="hidden"  class="hide" name="ss"/>
			</div>	
		</div>
		<form method="post">
		<div class="shoukuan">
			<div class="logo">
				<span >收款</span> 
				<img class="shoukuanbiaozhi" src="static/img/close-g.png" />
			</div>
			<table class="jiesudindan">
				<tr>
					<td><label> 充值电话: </label></td>
					<td>
						<input class="fukuanjine" name="chozhitel" type="text" />
					</td>
				</tr>	
					<tr>
					<td><label> 充值金额</label></td>
					<td>
						<input class="chozhijine" name="chochozhizhi" type="text" />
					</td>
				</tr>			
			</table>
			<div class="bottom">
				<input type="submit" class="tijiaoo" value="提交" />					
			</div>	
		</div>
		</form>
		<div class="systemhdp">
			<span>会员管理</span>
			
			<div class="hdpanniu">
				
				<input type="text" placeholder="请输入电话号码或者会员等级" class="shurudindan" />
				<a class="chaxun" onclick="chaxun()"><input class="button1" type="button"
					value="查询" /></a> 
				<!-- //<input class="button8" type="button" value="充值" />  -->					
					<input class="tianjiaanniu" type="button" value="+添加" />
			</div>
			<div class="hdpnr">
				<table>
					<tr>
						<th width="120px">名称</th>
						<th width="150px">角色</th>
						<th width="140px">电话</th>
						<th width="200px">email</th>
						<th width="200px">状态</th>
						<th width="140px">余额</th>
						<th width="150px">积分</th>
						<th width="400px">操作</th>
					</tr>
					<c:forEach var="user" items="${userlist}">
						<tr>
							<td>${user.userName}</td>
							<td>${user.userType }</td>
							<td>${user.userTel }</td>
							<td>${user.userEmail}</td>
							<c:choose>
							<c:when test="${user.userZt=='正常' }">
							<td style="color: #49bf67">${user.userZt }</td>
							</c:when>
							<c:otherwise>
							<td style="color: #f34541">${user.userZt }</td>
							</c:otherwise>	
							</c:choose>							
							<td>${user.userMoney }</td>
							<td>${user.userJf}</td>
							<td><a class="anniu3" userid="${user.userId}"> 修改</a>
								<a class="anniu5" tel="${user.userTel }"> 充值记录</a>
								<a class="button8" tel="${user.userTel }"> 充值</a>
							</td>	
						</tr>											
					</c:forEach>
				</table>
			</div>
		</div>
	</form>
</body>
</html>