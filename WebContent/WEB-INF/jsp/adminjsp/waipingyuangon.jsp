<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html >
<html>
<head>
<base href="<%=request.getContextPath()%>/">
<link rel="stylesheet" type="text/css" href="static/css/waipingyuangon.css">
<link rel="stylesheet" href="static/css/shangchuantupiananniu.css"/>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<script src="static/js/jquery-1.12.4.js"></script>
<script>
	$(function() {
		$(".button2").on('click', function() {
			var s = [];
			$("input:checked").each(function() {
				var aa = $(this).attr("ss");
				s.push(aa);
			})
			$(".tupianid").attr("value",s);
		});
		//修改状态
		$(".anniu3").on('click',function(){
			$(".bj").css("display", "block");
			$(".xiugaizhuantai").css("display","block");
			$(".select").val($(this).attr('value'));
			$(".xuanzeid").attr("value",$(this).attr("ss"));
		});
		//相册
	 $('.anniu4').on('click',function(){
		 var name=$(this).attr('name');
		 var formata=new FormData();
		 if($('.file1')[0].files[0]!=null)
		 formata.append("logoImg",$('.file1')[0].files[0]);		
		 formata.append("name",name);
		 $(".tupiankuang img").attr("src", "");
		 $(".tupian").html("");
			$.ajax({
				type:"post",
				url:"xiangcheshangchuan",
				cache:false,
				processData:false,
				contentType:false,
				data:formata,
				success:function(data){
				$.each(JSON.parse(data),function(i,item){	
					var img=$("<img style='padding-left:5px' src='static/img/"+item.url+"' width='100px' height='100px'/>");
					$(".tupian").append(img);
					})
				},		             
			error:function(error){  
	           alert('请选择图片！！！');  
			}
			}) 
		}); 
		
		 $(".anniu5").on('click',function(){
			$(".bj").css("display", "block");
			$(".xiangche").css("display", "block");
			var name=$(this).attr('name');
			$('.anniu4').attr('name',name);
			 $.ajax({
				type:"post",
				url:"xiangche",
				cache:false,
				data:{"name":name},
				dataType:'json',
				success:function(data){
					$.each(data,function(i,item){	
						var img=$("<img style='padding-left:5px' src='static/img/"+item.url+"' width='100px' height='100px'/>");
						$(".tupian").append(img);
					})
				}
			}) 
		}); 
		//评论
		$(".anniu6").on('click',function(){
			$(".bj").css("display", "block");
			$(".mk").css("display", "block");
		});
		//姓名
		$(".xingming").on('click',function(){
			$(".bj").css("display", "block");
			$(".gerenxinxi").css("display", "block");
			$(".td1").val($(this).attr("name"));
			$(".td2").val($(this).attr("sfz"));
			$(".td3").val($(this).attr("dh"));
			$(".td4").val($(this).attr("zt"));
		});
		//关闭建
		$(".cha").on('click',function(){
			$(".bj").css("display", "none");
			$(".gerenxinxi").css("display", "none");
			$(".mk").css("display", "none");
			$(".pinglun").css("display", "none");
			$(".xiugaizhuantai").css("display", "none");
		});
		
		$(".xiangchegb").on('click',function(){
			$(".bj").css("display", "none");		
			$(".xiangche").css("display", "none");
			$(".tupian").html("");
		});
		//表格颜色变换
		$("tr").hover(function(){
			$(this).css("background","#f1f1f1");
		});
		$("tr").mouseleave(function(){
			$(this).css("background","white");
		});
		$('.chaxun').on('click',function(){
			var xingming=$('.chaxunxinxi').val();
			$('.tbody').html("");
				$.ajax({
					type:'post',
					url:'chaxunwaipingyuangon',
					data:{'xm':xingming},
					cache:false,
					data:{'xingming':xingming},
					datatype:'json',
					success:function(data){
						$.each(JSON.parse(data),function(i,item){
							var td1=$("<td><input  style='border: none' type='checkbox' ss="+item.employeeid+"/><td>");
							var td2=$("<td>s</td>");
							var td3=$("<td>"+item.employeetel+"</td>");
							var td4=$("<td>"+item.employeesex+"</td>");
							var td5=$("<td>"+item.employeeage+"</td>");
							var td6=$("<td>"+item.employeesfz+"</td>");
							var td7=$("<td>"+item.employeejzbh+"</td>");
							var td8=$("<td>"+item.employeezt+"</td>");		
							
							var a1=$("<a class='anniu3' value="+item.employeezt+" ss="+item.employeeid+" >修改状态</a>");
							var a2=$("<a class='anniu5' name="+item.employeename+">相册</a>");
							var a3=$("<a class='anniu6'>评论</a>");
							var td9=$("<td> </td>");
							td9.append(a1);
							td9.append(a2);
							td9.append(a3);												
							var tr=$("<tr></tr>");
							tr.append(td1);							
							tr.append(td3);
							tr.append(td4);
							tr.append(td5);
							tr.append(td6);
							tr.append(td7);
							tr.append(td8);
							tr.append(td9); 					
							$('.tbody').append(tr);		
						})
					}
			})	
		})
	})
	function demo(file) {
			if (file.files && file.files[0]) {
				var reader = new FileReader();
				reader.onload = function(evt) {
					$(".tupiankuang img").attr("src", evt.target.result);
				}
				reader.readAsDataURL(file.files[0]);
			} else {
				$(".tupiankuang img").attr("src", evt.target.result);
			}
		}
</script>
<style>
.button1, .button2,.chaxun {
	padding: 8px 30px;
	border: none;
	background: rgb(54, 156, 218);
	border-radius: 4px;
	color: white;
	cursor:pointer;
	margin-left: 7px;
}
.chaxunxinxi{
	width: 100px;
    border-radius: 2px;
    outline: none;
    box-shadow: 1px 1px 3px 1px #e2e2e2;
    border: none;
    padding: 6px; 
}
.hdpnr th {
	background: #f1f1f1;
	line-height: 2em;
	font-size: 12px;
	padding: 8px;
	border: 1px solid #e2e2e2;
}
.hdpnr td {
	text-align: center;
	font-size: 12px;
	padding: 8px;
	border: 1px solid #e2e2e2;
}
body {
	margin: 0 auto;
}
.bj{
	background: black;
	position: absolute;
	width: 100%;
	height:100%;
	top: 0;
    left: 0;
	opacity: 0.1;
	z-index: 2;
	display:none;
}
.xiugaizhuantai,.xiangche,.pinglun,.gerenxinxi{
	font-family: "Microsoft YaHei";
	position: absolute;
	width: 300px;
	height: 200px;
	border-radius: 2px;
	background: white;
	z-index: 3;
	box-shadow: 1px 1px 1px 1px #e2e2e2;
	margin: 130px 0 0 350px;
	display:none;
}
.xiangche{
width:400px;
height:377px;
}
.logo {
	box-shadow: 0 1px 3px #c3c3c3;
	height: 42px;
	line-height: 42px;
	padding: 0 0 0 10px;
	background-color: #F8F8F8;
	font-family: "Microsoft YaHei";
	font-size: 14px;
}
.tijiao{
	width: 100px;
	border-radius: 4px;
	outline: none;
	cursor: pointer;
	background: #ff6600;
	margin: 8px 0 0px 20px;
	color: white;
	font-size: 14px;
	box-shadow: 0 1px 3px #c3c3c3;
	line-height: 35px;
	border: none;
	}
.cha,.xiangchegb{
	float: right;
	cursor: pointer;
	padding-top: 8px;
	padding-right: 8px;
}
.bottom{
    float: left;
    width: 100%;
    height: 50px;
    overflow: hidden;
    position: absolute;
    bottom: 0;
    left: 0;
    background: #f0f0f0;
    border-top: 1px solid #ccc;
}
.tijiaoo{
    padding: 0px 50px;
    background: #f60;
    margin: 8px 0 0px 84px;
    line-height: 35px;
    box-shadow: 2px 1px 4px 1px #666; 
    border-radius: 4px;
   border:none;
   font-size:14px;
   color:white;
    }
  
  .select{
   height: 2em;
   margin-top:7px;
    }
    .yuangon{
    font-size:14px;
    }
   .td1,.td2,.td3,.td4{
    border:none;
    background:white;
     disabled=disabled;
    }
    .tupiankuang {
	width: 100px;
	height: 100px;
	 margin: 14px 0px 5px 143px;
}
body {
	margin: 0 auto;
}

.logo {
	box-shadow: 0 1px 3px #c3c3c3;
	height: 42px;
	line-height: 42px;
	padding: 0 0 0 20px;
	background-color: #F8F8F8;
	font-family: "Microsoft YaHei";
	font-size: 14px;
}
.file1 {
	margin: 0 0 0 20px;
	outline: none;
}
.anniu4 {
	    width: 100px;
    border-radius: 4px;
    outline: none;
    cursor: pointer;
    background: #ff6600;
    margin: 8px 0 0px 20px;
    color: white;
    font-size: 14px;
    box-shadow: 0 1px 3px #c3c3c3;
    line-height: 28px;
    border: none;
}
.tupian{
position:absolute;
height:150px;
margin-top:5px;
overflow-y:auto; 
margin-left: 44px;
}
.selectt{
margin: 26px 47px;
}
.hdpanniu {
	position: absolute;
	margin-top: 20px;
	left: 2%;
}
.b{
    margin-top: -41px;
    margin-left: 190px;
    }
</style>
<body>
<div class="bj"></div>
	<form method="post">
		<div class="xiugaizhuantai">
			<div class="logo">
				<span>修改员工状态</span> <img class="cha" src="static/img/close-g.png" />
			</div>
		<div class="selectt">
			<table class="tablee">
			<tr>
				<td>
					<label class="yuangon">员工状态:</label>
				</td>
			<td>
			<select class="select" name="gaibianztai">
				<option value=" ">--请选择状态--</option>
				<option value="签订合约">签订合约</option>
				<option value="合适的">合适的</option>
				<option value="不合适的">不合适的</option>
			</select>
			<input  class="xuanzeid" style="display:none" name="id"/>
			</td>
			</td>
			</tr>
			</table>
			</div>
			<div class="bottom">	
			<input type="submit" class="tijiaoo" value="提交"/>
			</div>		
		</div>	
		<div class="xiangche">
			<form action="tupiankuang" method="post" enctype="multipart/form-data">
			<div class="logo">
				<span>查看相册</span><img class="xiangchegb" src="static/img/close-g.png" />
			</div>
			<div class="tupiankuang">
				<img width="100px" height="100px"/>
			</div>
			<a class="file" style="margin-left:95px;margin-top:10px">
			选择文件
			<input class="file1" type="file" name="file" onchange="demo(this)" />
			</a>
			<div class="b">
			<input type="button" class="anniu4" value="提交" />
			</div>
			<div class="tupian">		
			</div>
		</form>				
		</div>
		<div class="pinglun">
			<div class="logo">
				<span>评论信息</span> <img class="cha" src="static/img/close-g.png" />
			</div>		
		</div>
		<div class="gerenxinxi">
			<div class="logo">
				<span>查看详情</span>  <img class="cha" src="static/img/close-g.png" />
			</div>	
		<div>
				<table>
					<tr>
						<td>员工姓名:<td>
						<td><input  type="text" class="td1"/><td>						
					</tr>
					<tr>
						<td>身份证编号:<td>
						<td><input  type="text" class="td2"/><td>						
					</tr>
					<tr>
						<td>电话:<td>
						<td><input  type="text" class="td3"/><td>						
					</tr>
					<tr>
						<td>状态:<td>
						<td><input type="text" class="td4"/><td>						
					</tr>
				</table>
			</div>
		</div>
		<div class="systemhdp">
			<span style="margin-left:30px">外聘员工</span>
			<div class="hdpanniu">
			<input type="text" name="chaxunxinxi" placeholder="请输入员工姓名" class="chaxunxinxi"/>
			<input type="button" class="chaxun" value="查询"/>
				<a href="tianjiayuangon"><input class="button1" type="button"
					value="+添加" /></a>
				 <input class="button2" type="submit"  value="-删除" />
				<input class="tupianid" type="text" name="tupianid" />
			</div>
			<div class="hdpnr">
				<table>
					<tr>
						<th><input type="checkbox" /></th>
						<th width="162px">员工姓名</th>
						<th width="147px">电话号码</th>
						<th width="108px">性别</th>
						<th width="110px">年龄</th>
						<th width="165px">身份证编号</th>
						<th width="100px">驾照编号</th>
						<th width="100px">状态</th>
						<th width="218px">操作</th>
					</tr>
					<tbody class="tbody">
					<c:forEach var="employee" items="${list}">
						<tr>
							<td><input  style="border: none" type="checkbox"
								ss="${employee.employeeid}" /></td>
							<td><a class="xingming" name="${employee.employeename}" sfz="${employee.employeesfz}" dh="${employee.employeetel}" zt="${employee.employeezt}">${employee.employeename}</a></td>
							<td>${employee.employeetel}</td>
							<td>${employee.employeesex}</td>
							<td>${employee.employeeage}</td>
							<td>${employee.employeesfz}</td>
							<td>${employee.employeejzbh}</td>
							<td>${employee.employeezt}</td>
							<td>
							<a class="anniu3" value="${employee.employeezt}" ss="${employee.employeeid}" >修改状态</a>
							<a class="anniu5" name="${employee.employeename}">相册</a>
							<a class="anniu6">评论</a>	</td>
						</tr>											
					</c:forEach>
				</tbody>
				</table>
			</div>
		</div>
	</form>
</body>
</html>