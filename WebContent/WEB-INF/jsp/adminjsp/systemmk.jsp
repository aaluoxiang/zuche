<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html >
<html>
<head>
<base href="<%=request.getContextPath()%>/">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script src="static/js/jquery-1.12.4.js" type="text/javascript"></script>
</head>
<script>
	$(function() {
		/* if(${ss}!=""){	
			alert("添加成功！！!");		
		} */
		$('.cha').on('click', function() {
			$('.bj').css('display', 'none');
			$('.sonmk').css('display', 'none');
		})
		$('.sclj').on('click', function() {
			var s = [];
			$('input:checked').each(function() {
				var sonid = $(this).attr("sonid");
				s.push(sonid);
			});
			$(this).attr("href", "systemmk?sonid=" + s + "");
		});
		$(".nr").on('click', '#webname', function() {
			var dd = $(this).next();
			var dds = dd.siblings('ul');
			var ddv = dd.siblings('ul:visible')
			dds.slideDown();
			ddv.slideUp();
			//dd.css('display','block');
			/* if(dd.hasClass('a')){
				dds=dd;
			}else{
				$("ul").slideDown().addClass('a');
			}
			$("ul").slideUp(function(){
				$(this).removeClass('a');
			})  */
		});

		$('.button8').on('click', function() {
			$('ul').css('display', 'block');
		});
		$('.tianjiaanniu').on('click', function() {
			$('ul').css('display', 'none');
		});
		$('.tianjiazimokuai').on('click', function() {
			$('.bj').css('display', 'block');
			$('.sonmk').css('display', 'block');
			var sonid = $(this).attr('sonid');
			$('.a').val(sonid);
		});
		$('.fp').on('click', function() {
			$('.sk').css("display", 'none');
			$('.tj').css("display", 'block');
		});
		$('.fh').on('click', function() {
			$('.tj').css("display", 'none');
			$('.sk').css("display", 'block');
			//$('.sk').fadeout();
		});

	})
</script>
<style>
* {
	font-family: "Microsoft YaHei";
}

label {
	font-size: 12px;
	margin-left: 10px;
}

.tj {
	display: none;
}

ul {
	font-size: 14px;
	color: #666;
}

.jbxxnr {
	position: relative;
	width: 100%;
	background: url(static/img/title_b.png) 10px 9px no-repeat;
	line-height: 40px;
	height: 40px;
	box-shadow: 0 1px 3px #c3c3c3;
	font-family: "Microsoft YaHei";
}

.jbxxnr span {
	margin-left: 50px;
}

.jbxxwz {
	position: absolute;
	margin-top: 10px;
	width: 100%;
}

.nr {
	position: absolute;
	width: 100%;
	margin-top: 20px;
}

.nr ul {
	display: none;
}

.sc, .button8, .tianjiaanniu, .fp {
	padding: 8px 30px;
	border: none;
	background: rgb(54, 156, 218);
	border-radius: 4px;
	color: white;
	margin-left: 5px;
}

.nr a {
	color: #fff;
	text-decoration: none;
	height: 3em;
	line-height: 3em;
	margin-left: 10px;
	padding: 2px 8px;
	border-radius: 4px;
}

.nr input[type="checkbox"] {
	width: 15px;
	height: 15px;
	cursor: pointer;
	margin-left: 8px;
	vertical-align: middle;
}

.nr span {
	font-size: 14px;
	color: #666;
	padding-left: 10px;
	line-height: 3em;
	cursor: pointer;
}

#webname {
	cursor: pointer;
}

.tianjiazimokuai {
	background: #f7990d;
}

.xiugai {
	background: #3eb05b;
}

.shanchu {
	background: #f34541;
}

#s {
	padding: 8px 30px;
	border: none;
	background: rgb(54, 156, 218);
	border-radius: 4px;
	color: white;
	margin-left: 5px;
}

.k {
	box-shadow: 1px 1px 3px 1px #e2e2e2;
	font-size: 14px;
	border: 1px solid #D9D9D9;
	outline: none;
	padding: 3px;
}

.sonmk {
	display: none;
	margin: 9% 0 0 32%;
	width: 290px;
	height: 283px;
	position: absolute;
	border-radius: 2px;
	background: white;
	z-index: 3;
	box-shadow: 1px 1px 1px 1px #e2e2e2;
}

.sonname, .sonURL, .sonpaixu {
	padding: 4px;
	box-shadow: 1px 1px 3px 1px #e2e2e2;
	font-size: 14px;
	border: 1px solid #D9D9D9;
	outline: none;
}

.logo {
	border-bottom: 1px solid #e2e2e2;
	height: 42px;
	line-height: 42px;
	padding: 0 0 0 10px;
	background-color: #F8F8F8;
	font-size: 14px;
}

.cha {
	float: right;
	cursor: pointer;
	padding-top: 8px;
	padding-right: 8px;
}

.xinxinr, .quchebiao {
	position: absolute;
	height: 280px;
	margin-top: 10px;
	margin-left: 33px;
}

.quchebiao td {
	border: none;
	width: 45px;
	font-size: 12px;
}

.quchebiao tr {
	height: 48px;
}

.quchexinxitj {
	padding: 5px 36px;
	border-radius: 5px;
	width: 101px;
	text-align: center;
	height: 31px;
	border-radius: 4px;
	cursor: pointer;
	background: #FF3300;
	color: white;
	font-size: 14px;
	border: none;
}

.sb {
	margin-left: 65px;
	margin-top: 17px;
}

.bj {
	background: black;
	position: absolute;
	width: 100%;
	height: 100%;
	top: 0;
	left: 0;
	opacity: 0.8;
	z-index: 2;
	display: none;
}

.a {
	display: none;
	width: 0px;
}
</style>
<body>

	<div class="bj"></div>
	<div class="sonmk">
		<div class="logo">
			<span>子模块添加</span> <img class="cha" src="static/img/close.png" />
		</div>
		<form method="post">
			<div class="quchebiao">
				<table>
					<tr>
						<td>名称:</td>
						<td><input type="text" name="sonname" class="sonname" /></td>
					</tr>
					<tr>
						<td>URL:</td>
						<td><input type="text" name="sonURL" class="sonURL" /></td>
					</tr>
					<tr>
						<td>排序:</td>
						<td><input type="text" value="${max}" name="sonpaixu"
							class="sonpaixu" /></td>
					</tr>
				</table>
				<div class="sb">
					<input class='quchexinxitj' type="submit" value="提交" /> <input
						class="a" name="webfatherid" type="text" />
				</div>
			</div>
		</form>
	</div>


<div class="sonmk">
		<div class="logo">
			<span>子模块添加</span> <img class="cha" src="static/img/close.png" />
		</div>
		<form method="post">
			<div class="quchebiao">
				<table>
					<tr>
						<td>名称:</td>
						<td><input type="text" name="sonname" class="sonname" /></td>
					</tr>
					<tr>
						<td>URL:</td>
						<td><input type="text" name="sonURL" class="sonURL" /></td>
					</tr>		
				</table>
				<div class="sb">
					<input class='quchexinxitj' type="submit" value="提交" /> <input
						class="a" name="webfatherid" type="text" />
				</div>
			</div>
		</form>
	</div>




	<div>
		<div class="jbxxnr">
			<span>基本信息内容</span>
		</div>
		<div class="jbxxwz">
			<div class='sk'>
				<input class="fp" type="button" value="添加一级模块" /> <input
					class="button8" type="button" value="全部打开" /> <input
					class="tianjiaanniu" type="button" value="全部关闭" /> <a class="sclj"><input
					class="sc" type="button" value="删除" /></a>

			</div>
			<div class="tj">
				<form method="post">
					<label>模块名称:</label> <input class='k' name="name" type="test" /> <label>URL:</label>
					<input class='k' name="url" type="test" /> <label>排序:</label> <input
						class='k' name="paixu" type="test" /> <input type="submit"
						class='qrtj' id="s" value='确认添加' /> <input type="button"
						class='fh' id="s" value='返回' />
				</form>
			</div>
			<div class="nr">
				<c:forEach var="list" items="${weburl}">
					<c:if test="${list.websonid<15}">
						<div>
							<input type="checkbox" sonid="${list.websonid }" /> <span
								id="webname">[+]${list.webmodelname} </span>
							<a class="tianjiazimokuai" href="javascript:void(0)" sonid="${list.websonid}">添加子模块</a>
							<a class="xiugai"href="javascript:void(0)"websonid="${list.websonid }" webmodelname="${list.webmodelname }" webmodelurl="${list.webmodelurl }" >修改</a>
							<ul>
								<c:forEach var="l" items="${weburl}">
									<c:if test="${list.websonid==l.webfatherid}">
										<div>
											<input type="checkbox" sonid="${l.websonid} " /> <span>${l.webmodelname}</span>
										</div>
									</c:if>
								</c:forEach>
							</ul>
						</div>
					</c:if>
				</c:forEach>
			</div>
		</div>
	</div>
</body>

</html>