<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.trkj.com/tags" prefix="Mytag"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=request.getContextPath()%>/">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="static/css/dindan.css" type='text/css'/>
<link rel="stylesheet" href="static/css/shangchuantupiananniu.css"/> 
<title>Insert title here</title>
</head>
<script src="static/js/jquery-1.12.4.js" type="text/javascript"></script>
<script>
	$(function() {
	//radio选中事件
	$(":radio").click(function(){
		if($('.xiufujine').val()==""){
			return;
		}else{
			var z=5000-$('.xiufujine').val()*$(this).val();
			$('.yingtuiyajin').val(z);			
		}
	})
		//表格颜色变换
		$(".hdpnr tr").hover(function(){
			$(this).css("background","#f1f1f1");
		});
		$(".hdpnr tr").mouseleave(function(){
			$(this).css("background","white");
		});
		//交易记录
		$('.cha').on('click', function() {
			$('.bj').css('display', 'none');
			$('.nr').css('display', 'none');
			$('.paiche').css('display', 'none');
			$('.suoyoushangping').css('display','none');
			$('.quchepingzheng').css('display', 'none');
			$('.tbody').html("");
			$('.suoyoushangpingtbody').html("");
			$('.paichetbody').html("");
			$('.qcpzckbiao').html("");
			$('.pzck').css('display','none');
		});
		$('.anniu7').on('click', function() {
			$('.bj').css('display', 'block');
			$('.nr').css('display', 'block');
		});
		//结束
		$('.jiesubiaozhi').on('click',function(){
			$('.bj').css('display', 'none');
			$('.jies').css('display', 'none');	
		})	
		
		$('.anniu3').on('click', function() {
			$('.bj').css('display', 'block');
			$('.jies').css('display', 'block');
			$('.yishoujine').attr("value",$(this).attr('money'));
			$('.ap').attr('value',$(this).attr('ss'));
			$('.tel').attr('value',$(this).attr('tel'));
			$('.carid').attr('value',$(this).attr('carid'));
		});
		$('.jiesutijiaoo').on('onclick',function(){
			$('.bj').css('display', 'none');
			$('.jies').css('display', 'none');
							
		})
		//收款
		$('.shoukuanbiaozhi').on('click',function(){
			$('.bj').css('display', 'none');
			$('.shoukuan').css('display', 'none');			
		});		
		$('.anniu5').on('click',function(){
			$('.bj').css('display','block');
			$('.shoukuan').css('display','block');
			$('.ad').attr("value",$(this).attr('ss'));
			$('.shoukuanmoney').attr("value",$(this).attr('money'));
			$('.shoukuantel').attr("value",$(this).attr('tel'));
		});
		$(".tijiaoo").on('click',function(){
			$('.bj').css('display', 'none');
			$('.shoukuan').css('display', 'none');	
		})
		$('.pzxx').on('click',function(){
			$('.pzck').css('display','block');
			$('.bj').css('display', 'block');
		})
		/* $('.tijiaoo').on('click',function(){
				var id=$(this).attr('aa'); 
				var money=$('.fukuanjine').val();
				var fashi=$('.fukuanfanshi').val();		
				$.ajax({
					type:"post",
					url:"shoukuang",
					cache:true,
					data:{"id":id,"money":money,"fashi":fashi},						
					success:function(data){
						if(data){
							alert("修改成功");
						}
					}											
				});
				$('.bj').css('display', 'none');
				$('.shoukuan').css('display', 'none');	
			}); */
		$('.anniu6').on('click',function(){
			$('.suoyoushangping').css('display','block');
			var dindanid=$(this).attr('dindanid');
			
			$.ajax({
				type:"post",
				url:"suoyoushangping",
				cache:false,
				data:{"dindanid":dindanid},
				dataType:'json',
				success:function(data){
					$.each(data,function(i,item){	
						var td2=$("<td>"+item.ordercarid+"</td>");
						var td3=$("<td>"+item.ordercartype+"</td>");
						var td4=$("<td>"+item.price+"</td>");					
						var tr=$("<tr></tr>");					
						tr.append(td2);
						tr.append(td3);
						tr.append(td4);				
						$('.suoyoushangpingtbody').append(tr);	
					});
				}	
			})
		})		
		
		//凭证信息
			$('.pzxx').on('click',function(){
				var orderid=$(this).attr("orderid");
				$.ajax({
					type:"post",
					url:'chakanpzxx',
					cache:false,
					data:{"orderid":orderid},
					dataType:'json',
					success:function(data){
						$.each(data,function(i,item){		
							var lxrname=item.lxrname;
							var tp=item.jzpicture;
							var yj=item.yjmoney;
							var tr1=$("<tr><td>姓名:</td><td><input readonly='readonly' type='text' value='"+lxrname+"' class='quchename'/></td></tr>");
							var tr2=$("<tr><td>押金:</td><td><input readonly='readonly' type='text' value='"+yj+"' class='qucheyajin'/></td></tr>");
							var tr3=$("<tr><td>照片:</td><td><img width='188px' src='static/img/"+tp+"' height='121px' style='border-radius: 3px'/></td></tr>");
						
							$('.qcpzckbiao').append(tr1);
							$('.qcpzckbiao').append(tr2);
							$('.qcpzckbiao').append(tr3);
						});
					}
				})
			})
		$('.anniu7').on('click',function(){
			var id=$(this).attr('ss');
			$.ajax({
				type:"Get",
				url:"judge",
				cache:false,
				data:{"id":id},
				dataType:'json',
				success:function(data){
					$.each(data,function(i,item){		
					var d=new Date(item.orderdata);
					var data=d.toLocaleString();
					var	orderid=item.orderid;
					var orderjiaoyifaxiang=item.orderjiaoyifaxiang;
					var orderjiaoyilaiyuan=item.orderjiaoyilaiyuan;
					var ordermoney=item.ordermoney;			
					var td2=$("<td>"+orderjiaoyifaxiang+"</td>");
					var td3=$("<td>"+orderjiaoyilaiyuan+"</td>");
					var td4=$("<td>"+ordermoney+"</td>");
					var td5=$("<td>"+data+"</td>");
					var tr=$("<tr></tr>");					
					tr.append(td2);
					tr.append(td3);
					tr.append(td4);
					tr.append(td5);
					$('.tbody').append(tr);				
					});
				}	
			});
		});		
			//发车的链接
			$('.xinxinr').on('click','.fachelj',function(){
				$('.quchepingzheng').css('display','block');
				$('.paiche').css('display','none');
				var cartypename=$(this).attr("cartypename");
				var chepaihao=$(this).attr("chepaihao");
				var dindanid=$(this).attr("dindanid");
				$(".a").val(cartypename);
				$(".b").val(chepaihao);
				$(".c").val(dindanid);
				//$('.quchexinxitj').attr({"cartypename":cartypename,"chepaihao":chepaihao,"dindanid":dindanid});			
			});
			$('.quchexinxitj').on('click',function(){
				 var formata=new FormData();
				 if($('.file1')[0].files[0]!=null){
				 formata.append("logoImg",$('.file1')[0].files[0]);
				 }
				var cartypename=$(this).attr("cartypename");
				var chepaihao=$(this).attr("chepaihao");
				var dindanid=$(this).attr("dindanid");
				 formata.append("chepaihao",chepaihao);
				var quchename=$(".quchename").val();
				var qucheyajin=$(".qucheyajin").val();
				$(this).attr("href","dindan?formate="+formata+"");
			});
			$('.xiufujine').on("input propertychange",function(){
				//alert($('input:radio[name="1"]:checked').val());
				var zk=$('input:radio[name="dxk"]:checked').val();
				
				var j=5000-$(this).val()*parseFloat(zk);
				
				$('.yingtuiyajin').val(j);
			})
			//派车
			$('.anniu8').on('click',function(){
				$('.paiche').css('display','block');
				$('.bj').css('display','block');
				var cheleixing=$(this).attr('cheleixing');
				var dindanid=$(this).attr('dindanid');			
				$.ajax({
					type:'post',
					url:'paichesuoyong',
					data:{'cheleixing':cheleixing,'dindanid':dindanid},
					cache:false,
					datatype:'json',
					success:function(data){
						$.each($.parseJSON(data),function(i,item){
							var	carlicenseid=item.carlicenseid;	
							var carTypeName=item.carTypeName;
							var carZtName=item.carZtName;	
							var td1=$("<td>"+carlicenseid+"</td>");
							var td2=$("<td>"+carTypeName+"</td>");
							var td3=$("<td>"+carZtName+"</td>");
							var td4=$("<td></td>");
							var button=$("<a class='fachelj' cartypename="+carTypeName+" chepaihao="+carlicenseid+" dindanid="+item.dindan+">发车</a>");
							///* href=dindan?cartypename="+carTypeName+"&chepaihao="+carlicenseid+"&dindanid="+item.dindan+" */
							var button1=$("<a>已租赁</a>");
							if(carZtName=='空闲中'){
								td4.append(button);	
							}else{
								td4.append(button1);	
							}			
							var tr=$("<tr></tr>");							
							tr.append(td1);
							tr.append(td2);
							tr.append(td3);	
							tr.append(td4);
							$('.paichetbody').append(tr);
						})
					}				
				});
			});			
			${ztxuanze};	
	});
	function chaxun(){	
	 	if($('.shurudindan').val()==""&&$('.ztxuanze').val()=="--状态选择--"){
			alert("请选择状态或者填写条件！！！");
		}else{ 
			$('.chaxun').attr('href',"dindan?id="+$('.shurudindan').val()+"&&ztxuanze="+$('.ztxuanze').val()+"");
		}	
	}
	function demo(file) {
		if (file.files && file.files[0]) {
			var reader = new FileReader();
			reader.onload = function(evt) {
				$(".quchebiao img").attr("src", evt.target.result);
			}
			reader.readAsDataURL(file.files[0]);
		} else {
			$(".quchebiao img").attr("src", evt.target.result);
		}
	}
</script>
<style>
.systemhdp {
	position: relative;
	width: 100%;
	background: url(../img/title_b.png) 10px 9px no-repeat;
	line-height: 40px;
	height: 40px;
	box-shadow: 0 1px 3px #c3c3c3;
	font-family: "Microsoft YaHei";
}
.a,.b,.c{
display:none;
}
#btspan{
	padding-left: 50px;
}
.hdpnr {
	position: absolute;
	margin-left: 10px;
	margin-top: 70px;
	box-shadow: 0 1px 3px #c3c3c3;
	height:570px;
	overflow-y:auto; 
}

.button1 {
	padding: 8px 30px;
	border: none;
	background: rgb(54, 156, 218);
	border-radius: 4px;
	color: white;
}
a {
	outline: none;
	text-decoration: none;
}

.hdpanniu {
	position: absolute;
	margin-top: 20px;
}

.anniu3 {
	background: red;
	border-radius: 4px;
	border: none;
	padding: 4px 8px;
	font-size: 14px;
	outline: none;
	color: white;
	cursor:pointer;
}
.tupianid {
	display: none;
}


.shurudindan {
	padding: 6px;
	box-shadow: 1px 1px 3px 1px #e2e2e2;
	font-size: 14px;
	border: 1px solid #D9D9D9;
	margin-left: 20px;
	outline: none;
}

.bj {
	background: black;
	position: absolute;
	width: 100%;
	height: 100%;
	top: 0;
	left: 0;
	opacity: 0.8;
	z-index: 2;
	display: none;
}
.nr, .jies,.shoukuan,.paiche,.suoyoushangping,.quchepingzheng,.pzck {
	width: 600px;
	height: 355px;
	font-family: "Microsoft YaHei";
	position: absolute;
	border-radius: 2px;
	background: white;
	z-index: 3;
	box-shadow: 1px 1px 1px 1px #e2e2e2;
	margin: 10% 0 0 30%;
	display: none;
}
.jies{
	width: 335px;
    height: 404px;
    
}
.quchepingzheng,.pzck{
	
    margin: 9% 0 0 32%;
    width: 317px;
    height: 363px;
}
.shoukuan{
width: 450px;
height: 301px;
}

.logo {
	border-bottom: 1px solid #e2e2e2;
	height: 42px;
	line-height: 42px;
	padding: 0 0 0 10px;
	background-color: #F8F8F8;
	font-family: "Microsoft YaHei";
	font-size: 14px;
}
.cha,.jiesubiaozhi,.shoukuanbiaozhi{
	float: right;
	cursor: pointer;
	padding-top: 8px;
	padding-right: 8px;
}
td {
	text-align: center;
	font-size: 12px;
	border: 1px solid #e2e2e2;
	font-family: "Microsoft YaHei";
	padding: 8px;
	
}
.hdpnr td{
	height:98px;
}
th {
	background: #f1f1f1;
	line-height: 2em;
	font-size: 12px;
	padding: 8px;
	border: 1px solid #e2e2e2;
	font-family: "Microsoft YaHei";
}
table {
	border-collapse: collapse;
	box-shadow: 1px solid #e2e2e2;
}
.xinxinr,.quchebiao {
	position: absolute;
	height:280px;
	margin-left: 30px;
	margin-top: 10px;
	box-shadow: 0 1px 3px #c3c3c3;
	
}
.quchebiao td{
border:none;
    width: 28px;
}
.quchebiao{
    margin-left: 20px;
    box-shadow:none;
}
.tijiaoo,.jiesutijiaoo {
	padding: 0px 52px;
	background: #f60;
	margin: 8px 0px 2px 20px;
	line-height: 35px;
	box-shadow: 2px 1px 4px 1px #666;
	border-radius: 4px;
	border: none;
	font-size: 14px;
	color: white;
}
/* .yccl{
padding: 0px 41px;
margin: 8px 0px 2px 20px;
	background: #f60;	
	line-height: 35px;
	box-shadow: 2px 1px 4px 1px #666;
	border-radius: 4px;
	border: none;
	font-size: 14px;
	color: white;
} */
.bottom {
	float: left;
	width: 100%;
	height: 50px;
	overflow: hidden;
	position: absolute;
	bottom: 0;
	left: 0;
	background: #f0f0f0;
	border-top: 1px solid #ccc;
}

.shishoujin,.fukuanjine,.yingtuiyajin,.xiufujine,.fukuanjin {
	box-shadow: 1px 1px 3px 1px #e2e2e2;
	padding: 6px;
	border-radius: 2px;
	font-size: 14px;
	outline: none;
	border: 1px solid #D9D9D9;
}
label{
    font-family: "Microsoft YaHei";
    font-size: 14px;
}
.jiesudindan td, .jiesudindan tr {
	border: none;
}
.shoukuanmoney,.ad,.ap,.tel,.shoukuantel,.carid{
display:none;
width:0px;
}
.yishoujine{
border:none;
background:white;
}
.anniu5, .anniu7, .anniu6,.anniu8,.anniu9,.anniu10,.pzxx {
	line-height: 10px;
	padding: 4px 8px;
	border-radius: 4px;
	margin-bottom: 3px;
	font-size: 14px;
	color: white;
	cursor:pointer;
}
.pzxx{
background: #A2983A;
}
.anniu5 {
	background: #3eb05b;
}

.anniu6 {
	background: #369CDA;
}
.anniu7 {
	background: #f7990d;
}
.anniu8{
background: #9735A2;
}
.anniu9{
background: #26ADBB;
}
.anniu10{
background:#E8BB16;
}
.xiabiao{
    margin-left: 472px;
    margin-top: 30px;
    position: absolute;
    bottom:2%;
}
.xiabiao a{
color:black;
}
a{
cursor: pointer;
}
.ztxuanze{
    float: right;
    margin-left: 23px;
    margin-top: 7px;
    box-shadow: 1px 1px 3px 1px #e2e2e2;
    height: 30px;
   
}
.quchename,.qucheyajin{
    padding: 6px;
    box-shadow: 1px 1px 3px 1px #e2e2e2;
    font-size: 14px;
    border: 1px solid #D9D9D9;

    outline: none;
}
.quchexinxitj{
    padding: 5px 36px;
    border-radius: 5px;
 	width: 101px;
    text-align: center;
    height: 31px;
    border-radius: 4px;
    cursor: pointer;
    background: #FF3300;  
    color: white;
    font-size: 14px;  
    line-height: 23px;
    border: none;
}
.zp{
    margin-left: 34px;
    margin-top: 12px;
}
.sb{ 
	margin-left: 155px;
    margin-top: -35px;
    }
.zzzz{
        margin-left: 27px;
}   
 .fukuanfanshi{
    box-shadow: 1px 1px 3px 1px #e2e2e2;
    height: 30px;
    font-size: 14px;
    }
</style>
<link rel="stylesheet" type="text/css" href="static/css/dindan.css">
<body>
	<div class="bj"></div>
	<div class="paiche">
			<div class="logo">
				<span>车辆详情</span> <img class="cha" src="static/img/close.png" />
			</div>
			<div class="xinxinr">
				<table class="jiaoyijilubiao">
				<thead>
					<tr>
						<th width="117px">车牌照</th>
						<th width="117px">车名称</th>
						<th width="117px">车状态</th>
						<th width="117px">操作</th>						
					</tr>
				</thead>
					<tbody class='paichetbody'>
									
					</tbody>							
				</table>
			</div>
		</div>	
		
		
		<div class="pzck">
			<div class="logo">
				<span>取车凭证信息</span> <img class="cha" src="static/img/close.png" />
			</div>
			<div class="quchebiao">			
			<table class="qcpzckbiao">
			
			</table>			
			</div>		
		</div>
		
		
		
		
		
		
		
		
		
		
		
		
		<div class="quchepingzheng">
			<div class="logo">
				<span>取车凭证信息</span> <img class="cha" src="static/img/close.png" />
			</div>
			<form method="post" enctype="multipart/form-data" >
			<div class="quchebiao">			
			<table>
				<tr>
					<td>姓名:</td>
					<td>
					<input type="text" name="quchename" class="quchename"/>
					</td>
				</tr>
				<tr>
					<td>押金:</td>
					<td>
					<input type="text" name="qucheyajin" value="5000" readonly="readonly" class="qucheyajin"/>
					</td>
				</tr>
				<tr>
					<td>照片:</td>
					<td>
				<img width="188px" height="121px" style="border-radius: 3px"/>		
					</td>
				</tr>
			</table>			
			<div class="zp">
			<a href="" class="file">选择文件
				<input class="file1" type="file" name="file" onchange="demo(this)" />
			</a>			
			</div>
			<div class="sb">
			<input class='quchexinxitj' type="submit" value="提交"/>			
			
			</div>
			<input type="text" class="a" name="cartypename"/>
			<input type="text" class="b" name="chepaihao"/>
			<input type="text" class="c" name="dindanid"/>
			</div>
			</form>
		</div>
	<div class="suoyoushangping">
			<div class="logo">
				<span>车辆详情</span> <img class="cha" src="static/img/close.png" />
			</div>
			<div class="xinxinr">
				<table class="jiaoyijilubiao">
				<thead>
					<tr>
						<th width="160px">车牌照</th>
						<th width="160px">车名称</th>
						<th width="160px">车价格</th>							
					</tr>
				</thead>
					<tbody class='suoyoushangpingtbody'>								
					</tbody>							
				</table>
			</div>
		</div>
	
		<div class="nr">
			<div class="logo">
				<span>交易记录</span> <img class="cha" src="static/img/close-g.png" />
			</div>
			<div class="xinxinr">
				<table class="jiaoyijilubiao">
				<thead>
					<tr>
						<th width="115px">交易方向</th>
						<th width="115px">交易来源</th>
						<th width="115px">金额</th>						
						<th width="115px">日期</th>
					</tr>
					</thead>
					<tbody class='tbody'>				
					</tbody>							
				</table>
			</div>
		</div>
		<form method="post">
		<div class="jies">
			<div class="logo">
				<span>结束订单</span> <img class="jiesubiaozhi" src="static/img/close-g.png" />
			</div>
			<div class="zzzz">
			<table class="jiesudindan">
				<tr>
					<td><label> 已收金额: </label></td>
					<td style="text-align:left">
					<input  class="yishoujine" name="jine" />
					</td>
				</tr>
				<tr>
					<td><label>实收金额:</label></td>
					<td><input name="shishoujine"   class="shishoujin" /></td>
				</tr>		
				<tr>
					<td><label>破损程度:</label></td>
					<td>
					<c:forEach var="damage" items="${damage}">
					<label><input value="${damage.addmaintainmoney}"  type="radio" name="dxk"/>${damage.levername}</label>
					</c:forEach>
					<!-- <label><input value="1.2" type="radio" name="1"/>轻度</label>
					<label><input value="1.3" type="radio" name="1"/>中度</label>
					<label><input value="1.5" type="radio" name="1"/>重度</label> -->									
					</td>
				</tr>
				<tr>
					<td><label>修复金额:</label></td>
					<td><input name="xiufujine" class="xiufujine" /></td>
				</tr>				
				<tr>
					<td><label>应退押金:</label></td>
					<td><input name="yingtuiyajin" value="5000" readonly="readonly" class="yingtuiyajin" /></td>
				</tr>
				
				<tr>
				<td><label>是否还车:</label></td>
				<td style="text-align:left">
					<select name="haiche">
						<option value="已还车">已还车</option>
						<option value="未还车">未还车</option>
					</select>
					</td>		
				</tr>
				
			</table>
			</div>
			<div class="bottom">
				<input type="submit" class="jiesutijiaoo" value="提交" />				
				<!-- <input type="submit" class="yccl" value="异常处理" /> -->
				<input type="text" class="ap" name="ap"/>
				<input type="text" class="tel" name="tel"/>
				<input type="text" class="carid" name="carid"/>
			</div>
		</div>
		</form>
		<form method="post">
		<div class="shoukuan">
			<div class="logo">
				<span>收款</span> 
				<img class="shoukuanbiaozhi" src="static/img/close-g.png" />
			</div>
			<table class="jiesudindan">
				<tr>
					<td><label> 付款金额: </label></td>
					<td>
						<input class="fukuanjin" name="fukuanjine" type="text" />
					</td>
				</tr>
				<tr>
					<td><label>付款方式:</label></td>
					<td style="text-align:left">
					<select class="fukuanfanshi" name="fukuanfanshi">
						<option value="余额">余额</option>
						<option value="微信支付">微信支付</option>
						<option value="支付宝支付">支付宝支付</option>
					</select>
					</td>
				</tr>				
			</table>
			<div class="bottom">
				<input type="submit" class="tijiaoo" value="提交" />
				<input type="text" class="ad" name="ad"/>
				<input type="text" class="shoukuanmoney" name="shoukuanmoney"/>
				<input type="text" class="shoukuantel" name="shoukuantel"/>
			</div>
			
		</div>
		</form>	
		<div class="systemhdp">
			<span id="btspan">订单管理</span>
			<div class="hdpanniu">		
				<input type="text" placeholder="请输入电话号码或姓名" class="shurudindan" name="shurudindan" value="${param.id}"/> 
				<a class="chaxun" onclick="chaxun()"><input class="button1" type="button" value="查询" /></a>			
				<select class='ztxuanze' name="ztxuanze">			
					<option>--状态选择--</option>	
					<option value="已完成" >已完成</option>
					<option value="已付款">已付款</option>		
					<option value="待付款">待付款</option>		
					<option value="租赁中">租赁中</option>		
					<option value="还车审核">还车审核</option>		
					<option value="退款中">退款中</option>		
					<option value="已退单">已退单</option>											
				</select>
				<!-- <input class="ztd" name="ztd"/> -->
			</div>
			<div class="hdpnr">
				<table>
					<tr>
						<th width="150px">编号</th>
						<th width="132px">状态</th>
						<th width="130px">金额</th>
						<th width="133px">电话</th>
						<th width="158px">名称</th>
						<th width="173px">订单时间</th>
						<th width="339px">操作</th>
					</tr>
					<c:forEach var="userorder" items="${listuserorder}">
						<tr>
							<td>${userorder.orderid }</td>
							<c:choose>
								<c:when test="${userorder.userorderzt=='已退单'}">
									<td style="color: #f34541">${userorder.userorderzt}</td>
								</c:when>
								<c:when test="${userorder.userorderzt=='退款中'}">
									<td style="color: #f34541">${userorder.userorderzt}</td>
								</c:when>
								<c:when test="${userorder.userorderzt=='还车审核'}">
									<td style="color: #f8a326">${userorder.userorderzt}</td>
								</c:when>
								<c:when test="${userorder.userorderzt=='租赁中'}">
									<td style="color: #9735A2">${userorder.userorderzt}</td>
								</c:when>
								<c:otherwise>
									<td style="color: #49bf67">${userorder.userorderzt}</td>
								</c:otherwise>
							</c:choose>
							<td>${userorder.odermoney }</td>
							<td>${userorder.usertel }</td>
							<td>${userorder.userid }</td>
							<td><fmt:formatDate value="${userorder.ordersingletime}" type="both"/></td>
							<c:choose>
								<c:when test="${userorder.userorderzt=='已完成'}">
									<td>
									 <a class="anniu6" dindanid="${userorder.orderid }"> 所有商品</a> 
									<a class="anniu7"  ss="${userorder.orderid }"> 交易记录</a>
								</td>
								</c:when>
								
								<c:when test="${userorder.userorderzt=='已退单'}">
									<td>
									 <a class="anniu6" dindanid="${userorder.orderid }"> 所有商品</a> 
									<a class="anniu7"  ss="${userorder.orderid }"> 交易记录</a>
								</td>
								</c:when>
															
								<c:when test="${userorder.userorderzt=='待付款'}">
									<td><a class="anniu3" carid="${userorder.ordercarid}" money="${userorder.odermoney}" tel="${userorder.usertel}" ss="${userorder.orderid }"> 结束</a>
									<a class="anniu5" ss="${userorder.orderid }" money="${userorder.odermoney }" tel="${userorder.usertel}"> 收款</a> 
									<a class="anniu6" dindanid="${userorder.orderid }"> 所有商品</a> 
									<a class="anniu7" ss="${userorder.orderid }"> 交易记录</a>
									<br/>											
									</td>
								</c:when>								
								<c:when test="${userorder.userorderzt=='租赁中'}">
										<td><a class="anniu3" carid="${userorder.ordercarid}" money="${userorder.odermoney}" tel="${userorder.usertel}" ss="${userorder.orderid }"> 结束</a>
										<a class="anniu5" ss="${userorder.orderid }" money="${userorder.odermoney }" tel="${userorder.usertel}"> 收款</a> 
									<a class="anniu6" dindanid="${userorder.orderid }"> 所有商品</a> 
									<a class="anniu7" ss="${userorder.orderid }"> 交易记录</a>
									<br/>	
									<a class="pzxx" orderid="${userorder.orderid}">凭证信息</a>
													
									
									</td>
								</c:when>
								<c:when test="${userorder.userorderzt=='退款中'}">
									<td> 
										<a class="anniu10" href="dindan?tel=${userorder.usertel }&& dindanbianhao=${userorder.orderid}&&money=${userorder.odermoney}" onclick="if(confirm('确定退款')==false)return false;">退款</a>
										 <a class="anniu6" dindanid="${userorder.orderid }"> 所有商品</a> 
									<a class="anniu7"  ss="${userorder.orderid }"> 交易记录</a>
									</td>
								</c:when>								
								<c:when test="${userorder.userorderzt=='还车审核'}">
									<td><a class="anniu3" carid="${userorder.ordercarid}" money="${userorder.odermoney}" tel="${userorder.usertel}" ss="${userorder.orderid }"> 结束</a> 
										<a class="anniu5" ss="${userorder.orderid }" money="${userorder.odermoney }" tel="${userorder.usertel}"> 收款</a> 
									<a class="anniu6" dindanid="${userorder.orderid }"> 所有商品</a> 
									<a class="anniu7" ss="${userorder.orderid }"> 交易记录</a>
									<br/>
									</td>									
												
								</c:when>																					
								<c:otherwise>
									<td><a class="anniu3" carid="${userorder.ordercarid}" money="${userorder.odermoney}" tel="${userorder.usertel}" ss="${userorder.orderid }"> 结束</a> 
									<a class="anniu5" ss="${userorder.orderid }" money="${userorder.odermoney }" tel="${userorder.usertel}"> 收款</a> 
									<a class="anniu6" dindanid="${userorder.orderid }"> 所有商品</a> 
									<a class="anniu7" ss="${userorder.orderid }"> 交易记录</a>
									<br/>
									<a class="anniu8" dindanid="${userorder.orderid }" cheleixing="${userorder.ordercartype }">派车</a>								
									</td>
								</c:otherwise>
							</c:choose>
						</tr>
					</c:forEach>
				</table>
			<div class="xiabiao">
				<Mytag:page page="${page}" url="dindan" suoyoujilu="${suoyoujilu}" totlepage="${suoyoushu}"/>
				<%-- <c:if  test="${page==1}">
				<a href="javascript:void(0)">首页</a>
				<a href="javascript:void(0)">上一页</a>
				</c:if>				
				<c:if  test="${page!=1}">
				<a href="dindan?page=1">首页</a>
				<a href="dindan?page=${page-1}">上一页</a>
				</c:if>				
				<c:if  test="${page!=suoyoushu}">
				<a href="dindan?page=${page+1}">下一页</a> 
				<a href="dindan?page=${suoyoushu}">尾页</a>
				</c:if>			
				<c:if  test="${page==suoyoushu}">
				<a href="javascript:void(0)">下一页</a> 
				<a href="javascript:void(0)">尾页</a>
				</c:if>
				<label>${page}/${suoyoushu}总页数:${suoyoushu}</label> 
				</div> --%>
			</div>
		</div>	
</body>
</html>