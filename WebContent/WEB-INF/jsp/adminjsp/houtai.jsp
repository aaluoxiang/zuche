<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html >
<html>
<head>
<base href="<%=request.getContextPath()%>/">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="styLesheet" type="text/css" href="static/css/index.css">
</head>
<script src="static/js/jquery-1.12.4.js" type="text/javascript"></script>
</head>
<script>
$(function(){
	setInterval(function(){
		$.ajax({
			type:'post',
			url:'lunxun',
			success:function(data){
				if(data!=null&&""!=data.trim()){
					alert("有新的订单已付款，请尽快派车！！！");
					data="";
				}
			}
		})	
	},1000);
})
</script>
<body>
<div class="up">
	<!-- <a href=""> <img src="" />
	</a> -->
	<label class="ksice"><a href="javascript:void(0)">租车管理系统</a></label>
	<label class="time">${time}</label>
   		<label class="exit"><a href="adminloginout" onclick="if(confirm('确定退出')==false)return false;">退出</a></label>
		<label class="xiugaimima"><a href="javascript:void(0)"> 修改密码</a></label>		
		<label class="mannage">你好,${zhanghao}</label>
		<img class="touxiang" src="static/img/touxiang.png"/>
</div>
<div class="left">
	<div class="ss">
		<ul class="according">
			<c:forEach var="a" items="${as}">
				<c:if test="${a.websonid<15}">
					<dt>${a.webmodelname}</dt>
				</c:if>
				<dd>
					<c:forEach var="b" items="${bs}">
						<c:if test="${a.websonid==b.webfatherid}">
							<li><a target="mainFrame" href="${b.webmodelurl}">${b.webmodelname}</a></li>
						</c:if>
					</c:forEach>
				</dd>
			</c:forEach>
		</ul>
	</div>
</div>
<div class="content">
	<!-- <iframe src="jbxxnr" position="absolute" width="87%" height="400px">			
	</iframe>  -->
	<iframe id="mainFrame" name="mainFrame" src="${dd}" frameborder="0" width="100%"> </iframe>
</div>
<div class="bottom">
	<div class="gray_left"></div>
	<div class="lianjie">
		<a href="ksices.com">www.ksices.com</a> <label style="color:white; font-size:14px">技术支持:罗祥、左桓旭、宋佳、熊涛</label>
	</div>
</div>
</body>
<script language="javascript">
	$(function() {
		var dts = $(".according dt");
		dts.on('click', function() {
			var dd = $(this).next();
			var dds = dd.siblings('dd:visible');
			$(this).css("background-color", "#25C3E6");
			if (dd.hasClass('a')) {
				$(this).css("background-color", "#25629b");
				dds = dd;
			} else {
				dd.slideDown().addClass('a');
			}
			dds.slideUp(function() {
				$(".according dt").css("background-color", "#25629b");
				$(this).removeClass('a');
			})
		});
		$(window).on('resize',function(){
			$("#mainFrame").height($(window).height() -82);
		});
		$("#mainFrame").height($(window).height() -82);
	});
</script>
</html>