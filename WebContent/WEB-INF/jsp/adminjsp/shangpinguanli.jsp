<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=request.getContextPath()%>/">
<%@taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c" %>
<link rel="stylesheet" type="text/css" href="static/css/shangpinguanli.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<script src="static/js/jquery-1.12.4.js"></script>
<script>
	$(function() {
		$(".button2").on('click', function() {
			var s = [];
			$("input:checked").each(function() {
				var aa = $(this).attr("ss");
				s.push(aa);
			})
			$(".tupianid").val(s);
		});
		$('.cha').on('click', function() {
			$('.bj').css('display', 'none');
			$('.nr').css('display', 'none');
			$('.chexinxinr').css('display', 'none');
			$('.jiaoyijilubiao').html("");
			$('.tbody').html("");
		});
		$('.xinzeng').on('click',function(){
		var typeid=$(this).attr('typeid');
		var chepaihao=$('.chepaihao').val();
		if(chepaihao==""){
			alert("车牌号不能为空！！！");
			return;
		}
		$('.tbody').html("");
			$.ajax({
				type:'post',
				url:'xinzeng',
				data:{'typeid':typeid,'chepaihao':chepaihao},
				datatype:'json',
				cache:false,
				success:function(data){
					$.each($.parseJSON(data),function(i,item){
						var	carlicenseid=item.carlicenseid;	
						var carTypeName=item.carTypeName;
						var carZtName=item.carZtName;
						var td1=$("<td>"+carlicenseid+"</td>");
						var td2=$("<td>"+carTypeName+"</td>");
						var td3=$("<td>"+carZtName+"</td>");						
						var tr=$("<tr></tr>");	
						tr.append(td1);
						tr.append(td2);
						tr.append(td3);
						$('.tbody').append(tr);
					})
				}
				
			});	
		
			$('.chepaihao').val("");
		})
		$('.kucun').on('click',function(){
			$('.bj').css('display', 'block');
			$('.nr').css('display', 'block');
			var typeid=$(this).attr('typeid');
			$('.xinzeng').attr('typeid',typeid);	
			$.ajax({
				type:'post',
				url:'cheliangxinagqing',
				cache:false,
				data:{'typeid':typeid},
				datatype:"json",
				success:function(data){
					$.each($.parseJSON(data),function(i,item){						
						var	carlicenseid=item.carlicenseid;	
						var carTypeName=item.carTypeName;
						var carZtName=item.carZtName;	
						var td1=$("<td>"+carlicenseid+"</td>");
						var td2=$("<td>"+carTypeName+"</td>");
						var td3=$("<td>"+carZtName+"</td>");					
						var tr=$("<tr></tr>");	
						tr.append(td1);
						tr.append(td2);
						tr.append(td3);	
						$('.tbody').append(tr);
					});
				}
			});
			$('.zonqian').text("元");
		})
	$('.mingcheng').on('click',function(){
		var name=$(this).attr('name');
		$('.bj').css('display', 'block');
		$('.chexinxinr').css('display', 'block');
			$.ajax({
				type:'post',
				url:"chepeizhixinxi",
				data:{'name':name},
				cache:false,
				datatype:"json",
				success:function(data){
					$.each($.parseJSON(data),function(i,item){
						var td1=$("<td width='100px'>商品名称:</td><td width='100px'>"+item.carTypeName+"</td>");
						var td2=$("<td width='100px'>单价:</td><td width='100px'>"+item.carPrice+"</td>");
						var td3=$("<td>新品:</td><td>"+item.carXinche+"</td>");
						var td4=$("<td>车门数:</td><td>"+item.carDoorNum+"</td>");
						var td5=$("<td>车坐垫数:</td><td>"+item.carSeatNum+"</td>");
						var td6=$("<td>燃油类型:</td><td>"+item.carOil+"</td>");
						var td7=$("<td>变速箱:</td><td>"+item.carBsx+"</td>");
						var td8=$("<td>排量:</td><td>"+item.carPail+"</td>");
						var td9=$("<td>驱动方式:</td><td>"+item.carDrive+"</td>");
						var td10=$("<td>油箱容量:</td><td>"+item.carOilBox+"</td>");
						var td11=$("<td>GPS:</td><td>"+item.carGps+"</td>");
						var td12=$("<td>天窗:</td><td>"+item.carSkyWindow+"</td>");
						var td13=$("<td>倒车雷达:</td><td>"+item.carBackld+"</td>");
						var td14=$("<td>安全气囊:</td><td>"+item.carGasbag+"</td>");
						var tr1=$("<tr></tr>");
						var tr2=$("<tr></tr>");
						var tr3=$("<tr></tr>");
						var tr4=$("<tr></tr>");
						var tr5=$("<tr></tr>");
						var tr6=$("<tr></tr>");
						var tr7=$("<tr></tr>");
						tr1.append(td1);
						tr1.append(td2);
						tr2.append(td3);
						tr2.append(td4);
						tr3.append(td5);
						tr3.append(td6);
						tr4.append(td7);
						tr4.append(td8);
						tr5.append(td9);
						tr5.append(td10);
						tr6.append(td11);
						tr6.append(td12);
						tr7.append(td13);
						tr7.append(td14);
						$('.jiaoyijilubiao').append(tr1);
						$('.jiaoyijilubiao').append(tr2);
						$('.jiaoyijilubiao').append(tr3);
						$('.jiaoyijilubiao').append(tr4);
						$('.jiaoyijilubiao').append(tr5);
						$('.jiaoyijilubiao').append(tr6);
						$('.jiaoyijilubiao').append(tr7);
					});					
				}
			})
	})
	
	})
	function upimage(target){
		if(confirm("是否修改")){
		var	id=$(target).attr('s');
		var	chexing=$(target).attr('chexing');
			$(target).attr('href',"shangpingunali?id="+id+"&&chexing="+chexing+"");
		}
	}
</script>
<style>
.bj {
	background: black;
	position: absolute;
	width: 100%;
	height: 100%;
	top: 0;
	left: 0;
	opacity: 0.8;
	z-index: 2;
	display: none;
}
th {
	background: #f1f1f1;
	line-height: 2em;
}
table {
	border-collapse: collapse;
}
td {
	text-align: center;
}
th,td{
		padding:8px;
		font-size:12px;
   		font-family: "Microsoft YaHei";
        margin-right: 5px;
        border:1px solid #e2e2e2;
}
.anniu3 {
	background: red;
	border-radius: 4px;
	border: none;
	padding: 3px 10px;
	outline: none;
	color: white;
}
.kucun,.mingcheng{
color:#369CDA;
cursor: pointer;
}
.kucun{
font-weight: blod;
}
.nr,.chexinxinr{
	width: 600px;
	height: 355px;
	font-family: "Microsoft YaHei";
	position: absolute;
	border-radius: 2px;
	background: white;
	z-index: 3;
	box-shadow: 1px 1px 1px 1px #e2e2e2;
	margin: 130px 0 0 350px;
	display: none;
}
.chexinxinr{
	width: 529px;
    height: 293px;
}
.xinxinr,.chexinxi,.chepeizhixinxi{
	position: absolute;
	height:230px;
	margin-left: 30px;
	margin-top: 6px;
	box-shadow: 0 1px 3px #c3c3c3;
	overflow-y: auto;
}
.chepeizhixinxi{
    height: 231px;
}
.logo {
	border-bottom: 1px solid #e2e2e2;
	height: 42px;
	line-height: 42px;
	padding: 0 0 0 10px;
	background-color: #F8F8F8;
	font-family: "Microsoft YaHei";
	font-size: 14px;
}
.cha{
	float: right;
	cursor: pointer;
	padding-top: 8px;
	padding-right: 8px;
}
.chepaihao{
	border-radius: 2px;
    width: 104px;
    outline: none;
    box-shadow: 1px 1px 3px 1px #e2e2e2;
    border: none;
    padding: 8px;
    margin-left: 372px;
}
.tianjiache{
margin-top:10px;
margin-bottom:10px;
border-bottom:1px solid #D8C7C7;
height:40px;
}
.hdpnr {
	position: absolute;
	margin-left: 20px;
	margin-top: 70px;
	box-shadow: 0 1px 3px #c3c3c3;
	margin-right: 8px;
}
.xinzeng{
 padding: 4px 18px;
    border: none;
    background: rgb(54, 156, 218);
    border-radius: 4px;
    color: white;
    font-size: 12px;
    cursor: pointer;
    margin-left: 23px;
    }
    a{
    cursor: pointer;
    }
</style>
<body>
<div class="bj"></div>
	<form method="post">
	<div class="nr">
			<div class="logo">
				<span>车辆详情</span> <img class="cha" src="static/img/close.png" />
			</div>
			<div class="tianjiache">
				<input class="chepaihao" type="text" placeholder="请输入车牌" />
				<input class="xinzeng" type="button" value="新增"/>
				<!-- <label>总金额:</label>
			<span class="zonqian"></span> -->
			</div>
			<div class="xinxinr">
				<table class="jiaoyijilubiao">
				<thead>
					<tr>
						<th width="165px">车牌照</th>
						<th width="165px">车名称</th>
						<th width="165px">车状态</th>						
					</tr>
				</thead>
					<tbody class='tbody'>
									
					</tbody>							
				</table>
			</div>		
		</div>
		
		<div class="chexinxinr">	
		<div class="logo">
				<span>车辆详情</span> <img class="cha" src="static/img/close.png" />
			</div>					
			<div class="chepeizhixinxi">
				<table class="jiaoyijilubiao">				
											
				</table>
			</div>		
		</div>
	
		<div class="systemhdp">
			<span>商品管理</span>
			<div class="hdpanniu">
				<a href="chexinxi"><input class="button1" type="button"
					value="+添加" /></a> 
					<input class="button2" type="submit" value="-删除" />
				<input class="tupianid" type="text" name="tupianid" />
			</div>
			<div class="hdpnr">
				<table border="1">
					<tr>
					<th><input type="checkbox" /></th>
						<th width="240px">名称</th>						
						<th width="200px">价格</th>
						<th width="170px">新品</th>
						<th width="180px">车信息</th>
						<th width="150px">库存</th>
						<th width="200px">操作</th>
					</tr>
					<c:forEach var="shangpinglist" items="${shangpinglist}">
						<tr>
							<td><input style="border: none" type="checkbox" ss="${shangpinglist.carTypeId}"/></td>
							<td><a class="mingcheng" name="${shangpinglist.carTypeId}">${shangpinglist.carTypeName}</a></td>							
							<td>${shangpinglist.carPrice }</td>
							<c:choose>
							<c:when test="${shangpinglist.carXinche=='新车型'}">
							<td>		
							<a class="updataimage" chexing="${shangpinglist.carXinche}" s="${shangpinglist.carTypeId }"  onclick="upimage(this)">				
								<img src="static/img/images/yes.gif" chexing="${shangpinglist.carXinche}" s="${shangpinglist.carlicenseid }"/>
							</a>
							</td>
							</c:when>
							<c:otherwise>
							<td>
							<a class="updataimage" chexing="${shangpinglist.carXinche}" s="${shangpinglist.carTypeId }"  onclick="upimage(this)">
							<img src="static/img/images/no.gif" />
							</a>
							</td>
							</c:otherwise>
							</c:choose>
							<td>${shangpinglist.carXinxi }</td>
							<td><a class="kucun" typeid="${shangpinglist.carTypeId}" >${shangpinglist.carkucun}</a></td>
							<td><a href="chexinxi?id=${shangpinglist.carTypeId}&mc=${shangpinglist.carTypeName}"><input class="anniu3" type="button" value="修改" /></a></td>				
						</tr>											
					</c:forEach>
				</table>
			</div>
		</div>
	</form>
</body>
</html>