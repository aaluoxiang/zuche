<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=request.getContextPath()%>/">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script src="static/js/jquery-1.12.4.js"></script>
<link rel="stylesheet" href="static/css/default.css" />
<link rel="stylesheet" href="static/css/shangchuantupiananniu.css"/>
<script charset="utf-8" src="static/js/kindeditor-min.js"></script>
<script charset="utf-8" src="static/js/zh_CN.js"></script>
<script>
	//插件
	var editor;
	KindEditor.ready(function(K) {
		editor = K.create('textarea[name="content"]', {
			allowFileManager : true
		});
		$('.tijiaoanniu').on('click',function(){
			var miaoshu=editor.text();
			var miaosuid=$(this).attr('miaosuid');
			 $.ajax({
				type:"post",
				url:"tianjiamiaoshu",
				data:{'miaoshu':miaoshu,'miaosuid':miaosuid},
				cache:false,			
				success:function(data){
					if(data=='true'){
						alert("提交成功");
					}
					
				}	             	
			})   
		})
		K('input[name=getHtml]').click(function(e) {
			alert(editor.html());
		});
		K('input[name=isEmpty]').click(function(e) {
			alert(editor.isEmpty());
		});
		K('input[name=getText]').click(function(e) {
			alert(editor.text());
		});
		K('input[name=selectedHtml]').click(function(e) {
			alert(editor.selectedHtml());
		});
		K('input[name=setHtml]').click(function(e) {
			editor.html('<h3>Hello KindEditor</h3>');
		});
		K('input[name=setText]').click(function(e) {
			editor.text('<h3>Hello KindEditor</h3>');
		});
		K('input[name=insertHtml]').click(function(e) {
			editor.insertHtml('<strong>插入HTML</strong>');
		});
		K('input[name=appendHtml]').click(function(e) {
			editor.appendHtml('<strong>添加HTML</strong>');
		});
		K('input[name=clear]').click(function(e) {
			editor.html('');
		});
	});

	$(function() {
		$(".tab").on(
				'click',
				'.item',
				function() {
					var target = $(this);
					if (!target.hasClass('selected')) {
						var tab = this.dataset.tab;
						var content = $(".container .content[data-content="
								+ tab + "]");
						var selectedContent = content.siblings('.selected');
						target.addClass('selected').siblings('.selected')
								.toggleClass('selected');
						selectedContent.toggleClass('selected');
						content.toggleClass('selected');
					}
				});
		//删除图片
		$(".tupian").on('click',".shanchutupian",function(){
			var name=$(this).attr("name");
			var url=$(this).attr("url");
			$.ajax({
				type:"post",
				url:"deletetupian",
				cache:false,
				data:{"name":name,"url":url},
				success:function(data){
					$.each(JSON.parse(data),function(i,item){	
								var name=item.name;
								var url=item.url;
								var img=$("<img class='chetupian'  src='static/img/"+url+"' width='140px' height='100px'/>");
								var a=$("<a class='shanchutupian' url='"+url+"'  name='"+name+"' >删除</a>");
								$('.tupian').append(img);
								$('.tupian').append(a);
						})
					},	
			})
			$('.tupian').html("");
		})
		
		
		$('.anniu4').on('click',function(){
			 var name=$(this).attr('mz');
			 var formata=new FormData();
			 if($('.file1')[0].files[0]!=null)
			 formata.append("logoImg",$('.file1')[0].files[0]);		
			 formata.append("name",name);
			 $(".tupiankuang img").attr("src", "");
			 $(".tupian").html("");
				$.ajax({
					type:"post",
					url:"xiangcheshangchuan",
					cache:false,
					processData:false,
					contentType:false,
					data:formata,
					success:function(data){
					$.each(JSON.parse(data),function(i,item){	
						var img=$("<img style='padding-left:5px' src='static/img/"+item.url+"' width='100px' height='100px'/>");
						$(".tupian").append(img);
						})
					},		             
				error:function(error){  
		           alert('请选择图片！！！');  
				}
				}) 
			}); 	
	})
	function demo(file) {
		if (file.files && file.files[0]) {
			var reader = new FileReader();
			reader.onload = function(evt) {
				$(".tupiankuang img").attr("src", evt.target.result);
			}
			reader.readAsDataURL(file.files[0]);
		} else {
			$(".tupiankuang img").attr("src", evt.target.result);
		}
	}
</script>
<style>
body {
	margin: 0;
}
.shanchutupian{
    float: left;
    width: 140px;
    height: 30px;
    line-height: 30px;
    background: #00CCFF;
    text-align: center;
    color: #fff;
    margin-top: 101px;
    margin-left: -141px;
    text-decoration: none;
    cursor: pointer;
}
.systemhdp {
	position: relative;
	width: 100%;
	background: url(img/title_b.png) 10px 9px no-repeat;
	line-height: 40px;
	height: 40px;
	box-shadow: 0 1px 3px #c3c3c3;
	font-family: "Microsoft YaHei";
}
.systemhdp .zhuti {
	padding-left: 50px;
}

.tabs {
	width: 97%;
	bottom: 10%;
	margin-left: 20px;
	margin-top: 20px;
	right: 10%;
}

.tabs .tab .item, .tabs .tab .action {
	float: left;
	height: 27px;
	line-height: 32px;
	padding: 0 20px;
	text-align: center;
	background: #2196F3;
	color: #fff;
	border: 1px solid #369CDA;
	position: relative;
	margin-right: 4px;
	font-size: 12px;
}

.tabs .tab .action {
	float: right;
	border-radius: 5px 5px 0 0;
	bottom: -1px;
}

.tabs .tab .item:hover, .tabs .tab .action:hover {
	cursor: pointer;
}

.tabs .tab .item.selected {
	color: #3F51B5;
	background: #fff;
	border-bottom: 2px solid #fff;
}

.tabs .content {
	display: none;
}

.tabs .container .content.selected {
	display: block;
}

.tabs .container {
	border: 1px solid #D8D8D8;
	box-shadow: 2px 2px 1px 1px #e3e3e3;
	clear: both;
}

td {
	font-family: "Microsoft YaHei";
	font-size: 14px;
	padding-top: 20px;
	margin-left: 5px;
}

.var_1 {
	width: 250px;
	border-radius: 2px;
	outline: none;
	box-shadow: 1px 1px 3px 1px #e2e2e2;
	border: none;
	padding: 6px;
}

.tonyonxinxi .var_1 {
	width: 300px;
}

.anniu4, .anniu5, .tijiaoanniu {
	padding: 8px 36px;
    border-radius: 5px;
    outline: none;
    cursor: pointer;
    background: #FF3300;
    margin: 8px 0 0px 45px;
    color: white;
    font-size: 14px;
    box-shadow: 2px 1px 4px 1px #666;
    line-height: 35px;
    border: none;
}
#tptj{
box-shadow: none;
}
.tuptijiao{
       position: relative;
    left: 43%;
    margin-top: -36px;
}
.anniu5 {
	padding: 2px 49px;
}
.zidinyimiaoshu {
	margin-top: 1%;
	margin-bottom: 1%;
}

.tijiaoanniu {
	margin-left: 32px;
}

.tupian {
	border-top: 1px solid #CCCCCC;
	margin-top: 15px;
}

.xiangtonxinxi {
	margin-top: 4%;
	margin-left: 41%;
	margin-bottom: 3%;
}

.baseshuxing {
	margin-left: 100px;
	margin-bottom: 3%;
	margin-top: 1%;
}
.ycangid{
display:none;
width:0px;
}
.shuxing{
padding-left:44px;
}
.tupiankuang{
    position: relative;
    left: 40%;
    margin-bottom: 10px;
    margin-top: 15px;
}
.xuanzewenjian{
    position: relative;
    left: 38%;
}
.chetupian{
    float: left;  

  
    border: 1px solid #e3e3e3;
}
</style>
</head>

<body>
	<div class="systemhdp">
		<span class="zhuti">车信息管理</span>
	</div>
	<form method="post">
		<div class="tabs">
			<div class="tab">
				<div data-tab="1" class="item selected">通用信息</div>
				<div data-tab="2" class="item">详细描述</div>
				<div data-tab="5" class="item">商品相册</div>
			</div>
			<div class="container">
				<div data-content="1" class="content selected">
					<div class="tonyonxinxi" style="margin-left: 13%">
						<table>
						
							<tr>
								<td class="shuxing">商品名称:</td>
								<td><input class="var_1" type="text" name="shangpingname"
									value="${chexinxijihe}" /></td>
								<td class="shuxing">单价:</td>
								<td><input class="var_1" type="text" name="danjia"
									name="webpaixu" value="${carprice}" /></td>
							</tr>
							<tr>
								<td class="shuxing">新品:</td>
								<td><input class="var_1" name="shifouxinping" value="${carxinche}" />
										
								</td>
								<td class="shuxing">车门数:</td>
								<td><input value="${cardoornum}" class="var_1" type="text" name="door" id="bt" /></td>
							</tr>
							<tr>
								<td class="shuxing">车坐垫数:</td>
								<td><input value="${ carseatnum}" class="var_1" type="text" name="chezuodianshu"
									id="bt" /></td>
								<td class="shuxing">燃油类型:</td>
								<td><input value="${caroil}" class="var_1" type="text" name="rangyouleixing" /></td>
							</tr>
							<tr>
								<td class="shuxing">变速箱:</td>
								<td><input value="${carbsx}" class="var_1" type="text" name="biansuxiang" /></td>
								<td class="shuxing">排量:</td>
								<td><input value=" ${carpail}" class="var_1" type="text" name="pailiang" /></td>
							</tr>
							<tr>
								<td class="shuxing">驱动方式:</td>
								<td><input  value="${cardrive}" class="var_1" type="text" value="" name="qudon" /></td>
								<td class="shuxing">油箱容量:</td>
								<td><input value="${caroilbox}" class="var_1" type="text" 
									name="youxiangronliang" /></td>
							</tr>
							<tr>
								<td class="shuxing">GPS:</td>
								<td><input type="text" class="var_1" name="gps" value="${cargps}">
									</td>
								<td class="shuxing">天窗:</td>
								<td><input  type="text" class="var_1" name="tianchuang" value="${carskywindow}"/>
										</td>
							</tr>
							<tr>
								<td class="shuxing">倒车雷达:</td>
								<td><input type="text" class="var_1" name="leida" value="${carbackld}"/>
										</td>
								<td class="shuxing">安全气囊:</td>
								<td><input type="text" class="var_1" name="anquanqinang" value="${cargasbag}"/>
										</td>
								</td>
							</tr>
						</table> 					
					</div>
					<div class="xiangtonxinxi">		
						<input type="submit" class="anniu5" value="提交" />	
						<input value="${cartypeid}" class="ycangid" type="text" name="ycangid"/>
					</div>
				</div>
				<div data-content="2" class="content">
					<textarea name="content" class="miaoshu"
						style="width: 800px; height: 400px; visibility: hidden;">${miaoshu}</textarea>
					<div class="zidinyimiaoshu">
						<a type="button" class="tijiaoanniu" miaosuid="${cartypeid}">提交</a>
					</div>
				</div>
				<div data-content="5" class="content">
					<div class="tupiankuang">
						<img width="150px" height="150px" style="border-radius: 3px">
					</div>
					<div class="xuanzewenjian">
					<a href="" class="file">选择文件
					<input class="file1" type="file" name="file" onchange="demo(this)" />
					</a>
					</div>
					<div class="tuptijiao">
					<a type="button" class="anniu4" id="tptj" mz="${chem}">提交</a>
					</div>
					<div class="tupian">
						<c:forEach	var="xc" items="${xc}">
							<img class="chetupian"  src='static/img/${xc.url}' width='140px' height='100px'/>
							<a class="shanchutupian" url="${xc.url}"  name="${chem}" >删除</a>
						</c:forEach>
					</div>
				</div>
			</div>
		</div>
	</form>
</body>
</html>