<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<script type="text/javascript" src="static/js/jquery-1.12.4.js"></script>
<script type="text/javascript" src="static/js/highcharts.js"></script>
<script type="text/javascript"
	src="http://sandbox.runjs.cn/uploads/rs/146/73fi1rab/amcharts.js"></script>
<script type="text/javascript"
	src="static/js/WebApplication1/JS/jquery.js"></script>
<script type="text/javascript"
	src="static/js/WebApplication1/highcharts/highcharts.js"></script>
<script src="static/js/My97DatePicker/WdatePicker.js" type="text/javascript" charset="utf-8"></script>
<script>
var ss; 		
var legend;
var Data = ${xiaoliang};

AmCharts.ready(function() {
    // 饼图
    ss = new AmCharts.AmPieChart();
    ss.dataProvider = Data;
    ss.titleField = "cartypename";
    ss.valueField = "num";
    ss.outlineColor = "";
    ss.outlineAlpha = 0.8;
    ss.outlineThickness = 2;
    // 3D
    ss.depth3D = 20;
    ss.angle = 30;
    // 图形写入
    ss.write("chartdiv");
}); 
var chart;
var chart1;
$(document).ready(function () {
	chart1 = new Highcharts.Chart({
        chart: {
            renderTo: 'container1',
            type: 'line',
            marginRight: 220,
            marginBottom: 25
        },
        title: {
            text: '收益曲线图表',
            x: -70 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
            categories:${s}         
        },
        yAxis: {
            title: {
                text: '数据'
            },
            plotLines: [{
                value: 0,
                width: 2,
                color: '#808080'
            }]
        },
        tooltip:{
            formatter: function () {
                return '<b>' + this.series.name + '</b><br/><br/>' +
			this.x + ': ' + this.y + '元';
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -20,
            y: 200,
            borderWidth: 0
        },
        series:${aa}
    });
	

chart = new Highcharts.Chart({
    chart: {
        renderTo: 'container2',
        type: 'column'
    },
    title: {
        text: '订单柱形图表'
    },
    subtitle: {
        text: ''
    },
    xAxis: {
       categories: [
	'2017年'
]
    },
    yAxis: {
        min: 0,
        title: {
            text: '数据'
        }
    },
    legend: {
        layout: 'vertical',
        backgroundColor: '#FFFFFF',
        align: 'left',
        verticalAlign: 'top',
        x: 50,
        y: 5,
        floating:true,
        shadow: true
    },
    tooltip: {
        formatter: function () {
        	   return '' +
				this.x + ': 数量' + this.y;
        }
    },
    plotOptions: {
        column: {
            pointPadding: 0.3,
            borderWidth: 0
        }
    },
    series:${dindanshuliang}
});
});
$(function(){
	$(".chaxun").on("click",function(){
		var suoyou=[];
		var zon=[];
		var begin=$(".begin").val();
		var down=$(".down").val();
		
		var chart;
		chart= new Highcharts.Chart({								
		    chart: {
		        renderTo: 'container2',
		        type: 'column'
		    },
		    title: {
		        text: '订单柱形图表'
		    },
		    subtitle: {
		        text: ''
		    },
		    xAxis: {
		       categories: [
			'2017年'
		]
		    },
		    yAxis: {
		        min: 0,
		        title: {
		            text: '数据'
		        }
		    },
		    legend: {
		        layout: 'vertical',
		        backgroundColor: '#FFFFFF',
		        align: 'left',
		        verticalAlign: 'top',
		        x: 50,
		        y: 5,
		        floating:true,
		        shadow: true
		    },
		    tooltip: {
		        formatter: function () {
		        	   return '' +
						this.x + ': 数量' + this.y;
		        }
		    },
		    plotOptions: {
		        column: {
		            pointPadding: 0.3,
		            borderWidth: 0
		        }
		    }, 
		    series:[{
		    	name:'已完成',
		    	data:[]
		    },{
		    	name:'已退单',
		    	data:[]
		    },{
		    	name:'待付款',
		    	data:[]
		    },{
		    	name:'已付款',
		    	data:[]
		    },{
		    	name:'租赁中',
		    	data:[]
		    },{
		    	name:'还车审核',
		    	data:[]
		    },{
		    	name:'退款中',
		    	data:[]
		    }]
		});
		$.ajax({
			type:"post",
			url:"dindantongjichaxun",
			cache:false,
			data:{"begin":begin,"down":down},
			datatype:"json",
			success:function(data){				
				$.each(JSON.parse(data),function(i,item){					
					var name=item.name;
					var num=item.num;				
					/* var s="{name:'"+name+"',data:["+num+"]}";
					suoyou.push(s);	 */				
					chart.series[i].setData([parseInt(num)]);
				})		
			}
		});		
	})	
})
</script>
<style>
.systemhdp {
	position: relative;
	width: 100%;
	background: url(static/img/title_b.png) 10px 9px no-repeat;
	line-height: 40px;
	height: 40px;
	box-shadow: 0 1px 3px #c3c3c3;
	font-family: "Microsoft YaHei";
}
span {
	padding-left: 50px;
}
body {
	background: white;
}

#chartdiv {
	width: 50%;
	height: 500px;
	position: absolute;
	right: 1%;
	top: -3%;
}

#dindan {
	width: 530px;
	height: 500px;
	position: absolute;
	top:-3%;
	left:2%;
}

.biaoti {
	float: right;
	margin-right: 272px;
	margin-top: -25px;
	font-size: 20px;
	color: red;
}

#container2 {
	width: 40%;
	height: 300px;
	float: left;
	margin-left: 20px;
	margin-top: 23px;
	margin-right: 20px;
}

#container1 {
	margin-left: 100px;
	width: 90%;
	height: 300px;
}
.rq {
	margin-top: 33%;
	margin-left: 32%;
	font-size: 12px;
}
a {
	text-decoration: none;
	color: white;
	cursor: pointer;
	padding: 1px 8px;
	background: #B2A220;
	border-radius: 2px;
}
#top{
    height: 45px;
    margin-top: 13px;
    margin-left: 14px;
}
.down,.begin{	
	box-shadow: 1px 1px 3px 1px #e2e2e2;
    font-size: 14px;
    border: 1px solid #D9D9D9;
    outline: none; 
    padding:7px;
}
.chaxun{
    padding: 8px 30px;
    border: none;
    background: rgb(54, 156, 218);
    border-radius: 4px;
    color: white;
    font-size: 14px;   
}
</style>
<script>	
</script>
<body>
	<form method="post">
		<div class="systemhdp">
			<span>统计管理</span>
		</div>
	</form>	
	<div id="top">
	<input type="text" class="begin" placeholder="起始时间" onfocus="WdatePicker({})" />
	<input type="text" class="down" placeholder="结束时间" onfocus="WdatePicker({})" />
	<input type="button" class="chaxun" value="查询"/>
	</div>
	<div class="bs">
	<div id="container2"></div>
	</div>
	<br />
	<div class="biaoti">
		<label>租车销量图</label>
	</div>
	<div id="chartdiv"></div>
	<div>
		<div class="rq">
			<a href="tongjimokuai?name=年度">年度</a> <a href="tongjimokuai?name=月度">月度</a>
		</div>
		<div id="container1"></div>
	</div>
</body>
</html>