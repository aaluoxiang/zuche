<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=request.getContextPath()%>/">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>我的会员等级</title>
<link rel="stylesheet" type="text/css" href="static/css/center.css">
<link rel="stylesheet" type="text/css" href="static/css/myinfor.css">
<link rel="stylesheet" type="text/css"
	href="static/css/owncentermylevel.css">
</head>
<body>
	<jsp:include page="header-inc.jsp" flush="true"></jsp:include>
	<jsp:include page="tou.jsp" flush="true" />
	<div class="a_acon b">
		<jsp:include page="menu.jsp" flush="true" />
		<div class="container">
			<div data-nr="1" class="nr mymessage fl selected">
				<h2>会员等级</h2>
				<div class="mymessageform" style="border-top: 1px solid #e9ebee">
					<div class="mylevelbox">
						<c:if test="${userType == '普卡会员' }">
						<span class="ordinary_levelcard"></span>
						</c:if>
						<c:if test="${userType == '银卡会员' }">
						<span class="yin_levelcard"></span>
						</c:if>
						<c:if test="${userType == '黄金会员' }">
						<span class="gold_levelcard"></span>
						</c:if>
						<c:if test="${userType == '白金会员' }">
						<span class="wgold_levelcard"></span>
						</c:if>
						<c:if test="${userType == '钻石会员' }">
						<span class="diamond_levelcard"></span>
						</c:if>
					</div>
					<div class="explain">
						<h3>会员分级</h3>
						<p>韬睿租车会员级别分为5个等级，分别为：普卡会员、银卡会员、黄金会员、白金会员、钻石会员。会员级别的调整均由神州租车根据会员累计积分自动处理，无需会员申请。</p>
						<p>会员级别由累计积分决定，累计积分越高会员等级越高，享受到的会员权益越大。</p>
						<h4>会员级别规则如下：</h4>
						<table width="100%" class="wztable">
							<tbody>
								<tr>
									<th width="105">项目</th>
									<th width="105">普卡会员</th>
									<th width="140">银卡会员</th>
									<th width="140">黄金会员</th>
									<th width="140">白金会员</th>
									<th>钻石会员</th>
								</tr>
								<tr>
									<td class="gray">加入方式</td>
									<td>免费加入</td>
									<td colspan="4">累计积分升级</td>
								</tr>
								<tr>
									<td class="gray">有效期</td>
									<td colspan="5">永久有效</td>
								</tr>
								<tr>
									<td class="gray">升级条件</td>
									<!-- <td>无</td>
									<td>累计满xx分</td>
									<td>累计满xx分</td>
									<td>累计满xx分</td>
									<td>累计满xx分</td> -->
									<c:forEach items="${usertypes }" var="usertype">
										<td>累计满${usertype.usertypeupjf }分</td>
									</c:forEach>
								</tr>
								<tr>
									<td class="gray">升级方式</td>
									<td>无</td>
									<td colspan="4">会员有效期内，满足升级标准即可自动升级，升级后会员永久有效</td>
								</tr>
								<tr>
									<td class="gray">会员折扣</td>
									<td>无</td>
									<c:forEach items="${usertypes }" begin="1" var="usertype">
										<td>${usertype.usertypecut*10 }折</td>
									</c:forEach>
								</tr>
							</tbody>
						</table>
						<h3>累计积分</h3>
						<p>累计积分是神州租车会员，根据会员级别、订单类型，按有效消费金额的一定比例所获得的，累计积分决定会员级别。会员级别越高，积分返比越大。</p>
						<h5>1、用途：	</h5>
						<p class="pl20">会员升级。升级时不抵扣对应分数，会员级别到期时抵扣。 </p>
						<h5>2、有效期：	</h5>
						<p class="pl20">长期有效，订单完成后计入会员名下  </p>
						<h5>3、获取方式：	</h5>
						<p class="pl20">积分=订单消费金额  </p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="footer-inc.jsp" flush="true"></jsp:include>
</body>
</html>