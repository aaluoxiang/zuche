<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="<%request.getContextPath();%>static/css/actcenter.css"/>
		<title>活动中心</title>
		
	</head>
	<body>
	<jsp:include page="header-inc.jsp"></jsp:include>
		<div class="fav-list">
			<dl id="yhhdlist">
				<dd class="top-item">
					<div class="item-vessel">
						<img class="fav-img" src="static/img/20170502wanliang69_pclist.jpg" alt="领取优惠卷活动"/>
						<h3 class="fav-title">万辆新车来袭 69元起租</h3>
						<p class="fav-con">神州租车万辆新车来袭，低至69元起租，适用于全国直营门店（北京、伊犁除外），万辆新车雪佛兰科鲁兹、丰田致炫、起亚K2、别克英朗、大众...</p>
						<div class="check-detail">
							<a class="detail-btn" href="actfree">查看活动</a>
						</div>
					</div>
				</dd>
				<dd class="top-item">
					<div class="item-vessel">
						<img class="fav-img" src="static/img/20170421wuyigengxin_pclist.jpg" alt="领取优惠卷活动"/>
						<h3 class="fav-title">五一五一 租五免一</h3>
						<p class="fav-con">神州租车五一租车特惠来袭，五一五一租五免一，即日起至5月1日，租期在5天及以上的五一节日租订单，即可享受租5天免1天的优惠活动。劳动节...</p>
						<div class="check-detail">
							<a class="detail-btn" href="getyhq">查看活动</a>
						</div>
					</div>
				</dd>
				<dd class="top-item">
					<div class="item-vessel">
						<img class="fav-img" src="static/img/20170220Newmember_pc.jpg" alt="领取优惠卷活动"/>
						<h3 class="fav-title">新用户专享，150元见面礼！</h3>
						<p class="fav-con">神州租车新用户注册赢豪礼！150元新用户专享租车代金券等你来拿，6月30日前，新用户注册即送100元电子代金券，首次租车结算后再送50元电子...</p>
						<div class="check-detail">
							<a class="detail-btn" href="actnew">查看活动</a>
						</div>
					</div>
				</dd>
			</dl>
		</div>
		<jsp:include page="footer-inc.jsp"></jsp:include>
	</body>
</html>
