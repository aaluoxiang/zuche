<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>修改密码</title>

<script src="static/js/jquery-1.12.4.js" type="text/javascript" charset="utf-8"></script>
<script src="static/js/owncentererrorclose.js" type="text/javascript" charset="utf-8"></script>

<link rel="stylesheet" type="text/css" href="static/css/center.css">
<link rel="stylesheet" type="text/css" href="static/css/owncentererror.css">
<link rel="stylesheet" type="text/css" href="static/css/myinfor.css">

</head>
<body>
	<!-- <div style="height: 100px;">页眉部分用include标签导入页面相同元素</div> -->
	<jsp:include page="header-inc.jsp" flush="true"></jsp:include>
	<jsp:include page="tou.jsp" flush="true"/>
	<div class="a_acon b">
		<jsp:include page="menu.jsp" flush="true"/>
		<div class="container">
			<div class="mymessage fl">
				<h2>修改密码</h2>
				<div class="mymessageform">
					<form method="post">
						<ul>
							<li>
								<div class="zjerrordiv" >
									<c:if test="${error == '输入旧密码有误' }">
										<div class="errorbox" style="top: -40px;">
											<span class="xxbox">◆</span>
											<span class="errorimg"></span>
											<span class="errorinfor">输入旧密码有误！</span>
											<span class="errorclose"></span>
										</div>
									</c:if>
									<c:if test="${error == '新密码不能为空' }">
										<div class="errorbox" style="top: 20px;">
											<span class="xxbox">◆</span>
											<span class="errorimg"></span>
											<span class="errorinfor">新密码不能为空！</span>
											<span class="errorclose"></span>
										</div>
									</c:if>
									<c:if test="${error == '两次密码不一致' }">
										<div class="errorbox" style="top: 80px;">
											<span class="xxbox">◆</span>
											<span class="errorimg"></span>
											<span class="errorinfor">两次密码不一致！</span>
											<span class="errorclose"></span>
										</div>
									</c:if>
								</div>
								<label class="fl">原密码</label>
								<span class="namespan fl">
									<input type="password" name="oldpassword" value="${oldpassword }"/>
								</span>
								<div class="fr">请输入原来的密码</div>
							</li>
							<li>
								<label class="fl">新密码</label>
								<span class="namespan fl">
									<input type="password" name="newpassword" value="${newpassword }"/>
								</span>
								<div class="fr">请牢记新密码</div>
							</li>
							<li>
								<label class="fl">确认密码</label>
								<span class="namespan fl">
									<input type="password" name="passwordagain" value="${passwordagain }"/>
								</span>
								<div class="fr">再次输入新密码</div>
							</li>
						</ul>
						<div class = "savebtn">
							<button>保存</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
		<jsp:include page="footer-inc.jsp" flush="true"></jsp:include>
		<c:if test="${updatemessage=='true' }">
			<script src="static/js/owncenterupdatemessage.js" type="text/javascript" charset="utf-8"></script>
		</c:if>
</body>
</html>