<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html >
<html>
<head>
<base href="<%=request.getContextPath()%>/">
<link rel="stylesheet" type="text/css"
	href="static/css/ordercomment.css">
<link rel="stylesheet" type="text/css"
	href="static/css/owncenterordererror.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>评论</title>
</head>
<body bgcolor="#f2f3f5">
	<jsp:include page="header-inc.jsp" flush="true"></jsp:include>
	<c:if test="${plok!=null }">
		<div class="error">
			<div class="errorinfo">
				<div style="padding-top: 64px;">
					${plok }
					<a href="myorder" class="tiaozhuan">返回我的订单></a>
				</div>
			</div>
		</div>
	</c:if>
	<div class="comment-main">

		<div>
			<div class="carimg-pic">
				<img src="${cartype.carImageUrl }" width="494px"/>
			</div>
			<div class="order-info">
				<h2>${cartype.carTypeName }${cartype.carXinxi }</h2>
				<div class="conmentinfo">
					<form method="post">
						评论级别： <select name="pldj">
							<option>请选择</option>
							<option>1</option>
							<option>2</option>
							<option>3</option>
							<option>4</option>
							<option>5</option>
						</select>
						<textarea rows="10" cols="60" name="plcon"></textarea>
						<input type="hidden" name="cartypename"
							value="${cartype.carTypeName }"> <input type="hidden"
							name="orderid" value="${userorder.orderid }"> <input
							type="hidden" name="userid" value="${userid }">
						<div style="text-align: center;">
							<button>提交</button>
						</div>
					</form>
				</div>

			</div>
		</div>
	</div>
	<jsp:include page="footer-inc.jsp" flush="true"></jsp:include>
</body>
</html>