<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=request.getContextPath()%>/">
<meta charset="UTF-8">
<title>我的联系人</title>

<script src="static/js/jquery-1.12.4.js" type="text/javascript"
	charset="utf-8"></script>
<script src="static/js/owncentererrorclose.js" type="text/javascript"
	charset="utf-8"></script>

<link rel="stylesheet" type="text/css" href="static/css/center.css">
<link rel="stylesheet" type="text/css" href="static/css/myinfor.css">
<link rel="stylesheet" type="text/css"
	href="static/css/owncentererror.css">
<link rel="stylesheet" type="text/css"
	href="static/css/owncentermyatten.css">
<link rel="stylesheet" type="text/css"
	href="static/css/owncentertable.css">
<!-- <link rel="stylesheet" type="text/css" href="static/css/head.css">
<link rel="stylesheet" type="text/css" href="static/css/foot.css"> -->
<script>
	function canSUbmit() {
		if (confirm("确认删除该联系人？")) {
			return true;
		}
		return false;
	}
</script>
</head>

<body bgcolor="#f2f3f5">
	<jsp:include page="header-inc.jsp" flush="true"></jsp:include>
	<jsp:include page="tou.jsp" flush="true" />
	<div class="a_acon b">
		<jsp:include page="menu.jsp" flush="true" />
		<div class="container">
			<div data-nr="1" class="nr mymessage fl selected">
				<h2>我的联系人</h2>
				<div class="mymessageform">
					<form method="post">
						<ul>
							<li>
								<div class="zjerrordiv">
									<c:if test="${error=='姓名不能为空' }">
										<div class="errorbox" style="top: -40px;">
											<span class="xxbox">◆</span> <span class="errorimg"></span> <span
												class="errorinfor">姓名不能为空</span> <span class="errorclose"></span>
										</div>
									</c:if>
									<c:if test="${error=='错误身份证' }">
										<div class="errorbox" style="top: 20px;">
											<span class="xxbox">◆</span> <span class="errorimg"></span> <span
												class="errorinfor">错误身份证</span> <span class="errorclose"></span>
										</div>
									</c:if>
									<c:if test="${error=='错误电话' }">
										<div class="errorbox" style="top: 80px;">
											<span class="xxbox">◆</span> <span class="errorimg"></span> <span
												class="errorinfor">错误电话</span> <span class="errorclose"></span>
										</div>
									</c:if>
								</div> <label class="fl">姓名</label> <span class="namespan fl">
									<input type="text" name="lxrname" value="" />
							</span>
								<div class="fr">真实姓名，方便租车时核对身份</div>
							</li>
							<li><label class="fl">身份证号码：</label> <span
								class="namespan fl"> <input type="text" name="lxrsfz"
									value="" />
							</span>
								<div class="fr">有效证件，方便租车时核对身份</div></li>
							<li><label class="fl">电话</label> <span class="namespan fl">
									<input type="text" name="lxrtel" class="tel" value="" />
							</span>
								<div class="fr">有效电话，方便派车时及时联系</div></li>
							<li><label class="fl">e-mail</label> <span
								class="namespan fl"> <input type="text" name="e_mail"
									class="e_mail" value="" />
							</span>
								<div class="fr">有效邮件</div></li>
						</ul>
						<div class="savebtn">
							<input type="hidden" name="formtype" value="tianjia">
							<button>添加</button>
						</div>
					</form>

				</div>
				<h2>联系人列表</h2>
				<div class="mymessageform">
					<table>
						<thead>
							<tr>
								<th>联系人姓名</th>
								<th>联系人证件</th>
								<th>联系人电话</th>
								<th>联系人邮箱</th>
								<th>操作</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${lxrs }" var="lxr">
								<tr>
									<td>${lxr.lxrname }</td>
									<td>${lxr.lxrsfz }</td>
									<td>${lxr.lxrphone }</td>
									<td>
										<c:choose>
											<c:when test="${lxr.lxremail=='null' || empty lxr.lxremail}">
												无
											</c:when>
											<c:otherwise>
												${lxr.lxremail}
											</c:otherwise>
										</c:choose> 
									</td>
									<td>
										<form method="post" onsubmit="return canSUbmit()">
											<input type="hidden" name="lxrid" value="${lxr.lxrid }">
											<input type="hidden" name="formtype" value="shanchu">
											<button>删除</button>
										</form>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="footer-inc.jsp" flush="true"></jsp:include>
	<%-- <c:if test="${updatemessage=='true' }">
		<script src="static/js/owncenterupdatemessage.js" type="text/javascript" charset="utf-8"></script>
	</c:if> --%>
</body>
</html>