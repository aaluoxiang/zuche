<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<base href="<%request.getContextPath();%>" >
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>login</title>
<link rel="stylesheet"type="text/css" href="static/css/login.css">
<link rel="stylesheet" type="text/css" href="static/css/error.css"/>
<link rel="stylesheet" type="text/css" href="static/css/sign.css"/>
<!-- <script src="static/js/jquery.min.js"></script> -->
<script type="text/javascript" src="static/js/jquery-1.12.4.js" ></script>
<script type="text/javascript" src="static/js/error-tip.js" ></script>
<script type="text/javascript" src="static/js/login.js" ></script>
  <script>
	    $(function() {
	    	${error_m};
	    	${error_w};
	    	${error_code};
	    	${error_code_emp};
	       $('#login #password').focus(function() {
	            $('#owl-login').addClass('password');
	        }).blur(function() {
	            $('#owl-login').removeClass('password');
	            window.setTimeout (function(){$('#email').focus();},0);
	        }); 
				
	    });
    </script>
</head>
<body>
	<div class="zc_head_bd">
			<div class="zc_main">
				<a href="main" class="zc_logo" alt="租车公司"></a>
				<div class="zc_font">
					欢迎登录
				</div>
			</div>
			<!--head end-->
		</div>
<!-- begin -->
<div id="login" style="min-width: 1300px;position: relative;">
    <div class="wrapper" style="min-width: 1300px;position: relative;    margin: 0 auto;">
        <div class="login" style="min-width: 1200px;position: relative;margin: 0 auto;">
        	
            <form method="post" class="container offset1 loginform">
                <!-- <div id="owl-login">
                    <div class="hand"></div>
                    <div class="hand hand-r"></div>
                    <div class="arms">
                        <div class="arm"></div>
                        <div class="arm arm-r"></div>
                    </div>
                </div> -->
                <div class="pad"style=" padding-bottom: 55px;    padding-top: 66px;">
                	<div class="error-tip">
					</div>
					<span style="display:block;border:0px solid red;width:400px;height:55px;line-height: 50px;margin-top: -66px;margin-left: -31px;font-size: 18px;text-align: center;letter-spacing: 10px;">登录</span>
                   <!--  <input type="hidden" name="_csrf" value="9IAtUxV2CatyxHiK2LxzOsT6wtBE6h8BpzOmk="> -->
                    <div class="control-group">
                        <div class="controls">
                            <label for="email" class="control-label fa fa-envelope"></label>
                            <input id="email" type="number" name="email" placeholder="请输入手机号" value="${param.email}" maxlength="11" tabindex="1" autofocus="autofocus" class="form-control input-medium email">
                       		<label>
								<span class="whitew" style="background: white;width: 68px;height: 35px;display: inline-block;margin-left: 256px;margin-top: -11px;border-radius: 10px;"></span>
							</label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="controls">
                            <label for="password" class="control-label fa fa-asterisk"></label>
                            <input id="password" type="password" name="password" placeholder="请输入密码"value="${param.password}"  tabindex="2" class="form-control input-medium password">
                        </div>
                    </div>
                    <div class="yanzheng">
                    <!-- <label for="email" class="control-label fa fa-envelope"></label> -->
                    	<input type="text " name="logincode" id="logincode"  placeholder="请输入验证码 " maxlength="4" style="clear:both;float:left;text-indent:0px;padding:10px 8px 10 0px;width:170px;border-radius:4px;border:1px solid #cccccc;"/>
								<div class="zc-imgcode ">
								<img onclick="this.src='captcha?r'+Date.now()" src="captcha" alt="验证码">
								</div> 
                    </div>
                </div>
                <div class="form-actions" style="padding-top: 10px; padding-bottom: 10px;">
                	<a href="sign" tabindex="5" class="btn pull-left btn-link text-muted">立即注册账号</a>
                    <button type="submit" tabindex="4" class="btn btn-primary">&nbsp;登&nbsp;&nbsp;录&nbsp;</button>
                </div>
                
            </form>
           <!-- <div class="imgs" style="width: 1300px;height: 800px;border:1px solid red;background:url(images/4.jpg);position: relative;">
                	
            </div>-->
        </div>
    </div>
</div>
<!-- end -->
</body>
</html>