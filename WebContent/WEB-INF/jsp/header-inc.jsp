<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    <style type="text/css">
    	.cwb {
    		letter-spacing: 3px;
    		font-size: 14px;
    		font-weight: bold;
            animation-iteration-count: infinite;
            animation-name: bluePulse;
            animation-duration: 3s;
        }
		/* @keyframes bluePulse{
            from {
                 color: #fff; 
                 color: #FFEB3B;
                text-shadow: 0 0 10px #ebebeb;
                margin-left: 0px;
            }
            50% {
                color: #FFEB3B; 
                text-shadow: 0 0 28px #2daebf;
                margin-left: 20px;
            }
            to {
                 color: #fff; 
                 color: #FFEB3B;
                text-shadow: 0 0 10px #ebebeb;
                margin-left: 00px;
            }
        } */

    </style>
<link rel="stylesheet" href="<%request.getContextPath();%>static/css/head.css" type='text/css'/>
		<div class="head">
			<div class="head_m">
				<div class="head_lf">
					尊敬的客户，欢迎来到：<span class="cwb">神州租车</span>
				</div>
				<div class="head_rg login-no">
					<c:if test="${userName!=null }">
						<a href="owncenter">欢迎您,${userName}</a>
						|<a href="logout" onclick="if(confirm('确定退出?')==false)return false;">退出</a>
					</c:if>
					<c:if test="${userName==null }">
						<a href="login">登录</a>
						|<a href="sign">注册</a>
					</c:if>
						|<a href="fltk">租车条款</a>
						|<a href="aboutus">关于我们</a>
				</div>
			</div>
		</div>
		<div class="nav">
			<div class="nav_m">
				<div class="nav_logo">
					<a href="main"><img src="static/img/logo1.png" /></a>
				</div>
					<ul class="nav_ul">
						<li><a href="main">首页</a></li>
						<li><a href="mainjump">快速租车</a></li>
						<li><a href="actcenter">活动中心</a></li>
						<li><a href="helpcenter">帮助中心</a></li>
					</ul>
			</div>
		</div>
		

