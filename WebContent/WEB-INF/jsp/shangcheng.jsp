<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=request.getContextPath()%>/">
<script src="static/js/jquery-1.12.4.js" type="text/javascript" charset="utf-8"></script>
<script src="static/js/choose.js" type="text/javascript" charset="utf-8"></script>

<script type="text/javascript" src="static/js/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="http://api.map.baidu.com/api?v=1.3"></script>
<link rel="stylesheet" href="static/css/xuanche.css" type="text/css">
<link rel="stylesheet" type="text/css" href="static/css/error.css"/>
<link rel="stylesheet" type="text/css" href="static/css/logincar.css"/>
<script type="text/javascript" src="static/js/error-tip.js" ></script>
<script type="text/javascript" src="static/js/login.js" ></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>选车</title>
<script>
function dateDiff(date1, date2) {
    var type1 = typeof date1, type2 = typeof date2;
    if (type1 == 'string')
        date1 = stringToTime(date1);
    else if (date1.getTime)
        date1 = date1.getTime();
    if (type2 == 'string')
        date2 = stringToTime(date2);
    else if (date2.getTime)
        date2 = date2.getTime();
    //alert((date1 - date2) / (1000*60*60)); 
    return (date1 - date2) / (1000 * 60 * 60 * 24); //结果是小时 
}
//字符串转成Time(dateDiff)所需方法 
function stringToTime(string) {
    var f = string.split(' ', 2);
    var d = (f[0] ? f[0] : '').split('-', 3);
    var t = (f[1] ? f[1] : '').split(':', 3);
    return (new Date(
   parseInt(d[0], 10) || null,
   (parseInt(d[1], 10) || 1) - 1,
    parseInt(d[2], 10) || null,
    parseInt(t[0], 10) || null,
    parseInt(t[1], 10) || null,
    parseInt(t[2], 10) || null
    )).getTime();
}
	function jsshijian(){
		var senttime1=$(".senttime_2").val();
		var backtime1=$(".backtime_2").val();
		var senttime2=$(".senttime_3").val();
		var backtime2=$(".backtime_3").val();
		
		console.log(senttime2);
		console.log(backtime2);
		console.log(backtime2-senttime2);
		$(".zuqi_3").text(dateDiff(backtime1,senttime1));
	}

$(function(){
	${error_denglu};
	${error_fu};
	${error_m};
	${error_w};
	${error_m_emp};
	${error_w_emp};
	${error_code};
	${error_code_emp};
	
	$(".zhanshi_3").on('click',".tt",function(){	
		if(${userId==null}){
			$(".denglu").css("display","block");
			$(".fu").css("display","block");
		}else{
		var name=$(this).attr("name");
		var quche1=$('.quche_2').val();
		var quche2=$(".quche_4").val();
		
		var huanche1=$(".huanche_2").val();
		var huanche2=$(".huanche_4").val();
		
		
		var senttime1=$(".senttime_2").val();
		var senttime2=$(".senttime_3 option:selected").text();
		
		
		var backtime1=$(".backtime_2").val();
		var backtime2=$(".backtime_3 option:selected").text();
		var zuqi=$(".zuqi_3").text();
		
		
		$(this).attr("href","dingdan?name="+name+"&quche1="+quche1+"&huanche1="+huanche1+"&senttime1="+senttime1+"&backtime1="+backtime1+
				"&quche2="+quche2+"&huanche2="+huanche2+"&senttime2="+senttime2+"&backtime2="+backtime2+
				"&zuqi="+zuqi+"");
				
		}
	});
	$(".zhanshi_3").on('click', ".btn", function(){
		/* 有改动，将"null"改为"" */
	
		if(${userId==null}){
			$(".denglu").css("display","block");
			$(".fu").css("display","block");
		}else{
		var name=$(this).attr("name");
		var tupian=$(this).attr("tupian");
		
		var quche1=$('.quche_2').val();
		var quche2=$(".quche_4").val();
		
		
		var huanche1=$(".huanche_2").val();
		var huanche2=$(".huanche_4").val();
	
		
		var senttime1=$(".senttime_2").val();
		var senttime2=$(".senttime_3 option:selected").text();
		
		
		var backtime1=$(".backtime_2").val();
		var backtime2=$(".backtime_3 option:selected").text();
		var zuqi=$(".zuqi_3").text();
		
		
		$(this).attr("href","dingdan?name="+name+"&quche1="+quche1+"&huanche1="+huanche1+"&senttime1="+senttime1+"&backtime1="+backtime1+
				"&quche2="+quche2+"&huanche2="+huanche2+"&senttime2="+senttime2+"&backtime2="+backtime2+
				"&zuqi="+zuqi+"");
				
		}
	});
	
	$(".m").click(function(){
	var chexing="";
	var jiage="";
	var pinpai="";
	var paixu="";//定义一个传递数据的变量  
			if ($(this).hasClass("chexing_3")) {
				 $(this).siblings().removeClass('selected');;
				 $(this).addClass('selected'); 
			} 
			if($(this).hasClass("jiage_3")){
				 $(this).siblings().removeClass('selected');  // 删除其他兄弟元素的样式
				 $(this).addClass('selected'); 
			}
			if($(this).parent().hasClass("pinpai_3")){
				 $(this).siblings().removeClass('selected');  // 删除其他兄弟元素的样式
				 $(this).addClass('selected'); 
			}
			
	      	chexing=$(".chexing_2").children(".selected").attr("name"); 
	      	
	   
	      	jiage=$(".jiage_3").parent().children(".selected").attr("value");

	      	pinpai=$(".pinpai_3").children(".selected").attr("pinpais");

	      	paixu=$(this).attr("values");


	$.ajax({
		type:"post",
		dataType:"json",
		data:{chexing:chexing,jiage:jiage,pinpai:pinpai,paixu:paixu},	
		url:"shangcheng",
		async:true,
		error:function(){
			alert("数据加载失败",0);
		},
		success:function(data){
			$(".zhanshi_3").html('');
			if(data.length<=0){	
				$('<tr></tr>').addClass("yinying").append($('<td></td>').addClass("zhanshi_6").text('您搜索的商品不存在！！！')).appendTo(".zhanshi_3");
			}
			
			$.each(data,function(i,item){
				
				var tupian=item.carImageUrl;
				var carname=item.carTypeName;
				var carxinxi=item.carXinxi;
				var carxinche =item.carXinche;
				var carremen=item.carRemen;
				var carprice=item.carPrice;
				var carztname=item.carZtName;
				 $('<tr></tr>').addClass("yinying").append($('<td></td>').addClass("zhanshi_6").append($("<a href='carinfo?cartpname="+carname+"'></a>").append($('<img>').addClass("zhanshi_5").attr("src", tupian)))).
				append($('<td></td>').addClass("zhanshi_4").append($('<p></p>').addClass("name").text(carname)).append($('<p></p>').addClass("oth").text(carxinxi)).
							append($('<div></div>').addClass("alltips").append($('<i></i>').addClass("tip tip_xin").text(carxinche)).append($('<i></i>').addClass("tip tip_re").text(carremen)))).
				 append($('<td></td>').addClass("zhanshi_4").append($('<div></div>').addClass("price").append($('<span></span>').addClass("price_2").append($('<em></em>').addClass("rmb").text("￥")).append($('<em></em>').
							 addClass("num").text(carprice)).append($('<em></em>').addClass("unit").text("/天"))).append($("<a></a>").addClass("btn").text("预定租车").attr("name", carname).attr("tupian",tupian).attr("carxinxi",carxinxi)))).appendTo(".zhanshi_3");
	
			});
			
		}
		
	});
	});

	
	function addDate(date, days) {
		var d = new Date(date);
		d.setDate(d.getDate() + days);
		var m = d.getMonth() + 1;
		return d.getFullYear() + '-' + m + '-' + d.getDate();
	}
	
	function convertDateFromString(dateString) {
		if(dateString) {
			var date = new Date(dateString.replace(/-/, "/"))
			return date;
		}
	}
	
	Date.prototype.Format = function(fmt) { //author: meizz 
		var o = {
			"M+": this.getMonth() + 1, //月份 
			"d+": this.getDate(), //日 
			"h+": this.getHours(), //小时 
			"m+": this.getMinutes(), //分 
			"s+": this.getSeconds(), //秒 
			"q+": Math.floor((this.getMonth() + 3)/3),
			"S": this.getMilliseconds() //毫秒 
		};
		if(/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
		for(var k in o)
			if(new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
		return fmt;
	}
		var oDate422 = document.getElementById('d422');
		var oDate423 = document.getElementById('d423');
		var qutimValue=qutimValue=parseInt($('.senttime_3 option:selected').val());
		var haitimValue=parseInt($('.backtime_3 option:selected').val());
		var	qutimSelect=document.querySelector('.senttime_3');
		var	haitimSelect=document.querySelector('.backtime_3');
		var longTime=document.querySelector('.zuqi_3');
		var date = new Date();
		var nowDate = date.Format('yyyy-MM-dd');
		oDate422.value =addDate(nowDate,1);
		oDate423.value = addDate(nowDate,3);
		longTime.innerText=(convertDateFromString(oDate423.value) - convertDateFromString(oDate422.value)) / (24 * 60 * 60 * 1000); 
		/*送车上门日期选取*/
		function getzuqi(){
			qutimValue=parseInt($('.senttime_3 option:selected').val());
			haitimValue=parseInt($('.backtime_3 option:selected').val());
			/* console.log(qutimValue);
			console.log(haitimValue);
			console.log(haitimValue-qutimValue); */
			if((convertDateFromString(oDate423.value) - convertDateFromString(oDate422.value)) / (24 * 60 * 60 * 1000)!=0){
				longTime.innerText=(convertDateFromString(oDate423.value) - convertDateFromString(oDate422.value)) / (24 * 60 * 60 * 1000);
				if(haitimValue-qutimValue>=4){
					longTime.innerText=parseInt(longTime.innerText)+1;
				}
			}else{
				longTime.innerText=1;
			} 
		}
		oDate422.onfocus = function() {
			WdatePicker({ minDate: addDate(nowDate,1), maxDate: addDate(nowDate, 40) });
			if(convertDateFromString(oDate422.value) > convertDateFromString(oDate423.value)) {
//				console.log("1 比2 大")
				oDate423.value = addDate(oDate422.value, 2);
			}
			getzuqi();
		}
		/*还车日期选取，*/
		oDate423.onfocus = function() {
			WdatePicker({ minDate:oDate422.value, maxDate: addDate(oDate422.value, 80) });
			getzuqi();
		}
		qutimSelect.onclick=function(){
			getzuqi();
		}
		haitimSelect.onclick=function(){
			getzuqi();
		}
		
		/*获得地图中需要的各标签class名称 */
		var send=document.querySelector('.send');
		var repay = document.querySelector('.repay');
		
		/* 获得送车上门与还车地址*/
		var sendValue=document.querySelector('.send').value;
		var repayValue = document.querySelector('.repay').value;
		
		var topRight=document.querySelector('.map-top-right');
		var topRighth=document.querySelector('.map-top-righth');
		var addrBtn=document.querySelector('.show-addr-btn');
		var addrBtnh=document.querySelector('.show-addr-btnh');
		
/*根据选取城市的id来跟踪地图  */
		function theLocation() {
			var city = $('.quche_2 option:selected').val();
			if(city != "") {
				map.centerAndZoom(city, 14); // 用城市名设置地图中心点
			}
		}
		/*js设置送车上门地图*/
		var showAddr = document.getElementsByName('show-addr');
		var ss = document.querySelector('#show-ss')
		var map = new BMap.Map("allmap");
/*当前的经纬度为株洲  */
		var point = new BMap.Point(113.14, 27.83);
		map.centerAndZoom(point, 14);
		map.enableScrollWheelZoom(true); //地图可以放大缩小

		var geoc = new BMap.Geocoder();
		var addComp;
		var lastAdd
		map.addEventListener("click", function(e) {
			var pt = e.point;
			geoc.getLocation(pt, function(rs) {
				addComp = rs.addressComponents;
/*获得地址的详细信息*/
				lastAdd = addComp.district + "" + addComp.street + "" + addComp.streetNumber;
				/*获取详细地址*/
//				console.log( /*addComp.province + ", " + addComp.city + ", " +  */ lastAdd);
//				showAddr.innerText = lastAdd;
				ss.innerText = lastAdd;
			});
		});
		
/*与上同理*/
		/*js设置还车地址地图*/
		var showAddrh = document.getElementsByName('show-addh');
		var ssh = document.querySelector('#show-ssh')
		var maps = new BMap.Map("allmaph");
		var points = new BMap.Point(113.1, 27.8);
		maps.centerAndZoom(points, 14);
		maps.enableScrollWheelZoom(true); //地图可以放大缩小

		function theLocationh() {
			var city = $('.huanche_2  option:selected').val();
			if(city != "") {
				maps.centerAndZoom(city, 14); // 用城市名设置地图中心点
			}
		}
		var geoch = new BMap.Geocoder();
		var addComph;
		var lastAddh; 
		maps.addEventListener("click", function(e) {
			var pt = e.point;
			geoch.getLocation(pt, function(rs) {
				addComph = rs.addressComponents;
				lastAddh = addComph.district + "" + addComph.street + "" + addComph.streetNumber;
				/*获取详细地址*/
//				console.log( /*addComp.province + ", " + addComp.city + ", " +  */ lastAdd);
//				showAddr.innerText = lastAddh;
				ssh.innerText = lastAddh;
			});
		});
		

		$('.quche_2').change(function(){
			document.querySelector('.send').value="";
		});
		$('.huanche_2 ').change(function(){
			document.querySelector('.repay').value="";
		});
		
		/*取车input输入框*/
		send.onfocus= function() {
//			mapKong();
			show('map-show');
			show("mbDIV");
			theLocation();
		}
		/*还车input输入框*/
		repay.onfocus = function() {
//			mapKong();
			show('map-showh');
			show("mbDIV");
			theLocationh();
		}
		/*送车地图中右上角的关闭按钮*/
		topRight.onclick=function(){
			hidden('map-show');
			hidden('mbDIV');
		}
		/*还车地图中右上角的关闭按钮*/
		topRighth.onclick=function(){
			hidden('map-showh');
			hidden('mbDIV');
		}
		/*送车地图中的确定按钮*/
		
		addrBtn.onclick=function(){
			hidden('map-show');
			hidden('mbDIV');
			if(typeof(lastAdd)=='undefined'){
					lastAdd='';
				}
			send.value=lastAdd;
		}
		/*还车地图中的确定按钮*/
		addrBtnh.onclick=function(){
			hidden('map-showh');
			hidden('mbDIV');
			if(typeof(lastAddh)=='undefined'){
					lastAddh='';
				}
			repay.value=lastAddh;
		}
		/*方法：传入某个标签的class或者id将其进行显示*/
		function show(ele) {
			document.querySelector("#"+ele).style.display='block';
		}
		/*方法：传入某个标签的class或者id将其进行隐藏*/
		function hidden(ele) {
			document.querySelector("#"+ele).style.display='none';
		}
		${qucity};
		${haicity};
		${qudate};
		${haidate};
		${qutime};
		${haitime};
		
		qutimValue=parseInt($('.senttime_3 option:selected').val());
		haitimValue=parseInt($('.backtime_3 option:selected').val());
		/* console.log(qutimValue);
		console.log(haitimValue);
		console.log(haitimValue-qutimValue); */
		if((convertDateFromString(oDate423.value) - convertDateFromString(oDate422.value)) / (24 * 60 * 60 * 1000)!=0){
			longTime.innerText=(convertDateFromString(oDate423.value) - convertDateFromString(oDate422.value)) / (24 * 60 * 60 * 1000);
			if(haitimValue-qutimValue>=4){
				longTime.innerText=parseInt(longTime.innerText)+1;
			}
		}else{
			longTime.innerText=1;
		} 
	
});
</script>
</head>
<body>

<jsp:include page="header-inc.jsp" flush="true"></jsp:include> 
<div id="mbDIV" >/**设置弹出层的底部颜色，让底部文字不可见**/
				</div>
				<div id="map-show">
					<div class="map-show-top">
						<span class="map-top-left">送车上门详细地址</span>
						<span class="map-top-right"></span>
					</div>
					<div id="allmap"></div>
					<div id="r-result">
						<div class="show-addr" >
							<span class="addr-icon"></span>
							<div class="show-addr-mess">
								<span id="show-ss">
									请选择送车上门地址
								</span>
								<a class="show-addr-btn">确认地址</a>
							</div>
						</div>
					</div>
				</div>
				<div id="map-showh">
					<div class="map-show-top">
						<span class="map-top-left">huan上门详细地址</span>
						<span class="map-top-righth"></span>
					</div>
					<div id="allmaph"></div>
					<div id="r-resulth">
						<div class="show-addr" >
							<span class="addr-icon"></span>
							<div class="show-addr-mess">
								<span id="show-ssh">
									请选择还车上门地址
								</span>
								<a class="show-addr-btnh">确认地址</a>
							</div>
						</div>
					</div>
				</div>
<div class="ding">
	<fieldset>	
			<form >
			<div class="didianCenter">
  				 <div class="didian tongyong">
   					<div class="quche tongyong_1">
   						<label class="quche_1">取车</label>

   						<select  name="sentAddress"  class="quche_2" >
   						<option selected="selected">株洲</option>
   						<option>武汉</option>
   						<option>长沙</option>
   						<option>北京</option>
   						</select>
   						<input type="text"   name="send" class="quche_4 tongyong_4 show-map send" placeholder="&nbsp;&nbsp;&nbsp;取车详细地址"value="${quche2}"/>
   					<!-- name="sentAddress2"  修改的地方-->

   					</div>
   					
   					
   					<div class="huanche tongyong_1">
   							<label class="huanche_1">还车</label>

   							<select  name="backAddress" class="huanche_2 " >
	   							<option selected="selected">株洲</option>
		   						<option>武汉</option>
		   						<option>长沙</option>
		   						<option>北京</option>
   							</select>
   							<input type="text" name="repay" class="huanche_4 tongyong_4 show-map repay" placeholder="&nbsp;&nbsp;&nbsp;还车详细地址" value="${huanche2}"/>
   					<!-- name="backAddress2" 修改的地方-->
   					</div>
   					
  				 </div>
  			
   <div class="shijian">
   				<div class="senttime">
   					<label class="senttime_1">取车时间</label>
   					 <input type="text"  class="senttime_2 Wdate" id="d422"  name="d422"readonly="readonly">
   					 <select class="senttime_3" name="selecttime" >
   					 	<option value="8">08:00</option>
   					 	<option value="9">09:00</option>
   					 	<option value="10" selected="selected">10:00</option>
   					 	<option value="11">11:00</option>
   					 	<option value="12">12:00</option>
   					 	<option value="13">13:00</option>
   					 	<option value="14">14:00</option>
   					 	<option value="15">15:00</option>
   					 	<option value="16">16:00</option>
   					 	<option value="17">17:00</option>
   					 	<option value="18">18:00</option>
   					 	<option value="19">19:00</option>
   					 	<option value="20">20:00</option>
   					 	<option value="21">21:00</option>
   					 	<option value="22">22:00</option>
   					 </select>
   				</div>
   				<div class="backtime">
   					<label class="backtime_1">还车时间</label>
   					 <input type="text"  class="backtime_2 Wdate" id="d423" name="d423" readonly="readonly">
   					 <select class="backtime_3" name="selecttime">
   					 	<option value="8">08:00</option>
   					 	<option value="9">09:00</option>
   					 	<option value="10" selected="selected">10:00</option>
   					 	<option value="11">11:00</option>
   					 	<option value="12">12:00</option>
   					 	<option value="13">13:00</option>
   					 	<option value="14">14:00</option>
   					 	<option value="15">15:00</option>
   					 	<option value="16">16:00</option>
   					 	<option value="17">17:00</option>
   					 	<option value="18">18:00</option>
   					 	<option value="19">19:00</option>
   					 	<option value="20">20:00</option>
   					 	<option value="21">21:00</option>
   					 	<option value="22">22:00</option>
   					 </select>
   				</div>
   </div>
   <div class="zuqi">
   			<label class="zuqi_1">租期 <span class="zuqi_3" style="color:#ff8650;font-size: 15px;">2</span><span style="color:#ff8650;font-size: 15px;">天</span>&nbsp;,&nbsp;不限里程</label>
   			<a class="zuqi_2" href="javascript:void(0);">立即选车</a>
   </div>
   </div>
	</form>
	</fieldset>	
</div>	
<div class="da">
<div class="one" >
	<div class="chexing">
		<label class="chexing_1">车型</label>
		<ul class="chexing_2">
				<li class="chexing_3 m selected" name=""><img src="static/img/11.png" as ="1"  class="tu"></li>
				<li class="chexing_3 m" name="手动紧凑型"><img src="static/img/2.png" as ="2" class="tu"></li>
				<li class="chexing_3 m" name="经济型" ><img src="static/img/3.png" as="3" class="tu"></li>
				<li class="chexing_3 m"  name="商务型"><img src="static/img/4.png" as="4" class="tu"></li>
				<li class="chexing_3 m"  name="豪华型"><img src="static/img/5.png" as="5"  class="tu"></li>
				<li class="chexing_3 m"  name="SUV"><img src="static/img/6.png" as="6" class="tu"></li>
				<li class="chexing_3 m" name="6至15座商务"><img src="static/img/7.png" as="7"  class="tu"></li>
				<li class="chexing_3 m" name="个性车"><img src="static/img/8.png" as="8"  class="tu"></li>
				<li class="chexing_3 m" name="电动车"><img src="static/img/9.png" as="9"  class="tu"></li>
			
		</ul>
	</div>
	<div class="jiage">
			<label class="jiage_1 m" >价格(元)</label>
		<div class="jiage_2">
			<label class="jiage_3 m selected" style="background-color: gold;" value="">不限</label>
			<label class="jiage_3 m"  value="0">0—100</label>
			<label class="jiage_3 m"  value="100">100—300</label>
			<label class="jiage_3 m"  value="300">300—500</label>
			<label class="jiage_3 m"  value="500">500+</label>
		</div>
	</div>
	<div class="pinpai">
		<label class="pinpai_1">品牌</label>
		<div class="pinpai_2">
		<ul class="pinpai_3">
		<li style="color: gold ;" class="m selected" pinpais="不限" >不限</li>
		<c:forEach items="${che}" var="che">
		   <li class="m" pinpais="${che.carPinpai}">${che.carPinpai}</li>
		</c:forEach>
		</ul>		
		</div>
	</div>
</div>
<div class="two">
	<div class="zhanshi_2">
		<p class="zhanshi zhanshi_moren m" values="">默认排序</p>
		<p class="zhanshi zhanshi_8 m" values="asc">租金排序</p>
		<p class="zhanshi zhanshi_8 m" values="desc">销量排序</p>
	</div>
	<div class="zhanshi_7">
	<table class="zhanshi_1">
		<tbody class="zhanshi_3">
		<c:forEach  items="${chexinxi}" var="chexinxi">
			<tr class="yinying">
				<td class="zhanshi_6 " ><a href="carinfo?cartpname=${chexinxi.carTypeName}"><img src="${chexinxi.carImageUrl}" class="zhanshi_5"></a></td>
				<td class="zhanshi_4">
					<p class="name"> ${chexinxi.carTypeName}</p>
					<p class="oth">${chexinxi.carXinxi}</p>
					<div class="alltips">
						<i class="tip tip_xin">${chexinxi.carXinche}</i>
						<i class="tip tip_re">${chexinxi.carRemen}</i>
					</div>
				</td>
				<td class="zhanshi_4">
					<div class="price">
						<div class="price_1">
							<span class="price_2">
								<em class="rmb">￥</em>
								<em class="num">${chexinxi.carPrice}</em>
								<em class="unit">/天</em>
							</span>
						</div>
						<a class="btn tt" name="${chexinxi.carTypeName}" >预定租车</a>
					</div>
				</td>
			</tr>
			</c:forEach>
		</tbody>
	</table>
	</div>
</div>
	<div class="denglu">
		<form action="logincar" method="post" id="login-form" class="login-form">
			<div class="login-main">
				<span class="close-cf"><img src="static/img/close-g.png"/></span>
				<span class="login-span">登录</span>
				<div class="error-tip">
					</div>
				<span class="email-img spanimg"></span>
				<input type="text" class="login-input email" name="email" id="email" value="${param.email}" placeholder="请输入手机号" autofocus="autofocus" />
				<span class="password-img spanimg"></span>
				<input type="password" class="login-input password" name="password" id="password" value="" placeholder="请输入密 码"  />
				<span class="logincode-img spanimg"></span>
				<input type="tel" name="logincode" id="logincode" value="" placeholder="请输入验证码" />
				<div class="login-img">
					<img onclick="this.src='captcha?r'+Date.now()" src="captcha" alt="验证码">
				</div>
			</div>
			<div class="other-btn">
					<a href="sign" class="login-a">立即注册</a>
					<a href="forget" class="login-b">忘记密码？</a>
				</div>
				<div class="login-footer">
					<input type="submit" value="登录" class="login-btn" id="login-btn" />
				</div>
		</form>
	</div>
</div>

   
 <jsp:include page="footer-inc.jsp" flush="true"></jsp:include> 
<div class="fu"></div>
</body>
</html>
