<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
<head>
<base href="<%=request.getContextPath()%>/">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>我的会员等级</title>
<link rel="stylesheet" type="text/css" href="static/css/center.css">
<link rel="stylesheet" type="text/css" href="static/css/myinfor.css">
<link rel="stylesheet" type="text/css" href="static/css/owncentermyjifen.css">
<link rel="stylesheet" type="text/css" href="static/css/owncentertable.css">

</head>
<body>
	<jsp:include page="header-inc.jsp" flush="true"></jsp:include>
	<jsp:include page="tou.jsp" flush="true" />
	<div class="a_acon b">
		<jsp:include page="menu.jsp" flush="true" />
		<div class="container" style="min-height: 600px;">
			<div data-nr="1" class="nr mymessage fl selected" style="min-height:300px;">
				<h2>我的积分</h2>
				<div class="mymessageform" style="border-top: 1px solid #e9ebee;min-height:300px;">
					<div class="myjifen_head">
						积分明细
						<div class="myjifen_chaxun">
							<div class="chaxun_box" style="padding-right: 10px;">
								<div class="chaxuncon">
									<input type="button" value="查询" class="chaxun_sub">
								</div>
								<div class="chaxuncon">
									<input type="text" class="chaxun_time">
								</div>
								<div class="chaxuncon">-</div>
								<div class="chaxuncon">
									<input type="text" class="chaxun_time">
								</div>
							</div>
						</div>
					</div>
					<c:if test="${empty sfjls}">
						<div class="myjifen_contain">
							<img src="static/img/grayintegral.png" width="120px" height="120px">
							<p>会员积分不能再低啦，多多租车就能翻身！</p>
						</div>
					</c:if>
					<c:if test="${!empty sfjls}">
						<div class="myjifen_contain" style="padding-top: 25px;min-height:300px;">
								<table>
									<thead>
										<tr>
											<th>变动时间</th>
											<th>变动积分</th>
											<th>变动方向</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${sfjls }" var="sfjl">
											<tr>
												<td>
													<fmt:formatDate value="${sfjl.orderxftime }" type="both"/>
												</td>
												<td>${sfjl.ordermoney }</td>
												<td>增加</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
						</div>
					</c:if>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="footer-inc.jsp" flush="true"></jsp:include>
</body>
</html>