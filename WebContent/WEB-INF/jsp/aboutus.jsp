<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>关于我们</title>
<script src="static/js/jquery-1.12.4.js" type="text/javascript"
	charset="utf-8"></script>
<style>
* {
	margin: 0;
	padding: 0;
}

.a {
	width: 100%;
	min-width: 1300px;
	position: relative;
	height: 100px;
	text-align: center;
	background-color: #fff;
}

.fl {
	float: left;
}

.fr {
	float: right;
}

.a_acon {
	width: 980px;
	position: relative;
	height: 100px;
	border: 0px solid red;
	margin: 0 auto
}

.b {
	padding-top: 15px;
	padding-bottom: 35px;
}

.menu {
	width: 240px;
	background-color: #fff;
	border: 1px solid #dbe4eb;
	box-shadow: 0 0 5px rgba(228, 230, 233, 1);
}

.zero {
	padding-bottom: 30px;
}

.one {
	padding-left: 14px;
	height: 32px;
	padding-top: 12px;
	border-bottom: 1px solid #e9ebee;
	border-top: 1px solid #e9ebee;
	font-size: 15px;
}

.two {
	border-bottom: 1px dashed #dbe4eb;
	padding: 17px 0px 17px 25px;
	font-size: 12px;
}

.container {
	/*width: 717px;
				min-height: 698px;
				position: relative;
				float: right;
				background-color: #fff;
				border: 0px solid red;*/
	width: 715px;
	margin-left: 20px;
	float: left;
	padding-top: 21px;
	padding-bottom: 20px;
	border: 1px solid #dbe4eb;
	border-bottom-width: 2px;
	background: #fff;
	min-height: 360px;
	box-shadow: 0 0 5px rgba(228, 230, 233, 1);
}

.zero dl dd {
	padding-left: 25px;
	color: #333;
	font-size: 16px;
	font-family: "微软雅黑", arial, Simsun, sans-serif;
	line-height: 55px;
	border-bottom: 1px solid #dbe4eb;
}

.articleins {
	padding: 0px 23px;
	line-height: 20px;
	font-size: 12px;
}
</style>
<script>
	$(function() {
		var selectnr = $(".container .selected");
		selectnr.css("display", "block");
		selectnr.siblings('.nr').css("display", "none");
		$(".menu .zero").on(
				"click",
				".two",
				function() {
					var target = $(this);
					if (!target.hasClass('selected')) {
						var menu = this.dataset.menu;
						var nr = $(".container .nr[data-nr=" + menu + "]");
						var selectednr = nr.siblings('.selected');
						target.addClass('selected').siblings('.selected')
								.toggleClass('selected');
						nr.addClass('selected').siblings('.selected')
								.toggleClass('selected');
						var other = nr.siblings('.nr');
						other.css("display", "none")
						nr.css("display", "block");
					}
				});
	});
</script>
</head>

<body bgcolor="#f6fafb">
	<jsp:include page="header-inc.jsp" flush="true"></jsp:include>
	<div class="a_acon b">
		<div class="menu fl">
			<div class="zero">
				<dl>
					<dd>关于我们</dd>
				</dl>
				<div data-menu="1" class="two selected">公司简介</div>
				<div data-menu="2" class="two">愿景与使命</div>
				<div data-menu="3" class="two">公司大事</div>
				<div data-menu="4" class="two">公司荣誉</div>
				<div data-menu="5" class="two">合作伙伴</div>
				<div data-menu="6" class="two">联系我们</div>
			</div>
		</div>
		<div class="container">
			<div data-nr="1" class="nr fl selected">
				<div class="articleins">
					<p>..租车有限公司成立于2017年4月，总部位于株洲。作为中国领先的汽车租赁服务提供商，xx租车积极借鉴国际上成功的汽车租赁模式，并结合中国客户的消费习惯，为客户提供短租、长租及融资租赁等专业化的汽车租赁服务，以及全国救援、异地还车等完善的配套服务。</p>
					<br>
					<p>..租车坚持以客户为本的专业态度，颠覆繁琐的传统租车模式，为客户提供了快速便捷的全新租车服务体验。公司遍布在中国大陆各主要城市及旅游地区的服务网络，以及24小时的取还车服务和配套服务，不仅可以随时随地满足客户的租车服务需求，更可为客户的安全行车保驾护航。</p>
					<br>
					<p>自成立以来，..租车获得了客户的一致认可和社会各界的广泛好评。</p>
					<br>
					<p>秉持“Any One、Any Time、Any Car、Any
						Where”的服务理念，..租车以推动绿色出行和新型汽车消费文化为己任，致力成为消费者首选的中国汽车租赁服务品牌，并立志为推动中国的汽车租赁产业和汽车工业的发展而努力。</p>
					<br>
					<p>根据第三方机构罗兰贝格的调查报告，..租车是目前中国最具潜力的汽车租赁服务提供商，并在车队规模、网点覆盖、市场份额和业务收入等各项运营和财务指标上，处于中国汽车租赁行业绝对的领导地位。</p>
				</div>
			</div>
			<div data-nr="2" class="nr fl">
				<div style="text-align: center;">
					<img
						src="https://image.zuchecdn.com/departmentSerch/pic/sub-banner4.jpg"
						alt="" style="margin-left: 26px;">
				</div>
				<div class="articleins">
					<p>
						<br>
						<b>愿景</b><br> 成为消费者首选的中国汽车出行服务品牌。
					</p>
					<p>
						<br>
						<b>使命</b><br> 以推动绿色出行和新型汽车消费文化为己任，引领中国汽车出行服务行业的发展。
					</p>
				</div>
			</div>
			<div data-nr="3" class="nr fl">公司大事</div>
			<div data-nr="4" class="nr fl">公司荣誉</div>
			<div data-nr="5" class="nr fl">
				<div class="articleins">
					<p>汽车合作商</p>
					111
					<p>银行合作商</p>
					111

				</div>
			</div>
			<div data-nr="6" class="nr fl">
				<div class="articleins">
					<p>
						<br>
						<b>总 部</b><br> 地 址：株洲市湖南工业大学<br> 邮 编：414100<br>
						电 话：ovo<br> 传 真： ovo
					</p>
					<p>
						<br>
						<b>客户服务</b><br> 24h热线：18173328976(欢迎来电) <br>
						港澳台及海外电话：13355555555 <br> 电子邮箱：cs@zuche.com <br>
					</p>
					<p>
						<br>
						<b>新闻采访</b> <br> 联系电话：18888888888<br> 电子邮箱：pr@zuche.com
					</p>

				</div>
			</div>

		</div>
	</div>
	<jsp:include page="footer-inc.jsp" flush="true"></jsp:include>
</body>

</html>