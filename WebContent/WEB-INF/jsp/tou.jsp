<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="a">

	<div class="a_acon">
		<div class="fl logo">
			<p>
				<b class="ziti">个人中心</b>
			</p>
		</div>
		<div class="fl message">
			<div class="myhydj">
				<i class="myhyimg"></i>
			</div>
			<p>
				<span class="my_name">${user.userName }</span>
				<span class="my_hydj">${user.userType }</span>
			</p>
			<c:if test="${upjf != '已满' }">
				<div class="progressbar">
					<div class="wbg" style="width: ${user.userJf/upjf*100  }%;"></div>
					<div class="number">
			           	<span id="showNumber1">${user.userJf  }</span>/<span id="showNumber2">${upjf }</span>
					</div>
				</div>
				<div class="under_progressbar">
					我的累计积分:<span style="color: #ff8650;">${user.userJf }</span>
					,<span style="color: #ff8650;">+${upjf-user.userJf }</span>可提升会员等级！
				</div>
			</c:if>
			<c:if test="${upjf == '已满' }">
				<div class="progressbar">
					<div class="wbg" style="width: 100%;"></div>
					<div class="number">
			           	<span id="showNumber1"></span>已满<span id="showNumber2"></span>
					</div>
				</div>
				<div class="under_progressbar">
					我的累计积分:<span style="color: #ff8650;">${user.userJf },恭喜您已成为钻石会员!</span>
				</div>
			</c:if>
		</div>
		<div class="yhqjfd">
			<ul class="aaa">
				<li class="fl">
					<div class="shuzi">
						<a href="" class=""> <b class="ziti">${user.userJf }</b>
						</a>
						<div>拥有积分</div>
					</div>
				</li>
				<li class="fl">
					<div class="shuzi">
						<a href="" class=""> <b class="ziti">${user.userYhq }</b>
						</a>
						<div>优惠券</div>
					</div>
				</li>
				<li class="fl">
					<div class="shuzi">
						<a href="" class=""> <b class="ziti">${user.userMoney }</b>
						</a>
						<div>余额</div>
					</div>
				</li>
			</ul>
		</div>
	</div>
</div>