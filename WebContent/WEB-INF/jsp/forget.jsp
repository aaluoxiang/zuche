<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<base href="<%request.getContextPath();%>">
		<meta charset="UTF-8">
		<title></title>
		<link rel="stylesheet" type="text/css" href="static/css/sign.css"/>
		<link rel="stylesheet" type="text/css" href="static/css/error.css"/>
		<link rel="stylesheet" type="text/css" href="static/css/forget.css"/>
		<script src="static/js/jquery-1.12.4.js" type="text/javascript" charset="utf-8"></script>
		<script src="static/js/error-tip.js" type="text/javascript" charset="utf-8"></script>
		<script src="static/js/forget.js" type="text/javascript" charset="utf-8"></script>
	</head>
	<body>
		<div class="zc_head_bd">
			<div class="zc_main">
				<a href="main" class="zc_logo" alt="租车公司"></a>
				<div class="zc_font">
					找回密码
				</div>
			</div>
			<!--head end-->
		</div>
		<div class="fotget-max">
			<div class="forget-main">
				<ul class="forget-ul">
					<!--流程1-->
					<li id="process1" class="forget-process">
						<span class="process-show line"></span>
						<span class="process-show"></span>
						<span class="process-number">1</span>
						<span class="process-font"> 填写手机号</span>
					</li>
					<!--流程2-->
					<li id="process2">
						<span class="process-show line"></span>
						<span class="process-show"></span>
						<span class="process-number">2</span>
						<span class="process-font">验证身份</span>
					</li>
					<!--流程3-->
					<li id="process3">
						<span class="process-show line"></span>
						<span class="process-show"></span>
						<span class="process-number">3</span>
						<span class="process-font">设置新密码</span>
					</li>
					<!--流程4-->
					<li id="process4">
						<span class="process-show line"></span>
						<span class="process-show"></span>
						<span class="process-number">√</span>
						<span class="process-font">完成</span>
					</li>
				</ul>
				<div class="forget-find" >
					<div class="error-tip">
								</div>
					<!--第一个div-->
					<div class="find-box1" style="display: ;">
						<ul class="find-box1-ul">
							<li class="find-box1-li">
								
								<label class="find-box1-lable">
									<span class="find-box1-img"></span>
								</label>
								<input type="text" name="usertel" id="usertel"  class="find-ipt usertel" value="" placeholder="通过手机号找回密码" autofocus="autofocus"/>
							</li>
						</ul>
						<a class="find-btn findback">找回</a>
					</div>
					<!--第二个div-->
					<div class="find-box2"style="display: none;">
						<ul class="find-box2-ul">
							<li class="find-box2-li">
								<p id="phonetip">
									请确认您的手机号为：
									<span id="youtel">
									</span>
									<a href="javascript:void(0);"class="change">修改</a>
								</p>
								<p id="sendtip" style="display: none;">
									我们已经向您的手机发送了验证码，请注意查收！(如果10分钟后仍未收到短信，请重新发送)
								</p>
							</li>
						<li class="fing-box-ipt" style="height: 40px;">
							<label >
								<span class="mobilecode-img"></span>
							</label>
							<input type="text" name="mobilecode" id="mobilecode" placeholder="请输入动态验证码" autofocus="autofocus"/>
							<div style="display: none;"><a href="main" id="tiaoa"></a></div>
							
							<span class="btn-code btn-get">获取手机验证码</span>
							<span class="btn-code btn-set" style="display:none;">60秒后可重发</span>
						</li>
						</ul>
						<a class="find-btn submit">提交</a>
					</div>
					<!--第三个div-->
					<div class="find-box3" style="display:none ;">
						<ul class="find-box3-ul">
							<li class="find-box3-li1">
								<label>
									<span class="passwdimg"></span>
								</label>
								<input type="password" class="ipassword" name="onepassword" id="onepassword" value="" placeholder="建议6-18位数字、字母、符号的组合密码" autofocus="autofocus"/>
							</li>
							<li class="find-box3-li1">
								<label>
									<span class="passwdimg"></span>
								</label>
								<input type="password" class="ipassword" name="agpassword" id="agpassword" value="" placeholder="再次输入密码" />
							</li>
						</ul>
						<a class="find-btn save">保存</a>
					</div>
					<!--第四个div-->
					<div class="find-box4" style="display:none ;">
						<ul class="find-box4-ul">
							<li class="find-box4-li">
								<p class="find-box4-p">
									<span class="find-box4-span"></span>
									恭喜您，密码重置成功！请牢记您新设置的密码。
								</p>
							</li>
						</ul>
						<div class="yesbtn">
							<a class="liji" href="shangcheng">马上租车</a>
							<a class="backmain" href="main">返回首页</a>
						</div>
					</div>
					<p class="forget-find-text">
						如果上述方法无法找回，请致电
						<span class="find-tel">18173328976</span>
						电话找回
					</p>
				</div>
			</div>
		</div>
	</body>
</html>
