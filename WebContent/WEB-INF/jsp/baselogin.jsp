<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title></title>
		<style type="text/css">
			#login-form {
				width: 400px;
				height: 272px;
				border: 1px solid #eaeaea;
			}
			
			.login-main {
				width: 400px;
				height: 220px;
				border: 0px solid green;
			}
			
			.login-span {
				display: block;
				width: 400px;
				height: 50px;
				line-height: 50px;
				text-align: center;
				letter-spacing: 10px;
				font-size: 18px;
				border: 0px solid blue;
			}
			
			.login-main input {
				text-indent: 40px;
				outline: none;
				font-size: 14px;
				display: block;
				width: 338px;
				height: 40px;
				margin: 0px 20px 12px 30px;
				border-radius: 5px;
				border: 1px solid #d0d0d0;
			}
			
			.login-main #login-code {
				float: left;
				width: 168px;
				height: 40px;
			}
			
			.login-main input:focus {
				border: 1px solid rgb(250, 190, 0);
				box-shadow: 0 0 1px rgb(250, 190, 0);
				transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
			}
			
			.login-img {
				width: 135px;
				height: 40px;
				border: 1px solid #d0d0d0;
				margin-left: 12px;
				float: left;
			}
			
			.login-a {
				float: left;
				display: inline-block;
				border: 0px solid green;
				margin: 10px 30px;
				text-decoration: none;
				text-align: center;
				line-height: 34px;
				width: 110px;
				height: 34px;
				color: #1a8fb4;
			}
			
			.login-a:hover {
				color: #fa7300;
				text-decoration: underline;
			}
			
			.login-btn {
				float: right;
				display: inline-block;
				width: 76px;
				height: 34px;
				margin: 10px 36px;
				border: 1px solid #e6dddd;
				border-radius: 5px;
				font-size: 14px;
				letter-spacing: 3px;
				color: white;
				outline: none;
				background-color: #428bdd;
				cursor: pointer;
			}
			
			.login-btn:hover {
				background-color: #fa7300;
			}
			
			.spanimg {
				display: block;
				position: absolute;
				border: 0px solid red;
				/*z-index: 9;*/
				width: 20px;
				height: 20px;
				margin: 10px 43px;
				background: url(img/icon.png) no-repeat;
			}
			
			.login-tel-img {
				background-position: -117px -50px;
			}
			
			.login-passwd-img {
				background-position: -38px -50px;
			}
			
			.login-code-img {
				background-position: -77px -50px;
			}
			
			.login-footer {
				width: 400px;
				height: 50px;
				border: 0px solid red;
				border-top: 1px solid #e4e4e4;
				background-color: #f7f7f7;
				text-align: right;
			}
		</style>
	</head>

	<body>
		<form action="" method="post" id="login-form" class="login-form">
			<div class="login-main">
				<span class="login-span">登录</span>
				<span class="login-tel-img spanimg"></span>
				<input type="text" class="login-input" name="login-tel" id="login-tel" value="" placeholder="请输入手机号" autofocus="autofocus" />
				<span class="login-passwd-img spanimg"></span>
				<input type="text" class="login-input" name="login-passwd" id="login-passwd" value="" placeholder="请输入密 码" autofocus="autofocus" />
				<span class="login-code-img spanimg"></span>
				<input type="tel" name="login-code" id="login-code" value="" placeholder="请输入验证码" />
				<div class="login-img">
					<img onclick="this.src='captcha?r'+Date.now()" src="captcha" alt="验证码">
				</div>
			</div>
			<div class="login-footer">
				<a href="" class="login-a">立即注册</a>
				<input type="button" value="登录" class="login-btn" />
			</div>
		</form>
	</body>

</html>