<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>我的信息</title>
<link rel="stylesheet" type="text/css" href="static/css/center.css">
<link rel="stylesheet" type="text/css" href="static/css/myinfor.css">
</head>
<body>
	<div style="height: 300px;">页眉部分用include标签导入页面相同元素</div>
	<jsp:include page="tou.jsp" flush="true"/>
	<div class="a_acon b">
		<jsp:include page="menu.jsp" flush="true"/>
		<div class="container">
			<div data-nr="1" class="nr mymessage fl selected">
				<h2>我的信息</h2>
				<div class="mymessageform">
					<form method="post">
						<ul>
							<li>
								<label class="fl">姓名</label> 
								<span class="namespan fl">
									<input type="text" name="yhname" value="zuohuanxu" />
								</span>
								<div class="fr">真实姓名，方便租车时核对身份</div>
							</li>
							<li class="sfzli">
								<label class="fl">证件</label> 
								<span class="namespan fl sfzspan"> 
									<select class="zjxz">
										<option>请选择有效证件</option>
										<option>身份证</option>
									</select> 
									<input type="text" name="yhsfz" class="sfz" value="4306002199501121115" />
								</span>
								<div class="fr">有效证件，方便租车时核对身份</div>
							</li>
							<li>
								<label class="fl">电话</label> 
								<span class="namespan fl">
									<input type="text" name="yhtel" class="tel" value="159****1155" />
								</span>
								<div class="fr">有效电话，方便派车时及时联系</div>
							</li>
							<li>
								<label class="fl">e-mail</label> 
								<span class="namespan fl"> 
									<input type="text" name="e_mail" class="e_mail" value="1940438844@qq.com" />
								</span>
								<div class="fr">有效邮件</div>
							</li>
						</ul>
						<div class = "savebtn">
							<button>保存</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div style="width: 100%; height: 300px;" class="fl">页脚部分</div>
</body>
</html>