<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=request.getContextPath()%>/">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>我的会员等级</title>
<script src="static/js/jquery-1.12.4.js" type="text/javascript"
	charset="utf-8"></script>

<link rel="stylesheet" type="text/css" href="static/css/center.css">
<link rel="stylesheet" type="text/css" href="static/css/myinfor.css">
<link rel="stylesheet" type="text/css"
	href="static/css/owncentermyyhq.css">
<script>
	$(function() {
		/* $('.adow,.yhqinfoclose').on('click', function() {
			$('.yhqinfo').toggle();
		}); */
		$('.yhqbottom').on('click','.adow' ,function(){
			//alert();
			$(this).parents('.yhqbottom').siblings('.yhqinfo').css('height',$('.container').css('height'));
			$(this).parents('.yhqbottom').siblings('.yhqinfo').toggle();
			
		});
		$('.yhqinfo').on('click','.yhqinfoclose',function(){
			$(this).parents('.yhqinfo').toggle();
		});
	});
</script>
</head>
<body>
	<jsp:include page="header-inc.jsp" flush="true"></jsp:include>
	<jsp:include page="tou.jsp" flush="true" />
	<div class="a_acon b">
		<jsp:include page="menu.jsp" flush="true" />
		<div class="container">
			<div data-nr="1" class="nr mymessage fl selected">
				<h2>我的优惠券</h2>
				<div class="yhqmymessageform">
					<ul>
						<c:forEach items="${userokyhqs }" var="userokyhq">
							<li>
								<div class="yhqtop">
									<span class="moneyyhqtou"> <em>¥</em>${userokyhq.yhqtype }
									</span>
									<div class="logoyhqtop"></div>
								</div>
								<div class="yhqbottom">
									<i class="yhq_time_img"></i> 
									<span style="padding-left: 10px;">有效期至：${userokyhq.outtime }</span>
									<span style="padding-left: 50px;">拥有数量：${userokyhq.yhqnumber }</span>
									<div class="gbyhqexplain">
										<div class="p-re">
											<a href="javascript:void(0);" class="adow"> 详细说明
											 <i class=""></i>
											</a>
										</div>
									</div>
								</div>
								<div class="yhqinfo">
									<div class="p-re">
										<div class="yhqinfotitle">详细信息</div>
										<a class="yhqinfoclose"></a>
										<div class="yhqinfocontian">
											${userokyhq.yhqsm }
											<!-- <b>新用户专享券</b> <b>使用时间</b>
											2016年4月1日-2017年6月30日期间注册的客户,注册日起6个月内首次租车并还车结算有效; <b>使用规则</b>
											1.实际租期2天及以上的订单可用，优惠券平日、周末、法定节假日均可使用;<br>
											2.不与顺风车、预付特价、其他优惠活动、优惠类电子券同享;<br>
											3.此优惠仅限每人享受1次,每单限用1张，限本人使用;<br>
											4.代金券不可转让、不开发票、不找零、不兑换现金；
											<b>使用范围</b> 全国各城市直营门店、不限制车型; -->
										</div>
									</div>
								</div>
							</li>
						</c:forEach>
					</ul>
				</div>
				</div>
				<div data-nr="1" class="nr mymessage fl selected">
				<h2>过期优惠券</h2>
				<div class="yhqmymessageform">
					<ul>
						<c:forEach items="${usernoyhqs }" var="usernoyhq">
							<li>
								<div class="yhqtop">
									<span class="moneyyhqtou"> <em>¥</em>${usernoyhq.yhqtype }
									</span>
									<div class="logoyhqtop"></div>
								</div>
								<div class="yhqbottom">
									<i class="yhq_time_img"></i> <span style="padding-left: 10px;">有效期至：${usernoyhq.outtime }</span>
									<span style="padding-left: 50px;">拥有数量：${usernoyhq.yhqnumber }</span>
									<div class="gbyhqexplain">
										<div class="p-re">已过期</div>
									</div>
								</div>
							</li>
						</c:forEach>
					</ul>
				</div>
			</div>

		</div>
	</div>
	<jsp:include page="footer-inc.jsp" flush="true"></jsp:include>
</body>
</html>