<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.hidnameTag.com/tags" prefix="hidname" %>
<!DOCTYPE html>
<html>
	<!--收到订单页面来的车辆 型号查询 本型号车辆的所有详细信息 -->
	<head>
		<base href="<%=request.getContextPath()%>/">
		<meta charset="UTF-8">
		<title>车辆信息</title>
		<link rel="stylesheet" type="text/css" href="static/css/carinfo.css">
	</head>
	<body bgcolor="#f6fafb">
	<jsp:include page="header-inc.jsp" flush="true"></jsp:include>
		<form method="post">
			<div class= "carinfor">
				<p class = "carinfor-title">
					<b>当前位置:</b>
					<a href="main">韬睿租车</a>
					>
					<a href="mainjump">选车租车</a>
					>
					<span>${cartype.carTypeName }${cartype.carXinxi }</span>
				</p>
				<div class="fl carinfor-left">
					<h1>${cartype.carTypeName }${cartype.carXinxi }</h1>
					<div class="carimg">
						<div class="carimg-pic">
							<img  src="${cartype.carImageUrl }">
						</div>
					</div>
					<h2>${cartype.carTypeName }租车配置信息</h2>
					<ul class="carinfor-ul">
						<li>
							<b class="zws"></b>座位数:
							<span>${cartype.carSeatNum }</span>
						</li>
						<li>
							<b class="cms"></b>车门数:
							<span>${cartype.carDoorNum }</span>
						</li>
						<li>
							<b class="rllx"></b>燃料类型:
							<span>${cartype.carOil }</span>
						</li>
						<li>
							<b class="bsxlx"></b>变速箱类型:
							<span>${cartype.carBsx }</span>
						</li>
						<li>
							<b class="pl"></b>排量:
							<span>${cartype.carPail }</span>
						</li>
						<li>
							<b class="qdfs"></b>驱动方式:
							<span>${cartype.carDrive }</span>
						</li>
						<li>
							<b class="tc"></b>天窗:
							<span>${cartype.carSkyWindow }</span>
						</li>
						<li>
							<b class="yxrl"></b>邮箱容量:
							<span>${cartype.carOilBox }</span>
						</li>
						<li>
							<b class="qina"></b>气囊:
							<span>${cartype.carGasbag }</span>
						</li>
						<li>
							<b class="gps"></b>gps导航:
							<span>${cartype.carGps }</span>
						</li>
						<li>
							<b class="backld"></b>倒车雷达:
							<span>${cartype.carBackld }</span>
						</li>
					</ul>
				</div>
				<div class="fr carinfor-right" style="margin-top: 10px;">
					<fieldset style="min-height: 475px;">
						<legend>用户评论</legend>
						<c:if test="${empty carcoms }">
							<div style="text-align: center;margin-top:30px;">暂时没有评论！~</div>
						</c:if>
						<c:forEach items="${carcoms }" var="carcom">
							<fieldset>
								<legend><hidname:hidname name="${carcom.userid }"></hidname:hidname>：</legend>
								<div style="width:315px;word-wrap: break-word;color:black;">${carcom.commentcon }</div>
								<div style="text-align: right;">评价星级：${carcom.commenttype }</div>
								<div style="text-align: right;font-size:10px;"><fmt:formatDate value="${carcom.commenttime }" type="both"/></div>
							</fieldset>
						</c:forEach>
					</fieldset>
				</div>
				<div class="fl btndiv" style="margin-bottom:40px;">
						<!-- <input type="button"  value="立即预定" class="ydbtn"/> -->
						<a href="mainjump" class="ydbtn">返回</a>
				</div>
			</div>
		</form>
		<jsp:include page="footer-inc.jsp" flush="true"></jsp:include>
	</body>
</html>
    